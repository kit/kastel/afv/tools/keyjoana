package SlicingAndViolations;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.util.CancelException;
import com.ibm.wala.util.graph.GraphIntegrity.UnsoundGraphException;

import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import joanakey.CombinedApproach;
import joanakey.JoanaAndKeYCheckData;
import joanakey.slicing.BranchAnalyzer;
import joanakey.slicing.BranchAnalyzer.IF;
import joanakey.slicing.ProgramSimplifier;

public class BranchAnalyzerTests {
	
	static JoanaAndKeYCheckData data;
	static SDG sdg;
	static String javaTargetPath = "/testdata/jUnitData/src/BranchAnalysis";
	
	private JoanaAndKeYCheckData getCheckData(String path) throws ClassHierarchyException, IOException, UnsoundGraphException, CancelException {
		 
        File file2 = new File(path);
		return CombinedApproach.parseInputFile(file2);
	}

	@Test
	public void getAnalysisResultsTest1() throws ClassHierarchyException, IOException, UnsoundGraphException, CancelException {
		String path = "testdata/jUnitData/Joak/BranchAnalysisTest1.joak";
		JoanaAndKeYCheckData data = getCheckData(path);
        CallGraph cg = data.getStateSaver().getCallGraph();
        SDG sdg = data.getAnalysis().getProgram().getSDG();

        SDGNode high = sdg.getNode(32);
        SDGNode low = sdg.getNode(38);
        ProgramSimplifier simplifer = new ProgramSimplifier(high, low, sdg);
//		Set<SDGNode> relevantNodes = simplifer.getRelevantNodes();
		Set<SDGNode> chop = simplifer.getChopNodes();
		
        BranchAnalyzer ba = new BranchAnalyzer(cg, sdg, chop);
        
        Map<String, Map<Integer,IF>>analysisResult = ba.getAnalysisResults();
        System.out.println(analysisResult);
        String name = data.getEntryMethodString()+".java";
        assertTrue(analysisResult.containsKey(name));
        System.out.println(analysisResult.get(name));
        for (Integer i :analysisResult.get(name).keySet()) {
        	assertTrue(analysisResult.get(name).get(i).equals(IF.TRUE));
        }

	}

	@Test
	public void getAnalysisResultsTest2() throws ClassHierarchyException, IOException, UnsoundGraphException, CancelException {
		String path = "testdata/jUnitData/Joak/BranchAnalysisTest2.joak";
		JoanaAndKeYCheckData data = getCheckData(path);
        CallGraph cg = data.getStateSaver().getCallGraph();
        SDG sdg = data.getAnalysis().getProgram().getSDG();

        SDGNode high = sdg.getNode(32);
        SDGNode low = sdg.getNode(38);
        ProgramSimplifier simplifer = new ProgramSimplifier(high, low, sdg);
//		Set<SDGNode> relevantNodes = simplifer.getRelevantNodes();
		Set<SDGNode> chop = simplifer.getChopNodes();
		
        BranchAnalyzer ba = new BranchAnalyzer(cg, sdg, chop);
        
        Map<String, Map<Integer,IF>>analysisResult = ba.getAnalysisResults();
        System.out.println(analysisResult);
        String name = data.getEntryMethodString()+".java";
        assertTrue(analysisResult.containsKey(name));
        System.out.println(analysisResult.get(name));
        for (Integer i :analysisResult.get(name).keySet()) {
        	assertTrue(analysisResult.get(name).get(i).equals(IF.FALSE));
        }
	}

	@Test
	public void getAnalysisResultsTest3() throws ClassHierarchyException, IOException, UnsoundGraphException, CancelException {
		String path = "testdata/jUnitData/Joak/BranchAnalysisTest3.joak";
		JoanaAndKeYCheckData data = getCheckData(path);
        CallGraph cg = data.getStateSaver().getCallGraph();
        SDG sdg = data.getAnalysis().getProgram().getSDG();

        SDGNode high = sdg.getNode(32);
        SDGNode low = sdg.getNode(38);
        ProgramSimplifier simplifer = new ProgramSimplifier(high, low, sdg);
//		Set<SDGNode> relevantNodes = simplifer.getRelevantNodes();
		Set<SDGNode> chop = simplifer.getChopNodes();
		
        BranchAnalyzer ba = new BranchAnalyzer(cg, sdg, chop);
        
        Map<String, Map<Integer,IF>>analysisResult = ba.getAnalysisResults();
        System.out.println(analysisResult);
        String name = data.getEntryMethodString()+".java";
        assertTrue(analysisResult.containsKey(name));
        System.out.println(analysisResult.get(name));
        for (Integer i :analysisResult.get(name).keySet()) {
        	assertTrue(analysisResult.get(name).get(i).equals(IF.BOTH));
        }
	}

}
