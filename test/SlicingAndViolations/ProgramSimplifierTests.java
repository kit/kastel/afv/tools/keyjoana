package SlicingAndViolations;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Collection;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.Iterables;
import com.ibm.wala.util.collections.ArraySet;

import edu.kit.joana.api.sdg.SDGProgram;
import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import edu.kit.joana.ifc.sdg.mhpoptimization.MHPType;
import joanakey.slicing.ProgramSimplifier;

public class ProgramSimplifierTests {
	
	
	@Test
	public void getChopNodesTest1() throws IOException {
		String pathToSDG = "testdata/jUnitData/SDG/BranchAnalysisTest.pdg";
        SDGProgram program = SDGProgram.loadSDG(pathToSDG, MHPType.PRECISE);
        SDG sdg = program.getSDG();
		SDGNode high = sdg.getNode(32);
		SDGNode low = sdg.getNode(38);
		ProgramSimplifier simplifier = new ProgramSimplifier(high, low, sdg);
		Set<Integer> expectedIDs = new ArraySet<Integer>();
		expectedIDs.add(32);
		expectedIDs.add(33);
		expectedIDs.add(34);
		expectedIDs.add(37);
		expectedIDs.add(38);
		Set<SDGNode> chop = simplifier.getChopNodes();
		Set<Integer> chopIDs = nodeSetToIDSet(chop);
		assertTrue(expectedIDs.equals(chopIDs));

	}

	@Test
	public void getChopNodesTest2() throws IOException {
		String pathToSDG = "testdata/jUnitData/SDG/BranchAnalysisTest2.pdg";
        SDGProgram program = SDGProgram.loadSDG(pathToSDG, MHPType.PRECISE);
        SDG sdg = program.getSDG();
		SDGNode high = sdg.getNode(32);
		SDGNode low = sdg.getNode(38);
		ProgramSimplifier simplifier = new ProgramSimplifier(high, low, sdg);
		Set<Integer> expectedIDs = new ArraySet<Integer>();
		expectedIDs.add(32);
		expectedIDs.add(33);
		expectedIDs.add(36);
		expectedIDs.add(37);
		expectedIDs.add(38);
		Set<SDGNode> chop = simplifier.getChopNodes();
		Set<Integer> chopIDs = nodeSetToIDSet(chop);
		assertTrue(expectedIDs.equals(chopIDs));
	}

	@Test
	public void getChopNodesTest3() throws IOException {
		String pathToSDG = "testdata/jUnitData/SDG/BranchAnalysisTest3.pdg";
        SDGProgram program = SDGProgram.loadSDG(pathToSDG, MHPType.PRECISE);
        SDG sdg = program.getSDG();
		SDGNode high = sdg.getNode(32);
		SDGNode low = sdg.getNode(38);
		ProgramSimplifier simplifier = new ProgramSimplifier(high, low, sdg);
		Set<Integer> expectedIDs = new ArraySet<Integer>();
		expectedIDs.add(32);
		expectedIDs.add(33);
		expectedIDs.add(34);
		expectedIDs.add(36);
		expectedIDs.add(37);
		expectedIDs.add(38);
		Set<SDGNode> chop = simplifier.getChopNodes();
		Set<Integer> chopIDs = nodeSetToIDSet(chop);
		assertTrue(expectedIDs.equals(chopIDs));
	}

	@Test
	public void getBackwardSliceTest1() throws IOException {
		String pathToSDG = "testdata/jUnitData/SDG/BranchAnalysisTest.pdg";
		SDGProgram program = SDGProgram.loadSDG(pathToSDG, MHPType.PRECISE);
		SDG sdg = program.getSDG();
		Set<SDGNode> testMethodNodes = sdg.getNodesOfProcedure(sdg.getNode(32));
		Collection<SDGNode> callTestMethodNodes = sdg.getCallers(sdg.getEntry(sdg.getNode(32)));
		assertTrue(callTestMethodNodes.size() == 1);
		SDGNode callNode = Iterables.get(callTestMethodNodes, 0);
		Set<SDGNode> mainMethodNodes = sdg.getNodesOfProcedure(callNode);
		testMethodNodes.addAll(mainMethodNodes);
		sdg = sdg.subgraph(testMethodNodes);
		SDGNode high = sdg.getNode(32);
		SDGNode low = sdg.getNode(38);
		ProgramSimplifier simplifier = new ProgramSimplifier(high, low, sdg);
		Set<Integer> expectedIDs = new ArraySet<Integer>();
		expectedIDs.add(32);
		expectedIDs.add(33);
		expectedIDs.add(34);
		expectedIDs.add(37);
		expectedIDs.add(38);
		expectedIDs.add(29);
		expectedIDs.add(6);
		expectedIDs.add(5);
		expectedIDs.add(1);
		Set<SDGNode> backwardSlice = simplifier.getBackwardsSlice();
		Set<Integer> chopIDs = nodeSetToIDSet(backwardSlice);
		assertTrue(expectedIDs.equals(chopIDs));
	}
	
	@Test
	public void getBackwardSliceTest2() throws IOException {
		String pathToSDG = "testdata/jUnitData/SDG/BranchAnalysisTest2.pdg";
		SDGProgram program = SDGProgram.loadSDG(pathToSDG, MHPType.PRECISE);
		SDG sdg = program.getSDG();
		Set<SDGNode> testMethodNodes = sdg.getNodesOfProcedure(sdg.getNode(32));
		Collection<SDGNode> callTestMethodNodes = sdg.getCallers(sdg.getEntry(sdg.getNode(32)));
		assertTrue(callTestMethodNodes.size() == 1);
		SDGNode callNode = Iterables.get(callTestMethodNodes, 0);
		Set<SDGNode> mainMethodNodes = sdg.getNodesOfProcedure(callNode);
		testMethodNodes.addAll(mainMethodNodes);
		sdg = sdg.subgraph(testMethodNodes);
		SDGNode high = sdg.getNode(32);
		SDGNode low = sdg.getNode(38);
		ProgramSimplifier simplifier = new ProgramSimplifier(high, low, sdg);
		Set<Integer> expectedIDs = new ArraySet<Integer>();
		expectedIDs.add(32);
		expectedIDs.add(33);
		expectedIDs.add(37);
		expectedIDs.add(36);
		expectedIDs.add(38);
		expectedIDs.add(29);
		expectedIDs.add(6);
		expectedIDs.add(5);
		expectedIDs.add(1);
		Set<SDGNode> backwardSlice = simplifier.getBackwardsSlice();
		Set<Integer> chopIDs = nodeSetToIDSet(backwardSlice);
		assertTrue(expectedIDs.equals(chopIDs));
	}
	
	@Test
	public void getBackwardSliceTest3() throws IOException {
		String pathToSDG = "testdata/jUnitData/SDG/BranchAnalysisTest3.pdg";
		SDGProgram program = SDGProgram.loadSDG(pathToSDG, MHPType.PRECISE);
		SDG sdg = program.getSDG();
		Set<SDGNode> testMethodNodes = sdg.getNodesOfProcedure(sdg.getNode(32));
		Collection<SDGNode> callTestMethodNodes = sdg.getCallers(sdg.getEntry(sdg.getNode(32)));
		assertTrue(callTestMethodNodes.size() == 1);
		SDGNode callNode = Iterables.get(callTestMethodNodes, 0);
		Set<SDGNode> mainMethodNodes = sdg.getNodesOfProcedure(callNode);
		testMethodNodes.addAll(mainMethodNodes);
		sdg = sdg.subgraph(testMethodNodes);
		SDGNode high = sdg.getNode(32);
		SDGNode low = sdg.getNode(38);
		ProgramSimplifier simplifier = new ProgramSimplifier(high, low, sdg);
		Set<Integer> expectedIDs = new ArraySet<Integer>();
		expectedIDs.add(29);
		expectedIDs.add(32);
		expectedIDs.add(33);
		expectedIDs.add(34);
		expectedIDs.add(36);
		expectedIDs.add(37);
		expectedIDs.add(38);
		expectedIDs.add(6);
		expectedIDs.add(5);
		expectedIDs.add(1);
		Set<SDGNode> backwardSlice = simplifier.getBackwardsSlice();
		Set<Integer> chopIDs = nodeSetToIDSet(backwardSlice);
		assertTrue(expectedIDs.equals(chopIDs));
	}
	
	private Set<Integer> nodeSetToIDSet(Set<SDGNode> nodes) {
		Set<Integer> chopIDs = new ArraySet<Integer>();
		for (SDGNode node : nodes) {
			Integer nodeID = node.getId();
			chopIDs.add(nodeID);
		}	
		return chopIDs;
	}
}
