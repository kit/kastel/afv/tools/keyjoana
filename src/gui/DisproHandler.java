/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.fxmisc.richtext.CodeArea;

import edu.kit.joana.ifc.sdg.core.SecurityNode;
import edu.kit.joana.ifc.sdg.core.violations.ClassifiedViolation;
import edu.kit.joana.ifc.sdg.core.violations.IViolation;
import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGEdge;
import edu.kit.joana.ifc.sdg.graph.SDGNodeTuple;
import gui.asynctaskhandler.AsyncAutoRunner;
import gui.asynctaskhandler.AsyncBackgroundDisproCreator;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.AnchorPane;

import javafx.scene.paint.Color;
import javafx.util.Callback;
import joanakey.JoanaAndKeYCheckData;
import joanakey.customlistener.ViolationsWrapperListener;
import joanakey.errorhandling.ErrorLogger;
import joanakey.persistence.DisprovingProject;
import joanakey.staticCG.javamodel.StaticCGJavaMethod;
import joanakey.violations.ViolationChop;
import joanakey.violations.ViolationsWrapper;

import keyjoana.settings.KeyJoanaSettings;
import keyjoana.settings.SettingsObserver;

/**
 *
 * @author holger, joachim
 */
public class DisproHandler implements ViolationsWrapperListener, SettingsObserver {

    //private static final String SOUND = "Victory Sound Effect.wav";

    private AsyncBackgroundDisproCreator backgroundDisproCreator;
    private DisprovingProject disprovingProject;
    private ViolationsWrapper violationsWrapper;
    private JoanaKeYInterfacer joanaKeyInterfacer;
    private KeyJoanaSettings keyJoanaSettings;

    private CodeArea methodCodeArea;
    private CodeArea loopInvariantCodeArea;
    private CodeArea keyContractCodeArea;

    private Label labelProjName;
    private Label labelSummaryEdge;
    private Label labelSomeOtherData;
    private MenuBar mainMenu;
//    private CheckMenuItem checkMenuItemUseSlicedFiles;
//    private CheckMenuItem checkMenuItemUseMethodInlining;
//    private CheckMenuItem checkMenuItemUseLoopUnwinding;

    private Button buttonSaveLoopInvariant;
    private Button buttonResetLoopInvariant;
    private Button buttonMarkAsDisproved;
    private Button buttonOpenSelected;
    private Button buttonTryDisprove;
    private Button buttonRunAuto;

    private ListView<String> listViewSummaryEdges;
    private ListView<String> listViewUncheckedChops;
    private ListView<String> listViewCalledMethodsInSE;
    private ListView<String> listViewLoopsInSE;
    private ListView<String> listViewFormalInoutPairs;

    //private AnchorPane anchorPaneMethodCode;
    private AnchorPane anchorPaneLoopInvariant;
    private AnchorPane anchorPaneKeyContract;

    private Map<Integer, ViolationChop> itemIndexToViolation = new HashMap<>();
    private Map<Integer, SDGEdge> itemIndexToSummaryEdge = new HashMap<>();
    private HashMap<Integer, SDGNodeTuple> currentIndexToNodeTuple = new HashMap<>();
    private Map<SDGEdge, StaticCGJavaMethod> summaryEdgesAndCorresJavaMethods;
    private Map<SDGEdge, ViolationChop> disprovenSUEdgesAndCorresViolation = new HashMap<>();
    private Map<SDGEdge, StaticCGJavaMethod> allSummaryEdgesAndCorresJavaMethodsCopy = new HashMap<>();
    private Map<SDGEdge, Boolean> suEdgeClosedMap = new HashMap<>();

    private ViolationChop currentSelectedViolation;
    private SDGEdge currentSelectedEdge;
    private StaticCGJavaMethod currentSelectedMethod;
    private CurrentActionLogger currentActionLogger;
    private boolean useSlicing = false;
    private boolean useLoopUnwinding = false;
    private boolean useMethodInlining = false;

    private static final String DEFAULT_CONTROL_INNER_BACKGROUND = "derive(-fx-base,80%)";
    private static final String HIGHLIGHTED_CONTROL_INNER_BACKGROUND = "derive(palegreen, 50%)";    

    public DisproHandler(
            CurrentActionLogger currentActionLogger,
            Label labelProjName,
            Label labelSummaryEdge,
            Label labelSomeOtherData,
            MenuBar mainMenu,
            ListView<String> listViewUncheckedEdges,
            ListView<String> listViewUncheckedChops,
            ListView<String> listViewCalledMethodsInSE,
            ListView<String> listViewLoopsInSE,
            ListView<String> listViewFormalInoutPairs,
            AnchorPane anchorPaneMethodCode,
            AnchorPane anchorPaneLoopInvariant,
            AnchorPane anchorPaneKeyContract,
            Button buttonSaveLoopInvariant,
            Button buttonResetLoopInvariant,
            Button buttonMarkAsDisproved,
            Button buttonOpenSelected,
            Button buttonTryDisprove,
            Button buttonRunAtuo,
            CheckMenuItem checkMenuItemUseSclicedFiles,
            CheckMenuItem checkMenuItemUseMethodInlining,
            CheckMenuItem checkMenuItemUseLoopUnwinding,
            KeyJoanaSettings keyJoanaSettings) {
    		
        backgroundDisproCreator = new AsyncBackgroundDisproCreator(currentActionLogger);
        this.keyJoanaSettings = keyJoanaSettings;
        this.keyJoanaSettings.addSettingObserver(this);
        this.labelProjName = labelProjName;
        this.labelSummaryEdge = labelSummaryEdge;
        this.labelSomeOtherData = labelSomeOtherData;
        this.mainMenu = mainMenu;
        this.listViewUncheckedChops = listViewUncheckedChops;
        this.listViewSummaryEdges = listViewUncheckedEdges;
        this.listViewCalledMethodsInSE = listViewCalledMethodsInSE;
        this.listViewLoopsInSE = listViewLoopsInSE;
        this.listViewFormalInoutPairs = listViewFormalInoutPairs;
        //this.anchorPaneMethodCode = anchorPaneMethodCode;
        this.anchorPaneLoopInvariant = anchorPaneLoopInvariant;
        this.anchorPaneKeyContract = anchorPaneKeyContract;
        this.buttonResetLoopInvariant = buttonResetLoopInvariant;
        this.buttonSaveLoopInvariant = buttonSaveLoopInvariant;
        this.buttonMarkAsDisproved = buttonMarkAsDisproved;
        this.buttonOpenSelected = buttonOpenSelected;
        this.buttonTryDisprove = buttonTryDisprove;
        this.buttonRunAuto = buttonRunAtuo;
        this.currentActionLogger = currentActionLogger;
//        this.checkMenuItemUseSlicedFiles = checkMenuItemUseSclicedFiles;
//        this.checkMenuItemUseLoopUnwinding = checkMenuItemUseLoopUnwinding;
//        this.checkMenuItemUseMethodInlining = checkMenuItemUseMethodInlining;
//        this.checkMenuItemUseSlicedFiles.setDisable(true);
//        this.checkMenuItemUseLoopUnwinding.setDisable(true);
//        this.checkMenuItemUseMethodInlining.setDisable(true);

        JavaCodeEditor javaCodeEditor = new JavaCodeEditor();
        methodCodeArea = javaCodeEditor.getCodeArea();
        addCodeAreaToAnchorPane(methodCodeArea, anchorPaneMethodCode);
        methodCodeArea.setDisable(false);

        keyContractCodeArea = javaCodeEditor.getCodeArea();
        addCodeAreaToAnchorPane(keyContractCodeArea, this.anchorPaneKeyContract);
        keyContractCodeArea.setDisable(true);

        loopInvariantCodeArea = javaCodeEditor.getCodeArea();
        addCodeAreaToAnchorPane(loopInvariantCodeArea, this.anchorPaneLoopInvariant);

        labelProjName.setText("");
        labelSummaryEdge.setText("");
        labelSomeOtherData.setText("");
        
        listViewUncheckedChops.getSelectionModel().selectedIndexProperty()
        .addListener((observable, oldValue, newValue) -> {
        	clearView();
        	onViolationSelectionChange((int) newValue);
        });

        listViewSummaryEdges.getSelectionModel().selectedIndexProperty()
        .addListener((observable, oldValue, newValue) -> {
            onSummaryEdgeSelectionChange((int) newValue);
        });
        listViewFormalInoutPairs.getSelectionModel().selectedIndexProperty()
        .addListener((observable, oldValue, newValue) -> {
            onFormalPairSelectionChange((int) newValue);
        });
        listViewLoopsInSE.getSelectionModel().selectedIndexProperty()
        .addListener((observable, oldValue, newValue) -> {
            onLoopInvSelectionChanged((int) newValue);
        });
        
        setChopViewListFactory();
        setSUViewListFactory();
    }

    /**
     * enable or disable all Buttons 
     * (auto run, mark as disproven, reset loop invariant, save loop invariant, open selected in KeY and try Disprove)
     * @param disable true to disable all {@link Button}s or false to enable all {@link Button}s.
     */
    public void setAllButtonsDisable(boolean disable) {
        buttonRunAuto.setDisable(disable);
        buttonMarkAsDisproved.setDisable(disable);
        buttonResetLoopInvariant.setDisable(disable);
        buttonSaveLoopInvariant.setDisable(disable);
        buttonOpenSelected.setDisable(disable);
        buttonTryDisprove.setDisable(disable);
    }
    
    /**
     * toggle availability of all Buttons except the Auto run button.
     */
    private void toggleButtonsForAutoRun() {
    	buttonMarkAsDisproved.setDisable(!buttonMarkAsDisproved.isDisabled());
    	buttonResetLoopInvariant.setDisable(!buttonResetLoopInvariant.isDisabled());
    	buttonSaveLoopInvariant.setDisable(!buttonSaveLoopInvariant.isDisabled());
    	buttonOpenSelected.setDisable(!buttonOpenSelected.isDisabled());
    	buttonTryDisprove.setDisable(!buttonTryDisprove.isDisabled());
    }
    
    
	private void stopAutoRun() {
		if (AsyncAutoRunner.checkRunning()) {
			buttonRunAuto.setText("Start auto mode.");
			buttonRunAuto.setDisable(true);
			Thread t = new Thread(() -> {
				AsyncAutoRunner.stop();
				toggleButtonsForAutoRun();
				mainMenu.setDisable(false);
				buttonRunAuto.setDisable(false);
			});
			t.start();
		}
    }
    
    
    private void startAutoRun() {
		currentActionLogger.startProgress("Running auto pilot.");
		buttonRunAuto.setText("Stop auto mode.");
		toggleButtonsForAutoRun();
		mainMenu.setDisable(true);
    }
    

    private void onPressRunAuto() {
        if (AsyncAutoRunner.checkRunning()) {
			stopAutoRun();
		} else {
			startAutoRun();
			AsyncAutoRunner.startAutoDisproving(violationsWrapper, joanaKeyInterfacer.getJavaForKeyCreator(),
					joanaKeyInterfacer.getSummaryEdgeToCorresData(), currentActionLogger, listViewSummaryEdges, this,
					this.useSlicing,
					this.useMethodInlining,
					this.useLoopUnwinding);
//					this.checkMenuItemUseSlicedFiles.isSelected(),
//					this.checkMenuItemUseMethodInlining.isSelected(),
//					this.checkMenuItemUseLoopUnwinding.isSelected());
		}
    }

    private void onPressMarkAsDisproved() {
    	try {
    		joanaKeyInterfacer.markAsDisproved(currentSelectedEdge);
    	} catch(IllegalArgumentException e) {
        	currentActionLogger.showMessage("No Summary Edge selected. Please select a Summary edge first.");
    	}
    }

    private void onPressOpenInKeY() {
        SDGNodeTuple formalNodeTuple = currentIndexToNodeTuple.get(0);
        try {
            joanaKeyInterfacer.openInKeY(
                    currentSelectedEdge,
                    formalNodeTuple,
                    summaryEdgesAndCorresJavaMethods.get(currentSelectedEdge));
            labelSummaryEdge.setText(currentSelectedEdge.toString());
        } catch (IllegalArgumentException e) {
        	currentActionLogger.showMessage("No Summary Edge selected. Please select a Summary edge first.");
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private void useSlicedFiles(boolean b) {
    	this.useSlicing = b;
    	joanaKeyInterfacer.setUseSclicedFiles(b);
    }
    
    private void useLoopUnwinding(boolean b) {
    	this.useLoopUnwinding = b;
    	joanaKeyInterfacer.setUseLoopUnwinding(b);
    }

    private void useMethodInlining(boolean b) {
    	this.useMethodInlining = b;
    	joanaKeyInterfacer.setUseMethodInlining(b);
    }

    private void onPressTryDisprove() {
        SDGNodeTuple formalNodeTuple = currentIndexToNodeTuple.get(0);
        try {
            boolean worked = joanaKeyInterfacer.tryDisproveEdge(
                    currentSelectedEdge,
                    formalNodeTuple,
                    summaryEdgesAndCorresJavaMethods.get(currentSelectedEdge));
            labelSummaryEdge.setText(currentSelectedEdge.toString());
            if (worked) {
//                labelSomeOtherData.setText("Disproving worked.");
            	setSomeOtherMessageAndColor("Disproving worked.", Color.GREEN);
                violationsWrapper.removeEdge(currentSelectedEdge);
            } else {
            	setSomeOtherMessageAndColor("Could not disprove edge.", Color.RED);
//                labelSomeOtherData.setText("Could not disprove edge.");
            }
            
            
        } catch (IllegalArgumentException e){
        	currentActionLogger.showMessage("No Summary Edge selected. Please select a Summary edge first.");
        }
        catch (Exception e) {
            e.printStackTrace();
            ErrorLogger.logError(
                    "An error occured while trying to create the Jave file to be disproved.",
                    ErrorLogger.ErrorTypes.ERROR_WRITING_FILE_TO_DISK
                    );
        }
    }

    private void addCodeAreaToAnchorPane(CodeArea codeArea, AnchorPane anchorPane) {
        anchorPane.getChildren().add(codeArea);
        AnchorPane.setTopAnchor(codeArea, 0.0);
        AnchorPane.setRightAnchor(codeArea, 0.0);
        AnchorPane.setBottomAnchor(codeArea, 0.0);
        AnchorPane.setLeftAnchor(codeArea, 0.0);
    }

    public DisprovingProject getDisprovingProject() {
        return disprovingProject;
    }


    /**
     * generate a new {@link DisprovingProject} with the given {@link JoanaAndKeYCheckData}. This method disable all Buttons while generating 
     * the {@link DisprovingProject}.
     * @param checkData 
     */
    public void handleNewDispro(JoanaAndKeYCheckData checkData) {
        setAllButtonsDisable(true);
        backgroundDisproCreator.generateFromCheckData(checkData, (dispro, worked) -> {
            if (worked) {
                try {
                    disprovingProject = dispro;
                    handleNewDisproSet();
                } catch (Exception e) {
                    ErrorLogger.logError("Setting up new project failed.",
                                         ErrorLogger.ErrorTypes.ERROR_PARSING_JOAK);
                }
            }
            setAllButtonsDisable(false);
            mainMenu.setDisable(false);
        });
    }

    public void handleNewDispro(DisprovingProject disprovingProject) {
        setAllButtonsDisable(true);
        try {
            this.disprovingProject = disprovingProject;
            handleNewDisproSet();
        } catch (Exception e) {
            ErrorLogger.logError("Setting up existing project failed.",
                                 ErrorLogger.ErrorTypes.ERROR_PARSING_JOAK);
        }
        mainMenu.setDisable(false);
        setAllButtonsDisable(false);
    }

    /*private void setLoopInvInCurrent(int pos, String inv) {
        joanaKeyInterfacer.setLoopInvariantFor(currentSelectedEdge, pos, inv);
    }*/

    private void resetListView(ListView<String> listView) {
        try {
            listView.getSelectionModel().clearSelection();
            listView.getItems().clear();
        } catch (Exception e) {
        }
    }

    private void clearCodeAreaForNewCode(CodeArea codeArea, String newCode) {
        try {
            codeArea.replaceText(0, codeArea.getLength(), newCode);
        } catch (Exception e) {
        }
    }
    
    private void disableDisproveButtons(boolean b) {
    	this.buttonOpenSelected.setDisable(b);
    	this.buttonRunAuto.setDisable(b);
    	this.buttonMarkAsDisproved.setDisable(b);
    	this.buttonTryDisprove.setDisable(b);
	}

    //#######################################################################
    //this gets run whenever the selected SUMMARY EDGE changes-------------->
    //#######################################################################
    private void onSummaryEdgeSelectionChange(int newValue) {
        if (newValue < 0) {
            return;
        }
        currentActionLogger.showMessage("");
        currentSelectedEdge = itemIndexToSummaryEdge.get(newValue);
//        System.out.println("currently selected Edge : " + this.currentSelectedEdge);
//        System.out.println("corresponding Method  : " + allSummaryEdgesAndCorresJavaMethodsCopy.get(currentSelectedEdge));

        currentSelectedMethod = summaryEdgesAndCorresJavaMethods != null ?
                allSummaryEdgesAndCorresJavaMethodsCopy.get(currentSelectedEdge) : null;
//                summaryEdgesAndCorresJavaMethods.get(currentSelectedEdge) : null;
        if (currentSelectedMethod == null && 0 < summaryEdgesAndCorresJavaMethods.size()) {
            currentSelectedEdge =
                    currentSelectedEdge == null ?
                            summaryEdgesAndCorresJavaMethods.keySet().iterator().next()
                            : currentSelectedEdge;
            currentSelectedMethod = allSummaryEdgesAndCorresJavaMethodsCopy.get(currentSelectedEdge);
//            currentSelectedMethod = summaryEdgesAndCorresJavaMethods.get(currentSelectedEdge);
        }
        if (currentSelectedMethod == null) {
            System.err.println("No method available, but proof not finished.");
            return;
        }

        //handle the code areas
        String methodBody = currentSelectedMethod.getMethodBody();

        clearCodeAreaForNewCode(loopInvariantCodeArea, "");
        clearCodeAreaForNewCode(methodCodeArea, methodBody);

        //populate the other list views
        resetListView(listViewCalledMethodsInSE);
        resetListView(listViewLoopsInSE);
        resetListView(listViewFormalInoutPairs);

        for (int relPos : currentSelectedMethod.getRelPosOfLoops()) {
            listViewLoopsInSE.getItems().add("Line " + String.valueOf(relPos));
        }
        for (StaticCGJavaMethod m : currentSelectedMethod.getCalledFunctionsRec()) {
            listViewCalledMethodsInSE.getItems().add(m.toString());
        }

        Collection<SDGNodeTuple> allFormalPairs = disprovingProject.getSdg().getAllFormalPairs(
                currentSelectedEdge.getSource(), currentSelectedEdge.getTarget());
        currentIndexToNodeTuple.clear();
        int index = 0;
        for (SDGNodeTuple t : allFormalPairs) {
            final String str
            = "(" +
                    t.getFirstNode().getLabel() + ":" + t.getFirstNode() +
                    ", " +
                    t.getSecondNode().getLabel() + ":" + t.getSecondNode() +
              ")";
            listViewFormalInoutPairs.getItems().add(str);
            currentIndexToNodeTuple.put(index++, t);
        }
        if (this.suEdgeClosedMap.get(currentSelectedEdge)) {
        	disableDisproveButtons(true);
        } else {
        	disableDisproveButtons(false);
        }
        listViewFormalInoutPairs.getSelectionModel().select(0);
    }

    //this gets run whenever the selected violation changes
    private void onViolationSelectionChange(int newValue) {
		if (newValue < 0) {
			return;
		}
		resetListView(listViewSummaryEdges);
		//index 0 "All" entry -> show all SU
		if (newValue == 0) {
			showAllSU();
		} else {
			SDGEdge firstSU = null;
			this.currentSelectedViolation = this.itemIndexToViolation.get(newValue);
			resetListView(listViewSummaryEdges);
			for (SDGEdge e : currentSelectedViolation.getSummaryEdges()) {
				if (e == null) break;
				if (firstSU == null) {
					firstSU = e;
				}
				String edge = e.toString();
				boolean notRemovedEdge = summaryEdgesAndCorresJavaMethods.containsKey(e);
				boolean isChecked = violationsWrapper.isCheckedEdge(e);
				if (notRemovedEdge && !isChecked) {
					String summaryEdge = summaryEdgesAndCorresJavaMethods.get(e).toString();
					listViewSummaryEdges.getItems()
							.add(edge + " : " + summaryEdge);
				} 
				
			}
			for (SDGEdge disprovenEdge : violationsWrapper.getChopAndCorrespondingDisprovenSummaryEdges().keySet()) {
				if (violationsWrapper.getChopAndCorrespondingDisprovenSummaryEdges().get(disprovenEdge)
						.equals(this.currentSelectedViolation)) {
					String summaryEdge = allSummaryEdgesAndCorresJavaMethodsCopy.get(disprovenEdge).toString();
					listViewSummaryEdges.getItems().add("CLOSED " + disprovenEdge + " : " + summaryEdge);
				}
			}

			if (firstSU != null) {
				int newIndex = getSUIndex(firstSU);
				if(newIndex == -1) {
					//TODO: can this occure ? 
				}
				this.listViewSummaryEdges.getSelectionModel().select(0);
				this.onSummaryEdgeSelectionChange(newIndex);
			}
			
		}
	}
    
    /**
     * Get the index of the summary edge in the corresponding listView
     * @param su the summary Edge you want the index for.  
     * @return the index of the summary edge in the {@link listView} listViewSummaryEdge. Returns -1 if the summary edge can not be found
     */
    private int getSUIndex(SDGEdge su) {
    	for(int i = 0; i<itemIndexToSummaryEdge.size(); i++) {
			if (itemIndexToSummaryEdge.get(i) != null) {
				if (itemIndexToSummaryEdge.get(i).equals(su)) {
					return i;
				}
			}
    					
    	}
    	return -1;
	}
    

    //#######################################################################
    //this gets run whenever the selected FORMAL NODE TUPLE changes-------------->
    //#######################################################################
    private void onFormalPairSelectionChange(int newValue) {
        if (newValue < 0) {
            return;
        }
        SDGNodeTuple nodeTuple = currentIndexToNodeTuple.get(newValue);
        clearCodeAreaForNewCode(
                keyContractCodeArea,
                joanaKeyInterfacer.getKeyContractFor(nodeTuple, currentSelectedMethod));
    }

    //#######################################################################
    //this gets run whenever the selected LOOP INVARIANT changes-------------->
    //#######################################################################
    private void onLoopInvSelectionChanged(int newValue) {
        if (newValue == -1) {
            return;
        }

        clearCodeAreaForNewCode(loopInvariantCodeArea,
                                joanaKeyInterfacer.getLoopInvariantFor(currentSelectedEdge,
                                                                       newValue));

        buttonResetLoopInvariant.setOnAction((ActionEvent event) -> {
            joanaKeyInterfacer.resetLoopInvariant(currentSelectedEdge, newValue);
            clearCodeAreaForNewCode(loopInvariantCodeArea,
                    joanaKeyInterfacer.getLoopInvariantFor(currentSelectedEdge, newValue));
        });

        buttonSaveLoopInvariant.setOnAction((event) -> {
            joanaKeyInterfacer.setLoopInvariantFor(currentSelectedEdge, newValue,
                                                   loopInvariantCodeArea.getText());
        });
    }

    /**
     * this gets called whenever a new .dispro file is loaded or a new
     * DisprovingProject is created from a .joak file.
     */
    private void handleNewDisproSet() throws IOException {
        violationsWrapper = disprovingProject.getViolationsWrapper();
        this.allSummaryEdgesAndCorresJavaMethodsCopy.putAll(violationsWrapper.getSummaryEdgesAndCorresJavaMethods());
        
        violationsWrapper.addListener(this);
        
        //Initialize the index data structure for all violations
        int violationIndex = 1;
        //index 0 for "all" entry 
        this.itemIndexToViolation.put(0, null);

//        violationsWrapper.getAllViolationChops()
        for (ViolationChop chop : violationsWrapper.getAllViolationChops())	{
			this.itemIndexToViolation.put(violationIndex, chop);
			violationIndex++;
        }

        joanaKeyInterfacer = new JoanaKeYInterfacer(disprovingProject);
        useMethodInlining(keyJoanaSettings.getMethodExpandSetting());
        useLoopUnwinding(keyJoanaSettings.getLoopUnwindSetting());
        useSlicedFiles(keyJoanaSettings.getSlicingSetting());
//        useMethodInlining();
//        useLoopUnwinding();
//        useSlicedFiles();
        for (SDGEdge su : allSummaryEdgesAndCorresJavaMethodsCopy.keySet()) {
        	this.suEdgeClosedMap.put(su, false);
        }
        resetViews();
        //
        //setup disproving buttons
        //
        buttonTryDisprove.setOnAction((event) -> {
            onPressTryDisprove();
        });
        buttonOpenSelected.setOnAction((event) -> {
            onPressOpenInKeY();
        });
        buttonMarkAsDisproved.setOnAction((event) -> {
            onPressMarkAsDisproved();
        });
        buttonRunAuto.setOnAction((event) -> {
            onPressRunAuto();
        });
//        checkMenuItemUseSlicedFiles.setOnAction((event) -> {
//        	useSlicedFiles();
//        });
//        checkMenuItemUseLoopUnwinding.setOnAction((event) -> {
//        	useLoopUnwinding();
//        });
//        checkMenuItemUseMethodInlining.setOnAction((event) -> {
//        	useMethodInlining();
//        });

        //Initially selected violation is "All"
        onViolationSelectionChange(0);
    }
    

    private void resetViews() {
        //
        //do view stuff
        //
        labelProjName.setText(disprovingProject.getProjName());

        resetListView(listViewSummaryEdges);
        resetListView(listViewUncheckedChops);
        resetListView(listViewCalledMethodsInSE);
        resetListView(listViewLoopsInSE);
        resetListView(listViewFormalInoutPairs);

        itemIndexToSummaryEdge = new HashMap<>();

        listViewUncheckedChops.getItems().add("All");
        
        
//        Collection<? extends IViolation<SecurityNode>> uncheckedViolations =
//                violationsWrapper.getUncheckedViolations();
        
        //TODO: cause exception if .dispro is loaded 
       Collection<? extends IViolation<SecurityNode>> uncheckedViolations =
    		   violationsWrapper.getAllViolationsToCheck();
  
       for (IViolation<SecurityNode> v : uncheckedViolations) {
            assert v instanceof ClassifiedViolation;
            final ClassifiedViolation c = (ClassifiedViolation)v;
            final String src =
                    (c.getSource().getBytecodeName().startsWith("<") ?
                            c.getSource().getSr() + ":" + c.getSource().getBytecodeName()
                            : c.getSource().getLabel())
                    + " (" + c.getSource() + ")";
            final String snk =
                    (c.getSink().getBytecodeName().startsWith("<") ?
                            c.getSink().getSr() + ":" + c.getSink().getBytecodeName()
                            : c.getSink().getLabel())
                    + " (" + c.getSink() + ")";
            final String add =
                    "Illegal flow from " + src + " to " + snk
                  + ", visible for " + c.getAttackerLevel();
            listViewUncheckedChops.getItems().add(add);
  
//        for (ViolationChop c : chops) {
////            assert v instanceof ClassifiedViolation;
////            final ClassifiedViolation c = (ClassifiedViolation)v;
//            final String src =
//                    (c.getViolationSource().getBytecodeName().startsWith("<") ?
//                            c.getViolationSource().getSr() + ":" + c.getViolationSource().getBytecodeName()
//                            : c.getViolationSource().getLabel())
//                    + " (" + c.getViolationSource() + ")";
//            final String snk =
//                    (c.getViolationSink().getBytecodeName().startsWith("<") ?
//                            c.getViolationSink().getSr() + ":" + c.getViolationSink().getBytecodeName()
//                            : c.getViolationSink().getLabel())
//                    + " (" + c.getViolationSink() + ")";
//            final String add =
//                    "Illegal flow from " + src + " to " + snk;
//                  + ", visible for " + c.getAttackerLevel();

//            listViewUncheckedChops.getItems().add(add);
            
        }

        summaryEdgesAndCorresJavaMethods = violationsWrapper.getSummaryEdgesAndCorresJavaMethods();
        this.showAllSU();
//        int i = 0;
//        for (SDGEdge e : summaryEdgesAndCorresJavaMethods.keySet()) {
//            listViewSummaryEdges.getItems().add(
//                        e.toString() +
//                        " : " +
//                        summaryEdgesAndCorresJavaMethods.get(e).toString()
//                    );
//            itemIndexToSummaryEdge.put(i++, e);
//        }
    }

    @Override
    public void parsedChop(ViolationChop chop) {
    }

    @Override
    public void disprovedEdge(SDGEdge e) {
        Platform.runLater(() -> {
            labelSomeOtherData.setText("Disproved summary edge " + e.toString() + ".");

            int selectedIndex = listViewSummaryEdges.getSelectionModel().getSelectedIndex();

//            currentIndexToNodeTuple.remove(selectedIndex);
//            itemIndexToSummaryEdge.remove(selectedIndex);
//
//            for (int i = selectedIndex + 1; i < listViewSummaryEdges.getItems().size(); ++i) {
//                SDGEdge currentEdge = itemIndexToSummaryEdge.remove(i);
//                itemIndexToSummaryEdge.put(i - 1, currentEdge);
//                SDGNodeTuple currentRemovedNodeTuple = currentIndexToNodeTuple.remove(i);
//                currentIndexToNodeTuple.put(i - 1, currentRemovedNodeTuple);
//            }

            clearCodeAreaForNewCode(methodCodeArea, "");
            clearCodeAreaForNewCode(loopInvariantCodeArea, "");
            clearCodeAreaForNewCode(keyContractCodeArea, "");

            resetListView(listViewLoopsInSE);
            resetListView(listViewFormalInoutPairs);
            resetListView(listViewCalledMethodsInSE);

//            listViewSummaryEdges.getItems().add("CLOSED " + listViewSummaryEdges.getItems().get(selectedIndex));
            if(selectedIndex >= 0) {
            	listViewSummaryEdges.getItems().set(selectedIndex,
					"CLOSED " + listViewSummaryEdges.getItems().get(selectedIndex)); 
            }
//            listViewSummaryEdges.getItems().removeIf((s) -> {
//                return s.startsWith(e.toString());
//            });
            
            this.disprovenSUEdgesAndCorresViolation.put(e ,currentSelectedViolation);
            suEdgeClosedMap.put(e, true);
//            
            listViewSummaryEdges.getSelectionModel().clearSelection();
            listViewSummaryEdges.getSelectionModel().select(0);
        });

    }

    @Override
    public void disprovedChop(ViolationChop chop) {
		Platform.runLater(() -> {
			checkCloseViolation(chop);
//			labelSomeOtherData.setText("The chop from " + chop.getViolationSource().toString() + " to "
//					+ chop.getViolationSink().toString() + " was fully disproved.");
			setSomeOtherMessageAndColor("The chop from " + chop.getViolationSource().toString() + " to "
					+ chop.getViolationSink().toString() + " was fully disproved.", Color.GREEN);
		});
    }

    @Override
    public void disprovedAll() {
        Platform.runLater(() -> {
//            labelSomeOtherData.setText("All security violations have been disproved with KeY!");
            this.setSomeOtherMessageAndColor("All security violations have been disproved with KeY!", Color.GREEN);
            buttonRunAuto.setDisable(true);
            displayAllViolationChecked();
           // AutomationHelper.playSound(SOUND);
        });
    }

    @Override
    public void addedNewEdges(Map<SDGEdge, StaticCGJavaMethod> edgesToMethods,
                              List<SDGEdge> edgesSorted, SDG sdg) {
        Platform.runLater(() -> {
            resetViews();
        });
    }
    
    public void notifyAutoRunCompletion() {
    	Platform.runLater(() -> {
    		stopAutoRun();
    	});
    }

	@Override
	public void notifySettingsChange() {
		useMethodInlining(keyJoanaSettings.getMethodExpandSetting());
		useLoopUnwinding(keyJoanaSettings.getLoopUnwindSetting());
		useSlicedFiles(keyJoanaSettings.getSlicingSetting());
	}
	
	private void showAllSU() {
		int i = 0;
        for (SDGEdge e : summaryEdgesAndCorresJavaMethods.keySet()) {
            listViewSummaryEdges.getItems().add(
                        e.toString() +
                        " : " +
                        summaryEdgesAndCorresJavaMethods.get(e).toString()
                    );
            itemIndexToSummaryEdge.put(i++, e);
        }
        Set<SDGEdge> collectedEdges = new HashSet<SDGEdge>();
        for (SDGEdge e : violationsWrapper.getChopAndCorrespondingDisprovenSummaryEdges().keySet()) {
        	collectedEdges.add(e);
        }
        for (SDGEdge e : collectedEdges) {
        	listViewSummaryEdges.getItems().add(
        				"CLOSED " +
                        e.toString() +
                        " : " +
                        allSummaryEdgesAndCorresJavaMethodsCopy.get(e).toString()
                    );
            itemIndexToSummaryEdge.put(i++, e);
        }
	}
	
	
	//TODO:	 "CLOSED" check with datastructure. Dont use the the list item name ?
	private void displayAllViolationChecked() {
//		for (int i : itemIndexToViolation.keySet()) {
		for (int i = 0; i < listViewUncheckedChops.getItems().size(); i++) {
			String itemText = listViewUncheckedChops.getItems().get(i);
			if (!itemText.startsWith("CLOSED: ")) {
				listViewUncheckedChops.getItems().set(i, "CLOSED: " + listViewUncheckedChops.getItems().get(i));
			}
		}
	}
	
	private void checkCloseViolation(ViolationChop v) {
		for(int i : itemIndexToViolation.keySet()) {
			if (i != 0) {
				if (itemIndexToViolation.get(i).equals(v)) {
					if(listViewUncheckedChops.getItems().get(i).startsWith("CLOSED: ")) {
						continue;
					}
					if (!violationsWrapper.getAllViolationChops().contains(v)) {
						String itemText = listViewUncheckedChops.getItems().get(i);
						if (!itemText.startsWith("CLOSED: ")) {
							listViewUncheckedChops.getItems().set(i, "CLOSED: " + listViewUncheckedChops.getItems().get(i));
						}
					}
				}
			}
		}
	}
	
	private void clearView() {
		resetListView(listViewSummaryEdges);
        resetListView(listViewCalledMethodsInSE);
        resetListView(listViewLoopsInSE);
        resetListView(listViewFormalInoutPairs);
        
        clearCodeAreaForNewCode(methodCodeArea, "");
        clearCodeAreaForNewCode(loopInvariantCodeArea, "");
        clearCodeAreaForNewCode(keyContractCodeArea, "");
	}
	
	
	/**
	 * this method set the cell factory of the violation listView. 
	 * Change the cells if the violation is closed/checked
	 */
	private void setChopViewListFactory() {
		listViewUncheckedChops.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new ListCell<String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setText(null);
                            setStyle("-fx-control-inner-background: " + DEFAULT_CONTROL_INNER_BACKGROUND + ";");
                        } else {
                            setText(item);
                            //TODO: maybe use a datastructure to remember which item is closed instead of checking the String 
                            if (item.startsWith("CLOSED")) {
                                setStyle("-fx-control-inner-background: " + HIGHLIGHTED_CONTROL_INNER_BACKGROUND + ";");
                                
//                                setDisable(true);
                                if(!violationsWrapper.allCheckedOrDisproved()) {
                                	listViewUncheckedChops.getSelectionModel().select(0);
//                                	onViolationSelectionChange(0);
                                }
                            } else {
                            	setDisable(false);
                                setStyle("-fx-control-inner-background: " + DEFAULT_CONTROL_INNER_BACKGROUND + ";");
                            }
                        }
                    }
                };
            }
        });	
	}
	
	/**
	 * this method set the cell factory of the summary edges listView. 
	 * Change the cells occurrence if the summary edge is closed/checked
	 */
	private void setSUViewListFactory() {
		listViewSummaryEdges.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new ListCell<String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setText(null);
                            setStyle("-fx-control-inner-background: " + DEFAULT_CONTROL_INNER_BACKGROUND + ";");
                        } else {
                            setText(item);
                            //TODO: maybe use a datastructure to remember which item is closed instead of checking the String 
                            if (item.startsWith("CLOSED")) {
                                setStyle("-fx-control-inner-background: " + HIGHLIGHTED_CONTROL_INNER_BACKGROUND + ";");
//                                setDisable(true);
                                
                            } else {
                            	setDisable(false);
                                setStyle("-fx-control-inner-background: " + DEFAULT_CONTROL_INNER_BACKGROUND + ";");
                            }
                        }
                    }
                };
            }
        });	
	}

	
	/**
	 * set the gui status 'other message' to the given message in the given color
	 * @param message you want to print at the gui
	 * @param color in which you would like to have the message 
	 */
	public void setSomeOtherMessageAndColor(String message, javafx.scene.paint.Color color) {
		this.labelSomeOtherData.setText(message);
		this.labelSomeOtherData.setTextFill(color);
	}
}
