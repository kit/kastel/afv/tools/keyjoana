/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import gui.asynctaskhandler.AsyncBackgroundLoader;
import gui.asynctaskhandler.AsyncCreateDisproSaveStr;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import joanakey.errorhandling.ErrorLogger;
import joanakey.persistence.DisprovingProject;
import keyjoana.settings.KeyJoanaSettings;
import keyjoana.settings.SettingsManager;
import keyjoana.settings.SettingsObserver;
import keyjoana.settings.SettingsPaths;
import keyjoana.util.FilesUtil;

/**
 *
 * @author holger,joachim
 */
public class MainWindowController implements Initializable, SettingsObserver {

	// ---------------------fxml fields---------------------------
	@FXML
	private AnchorPane anchorPaneBasicInfo;

	@FXML
	private Label labelProjName;

	@FXML
	private Label labelCurrentAction;

	@FXML
	private Label labelSummaryEdge;

	@FXML
	private Label labelSomeOtherData;

	@FXML
	private AnchorPane anchorPaneDisproProgress;

	@FXML
	private MenuItem menuItemOpenJoak;

	@FXML
	private MenuItem menuItemOpenDispro;

	@FXML
	private MenuItem menuItemSaveProgress;

	@FXML
	private MenuItem menuItemReloadFile;
	
	@FXML
	private MenuItem menuItemExit;
	
	@FXML
	private Menu menuOpenRecent;

	@FXML
	private CheckMenuItem checkMenuItemUseSlicedFiles;

	@FXML
	private CheckMenuItem checkMenuItemUseMethodInlining;

	@FXML
	private CheckMenuItem checkMenuItemUseLoopUnwinding;

	@FXML
	private ProgressIndicator progressIndicator;

	@FXML
	private AnchorPane anchorPaneMain;

	@FXML
	private MenuBar menuBarMain;

	@FXML
	private ListView<String> listViewUncheckedChops;

	@FXML
	private ListView<String> listViewUncheckedEdges;

	@FXML
	private ListView<String> listViewLoopsOfSE;

	@FXML
	private ListView<String> listViewCalledMethodsOfSE;

	@FXML
	private ListView<String> listViewFormalInoutPairs;

	@FXML
	private AnchorPane anchorPaneMethodCode;

	@FXML
	private AnchorPane anchorPaneLoopInvariant;

	@FXML
	private AnchorPane anchorPaneKeyContract;

	@FXML
	private Button buttonMarkAsDisproved;

	@FXML
	private Button buttonOpenSelected;

	@FXML
	private Button buttonTryDisprove;

	@FXML
	private Button buttonRunAtuo;

	@FXML
	private Button buttonSaveLoopInvariant;

	@FXML
	private Button buttonResetLoopInvariant;

	// ---------------------other fields---------------------------
	private static FileChooser fileChooser = new FileChooser();
	private Stage mainStage;
	// private LoopInvariantFromUserGetter loopInvariantGetter = new
	// LoopInvariantFromUserGetter();
	private AsyncBackgroundLoader asyncBackgroundLoader;
	private RecentlyUsedFilesManager recentlyUsedFilesManager = new RecentlyUsedFilesManager(
			SettingsPaths.getRecentLoadedFilesPath());

	private static final String DISPRO_FILE_ENDING = "dispro";
	private static final String DOT_DISPRO = "." + DISPRO_FILE_ENDING;
	private static final String PROJECT_FILE_ENDING = "joak";
	private static File lastFileLoaded = null;

	private DisproHandler disproHandler;
	private AsyncCreateDisproSaveStr disproSaveStrCreator;

	private CurrentActionLogger actionLogger;
	private SettingsManager settingsManager;
	private KeyJoanaSettings keyJoanaSettings;

	// ---------------------static methods boiiiiii-----------------------
	public static File letUserChooseFile(String title, String extensionExp, String extension, File baseDirectory,
			Window ownerWindow) {
		fileChooser.setTitle(title);
		if (baseDirectory != null) {
			fileChooser.setInitialDirectory(baseDirectory);
		}
		fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter(extensionExp, extension));
		File chosenFile = fileChooser.showOpenDialog(ownerWindow);
		return chosenFile;
	}

	// -------------------non-static methods-------------------------------
	private void tryLetUserChooseFileAndHandleResponse(String joakordispro) {
		if (!joakordispro.equals(DISPRO_FILE_ENDING) && !joakordispro.equals(PROJECT_FILE_ENDING)) {
			ErrorLogger.logError("This file extension is not known to the program. " + "Please step yo game up.",
					ErrorLogger.ErrorTypes.UNKNOWN_FILE_EXTENSION);
			return;
		}
		String title = "Please navigate to and select " + joakordispro + " file.";
		String extensionExp = "A *.joak file containing information about which Java project to load.";
		String extension = "*." + joakordispro;
		File file = letUserChooseFile(title, extensionExp, extension, null, mainStage);
		if (file != null && (!file.exists() || !file.getName().endsWith(joakordispro))) {
			ErrorLogger.logError(
					"No valid " + joakordispro + "file was chosen by user or the file was chosen incorrectly "
							+ "or another error (such as I/O) occurred, homeboy.",
					ErrorLogger.ErrorTypes.ERROR_USER_CHOOSING_FILE);
		} else if (file != null) {
			// TODO: make only certain control elements inactive, or handle user clicking in
			// some other way
			// might be more trouble than its worth though...idk
			menuBarMain.setDisable(true);
			loadFile(joakordispro, file);
		}
	}
	
	private void loadFile(String fileExtension, File file) {
		if (fileExtension.equals(PROJECT_FILE_ENDING)) {
			disproHandler.setAllButtonsDisable(true);
			asyncBackgroundLoader.loadJoakFile(file, (newCheckData, success) -> {
				if (success) {

					disableSettings(false);
					disproHandler.handleNewDispro(newCheckData);

//					lastFileLoaded = file;
					updateRecentlyLoadedFiles(file);

					menuItemReloadFile.setDisable(false);
					menuItemSaveProgress.setDisable(false);
					labelSomeOtherData.setText("");
				} else {
					menuBarMain.setDisable(false);
				}
			});
		} else if (fileExtension.equals(DISPRO_FILE_ENDING)) {
			disproHandler.setAllButtonsDisable(true);
			asyncBackgroundLoader.loadDisproFie(file, (dispro, succes) -> {
				if (succes) {
					disableSettings(false);
					disproHandler.handleNewDispro(dispro);

//					lastFileLoaded = file;
					updateRecentlyLoadedFiles(file);
					menuItemReloadFile.setDisable(false);
					menuItemSaveProgress.setDisable(false);
					labelSomeOtherData.setText("");
				} else {
					menuBarMain.setDisable(false);
				}
			});
		}
	}
	
	private void updateRecentlyLoadedFiles(File file) {
		recentlyUsedFilesManager.storeFileToRecentlyUsedFiles(file);
		updateOpenRecentMenu();
	}
	
	
	private void loadMostRecentlyUsedFile() {
		File mostRecentlyLoadedFile = recentlyUsedFilesManager.getMostRecentlyUsedFile();
		if (mostRecentlyLoadedFile == null) {
			return;
		} else {
			menuBarMain.setDisable(true);
			String fileExtension = getFileExtension(mostRecentlyLoadedFile);
			loadFile(fileExtension, mostRecentlyLoadedFile);
		}
	}

	private void saveDispro() {
		DisprovingProject disprovingProject = disproHandler.getDisprovingProject();
		disproSaveStrCreator.createSaveStr(disprovingProject, (String string, Boolean sucess) -> {
			if (sucess) {
				fileChooser.setTitle("Please navigate to where you want to " + "save this disproving project.");
				fileChooser.setSelectedExtensionFilter(
						new FileChooser.ExtensionFilter("Disproving project storage file", DOT_DISPRO));
				File saveFile = fileChooser.showSaveDialog(mainStage);
				if (saveFile == null) {
					ErrorLogger.logError(
							"No file was chosen by user or " + "the file was chosen incorrectly "
									+ "or another error (such as I/O) " + "occured, homeboy.",
							ErrorLogger.ErrorTypes.ERROR_USER_CHOOSING_FILE);
				} else {
					if (!saveFile.getName().endsWith(DOT_DISPRO)) {
						saveFile = new File(saveFile.getAbsolutePath() + DOT_DISPRO);
					}
					FileWriter fileWriter;
					try {
						fileWriter = new FileWriter(saveFile);
						fileWriter.write(string);
						fileWriter.close();
					} catch (IOException ex) {
						ErrorLogger.logError("Error while trying to write file to disk.",
								ErrorLogger.ErrorTypes.ERROR_WRITING_FILE_TO_DISK);
					}
				}
			}
		});
	}

	@SuppressWarnings("unused")
	@Deprecated
	private void reloadLastFile() {
		if (lastFileLoaded == null) {
            ErrorLogger.logError("no file was loaded yet.",
                    ErrorLogger.ErrorTypes.ERROR_USER_CHOOSING_FILE);
		}
    	if (lastFileLoaded != null && !lastFileLoaded.exists()) {
            ErrorLogger.logError("the file does not exist or no file was loaded yet.",
                    ErrorLogger.ErrorTypes.ERROR_USER_CHOOSING_FILE);
    	}
    	if (!((lastFileLoaded.getName().endsWith(DISPRO_FILE_ENDING)) || (lastFileLoaded.getName().endsWith(PROJECT_FILE_ENDING)))) {
    		ErrorLogger.logError("This file extension is not known to the program. ",
    				ErrorLogger.ErrorTypes.UNKNOWN_FILE_EXTENSION);
    	}
    	else if (lastFileLoaded != null) {
			menuBarMain.setDisable(true);
			String fileExtension = getFileExtension(lastFileLoaded);
    		loadFile(fileExtension, lastFileLoaded);
    	}
    }
	
	
	private String getFileExtension(File file) {
		return FilesUtil.getFileExtension(file);
	}

	public void setMainStage(Stage mainStage) {
		this.mainStage = mainStage;
	}
	
	private void updateOpenRecentMenu() {
		String[] allRecentlyUsedPaths = recentlyUsedFilesManager.getAllRecentlyUsedFilePaths();
		menuOpenRecent.getItems().clear();
		MenuItem[] items = new MenuItem[allRecentlyUsedPaths.length];
		for (int i = 0; i < allRecentlyUsedPaths.length; i++) {
			String path = allRecentlyUsedPaths[i];
			items[i] = new MenuItem(FilesUtil.getNameByFilePath(path));
			items[i].setOnAction((event) -> {
				File file = new File(path);
				loadFile(getFileExtension(file), file);
			});
		}
		menuOpenRecent.getItems().addAll(items);
	}
	

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		actionLogger = new CurrentActionLogger(labelCurrentAction, progressIndicator);
		asyncBackgroundLoader = new AsyncBackgroundLoader(actionLogger);
		
		keyJoanaSettings = new KeyJoanaSettings();
		settingsManager = new SettingsManager();
		//settingsManager.setkeyJoanaSettings(keyJoanaSettings);
		settingsManager.addSetting(keyJoanaSettings);
		keyJoanaSettings.addSettingObserver(settingsManager);
		keyJoanaSettings.addSettingObserver(this);
		settingsManager.loadSettings();
		
		disproHandler = new DisproHandler(actionLogger, labelProjName, labelSummaryEdge, labelSomeOtherData,
				menuBarMain, listViewUncheckedEdges, listViewUncheckedChops, listViewCalledMethodsOfSE,
				listViewLoopsOfSE, listViewFormalInoutPairs, anchorPaneMethodCode, anchorPaneLoopInvariant,
				anchorPaneKeyContract, buttonSaveLoopInvariant, buttonResetLoopInvariant, buttonMarkAsDisproved,
				buttonOpenSelected, buttonTryDisprove, buttonRunAtuo, checkMenuItemUseSlicedFiles,
				checkMenuItemUseMethodInlining, checkMenuItemUseLoopUnwinding, keyJoanaSettings);
		disproHandler.setAllButtonsDisable(true);
		disproSaveStrCreator = new AsyncCreateDisproSaveStr(actionLogger);

		menuItemOpenJoak.setOnAction((event) -> {
			tryLetUserChooseFileAndHandleResponse(PROJECT_FILE_ENDING);
		});
		menuItemOpenDispro.setOnAction((event) -> {
			tryLetUserChooseFileAndHandleResponse(DISPRO_FILE_ENDING);
		});
		menuItemSaveProgress.setOnAction((event) -> {
			saveDispro();
		});
		menuItemReloadFile.setOnAction((event) -> {
//			reloadLastFile(); deprecated
			loadMostRecentlyUsedFile();
		});
		menuItemExit.setOnAction((event) -> {
			System.exit(0);
		});
		checkMenuItemUseSlicedFiles.setOnAction((event) -> {
			keyJoanaSettings.setSlicingSetting(checkMenuItemUseSlicedFiles.isSelected());
        });
        checkMenuItemUseLoopUnwinding.setOnAction((event) -> {
        	keyJoanaSettings.setLoopUnwindSetting(checkMenuItemUseLoopUnwinding.isSelected());
        });
        checkMenuItemUseMethodInlining.setOnAction((event) -> {
        	keyJoanaSettings.setMethodExpandSetting(checkMenuItemUseMethodInlining.isSelected());
        });
		updateOpenRecentMenu();
	}
	
	private void disableSettings(boolean b) {
		checkMenuItemUseSlicedFiles.setDisable(b);
        checkMenuItemUseLoopUnwinding.setDisable(b);
        checkMenuItemUseMethodInlining.setDisable(b);
	}
	
	private void updateKeyJoanaSettings() {
		this.checkMenuItemUseLoopUnwinding.setSelected(this.keyJoanaSettings.getLoopUnwindSetting());
		this.checkMenuItemUseMethodInlining.setSelected(this.keyJoanaSettings.getMethodExpandSetting());
		this.checkMenuItemUseSlicedFiles.setSelected(this.keyJoanaSettings.getSlicingSetting());
	}

	@Override
	public void notifySettingsChange() {
		this.updateKeyJoanaSettings();
	}
}
