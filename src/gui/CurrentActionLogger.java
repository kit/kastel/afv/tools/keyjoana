/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.paint.Color;

/**
 *
 * @author holger
 */
public class CurrentActionLogger {

    private Label labelCurrentAction;
    private ProgressIndicator progressIndicator;

    public CurrentActionLogger(Label labelCurrentAction, ProgressIndicator progressIndicator) {
        this.labelCurrentAction = labelCurrentAction;
        this.progressIndicator = progressIndicator;
        endProgress();
    }

    
    /**
     * start progress indicator and show the given string as current action on the GUI
     * @param msg the message you want to show on the GUI as current action
     */
    public void startProgress(String msg) {
        progressIndicator.setVisible(true);
        labelCurrentAction.setText(msg);
        labelCurrentAction.setTextFill(Color.BLACK);
    }

    /**
     * start progress indicator and show the given string as current action on the GUI in a specific color
     * @param msg the message you want to show on the GUI as current action
     * @param c the color for the message
     */
    public void startProgress(String msg, Color c) {
        progressIndicator.setVisible(true);
        labelCurrentAction.setTextFill(c);
        labelCurrentAction.setText(msg);
    }

    /**
     * stop progress indicator and clear the current action text on the gui.
     */
    public void endProgress() {
        labelCurrentAction.setText("");
        progressIndicator.setVisible(false);
        labelCurrentAction.setTextFill(Color.BLACK);
    }
    
    /**
     * stop progress indicator and show the given string as current action on the GUI
     * @param msg the message you want to show on the GUI as current action
     */
    public void endProgressWithMessage(String msg) {
    	labelCurrentAction.setText(msg);
    	progressIndicator.setVisible(false);
        labelCurrentAction.setTextFill(Color.BLACK);
    }

    /**
     * stop progress indicator and show the given string as current action on the GUI
     * @param msg the message you want to show on the GUI as current action
     * @param c the color for the message
     */
    public void endProgressWithMessage(String msg, Color c) {
    	labelCurrentAction.setText(msg);
    	labelCurrentAction.setTextFill(c);
    	progressIndicator.setVisible(false);
    }
    
    /**
     * show the given string as current action on the GUI
     * @param msg the message you want to show on the GUI as current action
     */
    public void showMessage(String msg) {
    	labelCurrentAction.setText(msg);
        labelCurrentAction.setTextFill(Color.BLACK);
    }

    /**
     * show the given string as current action on the GUI
     * @param msg the message you want to show on the GUI as current action
     * @param c the color for the message
     */
    public void showMessage(String msg, Color c) {
    	labelCurrentAction.setText(msg);
        labelCurrentAction.setTextFill(c);
    }

}
