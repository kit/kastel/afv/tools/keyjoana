/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.io.IOException;
import java.util.ArrayList;

import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGEdge;
import edu.kit.joana.ifc.sdg.graph.SDGNodeTuple;
import joanakey.AutomationHelper;
import joanakey.customlistener.SummaryEdgeAndMethodToCorresData;
import joanakey.javaforkeycreator.JavaForKeYCreator;
import joanakey.persistence.DisprovingProject;
import joanakey.staticCG.javamodel.StaticCGJavaMethod;
import joanakey.violations.ViolationChop;
import joanakey.violations.ViolationsDisproverSemantic;
import joanakey.violations.ViolationsDisproverSemantic.PO_TYPE;
import joanakey.violations.ViolationsWrapper;

/**
 * this class can be used to start the disproving process with KeY (also with command line mode)
 * @author holger, joachim
 */
public class JoanaKeYInterfacer {

    private ViolationsWrapper violationsWrapper;
    private JavaForKeYCreator javaForKeyCreator;
    private SummaryEdgeAndMethodToCorresData summaryEdgeToCorresData;
    private boolean useSlicedFiles = true;
    private boolean useMethodInlining = false;
    private boolean useLoopUnwinding = false;

    public JoanaKeYInterfacer(
            DisprovingProject disprovingProject) throws IOException {
        this.violationsWrapper = disprovingProject.getViolationsWrapper();
        this.javaForKeyCreator
                = new JavaForKeYCreator(
                		disprovingProject.getPathToJava(),
                        disprovingProject.getCallGraph(),
                        disprovingProject.getSdg(),
                        disprovingProject.getStateSaver());
        //Map<SDGEdge, StaticCGJavaMethod> summaryEdgesAndCorresJavaMethods =
        violationsWrapper.getSummaryEdgesAndCorresJavaMethods();
        summaryEdgeToCorresData = disprovingProject.getSummaryEdgeToCorresData();
    }

    public String getKeyContractFor(SDGNodeTuple formalTuple,
                                    StaticCGJavaMethod methodCorresToSE) {
        return summaryEdgeToCorresData.getContractFor(formalTuple);
    }

    public String getLoopInvariantFor(SDGEdge e, int index) {
        return summaryEdgeToCorresData.getLoopInvariantFor(e, index);
    }

    public void setLoopInvariantFor(SDGEdge e, int index, String val) {
        summaryEdgeToCorresData.setLoopInvariantFor(e, index, val);
    }

    public void resetLoopInvariant(SDGEdge currentSelectedEdge, int newValue) {
        summaryEdgeToCorresData.resetLoopInvariant(currentSelectedEdge, newValue);
    }

    public void openInKeY(SDGEdge e,
                          SDGNodeTuple tuple,
                          StaticCGJavaMethod corresMethod) throws IOException {
    	if(corresMethod == null) {
    		throw new IllegalArgumentException("No method selected");
    	}
    	ArrayList<ViolationChop> chops = violationsWrapper.getSummaryEdgesAndContainingChops().get(e);
		if (chops != null && !chops.isEmpty()) {
			javaForKeyCreator.setMethodInlining(this.useMethodInlining);
			javaForKeyCreator.setUseLoopUnwinding(this.useLoopUnwinding);
			javaForKeyCreator.generateJavaForFormalTupleCalledFromGui(summaryEdgeToCorresData.getContractFor(tuple),
					corresMethod, summaryEdgeToCorresData.getEdgeToLoopInvariantTemplate().get(e),
					summaryEdgeToCorresData.getEdgeToLoopInvariant().get(e),
					summaryEdgeToCorresData.getMethodToMostGeneralContract(), chops.get(0), useSlicedFiles); //all chops same slice ? 
			AutomationHelper.openKeY(ViolationsDisproverSemantic.PO_PATH);
		}
    }

    /**
     * try to disprove the given summary Edge with KeY in command line mode.
     * @param e {@link SDGEdge} you want to disprove
     * @param tuple 
     * @param corresMethod
     * @return true if successfully disproven. Otherwise return False.
     * @throws IOException
     */
    public boolean tryDisproveEdge(
            SDGEdge e,
            SDGNodeTuple tuple,
            StaticCGJavaMethod corresMethod) throws IOException{
        if (corresMethod == null) {
            throw new IllegalArgumentException("No method selected!");
        }
        boolean worked = false;
    	ArrayList<ViolationChop> chops = violationsWrapper.getSummaryEdgesAndContainingChops().get(e);
    	if (chops != null && !chops.isEmpty()) {
			ViolationChop chop = chops.get(0); //all chop same slice ? 
			// String pathToTestJava =
			javaForKeyCreator.generateJavaForFormalTupleCalledFromGui(summaryEdgeToCorresData.getContractFor(tuple),
					corresMethod, summaryEdgeToCorresData.getEdgeToLoopInvariantTemplate().get(e),
					summaryEdgeToCorresData.getEdgeToLoopInvariant().get(e),
					summaryEdgeToCorresData.getMethodToMostGeneralContract(), chop, useSlicedFiles);
			worked = AutomationHelper.runKeY(ViolationsDisproverSemantic.PO_PATH, PO_TYPE.INFORMATION_FLOW);
    	}
        if (worked) {
            worked = AutomationHelper.runKeY(ViolationsDisproverSemantic.PO_PATH,
                                             PO_TYPE.FUNCTIONAL);
        }
        if (worked) {
            violationsWrapper.removeEdge(e);
        }
        return worked;
    }

    /**
     * mark the given {@link SDGEdge} as disproven and remove it from the {@link SDG} in {@link ViolationsWrapper}.
     * @param currentSelectedEdge the {@link SDGEdge} you want to mark as disproven.
     * @throws IllegalArgumentException
     */
    public void markAsDisproved(SDGEdge currentSelectedEdge) throws IllegalArgumentException{
    	if(currentSelectedEdge == null) {
    		throw new IllegalArgumentException("No method selected!");
    	}
        violationsWrapper.removeEdge(currentSelectedEdge);
    }
    
    

    public JavaForKeYCreator getJavaForKeyCreator() {
        return javaForKeyCreator;
    }

    public SummaryEdgeAndMethodToCorresData getSummaryEdgeToCorresData() {
        return summaryEdgeToCorresData;
    }

	public void setUseSclicedFiles(boolean selected) {
		this.useSlicedFiles = selected;
	}

	/**
	 * set method inlining option for the disproving process
	 * @param selected true, if you want to use method inlining 
	 */
	public void setUseMethodInlining(boolean selected) {
		this.useMethodInlining = selected;
		javaForKeyCreator.setMethodInlining(selected);
	}

	/**
	 * set loop unwinding option for the disproving process
	 * @param selected true, if you want to use loop unwinding 
	 */
	public void setUseLoopUnwinding(boolean selected) {
		this.useLoopUnwinding = selected;
		javaForKeyCreator.setUseLoopUnwinding(selected);
	}

	
}
