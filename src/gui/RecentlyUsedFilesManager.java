package gui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * this class manages the recently loaded files 
 * @author joachim
 *
 */
public class RecentlyUsedFilesManager {

	private String recentlyUsedFilesPath;
	private String mostRecentlyUsedFile;
	private ArrayList<String> recentlyUsedFiles;
	private int maxNumberOfSavedRecentlyLoadedFiles;

	public RecentlyUsedFilesManager(String path) {
//		recentlyUsedFilesPath = SettingsPaths.getRecentLoadedFilesPath();
		recentlyUsedFilesPath = path;
		this.maxNumberOfSavedRecentlyLoadedFiles = 5;
		File f = new File(recentlyUsedFilesPath);
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		loadRecentlyUsedFilesPaths(recentlyUsedFilesPath);
	}

	/**
	 * get the most recently loaded file
	 * @return the most recently loaded file otherwise null
	 */
	public File getMostRecentlyUsedFile() {
		if (mostRecentlyUsedFile != null) {
			return new File(mostRecentlyUsedFile);
		} else {
			loadRecentlyUsedFilesPaths(recentlyUsedFilesPath);
			boolean updated = updateMostRecentlyLoadedFilePath();
			if (!updated) {
				return null;
			}
			if (mostRecentlyUsedFile != null) {
				File f = new File(mostRecentlyUsedFile);
				if (f.exists()) {
					return f;
				}
			}
			return null;
		}
	}
	
	public String[] getAllRecentlyUsedFilePaths() {
		String[] names = new String[this.recentlyUsedFiles.size()];
		for (int i = 0; i < names.length; i++) {
			names[i] = recentlyUsedFiles.get(i);
		}
		return names;
	}
	

	/**
	 * Get the path of the most recently loaded file
	 * @return path of the most recently loaded file as path String
	 */
	public String getMostRecentlyUsedFilePath() {
		if (mostRecentlyUsedFile == null) {
			loadRecentlyUsedFilesPaths(recentlyUsedFilesPath);
			return mostRecentlyUsedFile;
		}
		return mostRecentlyUsedFile;
	}

	/**
	 * stores the given file to the recently loaded files and update the most recently loaded file to it
	 * @param file you want to store and update to the most recently loaded file
	 */
	public void storeFileToRecentlyUsedFiles(File file) {
		if(recentlyUsedFiles.contains(file.getAbsolutePath())) {
			int index = recentlyUsedFiles.indexOf(file.getAbsolutePath());
			recentlyUsedFiles.remove(index);
		}
		recentlyUsedFiles.add(0, file.getAbsolutePath());
		updateMostRecentlyLoadedFilePath();
		if (recentlyUsedFiles.size() > maxNumberOfSavedRecentlyLoadedFiles) {
			recentlyUsedFiles.remove(recentlyUsedFiles.size()-1);
		}
		//Update mostRecentlyUsedFile
		//store new content in the File for recently loaded files
		try {
			StringBuilder created = new StringBuilder();
			FileWriter writer = new FileWriter(recentlyUsedFilesPath);
//			PrintWriter out = new PrintWriter(writer);
			for (int i = 0; i < recentlyUsedFiles.size(); i++) {
				created.append(recentlyUsedFiles.get(i));
				if (i != recentlyUsedFiles.size()-1) {
					created.append("\n");
				}
			}
			writer.write("");
//			File recentlyUsedFiles = new File(recentlyUsedFilesPath);
//			recentlyUsedFiles.delete();
//			recentlyUsedFiles.createNewFile();
			writer.write(created.toString());
			writer.close();
//			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * set the maximum number of recently loaded files saved
	 * @param maxNumber
	 */
	public void setMaxNumberOfSavedFiles(int maxNumber) {
		this.maxNumberOfSavedRecentlyLoadedFiles = maxNumber;
	}
	
	private void loadRecentlyUsedFilesPaths(String path) {
		if (new File(path).exists()) {
			BufferedReader reader;
			recentlyUsedFiles = new ArrayList<String>(maxNumberOfSavedRecentlyLoadedFiles);
			try {
				reader = new BufferedReader(new FileReader(recentlyUsedFilesPath));
				String line = reader.readLine();
				while (line != null) {
					recentlyUsedFiles.add(line);
					line = reader.readLine();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * updates the most recently loaded file
	 * @return true if updated and false if there are no recently loaded files yet
	 */
	private boolean updateMostRecentlyLoadedFilePath() {
		if (recentlyUsedFiles.isEmpty()) {
			return false;
		}
		mostRecentlyUsedFile = recentlyUsedFiles.get(0);
		return true;
	}
	
	
}
