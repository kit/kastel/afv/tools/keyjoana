/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.asynctaskhandler;

import java.util.function.BiConsumer;

import com.sun.prism.paint.Color;

import gui.CurrentActionLogger;
import javafx.application.Platform;
import joanakey.errorhandling.ErrorLogger;
import joanakey.persistence.DisprovingProject;

/**
 *
 * @author holger
 */
public class AsyncCreateDisproSaveStr implements Runnable {
    private String saveStr;
    private boolean succes = false;
    private DisprovingProject disprovingProject;
    private BiConsumer<String, Boolean> uiThreadCallback;
    private CurrentActionLogger actionLogger;

    public AsyncCreateDisproSaveStr(CurrentActionLogger actionLogger) {
        this.actionLogger = actionLogger;
    }
        
    public void createSaveStr(DisprovingProject disprovingProject,
                              BiConsumer<String, Boolean> uiThreadCallback) {
        this.disprovingProject = disprovingProject;
        this.uiThreadCallback = uiThreadCallback;
        actionLogger.startProgress("Now creating storage file from project, please wait.");
        new Thread(this).start();
    }

    @Override
    public void run() {
        try {            
            disprovingProject.saveSDG();
            saveStr = disprovingProject.generateSaveString();
            succes = true;
        } catch (Exception ex) {
        	ex.printStackTrace();
            succes = false;
            ErrorLogger.logError("Error while trying to create storage String for disprovingProject.",
                    ErrorLogger.ErrorTypes.ERROR_CREATING_SAVE_STR_FOR_DISPRO);
        }
        Platform.runLater(() -> {
        	if (succes) {
        		actionLogger.endProgress();
        	} else {
            	actionLogger.endProgressWithMessage("Error while trying to create storage String", javafx.scene.paint.Color.RED);
        	}
            uiThreadCallback.accept(saveStr, succes);
        });
    }
}
