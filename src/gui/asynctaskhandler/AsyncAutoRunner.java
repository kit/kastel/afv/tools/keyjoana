/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.asynctaskhandler;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.stringtemplate.v4.compiler.CodeGenerator.list_return;

import com.google.common.util.concurrent.Service.Listener;

import edu.kit.joana.ifc.sdg.graph.SDGEdge;
import edu.kit.joana.ifc.sdg.graph.SDGNodeTuple;
import gui.CurrentActionLogger;
import gui.DisproHandler;
import javafx.application.Platform;
import javafx.scene.control.ListView;
import joanakey.AutomationHelper;
import joanakey.customlistener.SummaryEdgeAndMethodToCorresData;
import joanakey.javaforkeycreator.JavaForKeYCreator;
import joanakey.staticCG.javamodel.StaticCGJavaMethod;
import joanakey.violations.ViolationChop;
import joanakey.violations.ViolationsDisproverSemantic;
import joanakey.violations.ViolationsDisproverSemantic.PO_TYPE;
import joanakey.violations.ViolationsWrapper;

/**
 *
 * @author holger,joachim
 */
public class AsyncAutoRunner implements Runnable {

    public static AtomicBoolean keepRunning = new AtomicBoolean();
    private static ViolationsWrapper violationsWrapper;
    private static JavaForKeYCreator javaForKeyCreator;
    private static SummaryEdgeAndMethodToCorresData edgeAndMethodToCorresData;
    private static CurrentActionLogger actionLogger;
    private static ListView<String> summaryEdges;
    private static boolean useSlicing;
    private static Thread autoDisprovingThread;
    private static DisproHandler listener;

    public static void startAutoDisproving(
            ViolationsWrapper violationsWrapper,
            JavaForKeYCreator javaForKeyCreator,
            SummaryEdgeAndMethodToCorresData edgeAndMethodToCorresData,
            CurrentActionLogger actionLogger,
            ListView<String> summaryEdges,
            DisproHandler handler,
            boolean useSlicing, boolean useMethodInlining, boolean useLoopUnwinding) {
    	listener = handler;
        AsyncAutoRunner.violationsWrapper = violationsWrapper;
        AsyncAutoRunner.javaForKeyCreator = javaForKeyCreator;
        AsyncAutoRunner.javaForKeyCreator.setMethodInlining(useMethodInlining);
        AsyncAutoRunner.javaForKeyCreator.setUseLoopUnwinding(useLoopUnwinding);
        AsyncAutoRunner.edgeAndMethodToCorresData = edgeAndMethodToCorresData;
        AsyncAutoRunner.actionLogger = actionLogger;
        AsyncAutoRunner.summaryEdges = summaryEdges;
        AsyncAutoRunner.useSlicing = useSlicing;
        keepRunning.set(true);
		autoDisprovingThread = new Thread(new AsyncAutoRunner());
		autoDisprovingThread.start();
//		return autoDisprovingThread;
    }

    public static void stop() {
        keepRunning.set(false);
        Platform.runLater(() -> {
            actionLogger.startProgress("Please wait while the last disproving process finishes.");
        });
        if (autoDisprovingThread == null) {
        	Platform.runLater(() -> {
        		actionLogger.endProgress();
        	});
        }
		try {
			autoDisprovingThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println("ERROR stop auto run mode");
		}
		Platform.runLater(() -> {
			actionLogger.endProgress();
		});
    }
    
    public static boolean checkRunning() {
    	return keepRunning.get();
    }

	@Override
	public void run() {
		try {
			long startTime = System.currentTimeMillis();
			while (keepRunning.get()) {

				SDGEdge nextSummaryEdge = violationsWrapper.nextSummaryEdge();

				// if null, then there are no more edges to check, we are done!
				if (nextSummaryEdge == null || violationsWrapper.allCheckedOrDisproved()) {
					break;
				}

				Platform.runLater(() -> {
					actionLogger.startProgress("Now generating files for " + nextSummaryEdge.toString() + ".");
					for (int i = 0; i < summaryEdges.getItems().size(); ++i) {
						if (summaryEdges.getItems().get(i).startsWith(nextSummaryEdge.toString())) {
							summaryEdges.getSelectionModel().select(i);
							break;
						}
					}
				});
				Collection<SDGNodeTuple> allFormalPairs = violationsWrapper.getSdg()
						.getAllFormalPairs(nextSummaryEdge.getSource(), nextSummaryEdge.getTarget());
				SDGNodeTuple firstFormalPair = allFormalPairs.iterator().next();
				StaticCGJavaMethod method = violationsWrapper.getMethodCorresToSummaryEdge(nextSummaryEdge);
				String contract = edgeAndMethodToCorresData.getContractFor(firstFormalPair);
				if (!keepRunning.get()) {
					break;
				}
				HashMap<StaticCGJavaMethod, String> methodToMostGeneralContract = edgeAndMethodToCorresData
						.getMethodToMostGeneralContract();
				Platform.runLater(() -> {
					actionLogger.startProgress("Now trying to disprove " + nextSummaryEdge.toString() + ".");
				});
				if (!keepRunning.get()) {
					break;
				}
				try {
					// String pathToJave =
					ArrayList<ViolationChop> chops = violationsWrapper.getSummaryEdgesAndContainingChops()
							.get(nextSummaryEdge);
					if (chops != null && !chops.isEmpty()) {
						javaForKeyCreator.generateJavaForFormalTupleCalledFromGui(contract, method,
								edgeAndMethodToCorresData.getEdgeToLoopInvariantTemplate().get(nextSummaryEdge),
								edgeAndMethodToCorresData.getEdgeToLoopInvariant().get(nextSummaryEdge),
								methodToMostGeneralContract, chops.get(0), useSlicing);
					}
					if (!keepRunning.get()) {
						break;
					}
					boolean worked = AutomationHelper.runKeY(ViolationsDisproverSemantic.PO_PATH,
							PO_TYPE.INFORMATION_FLOW);
					if (!keepRunning.get()) {
						break;
					}
					if (worked) {
						worked = AutomationHelper.runKeY(ViolationsDisproverSemantic.PO_PATH, PO_TYPE.FUNCTIONAL);
					}
					if (!keepRunning.get()) {
						break;
					}
					if (worked) {
						Platform.runLater(() -> {
							actionLogger.startProgress("Succeeded disproving " + nextSummaryEdge.toString()
									+ ", now removing from SDG ...");
						});
						violationsWrapper.removeEdge(nextSummaryEdge);
					} else {
						Platform.runLater(() -> {
							actionLogger.startProgress("Failed to disprove " + nextSummaryEdge.toString() + ".");
						});
						violationsWrapper.checkedEdge(nextSummaryEdge);
					}
				} catch (IOException ex) {
					Logger.getLogger(AsyncAutoRunner.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			long endTime = System.currentTimeMillis();
			NumberFormat formatter = new DecimalFormat("#0.000");
			Platform.runLater(() -> {
				if (!violationsWrapper.allDisproved()) {
					violationsWrapper.resetCheckedEdges(); //TODO: correct to reset checked edges ? 
					actionLogger.endProgressWithMessage("Run Auto finished attempt in "
							+ formatter.format((endTime - startTime) / 1000d) + " seconds");
				} else {
					actionLogger.endProgressWithMessage("All violations disproven in "
							+ formatter.format((endTime - startTime) / 1000d) + " seconds");
				}
			});
		} finally {
			listener.notifyAutoRunCompletion();

		}
    }
}
