package joanakey.slicing;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.pruned.ApplicationLoaderPolicy;
import com.ibm.wala.util.NullProgressMonitor;

import edu.kit.joana.api.sdg.SDGBuildPreparation;
import edu.kit.joana.api.sdg.SDGConfig;
import edu.kit.joana.api.sdg.SDGProgram;
import edu.kit.joana.ifc.sdg.core.SecurityNode;
import edu.kit.joana.ifc.sdg.core.violations.ClassifiedViolation;
import edu.kit.joana.ifc.sdg.core.violations.IViolation;
import edu.kit.joana.ifc.sdg.core.violations.paths.ViolationPath;
import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import edu.kit.joana.ifc.sdg.mhpoptimization.MHPType;
import edu.kit.joana.util.Stubs;
import edu.kit.joana.wala.core.SDGBuilder;
import edu.kit.joana.wala.core.SDGBuilder.CGResult;
import edu.kit.joana.wala.core.SDGBuilder.SDGBuilderConfig;
import joanakey.CombinedApproach;
import joanakey.JoanaAndKeYCheckData;
import joanakey.SDGAnalysis;
import joanakey.StateSaverCGConsumerBuilder;
import joanakey.customlistener.IfThenElseListener;
import joanakey.slicing.BranchAnalyzer.IF;

public class Main {

	public static void main(String[] args) {
		try {
			
			
			File joakFile = new File("testdata/loop2.joak");
//			File joakFile = new File("/home/mihai/workspace/keyjoana/testdata/loop2.joak");
//			File joakFile = new File("/home/joachim/JoanaKeYBeispiele/SecureExamples/IFLoop2/program/Joak/test.joak");
			System.out.println("Starting Joana...");
            JoanaAndKeYCheckData data = CombinedApproach.parseInputFile(joakFile);
            
            
            data.addAnnotations();            
            //IFCAnalysis ana = data.getAnalysis();
            SDGAnalysis ana = (SDGAnalysis) data.getAnalysis();
            
            Collection<? extends IViolation<SecurityNode>> violations = ana.doIFC();
            System.out.println("Joana found: "+violations.size()+" violations.");
            int i = 1;
            for(IViolation<SecurityNode> violation : violations){
            	System.out.println("Creating slice for violation "+i+".");
            	
            	
            	ViolationPath violationPath = getViolationPath(violation);
            	LinkedList<SecurityNode> violationPathList = violationPath.getPathList();
            	
            	SDGNode high = violationPathList.get(0);
                SDGNode low = violationPathList.get(1);            	
                SDG sdg = ana.getProgram().getSDG();
                CallGraph cg = data.getStateSaver().getCallGraph(); 

                StateSaverCGConsumerBuilder stateSaverBuilder = new StateSaverCGConsumerBuilder();
                SDGConfig config = new SDGConfig(data.getPathToJar(), data.getEntryMethod().toString(), Stubs.JRE_15); //changed from JRE_14 to JRE_15
                config.setComputeInterferences(true);
//                config.setSkipSDGProgramPart(true);
                config.setMhpType(MHPType.PRECISE);
                config.setPointsToPrecision(SDGBuilder.PointsToPrecision.INSTANCE_BASED);
                config.setExceptionAnalysis(SDGBuilder.ExceptionAnalysis.INTERPROC);
                config.setFieldPropagation(SDGBuilder.FieldPropagation.OBJ_GRAPH_NO_MERGE_AT_ALL);
                // save intermediate results of SDG generation points to call graph
                //        config.setCGConsumer(null);
                // Schneidet beim SDG application edges raus, so besser sichtbar mit dem graphviewer
                config.setPruningPolicy(ApplicationLoaderPolicy.INSTANCE);
                SDGBuilderConfig conf = SDGBuildPreparation.prepareBuild(System.out, SDGProgram.makeBuildPreparationConfig(config), new NullProgressMonitor()).snd;
                conf.abortAfterCG = true;
//                #GraphWriter<CallGraph>;
                SDGBuilder builder = SDGBuilder.create(conf);
//                StateSaver sv = stateSaverBuilder.buildStateSaver(sdg);
                CGResult result = builder.buildCallgraph(new NullProgressMonitor());
                cg = result.cg;
                
                cg = data.getStateSaver().getCallGraph(); 

            	ProgramSimplifier simplifer = new ProgramSimplifier(high, low, sdg);
            	//Set<SDGNode> relevantNodes = simplifer.getBackwardsSlice();
//            	Set<SDGNode> relevantNodes = simplifer.getChopNodes();
            	Set<SDGNode> relevantNodes = simplifer.getRelevantNodes();
            	SourceCodeSlicer slicer = new SourceCodeSlicer(sdg, cg, data.getPathToJavaFile());
            	Map<String,Set<Integer>> slice = slicer.computeSourceCodeSlice(relevantNodes);
            	
            	File target = new File(data.getPathToJavaFile());
            	
            	String targetPath = target.getParentFile().getAbsolutePath() + File.separator + "slice"+i + File.separator+ "src";
            	System.out.println("Writing slice to: "+targetPath);
            	slicer.writeSlice(slice, targetPath);
            	//slicer.printSlice(slice);
            	//System.out.println("Slice based on chop: ");
            	//slicer.printSlice(slicer.computeSourceCodeSlice(simplifer.getChopNodes()));
            	
            	BranchAnalyzer ba = new BranchAnalyzer(cg, sdg, simplifer.getChopNodes());
            	Map<String, Map<Integer, IF>> predResults = ba.getAnalysisResults();
            	System.out.println("Adding assertions: "+predResults);
            	IfThenElseListener.insertAssertionsToFiles(targetPath, predResults);
            	
            	System.out.println("done.");
            	i++;
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }

	}
	
	private static ViolationPath getViolationPath(IViolation<SecurityNode> v) {
        return ((ClassifiedViolation) v).getChops().iterator().next()
                .getViolationPathes().getPathesList().get(0);
    }
	
	
	
	
	

}
