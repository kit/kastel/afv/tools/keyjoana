package joanakey.slicing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.ibm.wala.cfg.Util;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.ISSABasicBlock;
import com.ibm.wala.ssa.SSACFG;
import com.ibm.wala.ssa.SSACFG.BasicBlock;
import com.ibm.wala.util.graph.dominators.Dominators;

import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGEdge;
import edu.kit.joana.ifc.sdg.graph.SDGNode;

/**
 * This class analyzes the if-nodes present in an
 * SDG w.r.t. a chop reported by Joana and determines
 * based on the Wala CFG whether the (potential) illegal information
 * flow described by the chop can only occur along the true branch or
 * only along the false branch or both.
 * @author mihai, joachim
 */

public class BranchAnalyzer {
	/**
	 * This flag tells us if the if conditions are inverted in Joana's IR. This 
	 * seems to always be the case, although for some Java complier it might be different...
	 */
	private static final boolean inverted  = true;
	
	/**
	 * This enum represents the result of the analysis
	 * for an if-node (i.e. a node representing an if-statement) in the SDG: 
	 * 
	 * TRUE: the illegal flow can only occur if the true branch of the if-statement is taken
	 * FALSE: the illegal flow can only occur if the false branch of the if-statement is taken
	 * BOTH: the illegal flow can occur for both branches of the if-statement
	 * NONE: this means that an illegal flow does not occur along the if-statement 
	 */
	public enum IF {TRUE, FALSE, BOTH, NONE};
	/**
	 * The call graph for the program.
	 */
	private CallGraph cg;
	/**
	 * The SDG of the program.
	 */
	private SDG sdg;
	/**
	 * The set of sdg nodes that are in the chop computed by Joana.
	 */
	private Set<SDGNode> chop;
	
	private Map<String,IMethod> methods;
	
	/**
	 * The set of control dependency edge types that we consider in the analysis.
	 */
	private static final Set<SDGEdge.Kind> CDEdges;	
	static{
		CDEdges = new HashSet<>();
		//CDEdges.add(SDGEdge.Kind.CONTROL_DEP_CALL);
		CDEdges.add(SDGEdge.Kind.CONTROL_DEP_COND);
		//CDEdges.add(SDGEdge.Kind.CONTROL_DEP_EXPR);
		//CDEdges.add(SDGEdge.Kind.CONTROL_DEP_UNCOND);		
	}

	public BranchAnalyzer(CallGraph cg, SDG sdg, Set<SDGNode> chop) {
		super();
		this.cg = cg;
		this.sdg = sdg;
		this.chop = chop;	
		this.methods = getAllMethods();
	}
	
	public Map<String, Map<Integer, IF>> getAnalysisResults(){
		Map<String, Map<Integer, IF>> result =  new HashMap<>();
		System.out.println("Analyze Predicates");
		Map<SDGNode, IF> sdgResults = analyzePredicates();
		
		for(Entry<SDGNode, IF> entry : sdgResults.entrySet()) {
			System.out.println("Branch in analyze Result :" + entry.getKey().getLabel() + ":"+ entry.getKey().getId());
			SDGNode node = entry.getKey();
			
			IF val = entry.getValue();			
			String file = node.getSource();			
			int lineNumber = computeLineNumber(node);
			
			if(!result.containsKey(file)) {
				Map<Integer, IF> mapForFile = new HashMap<>();
				result.put(file, mapForFile);
			}
			
			Map<Integer, IF> mappedBranches = result.get(file);
			
			mappedBranches.put(lineNumber, val);
			
			
		}
		
		return result;
		
	}
	
	/**
     * For a given node returns the source code line number 
     * of the statement represented by that node.
     */
	private int computeLineNumber(SDGNode node){
		int bcIndex = node.getBytecodeIndex();
		if(bcIndex>=0){
			
			IMethod method = methods.get(node.getBytecodeMethod());
			if(method != null){
				return method.getLineNumber(bcIndex); 
			}
		}
		return -1;
	}
	
	private Map<String,IMethod> getAllMethods(){
		Map<String, IMethod> result = new HashMap<>();
		IClassHierarchy classHierarchy = cg.getClassHierarchy();
		for(IClass cl : classHierarchy){
			/*
			 * Primordial stuff is library stuff so we ignore it.
			 */
			if(cl.getClassLoader().getReference().getName().toString().equals("Primordial")){
				continue;
			}
			for(IMethod method : cl.getAllMethods()){
				if(method.getDeclaringClass().getClassLoader()
				        .getReference().getName().toString().equals("Primordial")){
					continue;
				}
				result.put(method.getSignature(), method);
			}
		}

		return result;
	}
	
	private IF invert(IF in) {
		
		if(in.equals(IF.FALSE)) {
			return IF.TRUE;
		}
		else if(in.equals(IF.TRUE)) {
			return IF.FALSE;
		}
		else {
			return in;
		}
		
		
	}
	
	/**
	 * This method computes for the if-nodes in the
	 * chop and for the if-nodes on which other (byte code) nodes
	 * in the chop are directly or indirectly control dependent
	 * whether the illegal information flow described by the chop
	 * occurs on the true branch, false branch, both, or neither.
	 * 
	 * The result is a map with SDG nodes as key and analysis results as values.
	 *
	 */
	private Map<SDGNode, IF> analyzePredicates(){
		Map<SDGNode, IF> result = new HashMap<>();
		//get the if-nodes in the chop
		Set<SDGNode> chopPredicates = getPredicates(chop);
		//get the nodes in the chop which have a corresponding byte code instruction
		Set<SDGNode> chopByteCodeNodes = getByteCodeNodes(chop);
		//analyze the predicates in the chop
		for(SDGNode pred : chopPredicates){
			IF predResult = checkPredicate(pred, chopByteCodeNodes);
			//TODO always invert ?
			if (inverted) {
				predResult = invert(predResult);
			}
			result.put(pred, predResult);
		}
		//we store here the nodes analyzed in the previous iteration, 
		//but at the beginning the bc-nodes in the chop 
		Set<SDGNode> former = chopByteCodeNodes;
		//we store here the if-nodes on which the nodes in the previous iteration are 
		//control dependent
		Set<SDGNode> preds = getPredicatePredecessors(chopByteCodeNodes);
		do{
			//we analyze the newly discovered predicates
			for(SDGNode pred : preds){
				IF predResult = checkPredicate(pred, former);
				if(inverted) {
					predResult = invert(predResult);
				}
				
				result.put(pred, predResult);								
			}
			
			former = preds;
			//in the next iteration we analyze the if-nodes on which the current if-nodes are control dependent
			preds = getPredicatePredecessors(preds);
			
		}while(!preds.isEmpty());
		
		
		return result;
		
		
	}

	
	/**
	 * This method analyzes an if-node w.r.t. a set of nodes in the SDG.
	 * The method checks whether there are any true- or false-successors 
	 * of the given if-node in the given set and returns:
	 * 
	 *  TRUE, if there are only true-successors in the given set
	 *  FALSE, if there are only false-successors in the given set
	 *  BOTH, if there are both true and false-successors in the given set
	 *  NONE, if there are neither true nor false-successors in the given set.
	 *  
	 *  This method only considers those nodes in the set for which there actually 
	 *  exists a corresponding byte code instruction.
	 */
	private IF checkPredicate(SDGNode pred, Set<SDGNode> nodes){
	    //get the control dependent successors of the if-node in the SDG 
		Set<SDGNode> cdSucc = getCDSuccessors(pred);
		//we only look at those successors that are in the given set
		cdSucc.retainAll(nodes);
		
		/*
		 * The if-node has no successors in the set.
		 * This should never be the case...
		 */
		if(cdSucc.isEmpty()){
			return IF.NONE;
		}
		//this flag is set to true if we find a true successor
		boolean foundTrueSucc = false;	
		//this flag is set to true if we find a false successor 
		boolean foundFalseSucc = false;
		//get the cfg for the method containing the instruction corresponding to the if-node
		SSACFG cfg = getCFG(pred);
		//get the basic block containing the if-statement coresponding to the if.node
		BasicBlock basicblock = cfg.getBlockForInstruction(sdg.getInstructionIndex(pred));
		//get the successor basic block corresponding to the true branch
		ISSABasicBlock trueBlock = Util.getTakenSuccessor(cfg, basicblock);
		//get the successor basic block corresponding to the false branch
		ISSABasicBlock falseBlock = Util.getNotTakenSuccessor(cfg, basicblock);
		//construct the set of successor basic blocks along the true branch  
		Collection<ISSABasicBlock> trueSuccessors = cfg.getNormalSuccessors(trueBlock);
		trueSuccessors.add(trueBlock);
		//construct the set of successor basic blocks along the false branch
		Collection<ISSABasicBlock> falseSuccessors = cfg.getNormalSuccessors(falseBlock);
		falseSuccessors.add(falseBlock);
		
		//for each successor in the given set
		for(SDGNode succ : cdSucc){
			/*
			 * If we found both a true and a false successor, then stop searching
			 */
			if(foundTrueSucc && foundFalseSucc){
				break;
			}
			//skip non byte code successors
            int succInstrIndex = sdg.getInstructionIndex(succ);			
			if(succInstrIndex < 0){
				continue;
			}
			//get basic block for successor
			SSACFG succCFG = getCFG(succ);
			BasicBlock succBlock = succCFG.getBlockForInstruction(succInstrIndex);
			List<ISSABasicBlock> dominators = calculateDominators(succBlock, cfg);

			if(inFalseBranch(dominators, falseBlock)) {
				//TODO: remove if not needed
			}
			
			//set flag if the successor is among the true successors
			if(!foundTrueSucc){
//				foundTrueSucc = trueSuccessors.contains(succBlock);
				foundTrueSucc = inTrueBranch(dominators, trueBlock);
			}
			//set flag if successor is among the false successors
			if(!foundFalseSucc){
//				foundFalseSucc  =falseSuccessors.contains(succBlock);
				foundFalseSucc = inFalseBranch(dominators, falseBlock);
			}
			
			
		}
		
		//now we return the appropriate value, according to the flags.
		
		if(foundTrueSucc && !foundFalseSucc){
			return IF.TRUE;
		}
		
		if(!foundTrueSucc && foundFalseSucc){
			return IF.FALSE;
		}	
		
		if(!foundTrueSucc && !foundFalseSucc){
			return IF.NONE;
		}
		
		return IF.BOTH;		
	}	
	
	/**
	 * calculate the dominator blocks in a {@link SSACFG} for the given {@link BasicBlock}.
	 * @param block the {@link ISSABasicBlock} you want the dominators for.
	 * @return a list of the calculated dominators.
	 */
	private List<ISSABasicBlock> calculateDominators(BasicBlock block, SSACFG cfg) {
		List<ISSABasicBlock> result = new ArrayList<ISSABasicBlock>();
		Dominators<ISSABasicBlock> d = com.ibm.wala.util.graph.dominators.Dominators.make(cfg, cfg.entry());
		Iterator<ISSABasicBlock> dominatorIterator = d.dominators(block);
		while (dominatorIterator.hasNext()) {
			ISSABasicBlock b = dominatorIterator.next();
			result.add(b);
		}
		return result;
		
	}
	
	private boolean inFalseBranch(List<ISSABasicBlock> dominators, ISSABasicBlock firstFalseBlock) {
		//return false if first true block of predicate block is contained in dominators
		if (dominators.contains(firstFalseBlock)) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean inTrueBranch(List<ISSABasicBlock> dominators, ISSABasicBlock firstTrueBlock) {
		//return true if first true block of predicate block is contained in dominators
		if (dominators.contains(firstTrueBlock)) {
			return true; 
		} else {
			return false;
		}
	}
	
    /**
     * Returns the CFG of the method containing the instruction
     * corresponding to the given sdg node.
     */
	private SSACFG getCFG(SDGNode node) {
		CGNode cgNode = getCGNode(node);		
		IR ir = cgNode.getIR();		
		SSACFG cfg = ir.getControlFlowGraph();
		return cfg;
	}
	/**
	 * Get the call graph node corresponding to the given SDG node.
	 */
	private CGNode getCGNode(SDGNode node){
		SDGNode entry = sdg.getEntry(node);
		int cgNodeNumber = sdg.getCGNodeId(entry);
		CGNode cgNode = cg.getNode(cgNodeNumber);
		return cgNode;
	}
	
	private Set<SDGNode> getCDSuccessors(SDGNode node){
		Set<SDGNode> result = new HashSet<>();		
		for(SDGEdge edge : sdg.outgoingEdgesOf(node)){			
			if(CDEdges.contains(edge.getKind())){				
				result.add(edge.getTarget());				
			}			
		}		
		return result;		
	}
	
	private Set<SDGNode> getPredicatePredecessors(SDGNode node){
		Set<SDGNode> result = new HashSet<>();		
		for(SDGEdge edge : sdg.incomingEdgesOf(node)){			
			if(CDEdges.contains(edge.getKind())){				
				SDGNode pred = edge.getSource();
				if(pred.getKind().equals(SDGNode.Kind.PREDICATE)){
					result.add(pred);
				}
			}			
		}		
		return result;	
	}
	
	private Set<SDGNode> getByteCodeNodes(Collection<? extends SDGNode> nodes){
		Set<SDGNode> result = new HashSet<>();
		
		for (SDGNode sdgNode : nodes) {
			if(sdgNode.getBytecodeIndex() > 0){
				result.add(sdgNode);
			}
		}
		
		return result;
	}
	
	private Set<SDGNode> getPredicatePredecessors(Collection<? extends SDGNode> nodes){
		Set<SDGNode> result = new HashSet<>();		
		for(SDGNode node : nodes){			
			for(SDGNode pred : getPredicatePredecessors(node)){
				if(!nodes.contains(pred)){					
					result.add(pred);					
				}
			}			
		}		
		return result;		
	}
	
	private Set<SDGNode> getPredicates(Collection<? extends SDGNode> nodes){
		Set<SDGNode> result = new HashSet<>();		
		for(SDGNode node : nodes){
			if(node.getKind().equals(SDGNode.Kind.PREDICATE)){
				result.add(node);
			}
		}		
		return result;	
	}
	
	/**
	 * generate the contact and the assume method. This string should be appended to the sliced java file.
	 * @return String with assume contract and assume method
	 */
	public String getAssumeMethod() {
		String contract = "/*@\n"
				+ "@ requires true;\n"
				+ "@ ensures b;\n"
				+ "@*/\n";
		String methodHead = "private void assume(boolean b)";
		String methodBody = "return b;";
		
		return contract
				+ methodHead + "{ \n"
				+ methodBody + "\n }";
	}
	
	
}
