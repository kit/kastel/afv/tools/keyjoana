package joanakey.slicing;

import java.util.HashSet;
import java.util.Set;

import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGEdge;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import edu.kit.joana.ifc.sdg.graph.SDGNode.Kind;
import edu.kit.joana.ifc.sdg.graph.chopper.Chopper;
import edu.kit.joana.ifc.sdg.graph.chopper.RepsRosayChopper;
import edu.kit.joana.ifc.sdg.graph.slicer.IntraproceduralSlicerBackward;
import edu.kit.joana.ifc.sdg.graph.slicer.Slicer;
import edu.kit.joana.ifc.sdg.graph.slicer.conc.I2PBackward;
/**
 * This class generates a simplified Program from an SDG and a Violation. 
 */
public class ProgramSimplifier {
	/**
	 * The high source.
	 */
	private SDGNode high;
	/**
	 * The low sink.
	 */
	private SDGNode low;
	/**
	 * The SDG of the program.
	 */
	private SDG sdg;	
	
	public ProgramSimplifier(SDGNode high, SDGNode low, SDG sdg) {
		super();
		this.high = high;
		this.low = low;
		this.sdg = sdg;
	}

	/**
	 * Returns a set of nodes that are in the chop determined 
	 * by high and low.
	 */
	public Set<SDGNode> getChopNodes(){
		Set<SDGNode> result = new HashSet<>();
		Chopper chopper = new RepsRosayChopper(sdg);
		result.addAll(chopper.chop(high, low));
		return result;
	}
	
	/**
	 * Returns the nodes that are relevant for the simplified
	 * program.
	 */
	public Set<SDGNode> getRelevantNodes(){
		Set<SDGNode> result = new HashSet<>();
		/*
		 * 1. Get the nodes that are part of the chop.
		 */
		Set<SDGNode> chopNodes = getChopNodes();
		//if the chop is empty we are finished.
		if(chopNodes.isEmpty()){
			return result;
		}
		result.addAll(chopNodes);
		/*
		 * 2. Add method call nodes. 
		 */
		Set<SDGNode> calls = getRelevantCalls(chopNodes);
		result.addAll(calls);
		/*
		 * 3. Find predicates on which the nodes in the 
		 *    relevant nodes are control dependent.
		 */
		Set<SDGNode> predicates = getRelevantPreds(result, sdg);
		result.addAll(predicates);
        /*
         * 4. Add backwards slices for the relevant predicates.
         */
		Set<SDGNode> predicatesSlice = computeSliceForPredicates(result, sdg);
		result.addAll(predicatesSlice);		
		return result;
	}
	
	/**
	 * Returns the set of nodes in the backwards slice of the low sink.
	 */
	public Set<SDGNode> getBackwardsSlice(){
		Set<SDGNode> result = new HashSet<>();
		//TODO: is this the right type of slicer?
//		Slicer bwSlicer = new ContextSlicerBackward(sdg,false);
        I2PBackward bwSlicer = new I2PBackward(sdg);
		result.addAll(bwSlicer.slice(low));
		return result;
	}	
	
	/**
	 * Given a set of nodes, returns the 
	 */
	private Set<SDGNode> getRelevantCalls(Set<SDGNode> nodeSet){
		Set<SDGNode> result = new HashSet<>();
		Set<SDGNode> ignoredActualIns  =new HashSet<>();
		for(SDGNode node : nodeSet){
			if(node.getKind().equals(Kind.ACTUAL_IN)&& !ignoredActualIns.contains(node)){
				for(SDGNode call : getMethodCallsSuccessors(node)){
					result.add(call);
					ignoredActualIns.addAll(getActualInPredecessors(call));
				}
			}
		}
		return result;
	}
	/**
	 * Returns the predecessor nodes of kind call for the given node.
	 */
	private Set<SDGNode> getMethodCallsSuccessors(SDGNode node){
		Set<SDGNode> result = new HashSet<>();
		for(SDGEdge edge : sdg.outgoingEdgesOf(node)){
			SDGNode succ = edge.getTarget();
			if(succ.getKind().equals(Kind.CALL)){
				result.add(succ);
			}
		}
		return result;
	}
	/**
	 * Returns the predecessor nodes of kind actual in for the given node.
	 */
	private Set<SDGNode> getActualInPredecessors(SDGNode node){
		Set<SDGNode> result = new HashSet<>();
		for(SDGEdge edge : sdg.incomingEdgesOf(node)){
			SDGNode pred = edge.getSource();
			if(pred.getKind().equals(Kind.ACTUAL_IN)){
				result.add(pred);
			}
		}
		return result;
	}
	
	/**
	 * Given a set of nodes and an SDG, this method will return the subset of 
	 * nodes from the SDG such that 
	 *  1. All nodes in the result set are predicates
	 *  2. For every node P in the result set there is at least one
	 *     node N in the input set such that N is control dependent on P 
	 */
	private Set<SDGNode> getRelevantPreds(Set<SDGNode> chopNodes, SDG sdg){
		Set<SDGNode> result = new HashSet<>();
		for(SDGNode chopNode : chopNodes){
			if(chopNode.getBytecodeIndex() < 0){
				continue;
			}
			for(SDGEdge edge : sdg.incomingEdgesOf(chopNode)){
				if(edge.getKind().equals(SDGEdge.Kind.CONTROL_DEP_COND)){
					SDGNode pred = edge.getSource();
					if(!chopNodes.contains(pred) && pred.getKind().equals(Kind.PREDICATE)){
						result.add(pred);
					}				
				}
			}
		}
		return result;
	}
	
	/**
	 * Given a set of nodes and an SDG, this method will return the nodes that are in the
	 * backwards slices of all predicate nodes in the input set. 
	 */
	private Set<SDGNode> computeSliceForPredicates(Set<SDGNode> chopNodes, SDG sdg){
		Set<SDGNode> result = new HashSet<>();
		Slicer bwSlicer = new IntraproceduralSlicerBackward(sdg);
		Set<SDGNode> predicates = filterNodesOfKind(chopNodes, Kind.PREDICATE);
		result.addAll(bwSlicer.slice(predicates));
		return result;
	}
	
    /**
     *  Given a set of nodes and a kind, the method will return the subset of 
     *  the nodes which are of the given kind.
     */
	private Set<SDGNode> filterNodesOfKind(Set<SDGNode> nodes, Kind kind){
		Set<SDGNode> result = new HashSet<>();
		for(SDGNode node : nodes){
			if(node.getKind().equals(kind)){
				result.add(node);
			}
		}
		return result;
	}
	
	
	
	

}
