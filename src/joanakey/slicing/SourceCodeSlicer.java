package joanakey.slicing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.io.Files;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.cha.IClassHierarchy;

import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGNode;

public class SourceCodeSlicer {
	
	private SDG sdg;
	
	private CallGraph cg;	
	
	private String pathToJavaSource;
	
	private Map<String,IMethod> methods;
	
	public SourceCodeSlicer(SDG sdg, CallGraph cg, String pathToJavaSource) {
		super();
		this.sdg = sdg;
		this.cg = cg;
		this.pathToJavaSource  = pathToJavaSource;
		this.methods = getAllMethods();
	}
	
	/**
	 * For a slice determined by the given set of nodes this method
	 * will return a map that will contain for each Java Source File
	 * of the analyzed program the lined that can be sliced away.
	 * 
	 * The keys of the returned map are the Java Source File names.
	 * The values of the returned map are the line number that are to be removed.
	 */
	public Map<String, Set<Integer>> computeSourceCodeSlice(Set<SDGNode> sliceNodes){		
		Map<String, Set<Integer>> result = new HashMap<>();
		//compute the line numbers for each file that are in the slice
		Map<String, Set<Integer>> sliceLines = computeSourceLineNumbers(sliceNodes);
		//compute the line numbers for each file that are in the sdg
		Map<String, Set<Integer>> sdgLines = computeSourceLineNumbers(sdg.vertexSet());
		
		for(String file : sliceLines.keySet()){
			
			Set<Integer> sdgLinesForFile = sdgLines.get(file);
			Set<Integer> sliceLinesForFile = sliceLines.get(file); 
			Set<Integer> linesToBeSliced = new HashSet<>();
			
			for(int lineNumber : sdgLinesForFile){
				/*
				 * A line is to be removed if it is in the SDG but not in the slice. 
				 */
				if(!sliceLinesForFile.contains(lineNumber)){
					linesToBeSliced.add(lineNumber);
				}
				
			}
			
			result.put(file, linesToBeSliced);
			
		}
		
		return result;
	}
	
	
	/**
	 * This method returns true for nodes that we do not want to 
	 * slice away (because otherwise the slice would not be compilable)
	 */
	private boolean isIgnoredNode(SDGNode node) {
		
		if(node.getLabel().startsWith("goto")) {
			return true;
		}
		
		if(node.getLabel().contains("return")) {
			return true;
		}
		
		if(node.getLabel().contains("<init>")) {
			return true;
		}
		
		if(node.getLabel().contains("catch")) {
			return true;
		}
		
		return false;
	}
	
	
    /**
     * For a given set of nodes, this method will return the Java Source file names 
     * and the line numbers that are represented by the given nodes.
     * 
     * The keys of the map are the names of the Java Source files.
     * The values are the line numbers of the statements represented by the nodes.
     */
	private  Map<String,Set<Integer>> computeSourceLineNumbers(Set<SDGNode> nodes){
		Map<String,Set<Integer>> result = new HashMap<>();		
		for(SDGNode node : nodes){			
			
			if(isIgnoredNode(node)){
				continue;
			}
			
			int lineNumber = computeLineNumber(node);
			if(lineNumber >=0){
				String source = node.getSource();				
				if(!result.containsKey(source)){
					result.put(source, new HashSet<Integer>());
				}
				Set<Integer> lineNumbers = result.get(source);
				lineNumbers.add(lineNumber);
			}
		}
		return result;
	}
    /**
     * For a given node returns the source code line number 
     * of the statement represented by that node.
     */
	private int computeLineNumber(SDGNode node){
		int bcIndex = node.getBytecodeIndex();
		if(bcIndex>=0){
			IMethod method = methods.get(node.getBytecodeMethod());
			if(method != null){
				return method.getLineNumber(bcIndex); 
			}
		}
		return -1;
	}
	/**
	 * This method returns a map with method signatures
	 * as its key and IMethod objects as its values.
	 * IMethod objects are needed for computing the source 
	 * code line number from a given byte code index. 
	 */
	private Map<String,IMethod> getAllMethods(){
		Map<String, IMethod> result = new HashMap<>();
		IClassHierarchy classHierarchy = cg.getClassHierarchy();
		for(IClass cl : classHierarchy){
			/*
			 * Primordial stuff is library stuff so we ignore it.
			 */
			if(cl.getClassLoader().getReference().getName().toString().equals("Primordial")){
				continue;
			}
			for(IMethod method : cl.getAllMethods()){
				if(method.getDeclaringClass().getClassLoader()
				        .getReference().getName().toString().equals("Primordial")){
					continue;
				}
				result.put(method.getSignature(), method);
			}
		}

		return result;
	}
	
	private List<String> readLinesOfFile(String fileName) {
		List<String> originalLines = new LinkedList<>();
		try{
			File javaFile = new File(pathToJavaSource+File.separator+fileName);			
			BufferedReader br = new BufferedReader(new FileReader(javaFile));
			String origLine = null;				
			while((origLine = br.readLine())!=null){					
				originalLines.add(origLine);
			}
			br.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return originalLines;
	}
	
	
	
	private void deleteDir(File dir) {
		
		
		if(!dir.isDirectory()) {
			dir.delete();
		}
		else {
			for(File sub : dir.listFiles()) {
				deleteDir(sub);
			}
			dir.delete();
		}
		
		
	}
	
	private void copySourceFolder(File dirFrom, File dirTo) throws IOException {
		
		
		
		dirTo.mkdirs();
		
		for(File from : dirFrom.listFiles()) {
			
			String targetPath = dirTo.getAbsolutePath() + File.separator + from.getName();
			File to = new File(targetPath);
			
			if(from.isDirectory()) {				
				to.mkdirs();				
				copySourceFolder(from, to);				
			}
			else {
				Files.copy(from, to);
			}
			
			
		}
		
		
		
	}
	
	
	/**
	 * Write the slice to a File
	 * @param slice result you want to write to a file
	 * @param target path to the File
	 * @throws IOException
	 */
	public void writeSlice(Map<String, Set<Integer>> slice, String target) throws IOException {
		
		File sourceFolder = new File(pathToJavaSource);
		
		File targetFolder = new File(target);
		
		
		deleteDir(targetFolder);
		
		copySourceFolder(sourceFolder, targetFolder);
		
		//Files.copy(new File(pathToJavaSource), dir);
		
		for(String f : slice.keySet()) {
			
			File file = new File(target+File.separator+f);
			file.getParentFile().mkdirs();
			file.createNewFile();
			FileWriter writer = new FileWriter(file, false);
			
			List<String> originalLines = readLinesOfFile(f);
			Set<Integer> tobeRemoved = slice.get(f);
			
			for(int i = 0;i<originalLines.size();i++){
				int lineNumber = i + 1;
				String line = originalLines.get(i);
				if(tobeRemoved.contains(lineNumber)){
					writer.write("//sliced: "+line+"\n");
				}
				else{
					writer.write(line+"\n");
				}
				
			}
			
			writer.close();
			
			
			
		}		
	}
	
	/**
	 * print the slice result
	 * @param slice
	 */
	public void printSlice(Map<String, Set<Integer>> slice){
		
		for(String file : slice.keySet()){
			
			System.out.println("\nFile: "+file+"\n------------");
			List<String> originalLines = readLinesOfFile(file);
			Set<Integer> tobeRemoved = slice.get(file); 
			for(int i = 0;i<originalLines.size();i++){
				int lineNumber = i + 1;
				String line = originalLines.get(i);
				if(tobeRemoved.contains(lineNumber)){
					System.out.println("//sliced: "+line);
				}
				else{
					System.out.println(line);
				}
				
			}
			
			
			
		}
		
		
	}
	
	/**
	 * insert a given string into the line before the last bracket "}" of a java class file
	 * @param targetPath The path of Java fail, which contains a java class
	 * @param string The String you want to insert.
	 * @throws IOException
	 */
	public void addStringToGivenJavaFile(String targetPath, String string) throws IOException {
		File f = new File(targetPath);
		List<String> originalLines = new ArrayList<String>();
//			File javaFile = new File(f.getAbsolutePath());			
		BufferedReader br = new BufferedReader(new FileReader(f));
		String origLine = br.readLine();
		FileWriter writer = new FileWriter(f, false);
		while ((origLine != null)) {
			originalLines.add(origLine);
			origLine = br.readLine();
		}
		br.close();
		f.createNewFile();

		for (int i = originalLines.size() - 1; i >= 0; i--) {
			String line = originalLines.get(i);
			if (line.contains("}")) {
				originalLines.add(i, string);
				break;
			}
		}

		for (int i = 0; i < originalLines.size(); i++) {
			String line = originalLines.get(i);
			writer.write(line + "\n");

		}

//			for (int i = 0; i<originalLines.size(); i++) {
//				writer.append(originalLines.get(i));
//			}
		writer.close();
//		}

	}
}
