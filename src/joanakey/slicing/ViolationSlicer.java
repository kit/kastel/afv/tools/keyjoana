package joanakey.slicing;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import com.ibm.wala.ipa.callgraph.CallGraph;

import edu.kit.joana.ifc.sdg.core.SecurityNode;
import edu.kit.joana.ifc.sdg.core.violations.ClassifiedViolation;
import edu.kit.joana.ifc.sdg.core.violations.IViolation;
import edu.kit.joana.ifc.sdg.core.violations.paths.ViolationPath;
import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import joanakey.JoanaAndKeYCheckData;
import joanakey.customlistener.IfThenElseListener;
import joanakey.slicing.BranchAnalyzer.IF;

public class ViolationSlicer {
	
	private final static String ASSUME_METHOD = "\t/*@\n"
				+ "\t@ requires true;\n"
				+ "\t@ ensures b;\n"
				+ "\t@*/\n"
				+ "\tprivate void assume(boolean b) { \n"
				+ "\n\t}";
	
	public static void doSlicingChop(Collection<? extends IViolation<SecurityNode>> violations, SDG sdg, JoanaAndKeYCheckData checkData) {
		
	}

	/**
	 * 
	 * @param high Node of the violation you want to simplify 
	 * @param low Node of the violation you want to simplify
	 * @param sdg Graph of the java file
	 * @param pathToJavaFile The path to the current java file
	 * @param cg the Call graph to the corresponding sdg
	 */
	public static String simpifyProgram(SDGNode high, SDGNode low, SDG sdg, String pathToJavaFile, CallGraph cg) {
		ProgramSimplifier simplifer = new ProgramSimplifier(high, low, sdg);
//		Set<SDGNode> relevantNodes = simplifer.getRelevantNodes();
		Set<SDGNode> relevantNodes = simplifer.getBackwardsSlice();

		SourceCodeSlicer slicer = new SourceCodeSlicer(sdg, cg, pathToJavaFile);
		Map<String, Set<Integer>> slice = slicer.computeSourceCodeSlice(relevantNodes);
		File target = new File(pathToJavaFile);
		String targetPath = target.getParentFile().getAbsolutePath() + File.separator + "slices" + File.separator
				+ "slice" + "Source" + high + "Sink" + low + File.separator + "src";
		System.out.println("Writing slice to: " + targetPath);
		try {
			slicer.writeSlice(slice, targetPath);
		} catch (IOException e) {
			System.out.println("ERROR while write slice to file " + targetPath);
			e.printStackTrace();
		}
		// slicer.printSlice(slice);
		// System.out.println("Slice based on chop: ");
		// slicer.printSlice(slicer.computeSourceCodeSlice(simplifer.getChopNodes()));

		BranchAnalyzer ba = new BranchAnalyzer(cg, sdg, simplifer.getChopNodes());
		Map<String, Map<Integer, IF>> predResults = ba.getAnalysisResults();
		System.out.println("Adding assertions: " + predResults);
		try {
			IfThenElseListener.insertAssertionsToFiles(targetPath, predResults);
		} catch (IOException e) {
			System.out.println("ERROR while insert assertion to slice " + targetPath);
			e.printStackTrace();
		}
		
		//TODO: assume method for each file in targetPath ? 
		System.out.println("Adding assume method");
		try {
			for (String s : slice.keySet()) {
				slicer.addStringToGivenJavaFile(targetPath + File.separator + s, ASSUME_METHOD);
			}
		} catch (IOException e) {
			System.out.println("ERROR while adding assume method to " + targetPath);
			e.printStackTrace();
		}
		
		System.out.println("done.");
		return targetPath + "/";
	}
	
	
	public static String addAssumeMethodToCodeString(String code) {
		int lastIndexOfBracket = code.lastIndexOf("}");
		String newCode = new String();
		for (int i = 0; i < lastIndexOfBracket; i++) {
			newCode += code.charAt(i);
		}
		newCode += ASSUME_METHOD;
		newCode += "\n}";
		return newCode;
	}
	
	

	//TODO: finish
	//warning not finished yet
	@Deprecated
    public static void simpifyProgramForAllViolations(Collection<? extends IViolation<SecurityNode>> violations, SDG sdg, JoanaAndKeYCheckData checkData) {
    	int i = 1;
		for (IViolation<SecurityNode> violation : violations) {
			System.out.println("Creating slice for violation " + i + ".");

			ViolationPath violationPath = getViolationPath(violation);
			LinkedList<SecurityNode> violationPathList = violationPath.getPathList();

			SDGNode high = violationPathList.get(0);
			SDGNode low = violationPathList.get(1);
			CallGraph cg = checkData.getStateSaver().getCallGraph();

			ProgramSimplifier simplifer = new ProgramSimplifier(high, low, sdg);
			// Set<SDGNode> relevantNodes = simplifer.getBackwardsSlice();
//            	Set<SDGNode> relevantNodes = simplifer.getChopNodes();
			Set<SDGNode> relevantNodes = simplifer.getRelevantNodes();
			SourceCodeSlicer slicer = new SourceCodeSlicer(sdg, cg, checkData.getPathToJavaFile());
			Map<String, Set<Integer>> slice = slicer.computeSourceCodeSlice(relevantNodes);

			File target = new File(checkData.getPathToJavaFile());

			String targetPath = target.getParentFile().getAbsolutePath() + File.separator + "slices"
					+ File.separator + "slice"
					+ "Source" + high + "Sink" + low + File.separator
					+ "src";
			System.out.println("Writing slice to: " + targetPath);
			try {
				slicer.writeSlice(slice, targetPath);
			} catch (IOException e) {
				System.out.println("ERROR while write slice to file "+targetPath);
				e.printStackTrace();
			}
			// slicer.printSlice(slice);
			// System.out.println("Slice based on chop: ");
			// slicer.printSlice(slicer.computeSourceCodeSlice(simplifer.getChopNodes()));

			BranchAnalyzer ba = new BranchAnalyzer(cg, sdg, simplifer.getChopNodes());
			Map<String, Map<Integer, IF>> predResults = ba.getAnalysisResults();
			System.out.println("Adding assertions: " + predResults);
			try {
				IfThenElseListener.insertAssertionsToFiles(targetPath, predResults);
			} catch (IOException e) {
				System.out.println("ERROR while insert assertion to slice " + targetPath);
				e.printStackTrace();
			}

			System.out.println("done.");
			i++;
		}
	}    

	private static ViolationPath getViolationPath(IViolation<SecurityNode> v) {
        return ((ClassifiedViolation) v).getChops().iterator().next()
                .getViolationPathes().getPathesList().get(0);
    }	
}
