/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joanakey.javaforkeycreator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import org.apache.commons.io.FileUtils;

import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGEdge;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import edu.kit.joana.ifc.sdg.graph.SDGNodeTuple;
import edu.kit.joana.ifc.sdg.graph.slicer.conc.I2PBackward;
import edu.kit.joana.ifc.sdg.graph.slicer.conc.I2PForward;
import joanakey.AutomationHelper;
import joanakey.StateSaver;
import joanakey.customlistener.AddContractsAndLoopInvariantsListener;
import joanakey.customlistener.GetMethodBodyListener;
import joanakey.javaforkeycreator.javatokeypipeline.CopyKeYCompatibleListener;
import joanakey.slicing.ViolationSlicer;
import joanakey.staticCG.JCallGraph;
import joanakey.staticCG.javamodel.StaticCGJavaClass;
import joanakey.staticCG.javamodel.StaticCGJavaMethod;
import joanakey.violations.ViolationChop;
import joanakey.violations.ViolationsDisproverSemantic;

/**
 *
 * @author holger, joachim
 */
public class JavaForKeYCreator {

    private static final String RESULT = "\\result";
    private static final String NOTHING = "\\nothing";
    private static final String REQUIRES = "requires";
    private static final String SINK_SOURCE_EXCEPTION = "\\exception";
    private static final String NORMAL_BEHAVIOUR = "normal_behaviour";
    private static final String THIS = "this";
    private static final String PARAM = "<param>";
    private static final String EXCEPTION = "<exception>";
    private static final String FORMAL_IN ="FORMAL_IN";
    private static final String JOANA_ARRAY = "<[]>";
    private static final String ARRAY_TO_SEQ = "\\dl_array2seq";
    private static final String VOID = "void";

    static final String DETERMINES = "determines";
    static final String BY = "\\by";
    static final String ITSELF = "\\itself";

    private String pathToJavaSource;
    private JCallGraph callGraph;
    private SDG sdg;
    private StateSaver stateSaver;
    private JavaProjectCopyHandler javaProjectCopyHandler;
    private CopyKeYCompatibleListener keyCompatibleListener;
    private boolean useMethodInlining = false;
    private boolean useLoopUnwinding = false;

    private GetMethodBodyListener methodBodyListener = new GetMethodBodyListener();

    public JavaForKeYCreator(String pathToJavaSource, JCallGraph callGraph,
                             SDG sdg, StateSaver stateSaver) {
        this.pathToJavaSource = pathToJavaSource;
        this.callGraph = callGraph;
        this.sdg = sdg;
        this.stateSaver = stateSaver;
    }

    public static String getPathToJavaClassFile(String pathToSource,
                                                StaticCGJavaClass javaClass) {
        return pathToSource + JavaProjectCopyHandler.getRelPathForJavaClass(javaClass)
                + javaClass.getOnlyClassName() + AutomationHelper.DOT_JAVA;
    }
    

    public String generateJavaForFormalTupleCalledFromGui(
            String contract,
            StaticCGJavaMethod method,
            String loopInvariantTemplate,
            List<String> loopInvariants,
            Map<StaticCGJavaMethod, String> methodsToMostGeneralContract, ViolationChop chop, boolean useSlicedFiles) throws IOException {

        FileUtils.deleteDirectory(new File(ViolationsDisproverSemantic.PO_PATH));
        this.keyCompatibleListener = new CopyKeYCompatibleListener(callGraph.getPackageName());
        AddContractsAndLoopInvariantsListener addContractsAndLoopInvariantsListener =
                new AddContractsAndLoopInvariantsListener();
        Map<StaticCGJavaClass, Set<StaticCGJavaMethod>> allNecessaryClasses =
                callGraph.getAllNecessaryClasses(method);

        String slicedJavaFilePath = pathToJavaSource;
        if (useSlicedFiles) {
			slicedJavaFilePath = ViolationSlicer.simpifyProgram(chop.getViolationSource(), chop.getViolationSink(), sdg,
					pathToJavaSource, stateSaver.getCallGraph());
			javaProjectCopyHandler = new JavaProjectCopyHandler(slicedJavaFilePath, ViolationsDisproverSemantic.PO_PATH,
					keyCompatibleListener);

        } else {
			javaProjectCopyHandler = new JavaProjectCopyHandler(pathToJavaSource, ViolationsDisproverSemantic.PO_PATH,
					keyCompatibleListener);
        }
		for (StaticCGJavaClass c : allNecessaryClasses.keySet()) {
			Set<StaticCGJavaMethod> set = allNecessaryClasses.get(c);
//           
            File javaClassFile;
			String relPathForJavaClass = JavaProjectCopyHandler.getRelPathForJavaClass(c) + c.getOnlyClassName()
					+ AutomationHelper.DOT_JAVA;

            System.out.println("Load sliced Files : " + useSlicedFiles);
            if(useSlicedFiles) {
            	javaClassFile = new File(slicedJavaFilePath + relPathForJavaClass);
                             
            } else {
				javaClassFile = new File(pathToJavaSource +	relPathForJavaClass);
            }
                   
            if (!javaClassFile.exists()) {
                // It is a library class since it does not exist in the project,
                // but since the violations wrapper sorts out all methods which
                // are not key compatible,
                // it should be a KeY-compatible library method.
                continue;
            }
            String rawFileContents = new String(Files.readAllBytes(javaClassFile.toPath()));
            
            String keyCompatibleContents = keyCompatibleListener.transformCode(
                    rawFileContents, allNecessaryClasses.get(c));
            String codeWithContrAndInv
                    = addContractsAndLoopInvariantsListener.addContractsAndLoopInvariants(
                            method, contract, loopInvariants, loopInvariantTemplate, set,
                            methodsToMostGeneralContract, keyCompatibleContents);
            if (useSlicedFiles) {
            	codeWithContrAndInv = ViolationSlicer.addAssumeMethodToCodeString(codeWithContrAndInv);
            }
            javaProjectCopyHandler.copyClassContentsIntoTestDir(codeWithContrAndInv, c);
            
        }

        KeYFileCreator.createKeYFileIF(method, ViolationsDisproverSemantic.PO_PATH, useLoopUnwinding, useMethodInlining);
        boolean normalBehaviour = contract.contains(NORMAL_BEHAVIOUR);
        KeYFileCreator.createKeYFileFunctional(method, ViolationsDisproverSemantic.PO_PATH,normalBehaviour,useLoopUnwinding, useMethodInlining);

        return ViolationsDisproverSemantic.PO_PATH;
    }

    public String generateJavaForFormalNodeTuple(
            SDGNodeTuple formalNodeTuple,
            StaticCGJavaMethod methodCorresToSE) throws IOException {
        this.keyCompatibleListener = new CopyKeYCompatibleListener(callGraph.getPackageName());

        SDGNode formalInNode = formalNodeTuple.getFirstNode();
        StaticCGJavaClass containingClass = methodCorresToSE.getContainingClass();
        String relPathForJavaClass
                = JavaProjectCopyHandler.getRelPathForJavaClass(containingClass);
        File javaClassFile =
                new File(pathToJavaSource +
                         relPathForJavaClass +
                         containingClass.getOnlyClassName() +
                         AutomationHelper.DOT_JAVA);

        if (!javaClassFile.exists()) {
            //it is a library class since it does not exist in the project
            throw new FileNotFoundException();
        }

        String contents = new String(Files.readAllBytes(javaClassFile.toPath()));
        Map<StaticCGJavaClass, Set<StaticCGJavaMethod>> allNecessaryClasses =
                callGraph.getAllNecessaryClasses(methodCorresToSE);

        String keyCompatibleContents = keyCompatibleListener.transformCode(
                contents, allNecessaryClasses.get(methodCorresToSE.getContainingClass()));

        methodBodyListener.parseFile(keyCompatibleContents, methodCorresToSE);

        javaProjectCopyHandler =
                new JavaProjectCopyHandler(pathToJavaSource,
                                           ViolationsDisproverSemantic.PO_PATH,
                                           keyCompatibleListener);
        javaProjectCopyHandler.copyClasses(allNecessaryClasses);

        String descriptionForKey
                = getMethodContract(
                        formalInNode, formalNodeTuple.getSecondNode(), methodCorresToSE);

        List<String> classFileForKey =
                generateClassFileForKey(descriptionForKey, keyCompatibleContents);

        javaProjectCopyHandler.addClassToTest(classFileForKey, containingClass);

        KeYFileCreator.createKeYFileIF(methodCorresToSE,
                                       ViolationsDisproverSemantic.PO_PATH, useLoopUnwinding, useMethodInlining);
        boolean normalBehaviour = descriptionForKey.contains(NORMAL_BEHAVIOUR);
        KeYFileCreator.createKeYFileFunctional(methodCorresToSE,
                                               ViolationsDisproverSemantic.PO_PATH,normalBehaviour, useLoopUnwinding, useMethodInlining);

        return ViolationsDisproverSemantic.PO_PATH;
    }

    private String getMethodContract(
            SDGNode formalInNode,
            SDGNode formalOutNode,
            StaticCGJavaMethod methodCorresToSE) throws IOException {
        this.keyCompatibleListener = new CopyKeYCompatibleListener(callGraph.getPackageName());

        StaticCGJavaClass containingClass = methodCorresToSE.getContainingClass();
        String relPathForJavaClass
                = JavaProjectCopyHandler.getRelPathForJavaClass(containingClass);
        File javaClassFile =
                new File(pathToJavaSource +
                         relPathForJavaClass +
                         containingClass.getOnlyClassName() +
                         AutomationHelper.DOT_JAVA);

        if (!javaClassFile.exists()) {
            // it is a library class since it does not exist in the project
            throw new FileNotFoundException();
        }

        String contents = new String(Files.readAllBytes(javaClassFile.toPath()));
        Map<StaticCGJavaClass, Set<StaticCGJavaMethod>> allNecessaryClasses =
                callGraph.getAllNecessaryClasses(methodCorresToSE);

        String keyCompatibleContents = keyCompatibleListener.transformCode(
                contents, allNecessaryClasses.get(methodCorresToSE.getContainingClass()));

        methodBodyListener.parseFile(keyCompatibleContents, methodCorresToSE);

        String inputDescrExceptFormalIn = getAllInputIf((n) -> {
            return n != formalInNode;
        }, formalInNode, methodCorresToSE, sdg);
        String sinkDescr = generateSinkDescr(formalOutNode, methodCorresToSE);
        String pointsToDecsr = PointsToGenerator.generatePreconditionFromPointsToSet(
                sdg, formalInNode, stateSaver);

        return createContractString(pointsToDecsr, sinkDescr, inputDescrExceptFormalIn);

    }
    
    
    private List<SDGNode> getEntrieNodeForCalledMethod(List<SDGNodeTuple> callSites, StaticCGJavaMethod methodCall) {
        List<SDGNode> result = new ArrayList<SDGNode>();
        
        if (callSites == null) {
        	callSites = sdg.getAllCallSites();
        }
        
        for (SDGNodeTuple callSite : callSites) {
            SDGNode callNode = callSite.getFirstNode();
            Collection<SDGNode> entries = sdg.getPossibleTargets(callNode);
            
//            if (entries.size() > 1) {
//                System.out.println("multiple entrie for the current call " + callNode.getLabel());
//            }
//            if (entries.isEmpty()) {
//                System.out.println("no entry for the current call " + callNode.getLabel());
//                throw new IOException();
//            }
            
                for (SDGNode currentEntry : entries) {
                	String call = methodCall.toString();
                    if (call.startsWith(currentEntry.getLabel())) {
                        result.add(currentEntry);
                    }
                }
        }
        return result;
    }
    
    /**
     * checks if the given String for the determine-contract part contains exceptions
     * @param determineString 
     * @return true if no exceptions contained in the given String. False otherwise.
     */
    private boolean checkIfNormalBehavior(String determineString) {
        if (!determineString.contains(SINK_SOURCE_EXCEPTION)) {
            return true;
        } else {
            return false;
        }
    }
    
    private String generateDetermineStringForContract(Collection<SDGNode> sinks, StaticCGJavaMethod methodCall) {
        HashSet<String> allOutStrings = new HashSet<>();
        String allOutput = "";
        
        if(sinks == null || sinks.isEmpty()) {
        	return NOTHING;
        }
        
        for (SDGNode n : sinks) {
            String sinkDescr = generateSinkDescr(n, methodCall);
            sinkDescr = replaceExceptionSinkSource(sinkDescr);
            if (!allOutStrings.contains(sinkDescr) && !sinkDescr.isEmpty()) {
                allOutStrings.add(sinkDescr);
                allOutput += sinkDescr + ", ";
            }
        }
        if (allOutput.lastIndexOf(",") != -1) {
            allOutput = allOutput.substring(0, allOutput.length() - 2);
        }
        return allOutput;
    }
    
    private String generateContractForMethod(SDGNode entry, StaticCGJavaMethod methodCall
            , Map<StaticCGJavaMethod, String> methodToGeneralContract) throws IOException {

    	Set<SDGNode> formOuts = sdg.getFormalOutsOfProcedure(entry);
        Set<SDGNode> formIns = sdg.getFormalInsOfProcedure(entry);
        
        String generalContract = methodToGeneralContract.get(methodCall);
        if (generalContract == null) {
            StaticCGJavaClass containingClass = methodCall.getContainingClass();
            String relPathForJavaClass
                = JavaProjectCopyHandler.getRelPathForJavaClass(containingClass);
            File javaClassFile =
                    new File(pathToJavaSource + File.separator+
                            relPathForJavaClass +
                            containingClass.getOnlyClassName() +
                            AutomationHelper.DOT_JAVA);
            if (!javaClassFile.exists()) {
            	if (keyCompatibleListener.isImportKeyCompatible(containingClass.getId())) {
            		return "";
            	}
                // it is a library class since it does not exist in the project
                throw new FileNotFoundException(javaClassFile.getName());
            }

            String contents = new String(Files.readAllBytes(javaClassFile.toPath()));
            Map<StaticCGJavaClass, Set<StaticCGJavaMethod>> allNecessaryClasses =
                    callGraph.getAllNecessaryClasses(methodCall);

            String keyCompatibleContents = keyCompatibleListener.transformCode(
                    contents, allNecessaryClasses.get(methodCall.getContainingClass()));
            methodBodyListener.parseFile(keyCompatibleContents, methodCall);
              
            /* Calculate precondtion from the points-to analysis results.
             */
            String pointsToDecsr = generatePointsToDescr(formIns);
            //check if exception is reachable -- if not then method has normal behaviour
            boolean normalBehaviour = checkNormalBehaviour(methodCall, formOuts);
            //Filter the sources which are reachable from the sinks
            Collection<SDGNode> sources = caluclateDetermineSourceNodes(formIns, formOuts);
            //Generate comma separated list of sources to be used in determines clause            
            String allInput = generateAllInputs(methodCall, sources);
            //Generate comma separated list of sinks to be used in determines clause 
            String allOutput = generateDetermineStringForContract(formOuts, methodCall);
            //Generate method contract
            String contract = createContractString(pointsToDecsr, allOutput, allInput,normalBehaviour);
            return contract;
        }
        return generalContract;
    }

	private boolean checkNormalBehaviour(StaticCGJavaMethod methodCall, Set<SDGNode> formOuts) {
		SDGNode exc = getExceptionNode(formOuts, methodCall);
        if(exc != null) {
            for(SDGEdge edge : sdg.incomingEdgesOf(exc)) {            	
            	SDGNode node = edge.getSource();
            	if(!node.getKind().equals(SDGNode.Kind.ENTRY)) {
            		return false;
            	} 
            }
        }
        
        return true;
	}

	
	/**
	 * Return exception formal out node from the list or null.
	 * @param sinks
	 * @param methodCall
	 * @return
	 */
	private SDGNode getExceptionNode(Collection<SDGNode> sinks, StaticCGJavaMethod methodCall) {
		SDGNode result = null;
		
		for (SDGNode node : sinks) {
			String descr = generateSinkDescr(node, methodCall);
			if(descr.startsWith(EXCEPTION)) {
				return node;
			}
		}
		
		return result;
	}

	private String generateAllInputs(StaticCGJavaMethod methodCall, Collection<SDGNode> formIns) {
		String allInput;
		//static method without params has no this formal in node 
		if(formIns == null) {
			allInput = NOTHING;
		}
		else if(!formIns.iterator().hasNext()) {
		    allInput = NOTHING;
		} else {
		    SDGNode formIn = formIns.iterator().next();
//                String mostGeneralContract = generateMostGeneralContract(
//                formIns.iterator().next(), formOuts.iterator().next(), methodCall);
		    
		    allInput = getAllInputIf((n) -> {
		        return true;
		    }, formIn , methodCall, sdg);
		    
		    
		}
		return allInput;
	}

	private String generatePointsToDescr(Set<SDGNode> formIns) {
		String pointsToDecsr = "";
		List<String> pointsToDescrList = new ArrayList<String>(); 
		for (SDGNode form : formIns) {
			String currentPreCondition = PointsToGenerator.generatePreconditionFromPointsToSet(sdg, form, stateSaver);
			if (!currentPreCondition.equals("true")) {
		    	pointsToDescrList.add(currentPreCondition);
			}
		}
		if (pointsToDescrList.isEmpty()) {
			pointsToDecsr = "true";
		} else {
			for (int i = 0; i < pointsToDescrList.size() - 1; i++) {
		    	pointsToDecsr += pointsToDescrList.get(i);
		    	pointsToDecsr += " && ";
		    }
			pointsToDecsr += pointsToDescrList.get(pointsToDescrList.size()-1);
		}
		return pointsToDecsr;
	}
    

    /**
     * This method creates contracts for all methods used in the given {@link StaticCGJavaMethod}. 
     * Just returns if the given {@link StaticCGJavaMethod} does not call other methods.
     * @param methodCG The current method which may call other methods. For these methods contracts will be generated
     * @param methodToGeneralContract Map which contains {@link StaticCGJavaMethod} with corresponding contracts
     * @throws IOException
     */
    public void addMethodContractForAllUsedMethods(
        StaticCGJavaMethod methodCG,
        Map<StaticCGJavaMethod, String> methodToGeneralContract) throws IOException {
        
        Set<StaticCGJavaMethod> calledMethodsCG = methodCG.getCalledMethods();
        if (calledMethodsCG.isEmpty()) {
            return; 
        }
        
        List<SDGNodeTuple> callSites = sdg.getAllCallSites();
      
        
        List<SDGNode> entryNodes;
        for (StaticCGJavaMethod methodCall : calledMethodsCG) {
            
            entryNodes = getEntrieNodeForCalledMethod(callSites, methodCall);
        

            callSites = null;
            if (entryNodes.isEmpty()) {
                return;
            }
            for(SDGNode entry : entryNodes) {
                String contract = generateContractForMethod(entry, methodCall, methodToGeneralContract);
                methodToGeneralContract.put(
                        methodCall, contract);  
                addMethodContractForAllUsedMethods(methodCall, methodToGeneralContract);
            }  
        }
    }
    
    

    

    public String getMethodContractAndSetLoopInvariantAndSetMostGeneralContract(
            SDGNode formalInNode,
            SDGNode formalOutNode,
            StaticCGJavaMethod methodCorresToSE,
            Map<SDGEdge, String> edgeToInvariantTempltate,
            SDGEdge e,
            Map<StaticCGJavaMethod, String> methodToGeneralContract) throws IOException {
        this.keyCompatibleListener = new CopyKeYCompatibleListener(callGraph.getPackageName());

        StaticCGJavaClass containingClass = methodCorresToSE.getContainingClass();
        String relPathForJavaClass
                = JavaProjectCopyHandler.getRelPathForJavaClass(containingClass);
        //if (pathToJavaSource.endsWith(//"") + relPathForJavaClass)
        File javaClassFile =
                new File(pathToJavaSource + File.separator+
                         relPathForJavaClass +
                         containingClass.getOnlyClassName() +
                         AutomationHelper.DOT_JAVA);

        if (!javaClassFile.exists()) {
            // it is a library class since it does not exist in the project
            throw new FileNotFoundException(javaClassFile.toString());
        }

        String contents = new String(Files.readAllBytes(javaClassFile.toPath()));
        Map<StaticCGJavaClass, Set<StaticCGJavaMethod>> allNecessaryClasses =
                callGraph.getAllNecessaryClasses(methodCorresToSE);

        String keyCompatibleContents = keyCompatibleListener.transformCode(
                contents, allNecessaryClasses.get(methodCorresToSE.getContainingClass()));

        methodBodyListener.parseFile(keyCompatibleContents, methodCorresToSE);

        //
        // most general contract, if it has not already been computed
        //
        String generalContract = methodToGeneralContract.get(methodCorresToSE);
        if (generalContract == null) {
            String mostGeneralContract = generateMostGeneralContract(
                    formalInNode, formalOutNode, methodCorresToSE);
            methodToGeneralContract.put(
                    methodCorresToSE, mostGeneralContract);
        }

        String inputDescrExceptFormalIn = getAllInputIf((n) -> {
            return n != formalInNode;
        }, formalInNode, methodCorresToSE, sdg);
        String sinkDescr = generateSinkDescr(formalOutNode, methodCorresToSE); 
        sinkDescr = replaceExceptionSinkSource(sinkDescr);
        
        String pointsToDecsr = PointsToGenerator.generatePreconditionFromPointsToSet(
                sdg, formalInNode, stateSaver);

        edgeToInvariantTempltate.put(e,
                LoopInvariantGenerator.createLoopInvariant(
                        sinkDescr, inputDescrExceptFormalIn));

        return createContractString(pointsToDecsr, sinkDescr, inputDescrExceptFormalIn);
    }

    private String generateMostGeneralContract(SDGNode formalIn, SDGNode formalOut,
                                               StaticCGJavaMethod method) {
        String allInput = getAllInputIf((n) -> {
            return true;
        }, formalIn, method, sdg);
        SDGNode methodNode = sdg.getEntry(formalIn);
        Set<SDGNode> formalOuts = sdg.getFormalInsOfProcedure(methodNode);
        String allOutput = "";
        
        allOutput = generateDetermineStringForContract(formalOuts, method);
         
        String pointsToDecsr = PointsToGenerator.generatePreconditionFromPointsToSet(
                sdg, formalIn, stateSaver);
        
        return createContractString(pointsToDecsr, allOutput, allInput);
    }
    

    /**
     * Performes a backward slicing in the sdg or given subgraph, for the given {@link SDGNode}s. 
     * @param nodes are the start nodes for the forward slicing
     * @return returns a List of nodes contained in the forward slice
     */
    private Collection<SDGNode> doBackwarSlicingForGivenNodes(Collection<SDGNode> nodes) {
        I2PBackward slicer = new I2PBackward(sdg);
        Collection<SDGNode> slicingResult = slicer.slice(nodes);
        return slicingResult;
    }
    
    /**
     * performes a forward slicing in the sdg or given subgraph, for the given {@link SDGNode}s. 
     * @param nodes are the start nodes for the forward slicing
     * @return returns a List of nodes contained in the forward slice
     */
    private Collection<SDGNode> doForwardSlicingForGivenNodes(Collection<SDGNode> nodes) {
        I2PForward slicer = new I2PForward(sdg);
        Collection<SDGNode> slicingResult = slicer.slice(nodes);
        return slicingResult;
    }
    
    /**
     * Perform a forward slicing to filter the formal out nodes which are not dependent from the formal in nodes
     * @param formalIns of the method 
     * @param formOuts of the method
     * @return Returns just the formal out nodes which are dependent of the formal in nodes. 
     * If the parameter formOuts is empty the method will return null.
     */
    private Collection<SDGNode> caluclateDetermineSinkNodes(Collection<SDGNode> formalIns, Collection<SDGNode> formOuts) {
        if (formOuts.isEmpty()) {
            return null;
        }
        Collection<SDGNode> slicingResult = doForwardSlicingForGivenNodes(formalIns);
        slicingResult.retainAll(formOuts);
        return slicingResult;
    }
    
    /**
     * Perform a forward slicing to filter the formal out nodes which are not dependent from the formal in nodes
     * @param formalIns of the method 
     * @param formOuts of the method
     * @return Returns just the formal out nodes which are dependent of the formal in nodes. 
     * If the parameter formOuts is empty the method will return null.
     */
    private Collection<SDGNode> caluclateDetermineSourceNodes(Collection<SDGNode> formalIns, Collection<SDGNode> formOuts) {
        if (formalIns.isEmpty()) {
            return null;
        }
        Collection<SDGNode> slicingResult = doBackwarSlicingForGivenNodes(formOuts);
        slicingResult.retainAll(formalIns);
        return slicingResult;
    }

    private List<String> generateClassFileForKey(
            String descriptionForKey,
            String classContents) {

        List<String> lines = new ArrayList<>();
        for (String l : classContents.split("\n")) {
            lines.add(l);
        }

        int startLine = methodBodyListener.getStartLine();
        int stopLine = methodBodyListener.getStopLine();

        //insert nullable between passed variables
        lines.add(startLine - 1, methodBodyListener.getMethodDeclWithNullable() + " {");
        for (int i = 0; i <= stopLine - startLine; ++i) {
            lines.remove(startLine);
        }

        lines.add(startLine - 1, descriptionForKey);

        return lines;
    }
    
    private String createContractString(String pointsToDecsr, String allOutput, String allInput, boolean normalBehaviour) {
        String behaviour = "";
        if (normalBehaviour) {
            behaviour = NORMAL_BEHAVIOUR;
        }
        String contract = "\t/*@ " 
                + behaviour + "\n\t  @ "
                + REQUIRES + " " + pointsToDecsr + ";\n\t  @ "
                + DETERMINES + " " + allOutput + " " 
                + BY + " " + allInput + "; */";
        return contract;
    }
    
    private String createContractString(String pointsToDecsr, String allOutput, String allInput) {
        boolean normalBehaviour = checkIfNormalBehavior(allOutput);
        return createContractString(pointsToDecsr, allOutput, allInput, normalBehaviour);
    }
    
    

    private String generateSinkDescr(SDGNode sinkNode,
                                     StaticCGJavaMethod methodCorresToSE) {
    	
    	
        if (sinkNode.getKind() == SDGNode.Kind.EXIT) {
        	String returnType = methodCorresToSE.getReturnType();
        	/*
        	 * For void method do not generated \result in determines.
        	 * TODO: This should be done more elegantly.
        	 */
        	if(returnType.equals(VOID)) {
        		return "";
        	}else {
        		return RESULT;
        	}            
        } else {
            return getCompleteNameOfOtherThanParam(sinkNode, methodCorresToSE);
        }
    }

    private String getCompleteNameOfOtherThanParam(SDGNode n,
                                                   StaticCGJavaMethod methodCorresToSE) {
        String bytecodeName = n.getBytecodeName();
        String[] forInNames = bytecodeName.split("\\.");
        String inputNameForKey = forInNames[forInNames.length - 1];

        List<SDGEdge> incomingParamStructEdges = sdg.getIncomingEdgesOfKind(
                n, SDGEdge.Kind.PARAMETER_STRUCTURE);

        SDGNode currentStructSource = incomingParamStructEdges.get(0).getSource();
        for (SDGEdge e : incomingParamStructEdges) {
            if (!e.getSource().getBytecodeName().startsWith("<")
                    || e.getSource().getBytecodeName().startsWith(PARAM)) {
                currentStructSource = e.getSource();
                break;
            }
        }
        String sourceBC = currentStructSource.getBytecodeName();
        while (!sourceBC.startsWith(PARAM) && !sourceBC.startsWith(EXCEPTION)) {
            String[] sourceBCSplit = sourceBC.split("\\.");
            inputNameForKey = sourceBCSplit[sourceBCSplit.length - 1] + "." + inputNameForKey;
            incomingParamStructEdges =
                    sdg.getIncomingEdgesOfKind(currentStructSource,
                                               SDGEdge.Kind.PARAMETER_STRUCTURE);
            if (incomingParamStructEdges == null || incomingParamStructEdges.isEmpty()) {
                // FIXME: Maybe review whether this break corrupts anything
                break;
            }
            currentStructSource = incomingParamStructEdges.get(0).getSource();
            for (SDGEdge e : incomingParamStructEdges) {
                if (!e.getSource().getBytecodeName().startsWith("<")
                        || e.getSource().getBytecodeName().startsWith(PARAM)) {
                    currentStructSource = e.getSource();
                    break;
                }
            }
            sourceBC = currentStructSource.getBytecodeName();
        }
        if (sourceBC.startsWith(PARAM)) {
            String nameForParam = getNameForParam(sourceBC, methodCorresToSE);
            return nameForParam + "." + inputNameForKey;
        } else {
            return EXCEPTION + inputNameForKey;
        }

    }

    private String getNameForParam(String byteCodeName, StaticCGJavaMethod methodCorresToSE) {
        int p_number =
                Integer.parseInt(byteCodeName.substring(PARAM.length()
                                 + 1)); //+ 1 for the trailing space
        if (!methodCorresToSE.isStatic()) {
            if (p_number == 0) {
                return THIS;
            } else {
                if (p_number <= methodBodyListener.getExtractedMethodParamNames().size()) {
                    return methodBodyListener.getExtractedMethodParamNames().get(p_number - 1);
                } else if (methodBodyListener.getMethodParamsNullable() != null) {
                    int lastSpace =
                            methodBodyListener.getMethodParamsNullable().lastIndexOf(" ") + 1;
                    return methodBodyListener.getMethodParamsNullable().substring(lastSpace);
                } else { return ""; }
            }
        } else {
            if (p_number < methodBodyListener.getExtractedMethodParamNames().size()) {
                return methodBodyListener.getExtractedMethodParamNames().get(p_number - 1);
            } else if (methodBodyListener.getMethodParamsNullable() != null) {
                int lastSpace =
                        methodBodyListener.getMethodParamsNullable().lastIndexOf(" ") + 1;
                return methodBodyListener.getMethodParamsNullable().substring(lastSpace);
            } else { return ""; }
        }
    }

    private String replaceExceptionSinkSource(String descr) {
    	if (descr.startsWith(EXCEPTION)) {
        	descr = SINK_SOURCE_EXCEPTION;
        }
    	return descr;
    }
    
    private String getAllInputIf(
            Predicate<SDGNode> predicate,
            SDGNode formalInNode,
            StaticCGJavaMethod methodCorresToSE,
            SDG sdg) {
        SDGNode methodNodeInSDG = sdg.getEntry(formalInNode);
        Set<SDGNode> formalInNodesOfProcedure = sdg.getFormalInsOfProcedure(methodNodeInSDG);
        String created = "";
        for (SDGNode currentFormalInNode : formalInNodesOfProcedure) {
            String nameOfKind = currentFormalInNode.getKind().name();
            if (!(predicate.test(currentFormalInNode))
                    || (!nameOfKind.startsWith(PARAM) && !nameOfKind.equals(FORMAL_IN))) {
                continue;
            }
            String bytecodeName = currentFormalInNode.getBytecodeName();
            String inputNameForKey = "";
            if (bytecodeName.startsWith(PARAM)) {
                try {
                    inputNameForKey = getNameForParam(bytecodeName, methodCorresToSE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                inputNameForKey = getCompleteNameOfOtherThanParam(currentFormalInNode,
                                                                  methodCorresToSE);
            }
            if (inputNameForKey.endsWith(JOANA_ARRAY)) {
                inputNameForKey =
                        ARRAY_TO_SEQ + "(" + inputNameForKey.replace("."+JOANA_ARRAY, "") + ")";
                inputNameForKey =
                        inputNameForKey.replace(JOANA_ARRAY, "");
            }
            created += inputNameForKey + ", ";
        }
        if (created.isEmpty()) {
            return NOTHING;
        } else {
            return created.substring(0, created.length() - 2);
        }
    }
    
    public void setUseLoopUnwinding(boolean useUnwinding) {
    	this.useLoopUnwinding = useUnwinding;
    }

    public void setMethodInlining(boolean useInlining) {
    	this.useMethodInlining = useInlining;
    }
}
