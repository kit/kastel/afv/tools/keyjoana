/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joanakey.javaforkeycreator;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import joanakey.AutomationHelper;
import joanakey.staticCG.javamodel.StaticCGJavaMethod;

/**
 *
 * @author holger
 */
public class KeYFileCreator {

    private static final String INT_ARR_JAVA = "int\\[\\]";
    private static final String INT_ARR_KeY = "\\[I";
    private static final String BYTE_ARR_JAVA = "byte\\[\\]";
    private static final String BYTE_ARR_KeY = "\\[B";
    private static final String CHAR_ARR_JAVA = "char\\[\\]";
    private static final String CHAR_ARR_KeY = "\\[C";

    /**
     * Creates the Information flow Proof Obligation for KeY.
     * @param method
     * @param pathToSave path for the proof obligation file
     * @param useLoopUnwinding boolean setting, true if KeY should use loop unwinding
     * @param useMethodInlining boolean setting, true if KeY should use method inlining
     */
    public static void createKeYFileIF(StaticCGJavaMethod method,
                                       String pathToSave,boolean useLoopUnwinding, boolean useMethodInlining) throws IOException {
        File proofObFile = new File(pathToSave + AutomationHelper.PO_NAME_IF);
        if (!proofObFile.exists()) {
            proofObFile.createNewFile();
        }

        String methodnameKey = getMethodnameKey(method);

        final String settingsStr = AutomationHelper.getSettings(useLoopUnwinding, useMethodInlining);
        final String javaSourceStr = "./";
        final String proofObligationTemplateString
                = "#Proof Obligation Settings\n"
                        + "name=" + AutomationHelper.CLASSNAME
                        + "[" + AutomationHelper.CLASSNAME
                        + "\\\\:\\\\:" + AutomationHelper.METHODNAME
                        + "].Non-interference contract.0\n"
                        + "contract=" + AutomationHelper.CLASSNAME
                        + "[" + AutomationHelper.CLASSNAME
                        + "\\\\:\\\\:" + AutomationHelper.METHODNAME
                        + "].Non-interference contract.0\n"
                        + "class=de.uka.ilkd.key.informationflow.po.InfFlowContractPO\n";
        final String proofObligationString = proofObligationTemplateString
                .replaceAll(AutomationHelper.METHODNAME, methodnameKey)
                .replaceAll(AutomationHelper.CLASSNAME, method.getContainingClass().getId());

        generateKeyFileFrom(AutomationHelper.JAVA_PROFILE, settingsStr,
                            javaSourceStr, proofObligationString, proofObFile);
    }

    /**
     * Generate the functional proof oblicagtion for KeY 
     * @param method
     * @param pathToSave path for the proof obligation file
     * @param useLoopUnwinding boolean setting, true if KeY should use loop unwinding
     * @param useMethodInlining boolean setting, true if KeY should use method inlining
     * @param normalBehaviour boolean setting, true if the method specification contains should 'normal behaviour'
     * @throws IOException
     */
    public static void createKeYFileFunctional(StaticCGJavaMethod method,
                                               String pathToSave, boolean normalBehaviour,
                                               boolean useLoopUnwinding, boolean useMethodInlining) throws IOException {
        File proofObFile = new File(pathToSave + "/" + AutomationHelper.PO_NAME_FUNCTIONAL);
        if (!proofObFile.exists()) {
            proofObFile.createNewFile();
        }

        final String settingsStr = AutomationHelper.getSettings(useLoopUnwinding, useMethodInlining);

        final String javaSourceStr = "./";

        String methodnameKey = getMethodnameKey(method);
        
        String normBehav = normalBehaviour ? "normal_behavior" : "";

        final String proofObligationTemplateString
                = "#Proof Obligation Settings\n"
                + "name=" + AutomationHelper.CLASSNAME
                + "[" + AutomationHelper.CLASSNAME
                + "\\\\:\\\\:" + AutomationHelper.METHODNAME
                + "].JML "+normBehav+" operation contract.0\n"
                + "contract=" + AutomationHelper.CLASSNAME
                + "[" + AutomationHelper.CLASSNAME
                + "\\\\:\\\\:" + AutomationHelper.METHODNAME
                + "].JML "+normBehav+" operation contract.0\n"
                + "class=de.uka.ilkd.key.proof.init.FunctionalOperationContractPO\n";
        final String proofObligationString = proofObligationTemplateString
                .replaceAll(AutomationHelper.METHODNAME, methodnameKey)
                .replaceAll(AutomationHelper.CLASSNAME, method.getContainingClass().getId());

        generateKeyFileFrom(AutomationHelper.JAVA_PROFILE, settingsStr,
                            javaSourceStr, proofObligationString, proofObFile);
    }

    private static String getMethodnameKey(StaticCGJavaMethod method) {
        String params =
                method.getParameterWithoutPackage()
                .replaceAll(INT_ARR_JAVA, INT_ARR_KeY)
                .replaceAll(BYTE_ARR_JAVA, BYTE_ARR_KeY)
                .replaceAll(CHAR_ARR_JAVA, CHAR_ARR_KeY);
        if (method.getId().equals(AutomationHelper.INIT)) {
            return method.getContainingClass().getOnlyClassName() + "(" + params + ")";
        } else {
            return method.getId() + "(" + params + ")";
        }
    }

    private static void generateKeyFileFrom(String profileString, String settingsString,
                                            String javaSourceString, String proofObligationString,
                                            File f) throws IOException {
        String profileTempStr =
                AutomationHelper.PROFILE_KeY + " " + AutomationHelper.PROFILE + ";\n";
        String javaSourceTempStr =
                AutomationHelper.JAVASRC_KeY + " " + AutomationHelper.JAVASRC + ";\n";
        String proofOblTempStr =
                AutomationHelper.PROOF_OBL_KeY + " " + AutomationHelper.PROOF_OBL + ";\n";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(profileTempStr.replace(AutomationHelper.PROFILE,
                                                    surroundWithApos(profileString)));
        stringBuilder.append('\n');
        stringBuilder.append(settingsString);
        stringBuilder.append('\n');
        stringBuilder.append(javaSourceTempStr.replace(AutomationHelper.JAVASRC,
                                                       surroundWithApos(javaSourceString)));
        stringBuilder.append('\n');
        stringBuilder.append(proofOblTempStr.replace(AutomationHelper.PROOF_OBL,
                                                     surroundWithApos(proofObligationString)));

        PrintWriter writer = new PrintWriter(f, AutomationHelper.ENCODING);
        writer.print(stringBuilder.toString());
        writer.close();
    }

    private static String surroundWithApos(String s) {
        return "\"" + s + "\"";
    }

}
