/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joanakey.javaforkeycreator;

/**
 *
 * @author holger
 */
public class LoopInvariantGenerator {

    private static final String LOOP_INVARIANT = "loop_invariant";
    private static final String ASSIGNABLE = "assignable";
    private static final String DECREASES = "decreases";

    /**
     * Creates loop invariants. Is not complete and only fills the determines
     * clause.
     *
     * @param descSinkG
     * @param descOtherParamsG
     * @param methodName
     * @return loop invariant
     */
    public static String createLoopInvariant(String descSinkG,
            String descOtherParamsG) {
        StringBuilder sb = new StringBuilder();
        String loopInvariant = "";

        String header = "\t/*\t@ " + LOOP_INVARIANT + " ";
        String assignable = "\t \t @ " + ASSIGNABLE + " ";
        //String descSink = descSinkG + "";
        String descOtherParams = descOtherParamsG + "";

        String determines =
                "\t \t @ " + JavaForKeYCreator.DETERMINES + " " + descOtherParams
                + " " + JavaForKeYCreator.BY + " " + JavaForKeYCreator.ITSELF + "; ";
        String decreases = "\t \t @ " + DECREASES + " */";

        sb.append(header);
        sb.append(System.lineSeparator());
        if (assignable.length() > 11) {
            sb.append(assignable);
            sb.append(System.lineSeparator());
        }
        sb.append(determines);
        sb.append(System.lineSeparator());
        sb.append(decreases);
        loopInvariant = sb.toString();

        return loopInvariant;
    }

    public static String getTemplate() {
        String header = "/*@ " + LOOP_INVARIANT + " \n";
        String decreases = "@ " + DECREASES + " */";
        return header + decreases;
    }
}
