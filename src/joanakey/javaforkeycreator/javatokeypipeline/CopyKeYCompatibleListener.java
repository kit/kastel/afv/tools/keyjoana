/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joanakey.javaforkeycreator.javatokeypipeline;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;

import joanakey.AutomationHelper;
import joanakey.antlr.java8.Java8BaseListener;
import joanakey.antlr.java8.Java8Lexer;
import joanakey.antlr.java8.Java8Parser;
import joanakey.customlistener.GetMethodBodyListener;
import joanakey.staticCG.javamodel.StaticCGJavaMethod;
import joanakey.violations.ViolationsWrapper;

/**
 *
 * @author holger
 */
public class CopyKeYCompatibleListener extends Java8BaseListener implements JavaToKeYPipelineStage {

    private static final String IMPORT = "import";
    private static final String PUBLIC = "public";
    private static final String SPEC_PUBLIC = "/*@spec_public@*/";
    private static final List<String> PRIMITIVE_TYPES = Arrays.asList("byte", "char","long", "int", "double", "float", "boolean", "void");
    private StringBuilder currentlyGenerated;
    private List<String> keyCompatibleJavaFeature = new ArrayList<>();
    private List<String> classCodeAsLines = new ArrayList<>();
    private List<String> importStatements = new ArrayList<>();
    private String mainPackageName;
    private String packageOfClass;
    Set<StaticCGJavaMethod> neededMethods;

    public CopyKeYCompatibleListener(String mainPackageName) throws FileNotFoundException, IOException {
//        InputStream is = new FileInputStream(ViolationsWrapper.KeY_COMPATIBLE_JAVA_FEATURES);
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(ViolationsWrapper.KeY_COMPATIBLE_JAVA_FEATURES);
        if (is == null) {
        	System.out.println("could not read JAVALANG.txt");
        }
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));
        String line = buf.readLine();
        this.mainPackageName = mainPackageName;
        while (line != null) {
            keyCompatibleJavaFeature.add(line);
            line = buf.readLine();
        }
        buf.close();
    }

    private List<String> getPossiblePackagesForType(String type) {
        List<String> created = new ArrayList<>();
        for (String currentImport : importStatements) {
            int lastIndexOfDot = currentImport.lastIndexOf(AutomationHelper.DOT);
            String importedType = currentImport.substring(lastIndexOfDot + 1, currentImport.length());
            if (importedType.equals(type)) {
                created.add(currentImport);
            }
        }

        if (created.isEmpty()) {
            created.add(packageOfClass + AutomationHelper.DOT + type);
            for (String currentImport : importStatements) {
                if (currentImport.endsWith(".*")) {
                    created.add(currentImport);
                }
            }
        }

        return created;
    }
    
    private boolean isPrimitiveType(String type) {
        return PRIMITIVE_TYPES.contains(type);
    }

    private boolean isTypeKeyCompatible(String type) {
        List<String> possiblePackagesForType = getPossiblePackagesForType(type);
        
        if(isPrimitiveType(type)) { //TODO not needed if otherdata/Javalang.txt is complete ?
            return true;
        }
        
        if (possiblePackagesForType.size() == 1) {
            if (isImportKeyCompatible(possiblePackagesForType.get(0))) {
                return true;
            }
        } else {
            for (String possImport : possiblePackagesForType) {
                if (!isImportKeyCompatible(possImport)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * check if the given import String is KeY compatible
     * @param importStmt import statement as String
     * @return boolean, True if KeY compatible
     */
    public boolean isImportKeyCompatible(String importStmt) {
        int firstDot = importStmt.indexOf(AutomationHelper.DOT);
        String mainPackageOfImportStmt = importStmt.substring(0, firstDot);
        if (mainPackageOfImportStmt.equals("null")) { //no main package
        	importStmt = importStmt.substring(firstDot+1);
        }
        if (mainPackageOfImportStmt.equals(mainPackageName)) {
            return true;
        }
        for (String s : keyCompatibleJavaFeature) {
            if (s.equals(importStmt)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void enterConstructorDeclaration(Java8Parser.ConstructorDeclarationContext ctx) {
        if (isCtorNecessary(ctx)) {
            currentlyGenerated.append(extractStringInBetween(ctx, classCodeAsLines)).append("\n");
        }
    }

    private boolean isCtorNecessary(Java8Parser.ConstructorDeclarationContext ctx) {
        String args = GetMethodBodyListener.getArgTypeString(
                ctx.constructorDeclarator().formalParameterList());
        return isMethodNeeded(AutomationHelper.INIT, args);
    }

    public static StaticCGJavaMethod findMethodInSet(Set<StaticCGJavaMethod> methods,
                                                     String id, String args) {
        for (StaticCGJavaMethod m : methods) {
            if (m.getId().equals(id) && m.getParameterWithoutPackage().equals(args)) {
                return m;
            }
        }
        return null;
    }

    private boolean isMethodNeeded(String methodId, String args) {
        StaticCGJavaMethod m = findMethodInSet(neededMethods, methodId, args);
        if (m == null) {
            return false;
        }
        return true;
    }

    @Override
    public void enterClassDeclaration(Java8Parser.ClassDeclarationContext ctx) {
        String classDecl = "";
        List<Java8Parser.ClassModifierContext> classModifier =
                ctx.normalClassDeclaration().classModifier();
        for (Java8Parser.ClassModifierContext modCtx : classModifier) {
            classDecl += modCtx.getText() + " ";
        }
        classDecl += "class ";
        String identifier = ctx.normalClassDeclaration().Identifier().getText();
        classDecl += identifier;
        currentlyGenerated.append(classDecl).append("{\n");
    }

    @Override
    public void exitClassDeclaration(Java8Parser.ClassDeclarationContext ctx) {
        currentlyGenerated.append("}\n");
    }

    public static String extractStringInBetween(ParserRuleContext ctx,
                                                List<String> classCodeAsLines) {
        int startLine = ctx.getStart().getLine();
        int startCharPositionInLine = ctx.getStart().getCharPositionInLine();
        int stopLine = ctx.getStop().getLine();
        int stopCharPositionInLine = ctx.getStop().getCharPositionInLine();

        String s = "";
        if (startLine == stopLine) {
            return classCodeAsLines.get(startLine - 1).substring(startCharPositionInLine,
                                                                 stopCharPositionInLine);
        }
        for (int i = startLine - 1; i < stopLine; ++i) {
            if (i == startLine) {
                s += classCodeAsLines.get(i).substring(startCharPositionInLine - 1,
                                                       classCodeAsLines.get(i).length()) + '\n';
            } else if (i == stopLine) {
                s += classCodeAsLines.get(i).substring(0, stopCharPositionInLine - 1);
            } else {
                s += classCodeAsLines.get(i) + '\n';
            }
        }

        return s;
    }

    @Override
    public void enterPackageDeclaration(Java8Parser.PackageDeclarationContext ctx) {
        List<TerminalNode> Identifier = ctx.Identifier();
        packageOfClass = "";
        for (TerminalNode n : Identifier) {
            packageOfClass += n.getText() + AutomationHelper.DOT;
        }
        packageOfClass = packageOfClass.substring(0, packageOfClass.length() - 1);
        currentlyGenerated.append(extractStringInBetween(ctx, classCodeAsLines)).append(";\n");
    }

    @Override
    public void enterImportDeclaration(Java8Parser.ImportDeclarationContext ctx) {
        String importStmt = ctx.getText().substring(IMPORT.length(), ctx.getText().length() - 1);
        importStatements.add(importStmt);
        if (isImportKeyCompatible(importStmt)) {
            currentlyGenerated.append(extractStringInBetween(ctx, classCodeAsLines)).append(";\n");
        }
    }

    @Override
    public void enterFieldDeclaration(Java8Parser.FieldDeclarationContext ctx) {
        String type = ctx.unannType().getText();
        int lastIndexOfLessThan = type.lastIndexOf("<");
        if (lastIndexOfLessThan != -1) {
            type = type.substring(0, lastIndexOfLessThan);
            return;
        }
        if (isTypeKeyCompatible(type)) {
            List<Java8Parser.FieldModifierContext> fieldModifier = ctx.fieldModifier();
            for (Java8Parser.FieldModifierContext currentMod : fieldModifier) {
                if (currentMod.getText().equals(PUBLIC)) {
                    currentlyGenerated.append(extractStringInBetween(ctx, classCodeAsLines))
                    .append(";\n");
                    return;
                }
            }
            currentlyGenerated
                    .append(SPEC_PUBLIC)
                    .append(extractStringInBetween(ctx, classCodeAsLines)).append(";\n");
        }
    }

    @Override
    public void enterMethodDeclaration(Java8Parser.MethodDeclarationContext ctx) {
        if (methodIsNeeded(ctx)) {
            currentlyGenerated.append(extractStringInBetween(ctx, classCodeAsLines)).append('\n');
        }
    }

    private boolean methodIsNeeded(Java8Parser.MethodDeclarationContext ctx) {
        String id = ctx.methodHeader().methodDeclarator().Identifier().getText();
        String args = GetMethodBodyListener.getArgTypeString(
                ctx.methodHeader().methodDeclarator().formalParameterList());
        return isMethodNeeded(id, args);
    }

    public static List<String> seperateCodeIntoLines(String code) {
        List<String> generated = new ArrayList<>();
        String[] split = code.split("\n");
        for (int i = 0; i < split.length; i++) {
            String string = split[i];
            generated.add(string);
        }
        return generated;
    }

    public String transformCode(String classCode, Set<StaticCGJavaMethod> neededMethods) { 
        currentlyGenerated = new StringBuilder();
        classCodeAsLines = new ArrayList<>();
        importStatements = new ArrayList<>();
        this.neededMethods = neededMethods; 
        classCodeAsLines = seperateCodeIntoLines(classCode);
        Java8Lexer java8Lexer = new Java8Lexer(new ANTLRInputStream(classCode));
        Java8Parser java8Parser = new Java8Parser(new CommonTokenStream(java8Lexer));
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(this, java8Parser.compilationUnit());

        return currentlyGenerated.toString();
    }

}
