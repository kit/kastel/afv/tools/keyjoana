package joanakey.customlistener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;

import joanakey.antlr.java8.Java8BaseListener;
import joanakey.antlr.java8.Java8Lexer;

public class PreProcessor extends Java8BaseListener {
	
	
	//TODO: dont remove comments

	public static void main(String[] args) throws IOException {
		String testFilePath = "testdata/plusMinusFalsePositive";
		PreProcessor pre  = new PreProcessor();		
	    pre.formatJavaSourceFolder(testFilePath);
	}

	private static CommonTokenStream getTokenStream(String path) throws FileNotFoundException, IOException {
		FileInputStream in = new FileInputStream(path);
		Java8Lexer java8Lexer = new Java8Lexer(new ANTLRInputStream(in));	
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
//		org.antlr.v4.runtime.BufferedTokenStream tokens = new org.antlr.v4.runtime.BufferedTokenStream(java8Lexer);
//		t.getTokens();
		tokens.getText();
		return tokens;
	}
	
	private static String formatJavaSource(CommonTokenStream tokens) {
		StringBuffer buffer = new StringBuffer();		
		int depth = 0;
		boolean br  = false;
		//System.out.println("tokens: "+);
		for(Token t : tokens.getTokens()){			
			if(t.getText().equals("{")){
				buffer.append("\n"+generateTab(depth)+t.getText()+"\n");
				depth++;
				br  =true;
			}			
			else if(t.getText().equals("}")){	
				depth--;
				buffer.append(generateTab(depth)+t.getText()+"\n");
				br  =true;
			}
			else if(t.getText().equals(";")){
				buffer.append(t.getText()+"\n");
				br  =true;
			}
			else if(t.getText().equals("<EOF>")) {
				continue;
			}
			else{
				if(br){buffer.append(generateTab(depth)); br=false;}
				buffer.append(t.getText()+" ");
				
			}
		}		
		return buffer.toString();
	}
	
	public void formatJavaSourceFolder(String path) throws FileNotFoundException, IOException {
		File file = new File(path);
		if(file.isDirectory()) {
			for(File sub : file.listFiles()) {
				formatJavaSourceFolder(sub.getAbsolutePath());
			}
		}
		else {
			if(file.getPath().endsWith(".java")) {
				formatJavaFile(path);
			}
		}
		
	}
	
	public void formatJavaFile(String path) throws FileNotFoundException, IOException {		
		CommonTokenStream tokens = getTokenStream(path);
		String formatted  = formatJavaSource(tokens);
		writeContentsToFile(path, formatted);
	}

	private void writeContentsToFile(String path, String formatted) throws IOException {
		File javaFile = new File(path);
		FileWriter writer = new FileWriter(javaFile, false);		
		writer.write(formatted);
		writer.close();
	}
	
	

	private static String generateTab(int len){
		String result = "";
		for (int i = 0; i < len; i++) {
			result += "    ";			
		}
		return result;
	}
	

}