package joanakey.customlistener;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import joanakey.antlr.java8.Java8BaseListener;
import joanakey.antlr.java8.Java8Lexer;
import joanakey.antlr.java8.Java8Parser;
import joanakey.antlr.java8.Java8Parser.WhileStatementContext;
import joanakey.slicing.BranchAnalyzer;
import joanakey.slicing.BranchAnalyzer.IF;

public class IfThenElseListener extends Java8BaseListener{
	
	private static final String ASSUME_FALSE = "assume(false);";
	
	private String pathToJavaFile;
	
	/**
	 * This is the result of the branch analysis.
	 */
	private Map<Integer, BranchAnalyzer.IF> analysis;
	
	private Map<Integer, String> insertions;
	
	
	
	public IfThenElseListener(Map<Integer, IF> analysis, String pathToJavaFile) {
		super();
		this.analysis = analysis;
		this.pathToJavaFile  = pathToJavaFile;
		this.insertions = new HashMap<>();
	}

//	public static void main(String[] args) throws IOException {
//		FileInputStream in = new FileInputStream(PATH);
//		Java8Lexer java8Lexer = new Java8Lexer(new ANTLRInputStream(in));
//		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
//        Java8Parser java8Parser = new Java8Parser(tokens);        
//        IfThenElseListener listener = new IfThenElseListener();        
//        java8Parser.addParseListener(listener);
//        ParseTreeWalker walker = new ParseTreeWalker();
//        walker.walk(listener, java8Parser.compilationUnit());
//	}
	
	
	private List<String> readLinesOfFile(String fileName) {
		List<String> originalLines = new LinkedList<>();
		try{
			File javaFile = new File(fileName);			
			BufferedReader br = new BufferedReader(new FileReader(javaFile));
			String origLine = null;				
			while((origLine = br.readLine())!=null){					
				originalLines.add(origLine);
			}
			br.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return originalLines;
	}
	
	private static Set<File> getJavaFiles(String path){
		
		Set<File> result = new HashSet<>();
		
		File file  = new File(path);
		
		if(!file.isDirectory() && file.getPath().endsWith(".java")) {
			result.add(file);
		}
		else if(file.isDirectory()) {			
			for(File sub : file.listFiles()) {				
				result.addAll(getJavaFiles(sub.getAbsolutePath()));				
			}			
		}		
		return result;
	}
	
	public static void insertAssertionsToFiles(String srcFolderPath, Map<String, Map<Integer, IF>> analysisResult) throws IOException {
		
		File dir = new File(srcFolderPath);
		
		Set<File> files = getJavaFiles(srcFolderPath);
		
		for(File file : files) {
			
			String filePath = file.getAbsolutePath();
			
			Path base = Paths.get(dir.getAbsolutePath());
			Path fileP = Paths.get(filePath);
			
			Path relativePath = base.relativize(fileP);
			
			Map<Integer, IF> resultForFile = new HashMap<>();
			
			if(analysisResult.containsKey(relativePath.toString())) {
				resultForFile = analysisResult.get(relativePath.toString());
				
				IfThenElseListener listener = new IfThenElseListener(resultForFile, filePath);
				
				listener.rewriteFileWithAssertions();
				
			}
			
			
		}
		
		
		
		
		
	}
	
	public void rewriteFileWithAssertions() throws IOException {
		
		insertAssertions();
		List<String> lines = readLinesOfFile(pathToJavaFile);
		File javaFile = new File(pathToJavaFile);
		PrintWriter pw = new PrintWriter(new FileWriter(javaFile, false));
		for(int i = 0; i < lines.size(); i++) {
			if(insertions.containsKey(i)) {
				pw.println(insertions.get(i));
			}
			pw.println(lines.get(i));
		}
		pw.close();		
	}
	
	
	private void insertAssertions() throws IOException {		
		FileInputStream in = new FileInputStream(pathToJavaFile);
		Java8Lexer java8Lexer = new Java8Lexer(new ANTLRInputStream(in));
		CommonTokenStream tokens = new CommonTokenStream(java8Lexer);
        Java8Parser java8Parser = new Java8Parser(tokens);       
        java8Parser.addParseListener(this);
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(this, java8Parser.compilationUnit());		
	}	
	
	@Override 
	public void enterIfThenElseStatement(Java8Parser.IfThenElseStatementContext ctx) { 
		if(ctx.statementNoShortIf()!=null && ctx.statement() != null) {
			int line = ctx.start.getLine();
			if(analysis.containsKey(line)) {
				
				IF analysisResult = analysis.get(line);
				if(analysisResult.equals(IF.TRUE)) {
					//close else part
					int insertionLine = ctx.statement().start.getLine();
					insertions.put(insertionLine, ASSUME_FALSE);
				}else if(analysisResult.equals(IF.FALSE)) {
					//close then part
					int insertionLine = ctx.statementNoShortIf().start.getLine();
					insertions.put(insertionLine, ASSUME_FALSE);					
				}				
			}			
		}
		
	}
	
	
	@Override
	public void enterWhileStatement(WhileStatementContext ctx) {
		if(ctx.statement() != null) {
			int line = ctx.start.getLine();
			if(analysis.containsKey(line)) {
				IF analysisResult = analysis.get(line);
				if(analysisResult.equals(IF.FALSE)) {
					//close then part
					int insertionLine = ctx.statement().start.getLine();
					insertions.put(insertionLine, ASSUME_FALSE);					
				}				
			}			
		}
	}
	
	@Override
	public void enterIfThenStatement(Java8Parser.IfThenStatementContext ctx){
		
		if(ctx.statement() != null) {
			int line = ctx.start.getLine();
			if(analysis.containsKey(line)) {
				IF analysisResult = analysis.get(line);
				if(analysisResult.equals(IF.FALSE)) {
					//close then part
					int insertionLine = ctx.statement().start.getLine();
					insertions.put(insertionLine, ASSUME_FALSE);					
				}				
			}			
		}
	}
}
