/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joanakey.customlistener;

import java.util.List;
import java.util.Map;

import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGEdge;
import joanakey.staticCG.javamodel.StaticCGJavaMethod;
import joanakey.violations.ViolationChop;

/**
 *
 * @author holger
 */
public interface ViolationsWrapperListener {
    public void parsedChop(ViolationChop chop);
    public void disprovedEdge(SDGEdge e);
    public void disprovedChop(ViolationChop chop);
    public void disprovedAll();
    public void addedNewEdges(Map<SDGEdge, StaticCGJavaMethod> edgesToMethods,
                              List<SDGEdge> edgesSorted, SDG sdg);
}
