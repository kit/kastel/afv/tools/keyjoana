package joanakey.violations;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGEdge;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import edu.kit.joana.ifc.sdg.graph.chopper.RepsRosayChopper;
import edu.kit.joana.ifc.sdg.util.BytecodeLocation;

public class Utils {

    /**
     * Given a chop (i.e., collection of SDGNodes) and a its respective
     * system dependence graph, this method computes a chop on the according SDG for
     * all of the method's actual-ins representing (static) fields as sources
     * and all of the method's actual-outs representing (static) fields as sinks,
     * and finally adds this new chop to the one given as a parameter.
     *
     * @param nodes The chop without field dependences
     * @param graph The system dependence graph
     * @param onlyStatic Set true if only static fields should be considered,
     *                   else all will
     * @return Chop including (static) field dependences
     */
    public static Collection<SDGNode> addChopForFields(Collection<SDGNode> nodes,
                                                       SDG graph,
                                                       boolean onlyStatic) {
        Set<SDGNode> nodesWithFields = new HashSet<SDGNode>();
        for(SDGNode node : nodes) {
            nodesWithFields.addAll(
                    chopBetweenStaticFields(node.getBytecodeMethod(),
                                            graph, onlyStatic)
                    );
        }
        nodesWithFields.addAll(nodes);
        return nodesWithFields;
    }

    /**
     * Given a method signature, computes a chop on the according SDG for
     * all of the method's actual-ins representing (static) fields as sources
     * and all of the method's actual-outs representing (static) fields as sinks.
     *
     * @param methodSignature The method signature
     * @param graph The system dependence graph
     * @param onlyStatic Set true if only static fields should be considered,
     *                   else all will
     * @return Chop
     */
    private static Collection<SDGNode>
            chopBetweenStaticFields(String methodSignature, SDG graph,
                                    boolean onlyStatic) {
        Set<SDGNode> sources = new HashSet<SDGNode>();
        Set<SDGNode> sinks = new HashSet<SDGNode>();
        // for each call to given method...
        for (SDGNode call : findCallsTo(methodSignature, graph)) {
            // ... add the actual-ins representing static field reads as sources
            sources.addAll(findReadStatics(call, graph, onlyStatic));
            // ... add the actual-outs representing static field reads as sinks
            sinks.addAll(findWrittenStatics(call, graph, onlyStatic));
        }
        // compute chop
        RepsRosayChopper chopper = new RepsRosayChopper(graph);
        return chopper.chop(sources, sinks);
    }

    /**
     * Given a method signature, finds all call nodes for calls
     * to the given method.
     *
     * @param methodSignature The method signature
     * @param graph The system dependence graph
     * @return Call nodes
     */
    private static Set<SDGNode> findCallsTo(String methodSignature, SDG graph) {
        Set<SDGNode> result = new HashSet<SDGNode>();
        for (SDGNode n : graph.vertexSet()) {
            if (n.getKind() != SDGNode.Kind.CALL)
                continue;
            Collection<SDGNode> possTgts = graph.getPossibleTargets(n);
            if (possTgts.isEmpty()
                    && n.getUnresolvedCallTarget() != null
                    && n.getUnresolvedCallTarget().equals(methodSignature)) {
                result.add(n);
            } else {
                for (SDGNode entry : possTgts) {
                    String tgtSignature = entry.getBytecodeName();
                    if (tgtSignature.equals(methodSignature)) {
                        result.add(n);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Given a method call, finds all actual-in nodes which represent
     * a (static) field read by the called method.
     *
     * @param call The method call
     * @param graph The system dependence graph
     * @param onlyStatic Set true if only static fields should be considered,
     *                   else all will
     * @return Actual-in nodes
     */
    private static Set<SDGNode> findReadStatics(SDGNode call, SDG graph,
                                                boolean onlyStatic) {
        Set<SDGNode> result = new HashSet<SDGNode>();
        for (SDGEdge eOut : graph.outgoingEdgesOf(call)) {
            if (eOut.getTarget().getKind() == SDGNode.Kind.ACTUAL_IN) {
                // TODO: We probably should check whether this actually is a HIGH source
                SDGNode actualIn = eOut.getTarget();
                if (!onlyStatic
                        || (actualIn.getBytecodeIndex()
                                == BytecodeLocation.STATIC_FIELD)) {
                    result.add(actualIn);
                }
            }
        }
        return result;
    }

    /**
     * Given a method call, finds all actual-in nodes which represent
     * a (static) field write to the called method.
     *
     * @param call The method call
     * @param graph The system dependence graph
     * @param onlyStatic Set true if only static fields should be considered,
     *                   else all will
     * @return Actual-in nodes
     */
    private static Set<SDGNode> findWrittenStatics(SDGNode call, SDG graph,
                                                   boolean onlyStatic) {
        Set<SDGNode> result = new HashSet<SDGNode>();
        for (SDGEdge eOut : graph.outgoingEdgesOf(call)) {
            if (eOut.getTarget().getKind() == SDGNode.Kind.ACTUAL_OUT) {
                // TODO: We probably should check whether this actually is a LOW sink
                SDGNode actualIn = eOut.getTarget();
                if (!onlyStatic
                        || (actualIn.getBytecodeIndex()
                                == BytecodeLocation.STATIC_FIELD)) {
                    result.add(actualIn);
                }
            }
        }
        return result;
    }
}
