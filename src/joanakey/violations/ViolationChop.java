/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joanakey.violations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGEdge;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import edu.kit.joana.ifc.sdg.graph.chopper.Chopper;
import edu.kit.joana.ifc.sdg.graph.chopper.RepsRosayChopper;

/**
 *
 * @author holgerklein
 */
public class ViolationChop {

    private static final String NODES_IN_CHOP = "nodes_in_chop";

    private SDGNode violationSource;
    private SDGNode violationSink;
    private Collection<SDGNode> nodesInChop;
    private SDG inducedSubgraph;
    private Chopper chopper;
    private List<SDGEdge> summaryEdges = new ArrayList<>();

    private ViolationChop() {}

    public ViolationChop(SDGNode violationSource, SDGNode violationSink, SDG sdg) {
        this.violationSource = violationSource;
        this.violationSink = violationSink;
        findSummaryEdges(sdg);
    }

    public String generateSaveString() {
        StringBuilder created = new StringBuilder();
        created.append("{").append(System.lineSeparator());
        int lengthOfLineSep = System.lineSeparator().length();

        created.append("\"" + ViolationsWrapper.SOURCE + "\" : ")
            .append(violationSource.getId()).append(",").append(System.lineSeparator());
        created.append("\"" + ViolationsWrapper.SINK + "\" : ")
            .append(violationSink.getId()).append(",").append(System.lineSeparator());

        created.append("\"" + NODES_IN_CHOP + "\" : [").append(System.lineSeparator());
        for (SDGNode n : nodesInChop) {
            created.append(n.getId()).append(",").append(System.lineSeparator());
        }

        //delete last comma 
        if (created.lastIndexOf("[") != created.length() - 2) {
            created.delete(created.length() - lengthOfLineSep - 1, created.length());
        }
        created.append("]").append(System.lineSeparator());
        created.append("}");
        return created.toString();
    }

    public static ViolationChop generateFromJsonObj(JSONObject jSONObject, SDG sdg) {

        int srcId = jSONObject.getInt(ViolationsWrapper.SOURCE);
        int sinkId = jSONObject.getInt(ViolationsWrapper.SINK);
        ViolationChop created = new ViolationChop();
        created.violationSource = sdg.getNode(srcId);
        created.violationSink = sdg.getNode(sinkId);
        created.nodesInChop = new HashSet<>();
        JSONArray nodesInChopArr = jSONObject.getJSONArray(NODES_IN_CHOP);
        for (int i = 0; i < nodesInChopArr.length(); ++i) {
            created.nodesInChop.add(sdg.getNode(nodesInChopArr.getInt(i)));
        }
        created.inducedSubgraph = sdg.subgraph(created.nodesInChop);
        created.inducedSubgraph.edgeSet().forEach((e) -> {
            if (isSummaryEdge(e)) {
                created.summaryEdges.add(e);
            }
        });
        return created;
    }

    public SDGNode getViolationSource() {
        return violationSource;
    }

    public SDGNode getViolationSink() {
        return violationSink;
    }

    public boolean isEmpty() {
        return nodesInChop.isEmpty();
    }

    public Collection<SDGEdge> getSummaryEdges() {
        return summaryEdges;
    }

    public void findSummaryEdges(SDG sdg) {
        this.chopper = new RepsRosayChopper(sdg);
        nodesInChop = chopper.chop(violationSource, violationSink);
        
        if (nodesInChop.isEmpty()) {
            return;
        }
        // currently only consider only static fields XXX
        // final boolean onlyStatic = true; XXX Currently commented out, since not quite there yet
        // nodesInChop = Utils.addChopForFields(nodesInChop, sdg, onlyStatic);
        inducedSubgraph = sdg.subgraph(nodesInChop);

        //clear summary edges and collect all remaining summary edges
        summaryEdges.clear();
        inducedSubgraph.edgeSet().forEach((e) -> {
            if (isSummaryEdge(e)) {
                summaryEdges.add(e);
            }
        });
    }
    
    private static boolean isSummaryEdge(SDGEdge currentEdge) {
        return currentEdge.getKind() == SDGEdge.Kind.SUMMARY;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ViolationChop other = (ViolationChop) obj;
        if (!Objects.equals(this.violationSource, other.violationSource)) {
            return false;
        }
        if (!Objects.equals(this.violationSink, other.violationSink)) {
			return false;
		}
		if (other.getSummaryEdges() == null || summaryEdges == null) {
			if ((other.getSummaryEdges() != null && summaryEdges == null)
					|| (other.getSummaryEdges() == null || summaryEdges != null)) {
				return false;
			}
		} else {
			if (other.getSummaryEdges().size() != summaryEdges.size()) {
				return false;
			}
			for (int i = 0; i < summaryEdges.size(); ++i) {
				if (!summaryEdges.get(i).equals(other.summaryEdges.get(i))) {
					return false;
				}
			}
		}
        
        return true;
    }
    
    
}
