/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joanakey.violations;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.kit.joana.api.IFCAnalysis;
import edu.kit.joana.api.sdg.SDGMethod;
import edu.kit.joana.ifc.sdg.core.SecurityNode;
import edu.kit.joana.ifc.sdg.core.libraries.IFC;
import edu.kit.joana.ifc.sdg.core.violations.ClassifiedViolation;
import edu.kit.joana.ifc.sdg.core.violations.IViolation;
import edu.kit.joana.ifc.sdg.core.violations.IViolationVisitor;
import edu.kit.joana.ifc.sdg.core.violations.paths.ViolationPath;
import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGEdge;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import edu.kit.joana.ifc.sdg.graph.SDGNodeTuple;
import edu.kit.joana.ifc.sdg.util.JavaType;
import joanakey.customlistener.ViolationsWrapperListener;
import joanakey.persistence.ViolationsSaverLoader;
import joanakey.staticCG.JCallGraph;
import joanakey.staticCG.javamodel.StaticCGJavaClass;
import joanakey.staticCG.javamodel.StaticCGJavaMethod;

/**
 *
 * @author holgerklein
 */
public class ViolationsWrapper {

    private static final String SUMMARY_EDGES_SORTED = "summary_edges_sorted";
    private static final String SUMMARY_EDGES_METHODS = "summary_edges_methods";
    private static final String SUMMARY_EDGES_CHOPS = "summary_edges_chops";

    private static final String POS = "pos";
    private static final String CHOPS = "chops";
    private static final String VIOLATIONS = "violations";

    private static final String ID = "id";
    private static final String CLASS_NAME = "class_name";
    private static final String METHOD_NAME = "method_name";
    private static final String ARG_STRING = "arg_string";

    static final String SOURCE = "src";
    static final String SINK = "sink";

    public static final String KeY_COMPATIBLE_JAVA_FEATURES = "resources/JAVALANG.txt";//"otherdata/JAVALANG.txt";

    private Collection<? extends IViolation<SecurityNode>> uncheckedViolations;
    private SDG sdg;
    private Collection<ViolationChop> violationChops = new ArrayList<>();
    private Collection<ViolationChop> allChops = new ArrayList<>();
    private HashSet<SDGEdge> checkedEdges = new HashSet<>();
    private HashSet<SDGEdge> edgesToCheck = new HashSet<>();
    private Map<SDGEdge, ArrayList<ViolationChop>> summaryEdgesAndContainingChops = new HashMap<>();
    private Map<SDGEdge, StaticCGJavaMethod> summaryEdgesAndCorresJavaMethods = new HashMap<>();
    private Map<SDGEdge, ViolationChop> disprovenSummaryEdgesAndCorresChop = new HashMap<>();
    private Map<ViolationChop, IViolation<SecurityNode>> chopToViolationMap = new HashMap<>();
    private List<SDGEdge> sortedEdgesToCheck = new ArrayList<>();
    private JCallGraph callGraph = new JCallGraph();
    private IFCAnalysis ana;
    private List<String> keyCompatibleJavaFeatures = new ArrayList<>();
    private List<ViolationsWrapperListener> listener = new ArrayList<>();
    private Collection<? extends IViolation<SecurityNode>> allViolationsToCheck;
    

    public ViolationsWrapper(
            Collection<? extends IViolation<SecurityNode>> violations,
            SDG sdg, IFCAnalysis ana, JCallGraph callGraph) throws IOException {
        this.uncheckedViolations = violations;
        this.sdg = sdg;
        this.callGraph = callGraph;
        this.ana = ana;

//        InputStream is = new FileInputStream(KeY_COMPATIBLE_JAVA_FEATURES);
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("resources/JAVALANG.txt");
        if (is == null) {
        	System.out.println("could not read JAVALANG.txt");
        }

        BufferedReader buf = new BufferedReader(new InputStreamReader(is));
        String line = buf.readLine();
        while (line != null) {
            keyCompatibleJavaFeatures.add(line);
            line = buf.readLine();
        }
        buf.close();
		Collection<? extends IViolation<SecurityNode>> allViolationsToHandle = getAllViolationsToHandle();
		allViolationsToCheck = allViolationsToHandle;

        prepareNextSummaryEdges();
    }

    public void addListener(ViolationsWrapperListener listener) {
        this.listener.add(listener);
    }

    public String generateSaveString() {
        StringBuilder created = new StringBuilder();
        created.append("{").append(System.lineSeparator());
        int lengthOfLineSep = System.lineSeparator().length();

        created.append(ViolationsSaverLoader.generateSaveString(uncheckedViolations));
        created.append(",\n");

        created.append("\"" + CHOPS + "\" : [")
                .append(System.lineSeparator());
        for (ViolationChop vc : violationChops) {
            created.append(vc.generateSaveString())
                    .append(",")
                    .append(System.lineSeparator());
        }
        //int length =
        created.length();
        //int lastIndexOf =
        created.lastIndexOf("[");
        if (created.lastIndexOf("[") != created.length() - lengthOfLineSep - 1) {
            created.delete(created.length() - lengthOfLineSep - 1, created.length());
        }
        created.append("],").append(System.lineSeparator());

        created.append("\"" + SUMMARY_EDGES_SORTED + "\" : [").append(System.lineSeparator());
        for (SDGEdge e : sortedEdgesToCheck) {
            created.append("{");

            int srcId = sdg.getEdgeSource(e).getId();
            int sinkId = sdg.getEdgeTarget(e).getId();

            if (srcId == 14724 && sinkId == 49581) {
                //SDGEdge foundEdge =
                sdg.getEdge(sdg.getNode(srcId), sdg.getNode(sinkId));
            }

            //SDGEdge foundEdge =
            sdg.getEdge(sdg.getNode(srcId), sdg.getNode(sinkId));

            created.append("\"" + SOURCE + "\" : ").append(srcId);
            created.append(", \"" + SINK + "\" : ").append(sinkId);
            created.append("},").append(System.lineSeparator());
        }
        if (created.lastIndexOf("[") != created.length() - lengthOfLineSep - 1) {
            created.delete(created.length() - lengthOfLineSep - 1, created.length());
        }
        created.append("],").append(System.lineSeparator());

        created.append("\"" + SUMMARY_EDGES_METHODS + "\": [").append(System.lineSeparator());
        for (Map.Entry<SDGEdge, StaticCGJavaMethod> entry
                : summaryEdgesAndCorresJavaMethods.entrySet()) {
            int edgeId = sortedEdgesToCheck.indexOf(entry.getKey());
            created.append("{");
            created.append("\"" + ID + "\" :").append(edgeId);
            created.append(", \"" + CLASS_NAME + "\" : ")
                    .append("\"").append(entry.getValue().getContainingClass().getId())
                    .append("\"");
            created.append(", \"" + METHOD_NAME + "\" : ")
                    .append("\"").append(entry.getValue().getId()).append("\"");
            created.append(", \"" + ARG_STRING + "\" : ")
                    .append("\"").append(entry.getValue().getParameterWithoutPackage())
                    .append("\"");
            created.append("},").append(System.lineSeparator());
        }
        if (created.lastIndexOf("[") != created.length() - lengthOfLineSep - 1) {
            created.replace(created.length() - lengthOfLineSep - 1, created.length(), "");
        }
        created.append("],");

        created.append("\"" + SUMMARY_EDGES_CHOPS + "\" : [").append(System.lineSeparator());
        for (Map.Entry<SDGEdge, ArrayList<ViolationChop>> entry
                : summaryEdgesAndContainingChops.entrySet()) {
            int edgeId = sortedEdgesToCheck.indexOf(entry.getKey());
            created.append("{");
            created.append("\"" + POS + "\" :").append(edgeId);
            created.append(", \"" + CHOPS + "\" : [").append(System.lineSeparator());
            for (ViolationChop vc : entry.getValue()) {
                created.append("{");
                created.append("\"" + SOURCE + "\" : ").append(vc.getViolationSource().getId());
                created.append(", \"" + SINK + "\" : ").append(vc.getViolationSink().getId());
                created.append("},").append(System.lineSeparator());
            }
            if (created.lastIndexOf("[") != created.length() - lengthOfLineSep - 1) {
                created.delete(created.length() - lengthOfLineSep - 1, created.length());
            }
            created.append("]");

            created.append("},").append(System.lineSeparator());
        }
        if (created.lastIndexOf("[") != created.length() - lengthOfLineSep - 1) {
            created.delete(created.length() - lengthOfLineSep - 1, created.length());
        }
        created.append("]").append(System.lineSeparator());
        created.append("}");
        return created.toString();
    }

    private ViolationsWrapper() {
    }

    public static ViolationsWrapper generateFromJsonObj(JSONObject jSONObject,
                                                        SDG sdg,
                                                        JCallGraph callGraph) {
        ViolationsWrapper created = new ViolationsWrapper();

        JSONArray violArr = jSONObject.getJSONArray(VIOLATIONS);
        created.uncheckedViolations = ViolationsSaverLoader.generateFromJSON(violArr, sdg);
        //TODO: maybe add a json field for allViolations to show all already disproven violations after loading a .dispro file
        List<IViolation<SecurityNode>> c = new ArrayList<>();
        for (IViolation<SecurityNode> v : created.uncheckedViolations) {
            c.add(v);
        }
        Collection<? extends IViolation<SecurityNode>> allViolationsToHandle = c; 
		created.allViolationsToCheck = allViolationsToHandle;
//		for (IViolation<SecurityNode> v : created.uncheckedViolations) {
//			created.allViolationsToCheck.add(v);
//			ad
//		}

        JSONArray chopArr = jSONObject.getJSONArray(CHOPS);
        for (int i = 0; i < chopArr.length(); ++i) {
            created.violationChops.add(ViolationChop.generateFromJsonObj(
                    chopArr.getJSONObject(i), sdg));
        }

        JSONArray summaryEdgesSortedArr = jSONObject.getJSONArray(SUMMARY_EDGES_SORTED);

        for (int i = 0; i < summaryEdgesSortedArr.length(); ++i) {
            int srcId = summaryEdgesSortedArr.getJSONObject(i).getInt(SOURCE);
            int sinkId = summaryEdgesSortedArr.getJSONObject(i).getInt(SINK);

            //SDGEdge edge =
            sdg.getEdge(sdg.getNode(srcId), sdg.getNode(sinkId));
            Set<SDGEdge> allEdges = sdg.getAllEdges(sdg.getNode(srcId), sdg.getNode(sinkId));
            if (allEdges.size() == 0) {
                System.out.println("Missing Summary Edge : " + SOURCE + " " +
                                   srcId + " " + SINK + " " + sinkId);
            }
            for (SDGEdge e : allEdges) {
                if (e.getKind() == SDGEdge.Kind.SUMMARY) {
                    created.sortedEdgesToCheck.add(e);
                    break;
                }
            }
        }

        JSONArray edgesToMethodsArr = jSONObject.getJSONArray(SUMMARY_EDGES_METHODS);

        for (int i = 0; i < edgesToMethodsArr.length(); ++i) {
            JSONObject currentJsonObj = edgesToMethodsArr.getJSONObject(i);
            int posInSorted = currentJsonObj.getInt(ID);
            String classname = currentJsonObj.getString(CLASS_NAME);
            String methodName = currentJsonObj.getString(METHOD_NAME);
            String args = currentJsonObj.getString(ARG_STRING);
            StaticCGJavaMethod method = callGraph.getMethodFor(classname, methodName, args);
            created.summaryEdgesAndCorresJavaMethods.put(
                    created.sortedEdgesToCheck.get(posInSorted),
                    method
            );
        }

        JSONArray jsonChopArr = jSONObject.getJSONArray(SUMMARY_EDGES_CHOPS);

        for (int i = 0; i < jsonChopArr.length(); ++i) {
            JSONObject currentJsonObj = jsonChopArr.getJSONObject(i);
            int posInSorted = currentJsonObj.getInt(POS);
            JSONArray currentChopJsonArr = currentJsonObj.getJSONArray(CHOPS);
            ArrayList<ViolationChop> chopList = new ArrayList<>();
            for (int j = 0; j < currentChopJsonArr.length(); ++j) {
                int sourceId = currentChopJsonArr.getJSONObject(j).getInt(SOURCE);
                int sinkId = currentChopJsonArr.getJSONObject(j).getInt(SINK);
                SDGNode sourceNode = sdg.getNode(sourceId);
                SDGNode sinkNode = sdg.getNode(sinkId);
                for (ViolationChop vc : created.violationChops) {
                    if (vc.getViolationSource().equals(sourceNode)
                            && vc.getViolationSink().equals(sinkNode)) {
                        chopList.add(vc);
                        break;
                    }
                }
            }
            created.summaryEdgesAndContainingChops.put(
                    created.sortedEdgesToCheck.get(posInSorted), chopList);
        }

        created.sdg = sdg;
        created.callGraph = callGraph;
        return created;
    }

    public ViolationChop getChop() {
        return violationChops.iterator().next();
    }

    private void prepareNextSummaryEdgesOld() {
        Collection<? extends IViolation<SecurityNode>> nextViolationsToHandle =
                getNextViolationsToHandle(1);
        removeFromUncheckViolations(nextViolationsToHandle);
//        Collection<? extends IViolation<SecurityNode>> nextViolationsToHandle =
//                getNextViolationsToHandle();

        nextViolationsToHandle.forEach((v) -> {
        	ViolationChop chop = createViolationChop(v, sdg);
            violationChops.add(chop);
        });
        putEdgesInSet();
        findCGMethodsForSummaryEdgesIfKeYCompatible();
        putEdgesAndChopsInMap();

        sortedEdgesToCheck = new ArrayList<>();
        for (SDGEdge e : summaryEdgesAndCorresJavaMethods.keySet()) {
            sortedEdgesToCheck.add(e);
        }
        for (int i = 0; i < 10; ++i) {
            try {
                sortedEdgesToCheck.sort(new SummaryEdgeComparator(this));
                break;
            } catch (Exception e) {
            }
        }

        violationChops.forEach((vc) -> {
            listener.forEach((l) -> {
                l.parsedChop(vc);
            });
        });
        listener.forEach((l) -> {
            l.addedNewEdges(summaryEdgesAndCorresJavaMethods, sortedEdgesToCheck, sdg);
        });
    }


    private void prepareNextSummaryEdges() {
//        Collection<? extends IViolation<SecurityNode>> nextViolationsToHandle =
//                getNextViolationsToHandle(1);
//        removeFromUncheckViolations(nextViolationsToHandle);
        Collection<? extends IViolation<SecurityNode>> nextViolationsToHandle =
                getAllViolationsToHandle();
//        removeFromUncheckViolations(nextViolationsToHandle);

        nextViolationsToHandle.forEach((v) -> {
        	ViolationChop chop = createViolationChop(v, sdg);
            violationChops.add(chop);
        });
        putEdgesInSet();
        findCGMethodsForSummaryEdgesIfKeYCompatible();
        putEdgesAndChopsInMap();

        sortedEdgesToCheck = new ArrayList<>();
        for (SDGEdge e : summaryEdgesAndCorresJavaMethods.keySet()) {
            sortedEdgesToCheck.add(e);
        }
        for (int i = 0; i < 10; ++i) {
            try {
                sortedEdgesToCheck.sort(new SummaryEdgeComparator(this));
                break;
            } catch (Exception e) {
            }
        }

        violationChops.forEach((vc) -> {
            listener.forEach((l) -> {
                l.parsedChop(vc);
            });
        });
        listener.forEach((l) -> {
            l.addedNewEdges(summaryEdgesAndCorresJavaMethods, sortedEdgesToCheck, sdg);
        });
    }

	private void putEdgesInSet() {
        for (ViolationChop vc : violationChops) {
            Collection<SDGEdge> summaryEdges = vc.getSummaryEdges();
            for (SDGEdge summaryEdge : summaryEdges) {
                edgesToCheck.add(summaryEdge);
            }
        }
    }
	
	private Collection<IViolation<SecurityNode>> getAllViolationsToHandle() {
        List<IViolation<SecurityNode>> created = new ArrayList<>();
        for (IViolation<SecurityNode> v : uncheckedViolations) {
            created.add(v);
        }
        return created;
		
	}
    
    private Collection<IViolation<SecurityNode>> getNextViolationsToHandle(int amtViols) {
//        int amt_viols = 1;
        int amt_viols = amtViols;
        List<IViolation<SecurityNode>> created = new ArrayList<>();
        int i = 0;
        for (IViolation<SecurityNode> v : uncheckedViolations) {
            if (i == amt_viols) {
                break;
            }
            created.add(v);
            ++i;
		}
		return created;
	}
    
    private void removeFromUncheckViolations(Collection<? extends IViolation<SecurityNode>> nextViolationsToHandle) {
        for (IViolation<SecurityNode> v : nextViolationsToHandle) {
            uncheckedViolations.remove(v);
        }
        return;
    }

    private void findCGMethodsForSummaryEdgesIfKeYCompatible() {
        for (SDGEdge summaryEdge : edgesToCheck) {
            Collection<SDGNodeTuple> allFormalPairs =
                    sdg.getAllFormalPairs(summaryEdge.getSource(), summaryEdge.getTarget());
            SDGNodeTuple firstPair = allFormalPairs.iterator().next();
            SDGNode methodNode = sdg.getEntry(firstPair.getFirstNode());
            String bytecodeMethod = methodNode.getBytecodeMethod();
            SDGMethod method = ana.getProgram().getMethod(bytecodeMethod);
            List<JavaType> argumentTypes = method.getSignature().getArgumentTypes();
            String types = "";
            for (JavaType currType : argumentTypes) {
                String toHRString = currType.toHRString();
                int lastIndexOfDot = toHRString.lastIndexOf(".");
                toHRString = toHRString.substring(lastIndexOfDot + 1, toHRString.length());
                types += toHRString + ",";
            }
            if (!types.isEmpty()) {
                types = types.substring(0, types.length() - 1);
            }
            String methodName = method.getSignature().getMethodName();
            String fullyQualifiedMethodName = method.getSignature().getFullyQualifiedMethodName();
            int classNameEndIndex = fullyQualifiedMethodName.lastIndexOf(".");
            String className = fullyQualifiedMethodName.substring(0, classNameEndIndex);
            boolean hasCallGraphMethod =
                    callGraph.hasMethodFor(className, methodName, types);
            StaticCGJavaMethod callGraphMethod =
                    callGraph.getMethodFor(className, methodName, types);
            if (hasCallGraphMethod && isIndepOfLibs(callGraphMethod)) {
                summaryEdgesAndCorresJavaMethods.put(summaryEdge, callGraphMethod);
            }
        }
        edgesToCheck.clear();
    }

    public StaticCGJavaMethod getMethodCorresToSummaryEdge(SDGEdge e) {
        return summaryEdgesAndCorresJavaMethods.get(e);
    }

    private void putEdgesAndChopsInMap() {
        summaryEdgesAndContainingChops = new HashMap<>();
        for (ViolationChop vc : violationChops) {
            Collection<SDGEdge> summaryEdges = vc.getSummaryEdges();
            
            for (SDGEdge summaryEdge : summaryEdges) {
                if (summaryEdgesAndCorresJavaMethods.containsKey(summaryEdge)) {
                    if (summaryEdgesAndContainingChops.containsKey(summaryEdge)) {
                        summaryEdgesAndContainingChops.get(summaryEdge).add(vc);
                    } else {
                        ArrayList<ViolationChop> vcList = new ArrayList<>();
                        vcList.add(vc);
                        summaryEdgesAndContainingChops.put(summaryEdge, vcList);
                    }
                }
            }
        }
    }
    

    private ViolationChop createViolationChop(IViolation<SecurityNode> violationNode, SDG sdg) {
        ViolationPath violationPath = getViolationPath(violationNode);
        LinkedList<SecurityNode> violationPathList = violationPath.getPathList();
        SDGNode violationSource = violationPathList.get(0);
        SDGNode violationSink = violationPathList.get(1);
        ViolationChop c = new ViolationChop(violationSource, violationSink, sdg);
        chopToViolationMap.put(c, violationNode);
        return c;
    }

    private ViolationPath getViolationPath(IViolation<SecurityNode> v) {
        return ((ClassifiedViolation) v).getChops().iterator().next()
                .getViolationPathes().getPathesList().get(0);
    }

    public boolean allDisproved() {
        for (ViolationChop vc : violationChops) {
            if (!vc.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public boolean allCheckedOrDisproved() {
        for (ViolationChop vc : violationChops) {
            if (!vc.isEmpty()) {
                for (SDGEdge se : vc.getSummaryEdges()) {
                    if (!checkedEdges.contains(se)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    

    /**
     * 
     * @param e
     */
    public void removeEdge(SDGEdge e) {
        sdg.removeEdge(e);
        sortedEdgesToCheck.remove(e);
        listener.forEach((l) -> {
            l.disprovedEdge(e);
        });
        if (summaryEdgesAndContainingChops.get(e) != null) {
        	
        	
            summaryEdgesAndContainingChops.get(e).forEach((vc) -> {
            	if (!disprovenSummaryEdgesAndCorresChop.containsKey(e)) {
            		disprovenSummaryEdgesAndCorresChop.put(e, vc);
            	}
                vc.findSummaryEdges(sdg);
                //vc is empty if no path exists between source and sink -> remove violation
                if (vc.isEmpty()) {
                    violationChops.remove(vc);
                    listener.forEach((l) -> {
                        l.disprovedChop(vc);
                    });
                    
                    //violation checked -> remove from unchecked
                    IViolation<SecurityNode> violation = chopToViolationMap.get(vc);
                    uncheckedViolations.remove(violation);
                }
            });
        }
        summaryEdgesAndContainingChops.remove(e);
        summaryEdgesAndCorresJavaMethods.remove(e);
        if (sortedEdgesToCheck.isEmpty()) {
            prepareNextSummaryEdges();
        }
        
        //TODO: correct ? finished if all violations are closed ! 
        if (sortedEdgesToCheck.isEmpty()
                || (violationChops.isEmpty()
                && uncheckedViolations.isEmpty())) { //if no more violations to check than we are finished.
            listener.forEach((l) -> {
                l.disprovedAll();
            });
        }
    }

    public void checkedEdge(SDGEdge e) {
        checkedEdges.add(e);
        sortedEdgesToCheck.remove(e);
        if (sortedEdgesToCheck.isEmpty()) {
            prepareNextSummaryEdges();
        }
    }

    public SDGEdge nextSummaryEdge() {
        if (sortedEdgesToCheck.isEmpty()) {
            return null;
        } else {
            return sortedEdgesToCheck.get(0);
        }
    }

    ArrayList<ViolationChop> getChopsContaining(SDGEdge e) {
        return summaryEdgesAndContainingChops.get(e);
    }

    private boolean isIndepOfLibs(StaticCGJavaMethod m) {
        //TODO remove: just for debug
        boolean debug = false;
        if (debug) {
            return true;
        } else {
        
        String packageName = callGraph.getPackageName();
        try {
            StaticCGJavaClass contClass = m.getContainingClass();
            String classPackageName = contClass.getPackageString();
            classPackageName =
                    contClass.getPackageString() == null ?
                            "" : contClass.getPackageString();
            if (!classPackageName.startsWith(packageName)) {
                return false;
            }
            Set<StaticCGJavaMethod> allMethodsCalledByMethodRec = m.getCalledFunctionsRec();
//            for (StaticCGJavaMethod calledM : allMethodsCalledByMethodRec) {
//                if (!calledM.getContainingClass().getPackageString().startsWith(packageName)) { //NullPointerException lostInCast, !?
//                    if (!isKeYFeature(calledM)) {
//                        return false;
//                    }
//                }
//            }
            for (StaticCGJavaMethod calledM : allMethodsCalledByMethodRec) {
                StaticCGJavaClass cgJavaClass = calledM.getContainingClass();
                String packageString = cgJavaClass.getPackageString(); //if packageString is null -> no package in this class used
                if (!(packageString == null) && !packageString.startsWith(packageName)) { //NullPointerException lostInCast, !?
                    if (!isKeYFeature(calledM)) {
                        return false;
                    }
                }
            }
        } catch (Exception e) {
            return false;
        }
        return true;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ViolationsWrapper other = (ViolationsWrapper) obj;
        if (sortedEdgesToCheck.size() != other.sortedEdgesToCheck.size()) {
            return false;
        }
        for (int i = 0; i < sortedEdgesToCheck.size(); ++i) {
            if (!sortedEdgesToCheck.get(i).equals(other.sortedEdgesToCheck.get(i))) {
                return false;
            }
        }
        for (SDGEdge e : summaryEdgesAndCorresJavaMethods.keySet()) {
            if (!summaryEdgesAndCorresJavaMethods.get(e).equals(
                    other.summaryEdgesAndCorresJavaMethods.get(e))) {
                return false;
            }

        }

        return true;
    }

    private boolean isKeYFeature(StaticCGJavaMethod calledM) {
        String onlyClassName = calledM.getContainingClass().getOnlyClassName();
        for (String compatible : keyCompatibleJavaFeatures) {
            if (compatible.endsWith(onlyClassName)) {
                return true;
            }
        }
        return false;
    }

    public Collection<? extends IViolation<SecurityNode>> getUncheckedViolations() {
        return uncheckedViolations;
    }

    public Collection<? extends IViolation<SecurityNode>> getAllViolationsToCheck() {
    	allViolationsToCheck.forEach((v)-> {
//    		System.out.println(v);
    	});
        return allViolationsToCheck;
    }

    public Map<SDGEdge, ArrayList<ViolationChop>> getSummaryEdgesAndContainingChops() {
        return summaryEdgesAndContainingChops;
    }

    public Map<SDGEdge, StaticCGJavaMethod> getSummaryEdgesAndCorresJavaMethods() {
        return summaryEdgesAndCorresJavaMethods;
    }
    
    public Collection<ViolationChop> getAllViolationChops() {
    	return violationChops;
    }
    
    public SDG getSdg() {
        return sdg;
    }
    
    public boolean isCheckedEdge(SDGEdge e) {
    	return checkedEdges.contains(e);
    }
    
    public void resetCheckedEdges() {
    	checkedEdges.clear();
    }

    public Map<SDGEdge, ViolationChop> getChopAndCorrespondingDisprovenSummaryEdges() {
    	return disprovenSummaryEdgesAndCorresChop;
	}
    
}
