package joanakey;

import java.util.Collection;

import edu.kit.joana.api.IFCAnalysis;
import edu.kit.joana.api.IFCType;
import edu.kit.joana.api.annotations.AnnotationType;
import edu.kit.joana.api.annotations.cause.AnnotationCause;
import edu.kit.joana.api.annotations.cause.UnknownCause;
import edu.kit.joana.api.sdg.SDGMethod;
import edu.kit.joana.api.sdg.SDGProgram;
import edu.kit.joana.api.sdg.SDGProgramPart;
import edu.kit.joana.ifc.sdg.core.SecurityNode;
import edu.kit.joana.ifc.sdg.core.conc.PossibilisticNIChecker;
import edu.kit.joana.ifc.sdg.core.violations.IViolation;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import edu.kit.joana.ifc.sdg.lattice.IStaticLattice;
import joanakey.annotations.SDGNodeAnnotation;
import joanakey.annotations.SDGNodeAnnotationManager;

/**
 * Subclass of {@link IFCAnalysis} which also allows to annotate {@link SDGNode}s instead of only {@link SDGProgramPart}s.
 * @author muessig.
 */
public class SDGAnalysis extends IFCAnalysis {
    
	/**
	 * @param program
	 */
	public SDGAnalysis(SDGProgram program) {
        this(program, stdLattice);
	}


    /**
	 * @param program
	 * @param stdlattice
	 */
	public SDGAnalysis(SDGProgram program, IStaticLattice<String> stdlattice) {
		super(program, stdLattice);
		//setProgramWithAnnotationManager(program);

	}
	
	@Override
	public void setProgram(SDGProgram program) {
        if (program == null) {
            throw new IllegalArgumentException("program must not be null!");
        }
        this.program = program;
        super.setProgram(program);
        annManager = new SDGNodeAnnotationManager(program, this);
        if (this.ifc != null) {
            this.ifc.setSDG(this.program.getSDG());
        } else {
            this.ifc = new PossibilisticNIChecker(this.program.getSDG(), secLattice);
        }
    }
	
	
	@Override
	public Collection<? extends IViolation<SecurityNode>> doIFC(IFCType ifcType) {
        assert ifc != null && ifc.getSDG() != null && ifc.getLattice() != null;
        this.annManager.applyAllAnnotations();
//        this.ifc = new SlicingBasedIFC(this.program.getSDG(), secLattice, new I2PForward(this.program.getSDG()), new I2PBackward(this.program.getSDG()));
//        super.doIFC(ifcType);
        setIFCType(ifcType);
        long time = 0L;
        time = System.currentTimeMillis();
        Collection<? extends IViolation<SecurityNode>> vios = this.ifc.checkIFlow(); 
        time = System.currentTimeMillis() - time;
//        debug.outln(String.format("IFC Analysis took %d ms.", time));
        annManager.unapplyAllAnnotations();
        return vios;
    }
	
	
	public void addAnnotation(SDGNodeAnnotation annotation) { 
	    annManager.addAnnotation(annotation);
	}
	
	 
	public void addSourceAnnotation(SDGNode sdgNode, String level) {
	    SDGProgramPart toMark = program.findCoveringProgramPart(sdgNode);
	    System.out.format("sdgNode: %s, PPart: %s, SecLevel: %s\n", sdgNode, toMark, level);
	    if (toMark == null) {
	        boolean candidatesFound = program.collectCoveringCandidates(sdgNode).iterator().hasNext();
	        if (candidatesFound) {
	            System.out.println("Warning: part may not be correct for node :" + sdgNode.getId() );
	            toMark = program.collectCoveringCandidates(sdgNode).iterator().next();
	        }
	    }
	    try {
	        addSourceAnnotation(sdgNode, toMark, level, null, UnknownCause.INSTANCE);
	    } catch(IllegalArgumentException e) {
	        //TODO: print something on gui.
	        System.out.println("no ProgramPart found for node :" + sdgNode.getId());
	    }
    }

    public void addSourceAnnotation(SDGNode sdgNode, String level, AnnotationCause cause) {
        SDGProgramPart toMark = program.findCoveringProgramPart(sdgNode);
        addSourceAnnotation(sdgNode, toMark, level, null, cause);
    }

    public void addSinkAnnotation(SDGNode sdgNode, String level) {
        SDGProgramPart toMark = program.findCoveringProgramPart(sdgNode); 
        System.out.format("sdgNode: %s, PPart: %s, SecLevel: %s\n", sdgNode, toMark, level);
        if (toMark == null) {
            boolean candidatesFound = program.collectCoveringCandidates(sdgNode).iterator().hasNext();
            if (candidatesFound) {
                System.out.println("Warning: part may not be correct for node :" + sdgNode.getId() );
                toMark = program.collectCoveringCandidates(sdgNode).iterator().next();
            }
        }
        try {
            addSinkAnnotation(sdgNode, toMark, level, null, UnknownCause.INSTANCE);
        } catch(IllegalArgumentException e) {
            //TODO: print something on gui.
            System.out.println("no ProgramPart found for node :" + sdgNode.getId());
        }
    }
    
    public void addSinkAnnotation(SDGNode sdgNode, String level, AnnotationCause cause) {
        SDGProgramPart toMark = program.findCoveringProgramPart(sdgNode);
        addSinkAnnotation(sdgNode, toMark, level, null, cause);
    }
    
    private void addSourceAnnotation(SDGNode sdgNode, SDGProgramPart toMark, String level, SDGMethod context, AnnotationCause cause) {
        addAnnotation(new SDGNodeAnnotation(sdgNode, AnnotationType.SOURCE, level, toMark, context, cause));
    }

    private void addSinkAnnotation(SDGNode sdgNode, SDGProgramPart toMark, String level, SDGMethod context, AnnotationCause cause) {
        addAnnotation(new SDGNodeAnnotation(sdgNode, AnnotationType.SINK, level, toMark, context, cause));
    }

}
