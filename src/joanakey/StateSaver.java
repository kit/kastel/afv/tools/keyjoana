package joanakey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import gnu.trove.map.TIntIntMap;
import joanakey.persistence.PersistentCGNode;
import joanakey.persistence.PersistentLocalPointerKey;

/**
 * Simple class to save intermediate results of SDG constructions (i.e.,
 * points-to and call graph) for later use.
 */

public class StateSaver {

    public static final String KEY = "key";
    public static final String VALUES = "values";
    public static final String SDG_NODE = "sdg_node";
    public static final String CG_NODE = "cg_node";
    public static final String FORMAL_INS_TO_PERS_CG = "formal_ins_to_pers_cg";
    public static final String LOCAL_POINTER_KEYS = "localPointerKeys";
    public static final String DISJUNCT_POINTS_TO = "disjunctPointsTo";
    public static final String ENTRY_NODES_TO_CG = "entryNodesToCG";
    public static final String SDG_NODES_TO_SSA = "nodeToSSA";
    public static final String I_INDEX = "iIndex";

    private CallGraph callGraph;
    private PointerAnalysis<? extends InstanceKey> pointerAnalysis;
    private List<PersistentLocalPointerKey> persistentLocalPointerKeys = new ArrayList<>();
    private List<PersistentCGNode> persistentCGNodes = new ArrayList<>();
    private Map<Integer, PersistentCGNode> cgNodeIdToPersistentCGNodes = new HashMap<>();
    private Map<CGNode, PersistentCGNode> cgNodesToPersistentCGNodes = new HashMap<>();
    private Map<PersistentLocalPointerKey, List<PersistentLocalPointerKey>> disjunctPointsToSets
            = new HashMap<>();
    private HashMap<SDGNode, PersistentCGNode> formalInsToPersistentCGNodes = new HashMap<>();
    private TIntIntMap node2CG = null;
    private TIntIntMap node2SSA = null;
   
    public CallGraph getCallGraph() {
		return callGraph;
	}

	public PointerAnalysis<? extends InstanceKey> getPointerAnalyis() {
		return pointerAnalysis;
	}

	public String getSaveString() {	

        StringBuilder created = new StringBuilder();
        created.append("{");

        created.append("\"" + FORMAL_INS_TO_PERS_CG + "\" : [\n");

        for (SDGNode n : formalInsToPersistentCGNodes.keySet()) {
            PersistentCGNode get = formalInsToPersistentCGNodes.get(n);
            try {
                created.append("{ \"" + SDG_NODE + "\" : " + n.getId() +
                        ", \"" + CG_NODE + "\" : {" +
                        get.generateSaveString() + "}},\n");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

//        if (created.lastIndexOf("[") == created.length() - 1) {
//            created.delete(created.length() - 2, created.length());
//        } else {
        	created.append("],\n");
//        }


        created.append("\"" + CG_NODE + "s" + "\" : [").append("\n");
        for (PersistentCGNode persistentCGNode : persistentCGNodes) {
        	try {
        		if (!(persistentCGNode.getIR() == null)) { 
        			String current_CG_Node = persistentCGNode.generateSaveString();
        			created.append("{");
        			created.append(current_CG_Node);
        			created.append("},\n");	
        		}
        		
        	} catch (NullPointerException e) {
        		e.printStackTrace();
        	}
        }
//        if (created.lastIndexOf("[") == created.length() - 1) {
//            created.delete(created.length() - 2, created.length());
//        } else {
        	created.append("],\n");
//        }

        created.append("\"" + LOCAL_POINTER_KEYS + "\" : [");
        for (PersistentLocalPointerKey persistentLocalPointerKey
                : persistentLocalPointerKeys) {
            created.append("{");
            created.append(persistentLocalPointerKey.generateSaveString());
            created.append("\n");
            created.append("},\n");
        }
//        if (created.lastIndexOf("[") == created.length() - 1) {
//            created.delete(created.length() - 2, created.length());
//        } else { 
        	created.append("],\n");
//        }

        created.append("\"" + DISJUNCT_POINTS_TO + "\" : [");

        disjunctPointsToSets.forEach((k, l) -> {
            created.append("{");
            created.append("\"" + KEY + "\" : " +
                           k.getId()).append(", \"" + VALUES + "\" : [ ");
            l.forEach((t) -> {
                created.append(t.getId()).append(", ");
            });
            if (created.lastIndexOf("[") != created.length() - 1) {
                created.delete(created.length() - 2, created.length());
            }
            created.append("]\n");
            created.append("},\n");
        });
//        if (created.lastIndexOf("[") == created.length() - 1) {
//            created.delete(created.length() - 2, created.length());
//        } else {
        	created.append("],\n");
//        }
        	
        created.append("\"" + ENTRY_NODES_TO_CG + "\" : [");
        for (int nodeId : node2CG.keys()) {
        	created.append("{ \"" + SDG_NODE + "\" : " + nodeId +
                        ", \"" + CG_NODE + "\" : " +
                        node2CG.get(nodeId) + "},\n");
        }
        created.append("],\n");

        created.append("\"" + SDG_NODES_TO_SSA + "\" : [");
        for (int nodeId : node2SSA.keys()) {
        	created.append("{ \"" + SDG_NODE + "\" : " + nodeId +
                        ", \"" + I_INDEX + "\" : " +
                        node2SSA.get(nodeId) + "},\n");
        }
        created.append("]\n");
        
        created.append("}");
        
        
        
        return created.toString();
    }

    public boolean pointsToSetsAreDisjunct(PersistentLocalPointerKey n1,
                                           PersistentLocalPointerKey n2) {
        return true;
    }

    public List<PersistentLocalPointerKey>
            getDisjunctLPKs(PersistentLocalPointerKey persistentLocalPointerKey) {
        return disjunctPointsToSets.get(persistentLocalPointerKey);
    }
    
    public PersistentCGNode getNode(int nodeID) {
        return cgNodeIdToPersistentCGNodes.get(nodeID);
    }

    public List<PersistentLocalPointerKey>
                    getPersistentLocalPointerKeys(PersistentCGNode cGNode) {
        List<PersistentLocalPointerKey> created = new ArrayList<>();
        for (PersistentLocalPointerKey persistentLocalPointerKey
                : persistentLocalPointerKeys) {
            if (persistentLocalPointerKey.getNode().equals(cGNode)) {
                created.add(persistentLocalPointerKey);
            }
        }
        return created;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StateSaver other = (StateSaver) obj;

        if (persistentCGNodes.size() != other.persistentCGNodes.size()) {
            return false;
        }

        for (int i = 0; i < persistentCGNodes.size(); ++i) {
            if (!persistentCGNodes.get(i)
                    .equals(other.persistentCGNodes.get(i))) {
                return false;
            }
        }

        for (Integer k : cgNodeIdToPersistentCGNodes.keySet()) {
            if (!cgNodeIdToPersistentCGNodes.get(k)
                    .equals(other.cgNodeIdToPersistentCGNodes.get(k))) {
                return false;
            }
        }

        if (persistentLocalPointerKeys.size()
                != other.persistentLocalPointerKeys.size()) {
            return false;
        }

        for (int i = 0; i < persistentLocalPointerKeys.size(); ++i) {
            if (!persistentLocalPointerKeys.get(i)
                    .equals(other.persistentLocalPointerKeys.get(i))) {
                return false;
            }
        }

        for (PersistentLocalPointerKey k : disjunctPointsToSets.keySet()) {
            if (!disjunctPointsToSets.get(k)
                    .equals(other.disjunctPointsToSets.get(k))) {
                return false;
            }
        }

        return true;
    }

    public PersistentCGNode getCGNodeForFormalIn(SDGNode methodNode) {
        return formalInsToPersistentCGNodes.get(methodNode);
    }
    
    public void setPointerAnalysis(PointerAnalysis<? extends InstanceKey> p) {
        this.pointerAnalysis = p;
    }
    
    public void setSDGEntry2CGMap(TIntIntMap entry2CG) {
    	this.node2CG = entry2CG;
    }
    
    public void setNode2SSA(TIntIntMap node2SSA) {
    	this.node2SSA = node2SSA;
    }
    
    public void setCallGraph(CallGraph callGraph) {
        this.callGraph = callGraph;
    }
    
    public Map<CGNode, PersistentCGNode> getCgNodesToPersistentCGNodes() {
        return cgNodesToPersistentCGNodes;
    }

    public List<PersistentLocalPointerKey> getPersistentLocalPointerKeys() {
        return persistentLocalPointerKeys;
    }

    public Map<PersistentLocalPointerKey, List<PersistentLocalPointerKey>> getDisjunctPointsToSets() {
        return disjunctPointsToSets;
    }

    public List<PersistentCGNode> getPersistentCGNodes() {
        return persistentCGNodes;
    }

    public Map<Integer, PersistentCGNode> getCgNodeIdToPersistentCGNodes() {
        return cgNodeIdToPersistentCGNodes;
    }

    public HashMap<SDGNode, PersistentCGNode> getFormalInsToPersistentCGNodes() {
        return formalInsToPersistentCGNodes;
    }

}
