/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joanakey.persistence;

/**
 *
 * @author holger
 */
public class JsonHelper {

	/**
	 * generate a key value pair and append it to the given stringBuilder object. The value is appended with quotes as String ("value").
	 * @param stringBuilder object to which you want append the key, value pair
	 * @param key json key 
	 * @param value as string
	 */
    public static void addJsonStringToStringBuilder(StringBuilder stringBuilder,
                                                    String key, String value) {
        stringBuilder.append("\"" + key + "\" : " + "\"" + value + "\"");
    }

    /**
	 * generate a key value pair and append it to the given stringBuilder object. The value is appended without quotes.
	 * @param stringBuilder object to which you want append the key, value pair
	 * @param key json key 
	 * @param value as string
     */
    public static void addKeyValueToJsonStringbuilder(StringBuilder stringBuilder,
                                                      String key, String value) {
        stringBuilder.append("\"" + key + "\" : " + value);
    }


    /**
     * generate a key value pair and append it to the given stringBuilder object. The value is appended with curly brackets.
     * @param stringBuilder object to which you want to append the key, value pair.
     * @param key json key
     * @param value as string
     */
    public static void addJsonObjValueToStringBuiler(StringBuilder stringBuilder,
                                                     String key, String value) {
        stringBuilder.append("\"" + key + "\" : {\n");
        stringBuilder.append(value);
        stringBuilder.append("}\n");
    }

}
