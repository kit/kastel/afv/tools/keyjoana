/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joanakey.persistence;

import java.util.List;

import org.json.JSONObject;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.propagation.LocalPointerKey;

//import net.bytebuddy.asm.Advice.This;

/**
 *
 * @author hklein
 */
public class PersistentCGNode {
    private PersistentIR persistentIR;
    private int uniqueId;    
    private int cgNodeId;

    private static final String ID ="id";
    private static final String CG_NODE_ID ="cg_node_id";
    private static final String IR ="ir";
    
    /**
     * generate a {@link PersistentCGNode} object from the given jsonObj
     * @param jsonObj which contains the call graph node information
     * @return {@link PersistentCGNode}
     */
    public static PersistentCGNode generateFromJsonObj(JSONObject jsonObj) {
        PersistentCGNode node =
                new PersistentCGNode(jsonObj.getInt(ID), jsonObj.getInt(CG_NODE_ID));
        node.persistentIR =
                PersistentIR.generateFromJsonObj(jsonObj.getJSONObject(IR), node.getUniqueId());
        return node;
    }

    public PersistentCGNode(int uniqueId) {
        this.uniqueId = uniqueId;
    }
        
    private PersistentCGNode(int id, int cgNodeId) {
        this.uniqueId = id;
        this.cgNodeId = cgNodeId;
    }
        
    public void createPersistentIR(CGNode n, List<LocalPointerKey> localPointerKeys) {
        persistentIR = new PersistentIR(localPointerKeys, n, uniqueId);
    }
    
    /**
     * generate a string which contain the information of this {@link PersistentCGNode}.
     * @return String containing information about this object (ID, CG Node ID, IR)
     */
    public String generateSaveString() { 
        return "\"" + ID + "\" : " + uniqueId + ", "
                + "\""+ CG_NODE_ID + "\" :" + cgNodeId + ", " +
                "\"" + IR + "\" : {\n" + persistentIR.generateSaveString() + "\n}";
    }
        
    public PersistentIR getIR() {
        return persistentIR;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public int getCgNodeId() {
        return cgNodeId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PersistentCGNode other = (PersistentCGNode) obj;
        if (this.uniqueId != other.uniqueId) {
            return false;
        }
        if(cgNodeId != other.cgNodeId) {
            return false;
        }
        if(!persistentIR.equals(other.persistentIR)) {
            return false;
        }
        return true;
    }

    public void setCgNodeId(int cgNodeId) {
        this.cgNodeId = cgNodeId;
    }
}
