/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joanakey.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.kit.joana.ifc.sdg.core.SecurityNode;
import edu.kit.joana.ifc.sdg.core.violations.ClassifiedViolation;
import edu.kit.joana.ifc.sdg.core.violations.IViolation;
import edu.kit.joana.ifc.sdg.core.violations.paths.ViolationPath;
import edu.kit.joana.ifc.sdg.core.violations.paths.ViolationPathes;
import edu.kit.joana.ifc.sdg.graph.SDG;

/**
 *
 * @author hklein
 */
public class ViolationsSaverLoader {

    private static final String VIOLATIONS = "violations";
    private static final String SOURCE_ID = "source_id";
    private static final String SINK_ID = "sink_id";
    private static final String ATTACKER_LEVEL = "attacker_level";
    private static final String VIOL_PATHS = "violpaths";
    private static final String NODE_IDS = "node_ids";

    /**
     * generate a string which contain information of the given {@link IViolation} objects.
     * @param violations you want to generate the String for.
     * @return String
     */
    public static String generateSaveString(Collection<? extends IViolation<SecurityNode>> violations) {
        StringBuilder created = new StringBuilder();

        created.append("\"" + VIOLATIONS + "\" : [");
        for (IViolation<SecurityNode> v : violations) {
            created.append("{");
            ClassifiedViolation classifiedViol = (ClassifiedViolation) v;
            int sourceId = classifiedViol.getSource().getId();
            created.append("\"" + SOURCE_ID + "\" : ").append(sourceId).append(", ");
            int sinkId = classifiedViol.getSink().getId();
            created.append("\"" + SINK_ID + "\" : ").append(sinkId).append(", ");
            String attackerLevel = classifiedViol.getAttackerLevel();
            created.append("\"" + ATTACKER_LEVEL + "\" : ")
                .append("\"" + attackerLevel + "\"").append(", ");

            ViolationPathes violationPathes = classifiedViol.getViolationPathes();
            created.append("\"" + VIOL_PATHS + "\" : [");
            for (ViolationPath p : violationPathes.getPathesList()) {
                created.append("{");
                LinkedList<SecurityNode> pathList = p.getPathList();
                created.append("\"" + NODE_IDS + "\" : [");
                for (SecurityNode n : pathList) {
                    int secNodeId = n.getId();
                    created.append(secNodeId).append(", ");
                }
                if (created.lastIndexOf("[") != created.length() - 1) {
                    created.replace(created.length() - 2, created.length(), "");
                }
                created.append("]");
                created.append("},\n");
            }
            if (created.lastIndexOf("[") != created.length() - 1) {
                created.replace(created.length() - 2, created.length(), "");
            }
            created.append("]");
            created.append("},\n");
        }
        if (created.lastIndexOf("[") != created.length() - 1) {
            created.replace(created.length() - 2, created.length(), "");
        }
        created.append("]");
        return created.toString();
    }

    /**
     * generate a collection of {@link ClassifiedViolation}s from the given {@link JSONArray}
     * @param violationsArr, containg multiple {@link JSONObjects} with violation informations.
     * @param sdg Graph, used to get the corresponding sdg nodes
     * @return 
     */
    public static Collection<ClassifiedViolation> generateFromJSON(JSONArray violationsArr, SDG sdg) {
        Collection<ClassifiedViolation> created = new ArrayList<>();

        for (int i = 0; i < violationsArr.length(); ++i) {
            JSONObject currentViolJsonObj = violationsArr.getJSONObject(i);
            int currentSourceId = currentViolJsonObj.getInt(SOURCE_ID);
            SecurityNode sourceNode = (SecurityNode) sdg.getNode(currentSourceId);
            int currentSinkId = currentViolJsonObj.getInt(SINK_ID);
            SecurityNode sinkNode = (SecurityNode) sdg.getNode(currentSinkId);
            String currentAttackerLvl = currentViolJsonObj.getString(ATTACKER_LEVEL);
            
            JSONArray pathesArray = currentViolJsonObj.getJSONArray(VIOL_PATHS);
            ViolationPathes pathes = new ViolationPathes();
            
            for(int j = 0; j < pathesArray.length(); ++j) {
                JSONArray nodeids = pathesArray.getJSONObject(j).getJSONArray(NODE_IDS);
                LinkedList<SecurityNode> list = new LinkedList<>();
                for(int k = 0; k < nodeids.length(); ++k) {
                    SecurityNode secNode = (SecurityNode) sdg.getNode(nodeids.getInt(k));
                    list.add(secNode);
                }
                pathes.add(new ViolationPath(list));
            }
            
            ClassifiedViolation createdViolation =
                    ClassifiedViolation.createViolation(sinkNode, sourceNode, pathes,
                                                        currentAttackerLvl);
            created.add(createdViolation);
        }
        
        return created;
    }
}
