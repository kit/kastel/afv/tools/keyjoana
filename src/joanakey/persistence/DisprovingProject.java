/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joanakey.persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import org.json.JSONObject;

import com.ibm.wala.ipa.callgraph.CallGraph;

import edu.kit.joana.api.sdg.SDGProgram;
import edu.kit.joana.ifc.sdg.core.SecurityNode;
import edu.kit.joana.ifc.sdg.core.violations.IViolation;
import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGSerializer;
import edu.kit.joana.ifc.sdg.mhpoptimization.MHPType;
import joanakey.JoanaAndKeYCheckData;
import joanakey.StateSaver;
import joanakey.StateSaverCGConsumerBuilder;
import joanakey.customlistener.SummaryEdgeAndMethodToCorresData;
import joanakey.javaforkeycreator.JavaForKeYCreator;
import joanakey.loopinvarianthandling.LoopInvPosAndMethBodExtracter;
import joanakey.staticCG.JCallGraph;
import joanakey.violations.ViolationsWrapper;

/**
 *
 * @author hklein
 */
public class DisprovingProject {

    private static final String DOT_JAR = ".jar";
    private static final String DOT_PDG = ".pdg";

    private static final String SAVEDATA = "savedata";
    private static final String PATH_TO_JAR = "path_to_jar";
    private static final String PATH_TO_JAVA_SRC = "path_to_java_source";
    private static final String PATH_TO_SDG = "path_to_sdg";
    private static final String STATE_SAVER = "state_saver";
    private static final String ENTRY_METHOD = "entry_method";
    private static final String EDGES_METHODS_TO_VALUES = "edges_methods_to_values";
    private static final String VIOLATION_WRAPPER = "violation_wrapper";

    private String pathToSDG;
    private String pathToJar;
    private String pathToJava;
    private String entryMethod;
    private StateSaver stateSaver;
    private JCallGraph callGraph;
    private ViolationsWrapper violationsWrapper;
    private SDG sdg;
    private SummaryEdgeAndMethodToCorresData summaryEdgeToCorresData;
    private LoopInvPosAndMethBodExtracter loopInvPosAndMethodBodyExtracter;

    private DisprovingProject() {
    }

    public String getPathToSDG() {
        return pathToSDG;
    }

    public String getPathToJar() {
        return pathToJar;
    }

    public String getPathToJava() {
        return pathToJava;
    }

    public SDG getSdg() {
        return sdg;
    }

    public StateSaver getStateSaver() {
        return stateSaver;
    }

    public JCallGraph getCallGraph() {
        return callGraph;
    }
        
    public ViolationsWrapper getViolationsWrapper() {
        return violationsWrapper;
    }

    public String getProjName() {
        return pathToJar.substring(pathToJar.lastIndexOf("/") + 1, pathToJar.length() - DOT_JAR.length());
    }

    public void saveSDG() throws FileNotFoundException, IOException {
        File f = File.createTempFile(getProjName(), DOT_PDG, new File(SAVEDATA));
        if (f.exists()) {
            f.delete();
        }
        f.getParentFile().mkdirs();
        PrintWriter out = new PrintWriter(f);
        SDGSerializer.toPDGFormat(sdg, out);
        pathToSDG = SAVEDATA + "/" + f.getName();
    }

    public String generateSaveString() {
        StringBuilder created = new StringBuilder();
        created.append("{");
        JsonHelper.addJsonStringToStringBuilder(created, PATH_TO_JAR, pathToJar);
        created.append(",\n");
        JsonHelper.addJsonStringToStringBuilder(created, PATH_TO_JAVA_SRC, pathToJava);
        created.append(",\n");
        JsonHelper.addJsonStringToStringBuilder(created, PATH_TO_SDG, pathToSDG);
        created.append(",\n");
        JsonHelper.addJsonStringToStringBuilder(created, ENTRY_METHOD, entryMethod);
        created.append(",\n");
        JsonHelper.addKeyValueToJsonStringbuilder(created, STATE_SAVER, stateSaver.getSaveString());
        created.append(",\n");
        JsonHelper.addKeyValueToJsonStringbuilder(created, EDGES_METHODS_TO_VALUES,
                summaryEdgeToCorresData.generateSaveString(violationsWrapper));
        created.append(",\n");
        JsonHelper.addKeyValueToJsonStringbuilder(created, VIOLATION_WRAPPER,
                violationsWrapper.generateSaveString());

        created.append("}");
        return created.toString();
    }
    
    public static DisprovingProject generateFromSavestring(String s) throws IOException {
        DisprovingProject disprovingProject = new DisprovingProject();
        JSONObject jSONObject = new JSONObject(s);
        String pathToJava = jSONObject.getString(PATH_TO_JAVA_SRC);
        String pathToJar = jSONObject.getString(PATH_TO_JAR);
        String pathToSdg = jSONObject.getString(PATH_TO_SDG);
        String entryMethod = jSONObject.getString(ENTRY_METHOD);
        JSONObject statesaveJsonObj = jSONObject.getJSONObject(STATE_SAVER);
        JSONObject violWrapperJsonObj = jSONObject.getJSONObject(VIOLATION_WRAPPER);
        disprovingProject.pathToJava = pathToJava;
        disprovingProject.pathToSDG = pathToSdg;
        disprovingProject.pathToJar = pathToJar;
        disprovingProject.entryMethod = entryMethod;
//        disprovingProject.sdg = SDGProgram.loadSDG(pathToSdg).getSDG(); 
        disprovingProject.sdg = SDGProgram.loadSDG(pathToSdg, MHPType.PRECISE).getSDG(); 
        disprovingProject.callGraph = new JCallGraph();
        disprovingProject.callGraph.generateCG(new File(pathToJar));
        StateSaverCGConsumerBuilder builder = new StateSaverCGConsumerBuilder();
        
        disprovingProject.stateSaver =
                builder.buildSateSaverFromJson(statesaveJsonObj, disprovingProject.sdg,
                		pathToJar, entryMethod); //FIXME 
//        disprovingProject.stateSaver =
//                stateSaver.generateFromJson(statesaveJsonObj, disprovingProject.sdg);
        disprovingProject.violationsWrapper = ViolationsWrapper.generateFromJsonObj(
                violWrapperJsonObj, disprovingProject.sdg, disprovingProject.callGraph);
        disprovingProject.loopInvPosAndMethodBodyExtracter = new LoopInvPosAndMethBodExtracter();
        disprovingProject.loopInvPosAndMethodBodyExtracter.findAllLoopPositionsAndMethodBodies(
                disprovingProject.violationsWrapper.getSummaryEdgesAndCorresJavaMethods().values(),
                disprovingProject.pathToJava);
        disprovingProject.summaryEdgeToCorresData
                = SummaryEdgeAndMethodToCorresData.generateFromJson(
                        jSONObject.getJSONObject("edges_methods_to_values"),
                        disprovingProject.callGraph, disprovingProject.sdg);
        disprovingProject.violationsWrapper.addListener(disprovingProject.summaryEdgeToCorresData);
        return disprovingProject;
    }

    public static DisprovingProject generateFromCheckdata(JoanaAndKeYCheckData checkData)
            throws IOException {
        DisprovingProject disprovingProject = new DisprovingProject();
        disprovingProject.pathToJar = checkData.getPathToJar();
        disprovingProject.pathToJava = checkData.getPathToJavaFile();
        disprovingProject.stateSaver = checkData.getStateSaver();
        
        disprovingProject.sdg = checkData.getAnalysis().getProgram().getSDG();
        disprovingProject.callGraph = new JCallGraph();
        disprovingProject.callGraph.generateCG(new File(disprovingProject.pathToJar));
        disprovingProject.entryMethod = checkData.getEntryMethod().toString();
        if (disprovingProject.getCallGraph() == null) {
        	disprovingProject.stateSaver.setCallGraph((CallGraph) disprovingProject.callGraph); //Different types for call graphs
        	//remove if cast cause errors !
        }
        
        checkData.addAnnotations();
        Collection<? extends IViolation<SecurityNode>> viols = checkData.getAnalysis().doIFC();
       
		disprovingProject.violationsWrapper = new ViolationsWrapper(viols, disprovingProject.sdg,
				checkData.getAnalysis(), disprovingProject.callGraph);

		disprovingProject.loopInvPosAndMethodBodyExtracter = new LoopInvPosAndMethBodExtracter();
		disprovingProject.loopInvPosAndMethodBodyExtracter.findAllLoopPositionsAndMethodBodies(
				disprovingProject.violationsWrapper.getSummaryEdgesAndCorresJavaMethods().values(),
				disprovingProject.pathToJava);
		disprovingProject.summaryEdgeToCorresData = new SummaryEdgeAndMethodToCorresData(
				disprovingProject.violationsWrapper.getSummaryEdgesAndCorresJavaMethods(), disprovingProject.sdg,
				new JavaForKeYCreator(checkData.getPathToJavaFile(), disprovingProject.callGraph, disprovingProject.sdg,
						disprovingProject.stateSaver));

		disprovingProject.violationsWrapper.addListener(disprovingProject.summaryEdgeToCorresData);

		return disprovingProject;
    }
         
    public SummaryEdgeAndMethodToCorresData getSummaryEdgeToCorresData() {
        return summaryEdgeToCorresData;
    }

}
