package joanakey;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.LocalPointerKey;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.callgraph.propagation.PointerKey;
import com.ibm.wala.ipa.callgraph.pruned.ApplicationLoaderPolicy;
import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.util.CancelException;
import com.ibm.wala.util.graph.GraphIntegrity.UnsoundGraphException;
import com.ibm.wala.util.intset.OrdinalSet;

import edu.kit.joana.api.sdg.SDGBuildPreparation;
import edu.kit.joana.api.sdg.SDGConfig;
import edu.kit.joana.api.sdg.SDGProgram;
import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGEdge;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import edu.kit.joana.ifc.sdg.graph.SDGNodeTuple;
import edu.kit.joana.ifc.sdg.mhpoptimization.MHPType;
import edu.kit.joana.util.Stubs;
import edu.kit.joana.wala.core.CGConsumer;
import edu.kit.joana.wala.core.NullProgressMonitor;
import edu.kit.joana.wala.core.SDGBuilder;
import edu.kit.joana.wala.core.SDGBuilder.CGResult;
import edu.kit.joana.wala.core.SDGBuilder.SDGBuilderConfig;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.hash.TIntIntHashMap;
import joanakey.persistence.PersistentCGNode;
import joanakey.persistence.PersistentLocalPointerKey;

/**
 * 
 * 
 *
 */
public class StateSaverCGConsumerBuilder implements StateSaverBuilder, CGConsumer {

//    private StateSaver stateSaver;
    private CallGraph callGraph;
    private PointerAnalysis<? extends InstanceKey> pointerAnalysis;
    
    
    @Override
    public void consume(CallGraph callGraph, PointerAnalysis<? extends InstanceKey> pointerAnalysis) {
        this.callGraph = callGraph;
        this.pointerAnalysis = pointerAnalysis;
    }
    

    /**
     * generate a StateSaver without generating a new SDGProgram. 
     * @param sdg the {@link SDG} you want to use to generate a {@link StateSaver}
     * @param pathToJar 
     * @param entryMethod
     * @return the {@link StateSaver} object
     */
    @Override
    public StateSaver buildStateSaver(SDG sdg, String pathToJar, String entryMethod) {
        StateSaver saver = new StateSaver();
        if (callGraph == null || pointerAnalysis == null) {
        	try {
				generateCallGraphAndPTA(pathToJar, entryMethod);
			} catch (ClassHierarchyException | UnsoundGraphException | CancelException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        saver.setCallGraph(callGraph);
        saver.setPointerAnalysis(pointerAnalysis);
//        generatePersistenceStructures(sdg, saver); //necessary ?
        try {
            generatePersistenceStructures(sdg, saver);
        } catch (NullPointerException e) {
        	System.out.print(e.getStackTrace());
        }
        return saver;
    }
    
    
    private void generatePersistenceStructures(SDG sdg, StateSaver saver) {
        ArrayList<LocalPointerKey> localPointerKeys = new ArrayList<>();

        int id = 0;
        for (PointerKey pk : pointerAnalysis.getPointerKeys()) { 
            if (pk instanceof LocalPointerKey) {
                LocalPointerKey localPointerKey = (LocalPointerKey) pk;
                if (!localPointerKey.isParameter()) {
                    continue;
                }
                CGNode corresCgNode = localPointerKey.getNode();
                localPointerKeys.add(localPointerKey);
                if (!saver.getCgNodesToPersistentCGNodes().containsKey(corresCgNode)) {
                    PersistentCGNode persistentCGNode = new PersistentCGNode(id);
                    saver.getCgNodesToPersistentCGNodes().put(corresCgNode, persistentCGNode);
                    saver.getPersistentCGNodes().add(persistentCGNode);
                    id++;
                }
            }
        }

        Set<SDGEdge> edgeSet = sdg.edgeSet();
        for (SDGEdge e : edgeSet) {
            if (e.getKind() == SDGEdge.Kind.SUMMARY) {
                SDGNode actualInNode = e.getSource();
                SDGNode actualOutNode = e.getTarget();
                Collection<SDGNodeTuple> formalNodePairs =
                        sdg.getAllFormalPairs(actualInNode, actualOutNode);
                for (SDGNodeTuple formalNodeTuple : formalNodePairs) {
                    SDGNode formalInNode = formalNodeTuple.getFirstNode();
                    SDGNode methodNode = sdg.getEntry(formalInNode);
                    int cgNodeId = sdg.getCGNodeId(methodNode);
                    CGNode cgNode = callGraph.getNode(cgNodeId);
                    PersistentCGNode corresPersCGNode =
                            saver.getCgNodesToPersistentCGNodes().get(cgNode);
                    if (corresPersCGNode != null) {
                        corresPersCGNode.setCgNodeId(cgNodeId);
                        saver.getFormalInsToPersistentCGNodes().put(formalInNode, corresPersCGNode);
                    }
                }
            }
        }
        persistentSDGEntry2CGMapping(sdg, saver);
        persistentSSAMapping(sdg, saver);
        generateIRsAndPersistentLocalPointerKeys(localPointerKeys, saver);
        calculateDisjunctPointsToKeys(localPointerKeys, saver);

    }
    
    private StateSaver generateFromJson(JSONObject jsonObj, SDG sdg)
            throws IOException {
        StateSaver stateSaver = new StateSaver();
        stateSaver.setCallGraph(callGraph);
        stateSaver.setPointerAnalysis(pointerAnalysis);

        JSONArray formalNodeToCGNodeArr = jsonObj.getJSONArray(StateSaver.FORMAL_INS_TO_PERS_CG);
        for (int i = 0; i < formalNodeToCGNodeArr.length(); ++i) {
            JSONObject currentPair = formalNodeToCGNodeArr.getJSONObject(i);
            int sdgIndex = currentPair.getInt(StateSaver.SDG_NODE);
            JSONObject persistentCGNode = currentPair.getJSONObject(StateSaver.CG_NODE);
            SDGNode node = sdg.getNode(sdgIndex);
            stateSaver.getFormalInsToPersistentCGNodes().put(node,
                                                        PersistentCGNode
                                                        .generateFromJsonObj(persistentCGNode));
        }

        JSONArray cgNodeArr = jsonObj.getJSONArray(StateSaver.CG_NODE + "s");
        for (int i = 0; i < cgNodeArr.length(); ++i) {
            JSONObject currentCGNodeSaveObj = cgNodeArr.getJSONObject(i);
            PersistentCGNode currentPersistentCGNode =
                    PersistentCGNode.generateFromJsonObj(currentCGNodeSaveObj);
            stateSaver.getPersistentCGNodes().add(currentPersistentCGNode);
            stateSaver.getCgNodeIdToPersistentCGNodes().put(currentPersistentCGNode.getCgNodeId(),
                                                       currentPersistentCGNode);
        }

        JSONArray pointerKeyArr = jsonObj.getJSONArray(StateSaver.LOCAL_POINTER_KEYS);
        for (int i = 0; i < pointerKeyArr.length(); ++i) {
            JSONObject currentPointerKeyJsonObj = pointerKeyArr.getJSONObject(i);
            PersistentLocalPointerKey currentLocalPointerKey =
                    PersistentLocalPointerKey.generateFromJsonObj(currentPointerKeyJsonObj,
                                                                  stateSaver.getPersistentCGNodes());
            stateSaver.getPersistentLocalPointerKeys().add(currentLocalPointerKey);
        }

        JSONArray disjunctPointstoArr = jsonObj.getJSONArray(StateSaver.DISJUNCT_POINTS_TO);
        for (int i = 0; i < disjunctPointstoArr.length(); ++i) {
            JSONObject currentDisjObj = disjunctPointstoArr.getJSONObject(i);
            int key = currentDisjObj.getInt(StateSaver.KEY);
            JSONArray values = currentDisjObj.getJSONArray(StateSaver.VALUES);
            List<PersistentLocalPointerKey> disPointers = new ArrayList<>();
            for (int j = 0; j < values.length(); ++j) {
                int currDisIndex = values.getInt(j);
                disPointers.add(stateSaver.getPersistentLocalPointerKeys().get(currDisIndex));
            }
            PersistentLocalPointerKey keyPointerKey =
                    stateSaver.getPersistentLocalPointerKeys().get(key);
            stateSaver.getDisjunctPointsToSets().put(keyPointerKey, disPointers);
        }
        
        TIntIntMap entry2CGNode = new TIntIntHashMap();
        JSONArray nodeToCGNodeArr = jsonObj.getJSONArray(StateSaver.ENTRY_NODES_TO_CG);
        for (int i = 0; i < nodeToCGNodeArr.length(); ++i) {
            JSONObject currentPair = nodeToCGNodeArr.getJSONObject(i);
            int entryID = currentPair.getInt(StateSaver.SDG_NODE);
            int cgID = currentPair.getInt(StateSaver.CG_NODE); 
            entry2CGNode.put(entryID, cgID);
        }
        sdg.setEntryToCGNode(entry2CGNode);
        stateSaver.setSDGEntry2CGMap(entry2CGNode);

        TIntIntMap entry2SSAIndex = new TIntIntHashMap();
        JSONArray nodeToSSAIndexArray = jsonObj.getJSONArray(StateSaver.SDG_NODES_TO_SSA);
        for (int i = 0; i < nodeToSSAIndexArray.length(); ++i) {
            JSONObject currentPair = nodeToSSAIndexArray.getJSONObject(i);
            int entryID = currentPair.getInt(StateSaver.SDG_NODE);
            int iID = currentPair.getInt(StateSaver.I_INDEX); 
            entry2SSAIndex.put(entryID, iID);
        }
        sdg.setNode2Instr(entry2SSAIndex);
        stateSaver.setNode2SSA(entry2SSAIndex);
        return stateSaver;
    }    
    
    /**
     * generate a StateSaver from a {@link JSONObject}, which contain the necessary information. 
     * @param jsonObj which contain the necessary information of the state saver
     * @param sdg 
     * @param pathToJar
     * @param entryMethod
     * @return
     */
    public StateSaver buildSateSaverFromJson(JSONObject jsonObj, SDG sdg, String pathToJar, String entryMethod) {
    	if (callGraph == null) {
    		try {
				generateCallGraphAndPTA(pathToJar, entryMethod);

			} catch (UnsoundGraphException | CancelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassHierarchyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
    	}
        try {
            StateSaver stateSaver = generateFromJson(jsonObj, sdg);
//            generatePersistenceStructures(sdg, stateSaver); //necessary ?
            return stateSaver;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        
    }

    private void generateCallGraphAndPTA(String pathToJar, String entryMethod) throws UnsoundGraphException, CancelException, ClassHierarchyException, IOException {
    	SDGConfig config = new SDGConfig(pathToJar, entryMethod, Stubs.JRE_15); //changed from JRE_14 to JRE_15
        config.setComputeInterferences(true);
        config.setMhpType(MHPType.PRECISE);
        config.setPointsToPrecision(SDGBuilder.PointsToPrecision.INSTANCE_BASED);
        config.setExceptionAnalysis(SDGBuilder.ExceptionAnalysis.INTERPROC);
		config.setFieldPropagation(SDGBuilder.FieldPropagation.OBJ_GRAPH_NO_MERGE_AT_ALL);
		// Schneidet beim SDG application edges raus, so besser sichtbar mit dem
		// graphviewer
		config.setPruningPolicy(ApplicationLoaderPolicy.INSTANCE);

		SDGBuilderConfig conf = SDGBuildPreparation.prepareBuild(System.out,
				SDGProgram.makeBuildPreparationConfig(config), new NullProgressMonitor()).snd;
		conf.abortAfterCG = true;
//                #GraphWriter<CallGraph>;
		SDGBuilder builder = SDGBuilder.create(conf);
		TIntIntMap map = builder.getEntryNode2CGNode();
//                StateSaver sv = stateSaverBuilder.buildStateSaver(sdg);
//                cg = sv.getCallGraph();
		CGResult result = builder.buildCallgraph(new NullProgressMonitor());
		this.callGraph = result.cg;
		this.pointerAnalysis = result.pts;
    }

    private void persistentSDGEntry2CGMapping(SDG sdg, StateSaver saver) {
    	TIntIntMap node2CG = new TIntIntHashMap();
    	for(SDGNode node : sdg.vertexSet()) {
    		SDGNode entry = sdg.getEntry(node);
    		int entryID = entry.getId();
    		if (!node2CG.containsKey(entryID)) {
    			int cgId = sdg.getCGNodeId(entry);
    			node2CG.put(entryID, cgId);
    		}
    	}
    	saver.setSDGEntry2CGMap(node2CG);
    }
    
    private void persistentSSAMapping(SDG sdg, StateSaver saver) {
    	TIntIntMap node2SSA = new TIntIntHashMap();
    	for(SDGNode node : sdg.vertexSet()) {
    		int iindex = sdg.getInstructionIndex(node);
    		if (iindex != -1) {
    			node2SSA.put(node.getId(), iindex);
    		}
    	}
    	saver.setNode2SSA(node2SSA);
    }
    
    private void generateIRsAndPersistentLocalPointerKeys(
            ArrayList<LocalPointerKey> localPointerKeys, StateSaver saver) {
        for (int i = 0; i < localPointerKeys.size(); ++i) {
            LocalPointerKey currentLocalPointerKey = localPointerKeys.get(i);
            CGNode cgNode = currentLocalPointerKey.getNode();
            PersistentCGNode persistentCGNode = saver.getCgNodesToPersistentCGNodes().get(cgNode);
            persistentCGNode.createPersistentIR(cgNode, localPointerKeys);

            Integer currentCGNodeId = persistentCGNode.getCgNodeId();
            saver.getCgNodeIdToPersistentCGNodes().put(currentCGNodeId, persistentCGNode);

            PersistentLocalPointerKey persistentLocalPointerKey = new PersistentLocalPointerKey(currentLocalPointerKey,
                    persistentCGNode, i);
            saver.getPersistentLocalPointerKeys().add(persistentLocalPointerKey);
        }
    }

    private void calculateDisjunctPointsToKeys(ArrayList<LocalPointerKey> localPointerKeys, StateSaver saver) {
        for (int i = 0; i < localPointerKeys.size(); ++i) {
            OrdinalSet<? extends InstanceKey> currentPointsToset = pointerAnalysis
                    .getPointsToSet(localPointerKeys.get(i));
            for (int j = 0; j < localPointerKeys.size(); ++j) {
                if (i == j) {
                    continue;
                } else if (!localPointerKeys.get(i).getNode().equals(localPointerKeys.get(j).getNode())) {
                    continue;
                }
                OrdinalSet<? extends InstanceKey> otherPointsToSet = pointerAnalysis
                        .getPointsToSet(localPointerKeys.get(j));
                if (disjunct(currentPointsToset, otherPointsToSet)) {
                    if (saver.getDisjunctPointsToSets().containsKey(saver.getPersistentLocalPointerKeys().get(i))) {
                        saver.getDisjunctPointsToSets().get(saver.getPersistentLocalPointerKeys().get(i))
                                .add(saver.getPersistentLocalPointerKeys().get(j));
                    } else {
                        List<PersistentLocalPointerKey> list = new ArrayList<>();
                        list.add(saver.getPersistentLocalPointerKeys().get(j));
                        saver.getDisjunctPointsToSets().put(saver.getPersistentLocalPointerKeys().get(i), list);
                    }
                }
            }
        }
    }
    
    private static boolean disjunct(OrdinalSet<?> s1, OrdinalSet<?> s2) {
        for (Object e1 : s1) {
            for (Object e2 : s2) {
                if (e1.equals(e2)) {
                    return false;
                }
            }
        }
        return true;
    }
}
