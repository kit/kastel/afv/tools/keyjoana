package joanakey;

import edu.kit.joana.ifc.sdg.graph.SDG;

public interface StateSaverBuilder {
    
    public StateSaver buildStateSaver(SDG sdg, String pathToJar, String entryMethod);
    

}
