package joanakey.annotations;

import java.util.Collection;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

import edu.kit.joana.ifc.sdg.graph.SDGNode;

public class SDGAnnotationAdder extends SingleAnnotationAdder {
    
    private Supplier<Collection<SDGNode>> sdgNodeSupplier;
    private BiConsumer<SDGNode, String> annoAddMethod;
//    private String secLevel;
    
    public SDGAnnotationAdder(Supplier<Collection<SDGNode>> sdgNodeSupplier,
            BiConsumer<SDGNode, String> annoAddMethod,
            String secLevel) {
        super(null, null, secLevel);
        this.sdgNodeSupplier = sdgNodeSupplier;
        this.annoAddMethod = annoAddMethod;
        this.secLevel = secLevel;
    }
    
    @Override
    public void addYourselfToAnalysis() {
        sdgNodeSupplier.get().forEach((node) -> {
            annoAddMethod.accept(node, secLevel);
        });
    }
}
