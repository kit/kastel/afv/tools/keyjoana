package joanakey.annotations;

import java.util.Collection;

import edu.kit.joana.api.IFCAnalysis;
import edu.kit.joana.api.annotations.AnnotationType;
import edu.kit.joana.api.annotations.IFCAnnotation;
import edu.kit.joana.api.annotations.IFCAnnotationApplicator;
import edu.kit.joana.api.sdg.SDGProgram;
import edu.kit.joana.ifc.sdg.graph.SDGNode;

/**
 * @author joachim
 */
public class SDGNodeAnnotationApplicator extends IFCAnnotationApplicator {
    
	public SDGNodeAnnotationApplicator(SDGProgram program, IFCAnalysis analysis) {
		super(program, analysis);
	}
	
	/**
	 * Apply the given annotations to the corresponding {@link SDGNode}s. For {@link IFCAnnotation}s
	 *  this method annotates each {@link SDGNode} which belongs to the corresponding ProgramPart. 
	 * For {@link SDGNodeAnnotations} the method annotates just the corresponding {@link SDGNode}.
	 * @param {@link Collection} of {@link IFCAnnotation}s (and subtype {@link SDGNodeAnnotation}s) you want to apply.
	 * 
	 */
	@Override
	public void applyAnnotations(Collection<IFCAnnotation> anns) {
//      AnnotationTypeBasedNodeCollector collector = program.getNodeCollector();
        for (IFCAnnotation ann : anns) {
            if (ann.getType() == AnnotationType.SOURCE || ann.getType() == AnnotationType.SINK) {
                annotationDebug.outln(String.format("Annnotation nodes for %s '%s' of security level %s...", ann.getType().toString(), ann.getProgramPart(), ann.getLevel1()));
            }
            ann.accept(this);
        }
    }
	
    protected void applySDGAnnotation(SDGNodeAnnotation ann) {
        annotateNode(ann.getAnnotatedNode(), ann);
    }
	
}
