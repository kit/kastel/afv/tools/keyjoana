/**
 * This file is part of the Joana IFC project. It is developed at the
 * Programming Paradigms Group of the Karlsruhe Institute of Technology.
 *
 * For further details on licensing please read the information at
 * http://joana.ipd.kit.edu or contact the authors.
 */
package joanakey.annotations;

import edu.kit.joana.api.annotations.AnnotationType;
import edu.kit.joana.api.annotations.IFCAnnotation;
import edu.kit.joana.api.annotations.IFCAnnotationApplicator;
import edu.kit.joana.api.annotations.cause.AnnotationCause;
import edu.kit.joana.api.sdg.SDGMethod;
import edu.kit.joana.api.sdg.SDGProgramPart;
import edu.kit.joana.ifc.sdg.graph.SDGNode;

/**
 * Special IFCAnnotation class to annotate SDGNodes instead of program parts.
 * @author muessig
 */
public class SDGNodeAnnotation extends IFCAnnotation {
	
	private SDGNode annotatedNode;
	
	public SDGNodeAnnotation(SDGNode annotatedNode, AnnotationType type, String level, SDGProgramPart annotatedPart) {
		super(type, level, annotatedPart);
		this.annotatedNode = annotatedNode;
	}
	
	public SDGNodeAnnotation(SDGNode annotatedNode, AnnotationType type, String level, SDGProgramPart annotatedPart, SDGMethod context, AnnotationCause cause) {
		super(type, level, annotatedPart);
		
		this.annotatedNode = annotatedNode;
	}
	
	public SDGNodeAnnotation(SDGNode annotatedNode, String level, String level2, SDGProgramPart annotatedPart) {
		super(level ,level2, annotatedPart);
		this.annotatedNode = annotatedNode;
	}
	
	public SDGNode getAnnotatedNode() {
		return this.annotatedNode;
	}
	   
	@Override
    public void accept(IFCAnnotationApplicator applicator) {
	    SDGNodeAnnotationApplicator app = (SDGNodeAnnotationApplicator) applicator;
        app.applySDGAnnotation(this);
    }
	
//	public void accept(SDGNodeAnnotationApplicator applicator) {
////        SDGNodeAnnotationApplicator app = (SDGNodeAnnotationApplicator) applicator;
//        applicator.applySDGAnnotation(this);
//    }
	
    

}
