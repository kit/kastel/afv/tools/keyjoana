/**
 * This file is part of the Joana IFC project. It is developed at the
 * Programming Paradigms Group of the Karlsruhe Institute of Technology.
 *
 * For further details on licensing please read the information at
 * http://joana.ipd.kit.edu or contact the authors.
 */
package joanakey.annotations;

import edu.kit.joana.api.IFCAnalysis;
import edu.kit.joana.api.annotations.AnnotationType;
import edu.kit.joana.api.annotations.IFCAnnotationManager;
import edu.kit.joana.api.annotations.cause.AnnotationCause;
import edu.kit.joana.api.sdg.SDGMethod;
import edu.kit.joana.api.sdg.SDGProgram;
import edu.kit.joana.api.sdg.SDGProgramPart;
import edu.kit.joana.ifc.sdg.graph.SDGNode;

/**
 * 
 *@author joachim
 */
public class SDGNodeAnnotationManager extends IFCAnnotationManager {

	public SDGNodeAnnotationManager(SDGProgram program, IFCAnalysis analysis) {
		super(program, analysis);
		this.app = new SDGNodeAnnotationApplicator(program, analysis);
	}

	/**
	 * remove the annotation with the given sdg node
	 * @param annotated node you want to remove the annotation from
	 */
	//TODO: Test 
	public void removeAnnotation(SDGNode node) {
		sourceAnnotations.remove(node);
        sinkAnnotations.remove(node);
        declassAnnotations.remove(node);
	}

	public void addAnnotation(SDGNodeAnnotation ann) {
		if (!isAnnotationLegal(ann)) {
			throw new IllegalArgumentException();
		}
		if (ann.getType() == AnnotationType.SOURCE) {
            sourceAnnotations.put(ann.getProgramPart(), ann);
        } else if (ann.getType() == AnnotationType.SINK) {
            this.sinkAnnotations.put(ann.getProgramPart(), ann);
        } else {
            this.declassAnnotations.put(ann.getProgramPart(), ann);
        }
	}
	
//	@Override
//	public void unapplyAllAnnotations() {
//        this.app.unapplyAnnotations(getAnnotations());
//    }
	
	public void addSourceAnnotation(SDGNode node, SDGProgramPart progPart, String level, SDGMethod context, AnnotationCause cause) {
		addAnnotation(new SDGNodeAnnotation(node, AnnotationType.SOURCE, level, progPart, context, cause));
	}

	public void addSinkAnnotation(SDGNode node, SDGProgramPart progPart, String level, SDGMethod context, AnnotationCause cause) {
		addAnnotation(new SDGNodeAnnotation(node, AnnotationType.SINK, level, progPart, context, cause));
	}

	public void addDeclassification(SDGNode node, SDGProgramPart progPart, String level1, String level2) {
		addAnnotation(new SDGNodeAnnotation(node, level1, level2, progPart));
	}
	
//	@Override
//	public Collection<IFCAnnotation> getAnnotations() {
//		LinkedList<IFCAnnotation> ret = new LinkedList<IFCAnnotation>();
//		ret.addAll(sourceAnnotations.values());
//		ret.addAll(sinkAnnotations.values());
//		ret.addAll(declassAnnotations.values());
//		return ret;
//	}
	
	//TODO test
	public boolean isAnnotated(SDGNode node) {
		return sourceAnnotations.keySet().contains(node) || sinkAnnotations.keySet().contains(node) || declassAnnotations.keySet().contains(node);
	}
}
