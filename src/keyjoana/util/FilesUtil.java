package keyjoana.util;

import java.io.File;
import java.nio.file.Path;

/**
 * provides different methods to work with files
 * @author joachim muessig
 *
 */
public class FilesUtil {

	/**
	 * Returns the home directory path.
	 * @return The home directory path as {@linkString}.
	 * 
	 */
	public static String getHomeDirectoryPath() {
		String path = System.getProperty("user.home");
		if (path != null) {
			return path;
		} else {
			return null;
		}
	}
	
	public static String getNameByFilePath(String path) {
		if (path == null) {
			return "";
		}
		File f = new File(path);
		return f.getName();
	}
	
	public static String getFileExtension(File file) {
		String fileName = file.getName();
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		} else {
			return "";
		}
	}
	
	public static String removeBasePathPart(Path filePath, Path basePath) {
    	return basePath.relativize(filePath).toString();
	}
}
