package keyjoana.settings;

import java.util.ArrayList;
import java.util.Properties;

/**
 * a class for the basic settings, containing loop unwinding, method inlining and slicing
 * @author joachim
 *
 */
public class KeyJoanaSettings implements ObservableSetting {
	

	/**
	 * settings
	 */
	public boolean useLoopUnwinding = false;
	public boolean useMethodExpand = false;
	public boolean useSlicing = false;
	
	
	private ArrayList<SettingsObserver> settingsObserver = new ArrayList<SettingsObserver>();

	private static final String LOOP_UNWIND = "[KeYJoanaSetting]loopUnwind";
	private static final String USE_METHOD_EXPAND = "[KeYJoanaSetting]methodExpand";
	private static final String USE_SLICING = "[KeYJoanaSetting]useSlicing";
	private static final String TRUE = "true";
//	private static final String FALSE = "false";
	
	@Override
	public void addSettingObserver(SettingsObserver o) {
		settingsObserver.add(o);
	}
	
	@Override
	public void removeSettingObserver(SettingsObserver o) {
		settingsObserver.remove(o);
	}
	
	/**
	 * save settings to this object, contained in the given {@link Properties} object.
	 * use {@link notifySettingObserver} to notify all observer of changed settings
	 * @param p the {@link Properties} object to read 
	 */
	public void readProperties(Properties p) {
		useLoopUnwinding = getValueOfProperty(p, LOOP_UNWIND);
		useMethodExpand = getValueOfProperty(p, USE_METHOD_EXPAND);
		useSlicing = getValueOfProperty(p, USE_SLICING);
	}
	
	/**
	 * generate a {@link Properties} object from the Settings of this object.
	 * @return {@link Properties} object, containing the settings of this object
	 */
	public Properties generateProperties() {
		Properties p = new Properties();
		p.setProperty(LOOP_UNWIND, String.valueOf(useLoopUnwinding));
		p.setProperty(USE_METHOD_EXPAND, String.valueOf(useMethodExpand));
		p.setProperty(USE_SLICING, String.valueOf(useSlicing));
		return p;
	}
	
	private boolean getValueOfProperty(Properties p, String key) {
		String value = p.getProperty(key);
		if (value == null) {
			//default value is false...
			return false;
		}
		else if (value.equals(TRUE)) {
			return true;
		}
		else {
			//default or false
			return false;
		}
	}

	public boolean getMethodExpandSetting() {
		return this.useMethodExpand;
	}

	public boolean getLoopUnwindSetting() {
		return this.useLoopUnwinding;
	}

	public boolean getSlicingSetting() {
		return this.useSlicing;
	}
	
	public void setMethodExpandSetting(boolean b) {
		this.useMethodExpand = b;
		notifySettingObserver();
	}
	
	public void setLoopUnwindSetting(boolean b) {
		this.useLoopUnwinding = b;
		notifySettingObserver();
	}
	public void setSlicingSetting(boolean b) {
		this.useSlicing = b;
		notifySettingObserver();
	}
	
	/**
	 * notify the observer of this setting
	 */
	public void notifySettingObserver() {
		for (SettingsObserver observer : settingsObserver) {
			observer.notifySettingsChange();
		}
	}
	
}
