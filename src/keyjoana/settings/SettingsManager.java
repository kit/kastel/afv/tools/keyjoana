package keyjoana.settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;


/**
 * This class handles all settings files. It can store settings to files and load them from files.
 * @author joachim 
 *
 */
public class SettingsManager implements SettingsObserver{
	
	String settingsPath;
	KeyJoanaSettings keyJoanaSettings;
	
	private ArrayList<ObservableSetting> settingsSet;

	public SettingsManager() {
		settingsPath = SettingsPaths.getIndependentSettingsPath();
		settingsSet = new ArrayList<ObservableSetting>();
	}
	
	/**
	 * add a {@link ObservableSetting} to the {@link SettingsManager}.
	 * @param setting you want to add to the {@link SettingsManager}
	 */
	public void addSetting(ObservableSetting setting) {
		this.settingsSet.add(setting);
	}

	/**
	 * load the settings from the file or create a new if the file does not exist.
	 * If the files are loaded, all observer of these settings will be notified.
	 */
	public void loadSettings() {
		File f = new File(settingsPath);

		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		try {
			loadSettingsFromFile(f);
		} catch (IOException e) {
			System.err.println("Cound not load " + settingsPath + "because of it does not exist : " + e.toString());
			e.printStackTrace();
		}
	}
	
	private void loadSettingsFromFile(File file) throws IOException{
		if (!file.exists()) {
			throw new IOException("Settingfile "+settingsPath+" does not exists!");
		}
		FileInputStream in = new FileInputStream(file);
        Properties p = new Properties();
		p.load(in);
		for (ObservableSetting setting : settingsSet) {
			setting.readProperties(p);
			setting.notifySettingObserver();
		}
		in.close();
	}

	/**
	 * write all {@link ObservableSetting}, managed by this {@link SettingsManager}, to a property file.
	 * The method will also create a new property file if the property file does not exist yet.
	 */
	public void writeSettingsToFile() {
		File settingsFile = new File(settingsPath);
		if (!settingsFile.exists()) {
			try {
				settingsFile.createNewFile();
			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		FileOutputStream fout = null;
		try {
			// creates a new file if it does not exist yet
			fout = new FileOutputStream(settingsFile);
			for (ObservableSetting s : settingsSet) {
				Properties p = s.generateProperties();
				s.getClass().getName();
				p.store(fout, s.getClass().getSimpleName());
			}
		} catch (IOException ex) {
			System.err.println("Cound not write settings to "+ settingsPath + "because of "+ ex.toString());
		} finally {
			try {
				if (fout != null)
					fout.close();
			} catch (IOException e) {
				System.out.println("Closing streams failed.");
			}
		}
	}

	/**
	 * notify message, which will save the settings to the file to make them persistent
	 */
	@Override
	public void notifySettingsChange() {
		writeSettingsToFile();
	}
}
