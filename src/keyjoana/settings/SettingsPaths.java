package keyjoana.settings;

import java.io.File;

import keyjoana.util.FilesUtil;

/**
 * Class to handle all settings paths or recently loaded file paths
 * @author joachim muessig
 *
 */
public class SettingsPaths {
	private static String independentSettings; 
	private static String recentlyLoadedFiles;
	private static String recentlyLoadedFilesSpecGUI;
	private static String settingsDir;
	
	//default directory for all config/settings/recently loaded files
	private final static String DIRECTORY_NAME = ".KeYJoana";
	
    static {
    	setSettingsDir(FilesUtil.getHomeDirectoryPath() + File.separator + DIRECTORY_NAME);
    }
    
    /**
     * create the settings directory and update the file path
     * This method is optional, the default directory is '.KeYJoana'
     * @param dir directory where to save the files
     */
    public static void setSettingsDir(String dir) {
    	settingsDir = dir;
    	File f = new File(dir);
    	if (!f.exists()) {
    		f.mkdir();
    	}
    	recentlyLoadedFiles = settingsDir + File.separator + "recentlyLoadedFiles.txt";
    	independentSettings = settingsDir + File.separator + "independentSettings.props";
    	recentlyLoadedFilesSpecGUI = settingsDir + File.separator + "recentlyLoadedFilesSpecGUI.txt";
    }
    
    public static String getSettingsDir() {
        return settingsDir;
    }

    /**
     * return the file path for the recently loaded files of the KeYJoana tool
     * @return the path as {@link String} to the file contain all recently loaded files 
     */
    public static String getRecentLoadedFilesPath() {
        return recentlyLoadedFiles;
    }

    /**
     * return the file path to the basic settings
     * @return the path as {@link String} to the file contain all basic settings
     */
    public static String getIndependentSettingsPath() {
        return independentSettings;
    }
    
    /**
     * return the file path for the recently loaded files of the spec gui.
     * @return the path as {@link String} to the file contain all recently loaded files
     */
    public static String getRecentlyLoadedSpecGUIFiles() {
    	return recentlyLoadedFilesSpecGUI;
    }
    
}
