package keyjoana.settings;

/**
 * Interface for Observers which can observes {@link ObservableSetting}.
 * @author joachim muessig
 *
 */
public interface SettingsObserver {

	/**
	 * notify this {@link SettingsObserver}-object about changing setting states
	 */
	void notifySettingsChange();
		
}
