package keyjoana.settings;

import java.util.Properties;

/**
 * 
 * @author joachim
 *
 */
public interface PropertySaver {
	
	void load(Properties p);
	
	void save(Properties p);
	
	public void getProperties();
	
	

}
