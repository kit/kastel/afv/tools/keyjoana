package keyjoana.settings;

import java.util.Properties;

/**
 * Interface for the settings of the program. These settings are observable by {@link SettingsObserver}. And can be handled through
 * the {@link SettingsManager}.
 * @author joachim muessig
 *
 */
public interface ObservableSetting {
	

	/**
	 * Add a new {@link SettingsObserver} to this {@link ObservableSetting}.
	 * @param o is the {@link SettingsObserver} you want to add to the set of observers.
	 */
	public void addSettingObserver(SettingsObserver o);
	
	/**
	 * Remove the passed {@link SettingsObserver} from the observer set.
	 * @param o is the {@link SettingsObserver} you want to remove from the set of observers.
	 */
	public void removeSettingObserver(SettingsObserver o);
	
	/**
	 * read all properties of this setting contained in the passed {@link Properties}.
	 * @param p {@link Properties} contain settings for this {@link ObservableSetting}.
	 */
	public void readProperties(Properties p);
	
	/**
	 * Generate a {@link Properties}-object for this {@link ObservableSetting}.
	 * @return a {@link Properties}-object.
	 */
	public Properties generateProperties();
	
	/**
	 * Notify all {@link SettingsObserver} of this {@link ObservableSetting}.
	 */
	public void notifySettingObserver();
	
}
