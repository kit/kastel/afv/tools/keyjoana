package specgui.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
//simport java.io.StringReader;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import com.sun.org.apache.xalan.internal.xsltc.compiler.CompilerException;

public class JarCreator {

	// buffer size for the .jar creation
	private static final int BUFFER_SIZE = 1024;

	/** A string representing the .jar file ending. */
	public static final String DOT_JAR = ".jar";
	/** A string representing the .java file ending. */
	public static final String DOT_JAVA = ".java";
	/** A string representing the .class file ending. */
	public static final String DOT_CLASS = ".class";

	// the relative folder path for the created .jar file
	private static final String BUILD_FOLDER_NAME = "testdata"+File.separator+"build";

	/**
	 * This creates a .jar file from the .java files in the given directory.
	 * It temporarily creates .class files to do so.
	 * @param dir The directory to be archived in a .jar file.
	 * @param name The name of the created file.
	 * @return The path of the created file.
	 * @throws IOException Any IOException that may occur.
	 */
	public static String createJarFromDirectory(File dir, String name) throws IOException, CompilerException {
		// compile .java files to .class files
		if (!dir.isDirectory())
		    throw new IllegalArgumentException("The given File \"dir\" is not a directory.");
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
		DiagnosticCollector<JavaFileObject> diagnosticsCollector = new DiagnosticCollector<JavaFileObject>();
		List<File> javaFiles = filterForType(dirToFileList(dir), DOT_JAVA);
		Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(javaFiles);
		try {
			boolean compileSuccess = compiler.getTask(null, fileManager, diagnosticsCollector, null, null, compilationUnits)
					.call();
			if (!compileSuccess) {
				StringBuilder compileErrorMessage = new StringBuilder();
				List<Diagnostic<? extends JavaFileObject>> diagnostics = diagnosticsCollector.getDiagnostics();
		        for (Diagnostic<? extends JavaFileObject> diagnostic : diagnostics) {
		            // read error dertails from the diagnostic object
//		            compileErrorMessage.append(diagnostic.getMessage(null));
		            compileErrorMessage.append(diagnostic.getSource().getName()+"\nLine "+diagnostic.getLineNumber()+" "+diagnostic.getMessage(null)+"\n");
		        }
				CompilerException ex = new CompilerException(compileErrorMessage.toString());
				throw ex;
			}
		} catch (RuntimeException e) {
			System.out.println(e.getCause());
		}
		
		
		fileManager.close();
		
		// set up .jar file
		
		File parent = dir.getParentFile();
		
		String parentFolder = getParentFolder(dir.getPath());

		System.out.println("Parent folder: "+parentFolder);
		String buildFolderPath = parent.getPath()+File.separator + BUILD_FOLDER_NAME;
		System.out.println("Build Folder:" + buildFolderPath);
		File buildFolder = new File(buildFolderPath);
		buildFolder.mkdirs();
		
		
		String path = buildFolderPath + File.separator + (name.endsWith(DOT_JAR)? name : name + DOT_JAR);
		System.out.println("Jar file: "+path);
		
		File jarFile = new File(path);
		jarFile.createNewFile();
		//file.mkdirs();
		JarOutputStream jar = new JarOutputStream(new FileOutputStream(jarFile));


		// archive .java and .class files to a .jar
		List<File> classFiles = filterForType(dirToFileList(dir), DOT_CLASS);
		addFilesToJar(javaFiles, jar, dir.getPath());
		addFilesToJar(classFiles, jar, dir.getPath());
		
		// cleanup
		jar.close();
		for (File f : classFiles) {
			f.delete(); // are they needed / wanted?
		}
		return path;
	}

	// adds given .class files to the given .jar
	private static void addFilesToJar(List<File> files, JarOutputStream jar,
	                                  String basePath) throws IOException {
		for (File f : files) {
			// find entry name
			assert f.getPath().startsWith(basePath);
			String entryPath =
			        correctFileInZipName(f.getPath().substring(basePath.length()));

			// actual entry
			JarEntry entry = new JarEntry(entryPath);
			entry.setTime(f.lastModified());
			jar.putNextEntry(entry);
			FileInputStream in = new FileInputStream(f);
			byte[] buffer = new byte[BUFFER_SIZE];
			for (int current = in.read(buffer, 0, buffer.length);
			        current >= 0;
			        current = in.read(buffer, 0, buffer.length)) {
				jar.write(buffer, 0, current);
			}

			// cleanup
			jar.closeEntry();
			in.close();
		}
	}

	// creates a list of files of the directory and all sub directories
	private static List<File> dirToFileList(File dir) {
		List<File> files = new LinkedList<>();
		if (dir.isDirectory()) {
			for (File f : dir.listFiles()) {
				files.addAll(dirToFileList(f));
			}
		} else {
			files.add(dir);
		}
		return files;
	}

	// filters the given file list for the given type ending
	private static List<File> filterForType(List<File> files, String type) {
		List<File> out = new LinkedList<>();
		type = type.startsWith(".") ? type : "." + type;
		for (File f : files) {
			if (f.getPath().endsWith(type) || f.getPath().endsWith(type+"/")) {
				out.add(f);
			}
		}
		return out;
	}

	private static String getParentFolder(String folder) {
		String parent = correctFileInZipName(folder);
		if (!parent.contains("/")) return "";
		parent = parent.substring(0, parent.lastIndexOf("/"));
		return parent;
	}

	// making really sure this is a correct path name (for zip file conventions).
	private static String correctFileInZipName(String name) {
		String correctName = name.replace("\\", "/");
		int from = 0, to = correctName.length()-1;
		for (; from < correctName.length(); from++) {
			if (correctName.charAt(from) != '/') break;
		}
		if (from >= correctName.length()) return "";
		for (; to >= 0; to--) {
			if (correctName.charAt(to) != '/') break;
		}
		correctName = correctName.substring(from, to+1);
		// could also check for forbidden characters
		return correctName;
	}
}