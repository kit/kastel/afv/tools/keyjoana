/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package specgui;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.org.apache.xalan.internal.xsltc.compiler.CompilerException;

import gui.RecentlyUsedFilesManager;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import joanakey.AutomationHelper;
import joanakey.CombinedApproach;
import keyjoana.settings.SettingsPaths;
import specgui.joanahandler.Helper;
import specgui.joanahandler.JoanaInstance;

/**
 * user interface controller for the spec gui
 * @author holgerklein, joachim
 */
public class FXMLDocumentController implements Initializable {

	private static final String DEBUG_FOLDER = "testdata/multipleClassesFalsePos/MultipleClassesFalsePos/";
	private static final String DEBUG_JAR = "dist/MultipleClassesFalsePos.jar";
	private static final String DOT_JOAK = ".joak";
	private static final String JARPATH = "JARPATH";
	private static final String SDGPATH = "SDGPATH";
	private static final String JAVAPATH = "JAVAPATH";
	private static final String SAVERPATH = "SAVERPATH";
	private static final String ENTRYMETHOD = "ENTRYMETHOD";
//	private static final String STATESAVER = "STATESAVER";
	private static final String DIRECTORY_PATH = "DIRECTORY";

	private JoanaView joanaView = new JoanaView(this);
	private SourceSinkAdderDialogHandler sourceSinkAdderDialogHandler;
	private Stage stage;
	private File chosenFile = null;
    private Thread asyncJoanaInstanceThread;
    private RecentlyUsedFilesManager recentlyUsedFilesManager = new RecentlyUsedFilesManager(
    		SettingsPaths.getRecentlyLoadedSpecGUIFiles());

	private Map<String, SinkOrSource> selectedSources = new HashMap<String, SinkOrSource>();
	private Map<String, SinkOrSource> selectedSinks = new HashMap<String, SinkOrSource>();

	// private JoanaInstance joanaInstance;

	@FXML
	private ComboBox<String> mainClassesCB;
	@FXML
	private AnchorPane srcSinkAP;
	@FXML
	private TextField javaPathText;
	@FXML
	private ListView<String> sourcesList;
	@FXML
	private ListView<String> sinkList;
	@FXML
	private CheckBox preProcessCheckBox;
	@FXML
	private ProgressIndicator progressIndicator;
	@FXML
	private Label labelStatusCompile;
	@FXML
	private Button buttonCompile;
	@FXML
	private Menu menuOpenRecent;
	@FXML
	private MenuBar menuBarMain;
	@FXML
	private MenuItem menuItemExit;
	@FXML
	private MenuItem menuItemReloadSource;
	@FXML
	private Text statusText;
	@FXML
	private ScrollPane statusScrollPane;

	@FXML
	public void onAddSrc() {
		SinkOrSource src = sourceSinkAdderDialogHandler.letUserAddSrc(/*stage*/);
		if (src != null && !sourcesList.getItems().contains(src.toString())) {
			sourcesList.getItems().add(src.toString());
			selectedSources.put(src.toString(), src);
		}
	}

	@FXML
	public void onRemoveSrc() {
		final int selected = sourcesList.getSelectionModel().getSelectedIndex();
		final int size = sourcesList.getItems().size();
		String selectedSrc = sourcesList.getSelectionModel().getSelectedItem();
		SinkOrSource objToRemove = selectedSources.get(selectedSrc);
//        String objStrToRemove = sourcesList.getSelectionModel().getSelectedItem();
		if (0 <= selected && selected < size) {
			// remove obj from adder List
			sourceSinkAdderDialogHandler.removeSourceObjFromAdderList(objToRemove);
			// remove src from gui
			sourcesList.getItems().remove(selected);
			selectedSources.remove(selectedSrc, objToRemove);
		}
	}

    
	/**
	 * is called when pressing 'Add' at the Sinks 
	 */
	@FXML
	public void onAddSink() {
		SinkOrSource sink = sourceSinkAdderDialogHandler.letUserAddSink(/*stage*/);
		if (sink != null && !sinkList.getItems().contains(sink.toString())) {
			sinkList.getItems().add(sink.toString());
			selectedSinks.put(sink.toString(), sink);
		}
	}

	/**
	 * is called when pressing 'remove' button at the sinks
	 */
	@FXML
	public void onRemoveSink() {
		final int selected = sinkList.getSelectionModel().getSelectedIndex();
		final int size = sinkList.getItems().size();
		String selectedSink = sinkList.getSelectionModel().getSelectedItem();
		SinkOrSource objToRemove = selectedSinks.get(selectedSink);
		if (0 <= selected && selected < size) {
			// remove sink from gui
			sinkList.getItems().remove(selected);
			// remove sink obj from adder list
			sourceSinkAdderDialogHandler.removeSinkObjFromAdderList(objToRemove);
			selectedSinks.remove(selectedSink, objToRemove);
		}
	}

	//TODO refactor code. Use and generate helper classes
	@FXML
	public void createJoak() throws FileNotFoundException, IOException { 
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Select storage location.");

    
		File chosenFile = fileChooser.showSaveDialog(stage);
		if (chosenFile == null)
			return; // this happens if you cancel choosing a file
		String filepath = chosenFile.getPath();
		filepath += filepath.endsWith(DOT_JOAK) ? "" : DOT_JOAK;
		String sinkSrcJson = sourceSinkAdderDialogHandler.createSinkSourceJson();
		String template = CombinedApproach.PATH_TO_DIRECTORY + " : \"" + DIRECTORY_PATH + "\",\n"
				+ CombinedApproach.PATH_KeY + " : \"dependencies/Key/" + // hard coded path to the KeY.jar
				AutomationHelper.KeY_JAR + "\",\n" + CombinedApproach.JAVA_CLASS + " : \"\",\n"
				+ CombinedApproach.PATH_TO_JAR + " : \"" + JARPATH + "\",\n" + CombinedApproach.PATH_TO_JAVA + " : \""
				+ JAVAPATH + "\",\n" + CombinedApproach.PATH_TO_SDG + " : \"" + SDGPATH + "\",\n"
				+ CombinedApproach.ENTRY_METHOD + " : \"" + ENTRYMETHOD + "\",\n" + CombinedApproach.ANNOTATION_PATH
				+ " : \"\",\n" + CombinedApproach.FULLY_AUTOMATIC + " : true,\n" + CombinedApproach.PATH_TO_STATE_SAVER
				+ " : \"" + SAVERPATH + "\",\n";
		template = template.replace(JARPATH, joanaView.removeBaseDirectoryPath(joanaView.getCurrentJarFile()));
//                                    joanaView.getCurrentJarFile().getPath());
		template = template.replace(JAVAPATH,
				joanaView.removeBaseDirectoryPath(joanaView.getCurrentJavaFolderFile()) + "/");
//                                    joanaView.getCurrentJavaFolderFile().getPath() + "/"); //added a single "/" at the end of the file path because of some problems loading the KeY proof obligation
		template = template.replace(ENTRYMETHOD, joanaView.getCurrentMainClass());
		template = template.replace(SDGPATH, joanaView.removeBaseDirectoryPath(joanaView.getCurrentSDGFile()));
//                                    joanaView.getCurrentSDGFile().getPath());
		template = template.replace(SAVERPATH, joanaView.removeBaseDirectoryPath(joanaView.getCurrentStateSaverFile()));
//        							joanaView.getCurrentStateSaverFile().getPath());
		template = template.replace(DIRECTORY_PATH, joanaView.getCurrentMainDirectory().getPath());

		sinkSrcJson = template + sinkSrcJson;

		BufferedWriter out = new BufferedWriter(new FileWriter(filepath));
		out.write(sinkSrcJson);
		out.close();
	}
	
	private void tryCreateJoanaAsync() {
		if (joanaView.checkCreateJoanaConditions()) {
			disableMainGUIElements(true, true, true, true);
			enableProgressIndicator(true);
			setStatusMessage("Generate Joana Instance and SDG files ...");
			setStatusMessageColor(javafx.scene.paint.Color.BLUE);
			asyncJoanaInstanceThread = new Thread(() -> {
				try {
					joanaView.tryCreateJoana();
					Platform.runLater(() -> {
						enableProgressIndicator(false);
						disableMainGUIElements(false, false, false, false);
						setStatusMessage("Success. Now choose Sinks and Sources to generate a *.Joak file.");
						setStatusMessageColor(javafx.scene.paint.Color.GREEN);

					});
				} catch (Exception e) {
					setStatusMessage("Something went wrong.");
					setStatusMessageColor(javafx.scene.paint.Color.RED);
				}
			});
			asyncJoanaInstanceThread.start();
    	}
    }

	@FXML
	public void onChooseSrcFolder() {
		DirectoryChooser directoryChooser = new DirectoryChooser();
		directoryChooser.setTitle("Navigate to source directory.");
		File chosenSrcDir = directoryChooser.showDialog(stage);
		if (chosenSrcDir == null)
			return; // this happens if you cancel choosing a file
		updateRecentlyLoadedSrcDirectories(chosenSrcDir);
		loadSrcFile(chosenSrcDir);
//        joanaView.setCurrentSDGFile(chosenSrcDir);
	}

	@FXML
	public void onPressCompile() {
		if (chosenFile != null) {
			this.loadSrcFile(chosenFile);
		} else {
			//should not happen
			setStatusMessage("Error, reload the file");
			setStatusMessageColor(Color.RED);
		}
	}

	@FXML
	public void onPreProcessingSelected() {
		joanaView.setPreProcessing(preProcessCheckBox.isSelected());
	}

	/**
	 * initialize spec gui 
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		setStatusMessage("");
		srcSinkAP.setDisable(true);
		progressIndicator.setVisible(false);
//		progressIndicator.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
		mainClassesCB.setDisable(true);
		
		mainClassesCB.setOnAction((event) -> {
//			progressIndicator.setVisible(true);
			String mainClass = mainClassesCB.getSelectionModel().getSelectedItem();
			selectMainClass(mainClass);
			buttonCompile.setDisable(false);
			tryCreateJoanaAsync();
//            joanaView.setCurrentMainClass(mainClassesCB.getSelectionModel().getSelectedItem());
		});
		try {
			sourceSinkAdderDialogHandler = new SourceSinkAdderDialogHandler(sourcesList, sinkList);
		} catch (IOException ex) {
			Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
		}
		joanaView.setPreProcessing(preProcessCheckBox.isSelected());
		menuItemReloadSource.setOnAction((event) -> {
//			reloadLastFile(); deprecated
			loadMostRecentlyLoadedSrcDirectory();
		});
		menuItemExit.setOnAction((event) -> {
			System.exit(0);
		});
		updateOpenRecentMenu();

		boolean debug = false;
		if (debug) {
			joanaView.setCurrentJarFile(new File(DEBUG_FOLDER + DEBUG_JAR));
			try {
				joanaView.setCurrentJavaFolderFile(new File(DEBUG_FOLDER + "src"));
			} catch (CompilerException e) {
				
				this.setStatusMessage(e.getMessage());
				this.setStatusMessageColor(Color.RED);
				e.printStackTrace();
			}
		}
//		Scene scene = new Scene(statusScrollPane,300,200);
		statusText = new Text();
//		statusText.wrappingWidthProperty().bind(scene.widthProperty());
		statusScrollPane.setFitToWidth(true);
		statusScrollPane.setContent(statusText);
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	void letUserChooseMainClass(List<String> allClassesContainingMainMethod) {
		mainClassesCB.getItems().clear();
		mainClassesCB.getItems().addAll(allClassesContainingMainMethod);
		mainClassesCB.setDisable(false);
		setStatusMessage("Choose Main Class");
	}

	void letUserAddSinksAndSrcs(JoanaInstance joanaInstance) {
		// this.joanaInstance = joanaInstance;
		sourceSinkAdderDialogHandler.setJoanaInstance(joanaInstance);
		srcSinkAP.setDisable(false);
	}

	void setFolderPath(String absolutePath) {
		javaPathText.setText(absolutePath);
	}
	
	private void loadSrcFile(File chosenSrcDir) {
		buttonCompile.setDisable(true);
		if (preProcessCheckBox.isScaleShape()) {
			setStatusMessage("Pre processing files ...");
			setStatusMessageColor(javafx.scene.paint.Color.BLUE);
			joanaView.preProcessAllJavaFilesInDirecotory(chosenSrcDir);
		}
		setStatusMessage("Generate Jar files ...");
		setStatusMessageColor(javafx.scene.paint.Color.BLUE);
		try {
			joanaView.setCurrentJavaFolderFile(chosenSrcDir);
			letUserChooseMainClass(Helper.getAllClassesContainingMainMethod(joanaView.getCurrentJavaFolderFile()));
			this.chosenFile = chosenSrcDir;
			disableMainGUIElements(false, true, false, false);
		} catch (CompilerException e) {
			compilationErrorMessage(e.getMessage());
		}
		this.chosenFile = chosenSrcDir;
	
	}

	/**
	 * eneable the progress indicator on the user interface
	 * @param enable
	 */
	public void enableProgressIndicator(boolean enable) {
		this.progressIndicator.setVisible(enable);
	}

	private void setStatusMessage(String message) {
		this.statusText.setText(message);
//		this.labelStatusCompile.setText(message);
	}
	
	private void setStatusMessageColor(javafx.scene.paint.Color color) {
		this.statusText.setFill(color);
//		this.labelStatusCompile.setTextFill(color);
	}

	private void selectMainClass(String mainClass) {
		joanaView.setCurrentMainClass(mainClassesCB.getSelectionModel().getSelectedItem());
	}

	private void updateRecentlyLoadedSrcDirectories(File file) {
		recentlyUsedFilesManager.storeFileToRecentlyUsedFiles(file);
		updateOpenRecentMenu();
	}
	
	private void updateOpenRecentMenu() {
		String[] allRecentlyUsedPaths = recentlyUsedFilesManager.getAllRecentlyUsedFilePaths();
		menuOpenRecent.getItems().clear();
		MenuItem[] items = new MenuItem[allRecentlyUsedPaths.length];
		for (int i = 0; i < allRecentlyUsedPaths.length; i++) {
			String path = allRecentlyUsedPaths[i];
			items[i] = new MenuItem(path);
			items[i].setOnAction((event) -> {
				File file = new File(path);
				disableMainGUIElements(true, true, true, true);
				loadSrcFile(file);
//				disableMainGUIElements(false, true, true, false);
			});
		}
		menuOpenRecent.getItems().addAll(items);
	}
		
	private void loadMostRecentlyLoadedSrcDirectory() {
		File mostRecentlyLoadedFile = recentlyUsedFilesManager.getMostRecentlyUsedFile();
		if (mostRecentlyLoadedFile == null) {
			return;
		} else {
			disableMainGUIElements(true, true, true, true);
			loadSrcFile(mostRecentlyLoadedFile);
		}
	}
	
	private void disableMainGUIElements(boolean menuBar, boolean srcSinkPane, boolean buttonRecompile
			, boolean cbMainClass) {
			menuBarMain.setDisable(menuBar);
			srcSinkAP.setDisable(srcSinkPane);
			buttonCompile.setDisable(buttonRecompile);
			mainClassesCB.setDisable(cbMainClass);
	}

	private void compilationErrorMessage(String message) {
		this.setStatusMessage("Compilation Error \n" + message);
		this.setStatusMessageColor(Color.RED);
		disableMainGUIElements(false, true, false, true);
	}


}
