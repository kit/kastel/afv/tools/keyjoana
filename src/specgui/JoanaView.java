/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package specgui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.util.CancelException;
import com.ibm.wala.util.graph.GraphIntegrity;
import com.sun.org.apache.xalan.internal.xsltc.compiler.CompilerException;

import edu.kit.joana.ifc.sdg.graph.SDGSerializer;
import joanakey.customlistener.PreProcessor;
import joanakey.errorhandling.ErrorLogger;
import joanakey.slicing.ProgramSimplifier;
import joanakey.slicing.SourceCodeSlicer;
import specgui.helper.JarCreator;
import specgui.joanahandler.JoanaInstance;

/**
 *
 * @author holgerklein, joachim
 */
public class JoanaView {

    private File currentJarFile;
    private File currentJavaFolderFile;
    private File currentSDGFile;
    private File currentBaseDirectory;
    private File currentStateSaverFile;
    private String currentMainClass;
    private FXMLDocumentController controller;
    private JoanaInstance joanaInstance;
    private boolean preProcessing;

    public JoanaView(FXMLDocumentController controller) {
        this.controller = controller;
    }

    public void setCurrentJarFile(File jarFile) {
        if (jarFile == null) {
            return;
        }
        this.currentJarFile = jarFile;
    }
    
    public void setPreProcessing(boolean selected) {
    	this.preProcessing = selected;
	}
 
    
    
    
    /**
     * Process the java files in the given directory. 
     * This should be done if you plan to slice the program
     * with {@link ProgramSimplifier} and {@link SourceCodeSlicer}.
     * The processing process will put each bracket in a separate line, 
     * so the sliced program lines will not remove necessary brackets.
     * @param dir directory of the java files which you want to pre process.
     */
    public void preProcessAllJavaFilesInDirecotory(File dir) {
    	if(!preProcessing) {
    		return;
    	}

    	System.out.println("pre processing files..");
    	List<File> files = collectJavaFilesInDirectory(dir);
    	for (File file : files) {
			try {
				preProcessMethod(file.getPath());
			} catch (FileNotFoundException e) {
				Logger.getLogger(JoanaView.class.getName()).log(Level.SEVERE, null, e);
				e.printStackTrace();
			} catch (IOException e) {
				Logger.getLogger(JoanaView.class.getName()).log(Level.SEVERE, null, e);
				e.printStackTrace();
			}
		}
    }


    /**
     * set current java folder of this {@link JoanaView} object and creates a Jar file.
     * @param folderDir the java source directory
     * @throws CompilerException if the jar creation throws a compilation exception
     */
    public void setCurrentJavaFolderFile(File folderDir) throws CompilerException {
        if (folderDir == null) {
            return;
        }

        this.currentBaseDirectory = folderDir.getParentFile();
        this.currentJavaFolderFile = folderDir;
        controller.setFolderPath(folderDir.getPath());
        try {

			this.currentJarFile = new File(JarCreator.createJarFromDirectory(folderDir, folderDir.getName()+".jar"));

//	        checkIfCorrectJarFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void setCurrentSDGFile(String sdgFile) {
        try {
            this.currentSDGFile = new File(sdgFile);
            currentSDGFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void setCurrentStateSaverFile(String stateSaverFile) {
        try {
            this.currentStateSaverFile = new File(stateSaverFile);
            currentStateSaverFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setCurrentMainClass(String currentMainClass) {
        this.currentMainClass = currentMainClass;
    }

    /**
     * check the conditions which are required to generate a {@link JoanaInstance}. 
     * The conditions are that the current Main class, current java folder and a jar file for this {@link JoanaView} is set.
     * To set this information, use {@code setCurrentJavaFolderFile}, {@code setCurrentMainClass}.
     * @return true if all conditions are fulfilled and {@code tryCreateJoana} can be called.
     */
    public boolean checkCreateJoanaConditions() {
    	if (currentMainClass == null
                || currentJavaFolderFile == null
                || currentJarFile == null) {
            return false;
        } else {
        	return true;
        }
    }

    /**
     * This method creates a {@link JoanaInstance} object. This object contains a {@link SDGProgram} 
     * which is needed to specify sources and sinks for the .joak file
     */
    public void tryCreateJoana() {
    	if (!checkCreateJoanaConditions()) {
    		return;
    	}
        try {
            joanaInstance = new JoanaInstance(
                    currentJarFile.getPath(),
                    currentJavaFolderFile.getPath(),
                    currentMainClass);
            createSDGFolder();
            String sdgFile = createFilePathInSDGFolder(currentMainClass+".pdg");
            String stateSaverFile = createFilePathInSDGFolder(currentMainClass+ ".dispro");
            createSDGFolder();
            setCurrentSDGFile(sdgFile);
            setCurrentStateSaverFile(stateSaverFile);
            writeStateSaverFile();
            SDGSerializer.toPDGFormat(joanaInstance.getSDG(), new FileOutputStream(currentSDGFile));
            controller.letUserAddSinksAndSrcs(joanaInstance);
        } catch (ClassHierarchyException ex) {
            Logger.getLogger(JoanaView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JoanaView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GraphIntegrity.UnsoundGraphException ex) {
            Logger.getLogger(JoanaView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CancelException ex) {
            Logger.getLogger(JoanaView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalStateException ex) {
        	Logger.getLogger(JoanaView.class.getName()).log(Level.SEVERE, null, ex);
        	System.out.println("Illegal State: "+ex+ " " );
        }
    }
    
	private List<File> collectJavaFilesInDirectory(File dir) {
		List<File> files = new LinkedList<>();
		if (dir.isDirectory()) {
			for (File f : dir.listFiles()) {
				files.addAll(collectJavaFilesInDirectory(f));
			}
		} else {
			if (dir.getName().endsWith(".java")) {
				files.add(dir);
			}
		}
		return files;
	}   

    private void writeStateSaverFile() {
        FileWriter fileWriter;
        try {
            fileWriter = new FileWriter(currentStateSaverFile);
            fileWriter.write(joanaInstance.getStateSaverSaveString());
            fileWriter.close();
        } catch (IOException ex) {
            ErrorLogger.logError("Error while trying to write StateSaver file to disk.",
                    ErrorLogger.ErrorTypes.ERROR_WRITING_FILE_TO_DISK);
        }
    }
    
    private void createSDGFolder() {
    	File sdgFolder = new File(createFilePathInSDGFolder(""));
    	try {
    		sdgFolder.mkdir();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    private void preProcessMethod(String path) throws FileNotFoundException, IOException {
    	PreProcessor processor = new PreProcessor();
    	processor.formatJavaFile(path);	
	}

    private String createFilePathInSDGFolder(String dataName) {
    	String filePath = currentJavaFolderFile.getParent()+File.separator+"SDG" 
                +File.separator+dataName;
    	return filePath;
    }
    
    /**
     * TODO: implement if still necessary
     * @return
     */
    private boolean checkIfCorrectJarFile() {
        return true;
    }

    public JoanaInstance getJoanaInstance() {
        return joanaInstance;
    }
    
    public File getCurrentStateSaverFile() {
        return currentStateSaverFile;
    }
    
    public File getCurrentJarFile() {
        return currentJarFile;
    }

    public File getCurrentJavaFolderFile() {
        return currentJavaFolderFile;
    }

    public String getCurrentMainClass() {
        return currentMainClass;
    }
    
    public File getCurrentMainDirectory() {
    	return currentBaseDirectory;
    }
    
    public File getCurrentSDGFile() {
        return currentSDGFile;
    }
    
    public String removeBaseDirectoryPath(File file) {
    	Path filePath = file.toPath();
    	Path basePath = this.currentBaseDirectory.toPath();
    	return basePath.relativize(filePath).toString();
    }
    
    private File getBaseDirectoryForGivenSrcFile(File srcFile) {
    	File currentFile = srcFile.getParentFile();
    	while(currentFile != null) {
    		if (currentFile.getName().equals("src")) {
    			return currentFile.getParentFile();
    		}
    		currentFile = currentFile.getParentFile();
    	}
    	return null;
    }
}
