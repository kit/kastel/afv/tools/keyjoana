/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package specgui;

import joanakey.CombinedApproach;

/**
 *
 * @author holger
 */
public class SinkOrSource {

    private static final String SEC_LEVEL = "SEC_LEVEL";
    private static final String PROGRAMPART = "PROGRAMPART";
    private static final String METHOD = "METHOD";
    private static final String PARAMPOS = "PARAMPOS";
    private static final String DESCR = "DESCR";
    private static final String SDGNODE = "SDGNODE";
    private static final String SDGNODEID = "SDGNODEID";

    private static final String THIS = "this";
    private static final String PARAM = "<param>";


    private String selectionMethod;
    private String selection;
    private int sdgNodeID;
    private int methodParam;
    private String securityLevel;

    public static SinkOrSource createMethod(String selection, String securityLevel) {
        SinkOrSource sinkOrSource = new SinkOrSource();
        sinkOrSource.selectionMethod = AddSourceDialogController.CALLS_TO_METHOD;
        sinkOrSource.securityLevel = securityLevel;
        return addSelectionAndParamForMethod(sinkOrSource, selection);
    }

    private static SinkOrSource addSelectionAndParamForMethod(SinkOrSource sinkOrSource,
                                                              String selection) {
        if (selection.endsWith(THIS)) {
            sinkOrSource.methodParam = 0;
            sinkOrSource.selection = selection.substring(0, selection.length() - THIS.length());
        } else {
            String[] split = selection.split(PARAM);
            int pos = Integer.valueOf(split[1].trim());
            sinkOrSource.methodParam = pos;
            sinkOrSource.selection = split[0];
        }
        return sinkOrSource;
    }

    public static SinkOrSource createProgramPart(String selection, String securityLevel) {
        SinkOrSource sinkOrSource = new SinkOrSource();
        sinkOrSource.selectionMethod = AddSourceDialogController.PROGRAM_PART;
        sinkOrSource.selection = selection;
        sinkOrSource.securityLevel = securityLevel;
        return sinkOrSource;
    }
    
    public static SinkOrSource createSDGNode(String selection, String securityLevel) {
        SinkOrSource sinkOrSource = new SinkOrSource();
        sinkOrSource.selectionMethod = AddSourceDialogController.SDG_NODE;
        sinkOrSource.securityLevel = securityLevel;
        String[] split = selection.split("::");
        int sdgNodeId = Integer.valueOf(split[0].trim());
        sinkOrSource.sdgNodeID = sdgNodeId;
        sinkOrSource.selection = split[1];
        return sinkOrSource;
    }

    public String generateJson() {
        String templateStr =
                CombinedApproach.SECURITY_LEVEL_CAT + " : \"" + SEC_LEVEL + "\", " +
                CombinedApproach.DESCR_CAT + " : " + "{" + DESCR + "}";
        String descrTemplateMethod =
                CombinedApproach.FROM + " : \"" +
                CombinedApproach.CALLS_TO_METHOD + "\", " +
                CombinedApproach.METHOD_CAT +
                " : \"" + METHOD + "\", " +
                CombinedApproach.PARAM_POS_CAT + " : " + PARAMPOS;
        String descrTemplateProgramPart =
                CombinedApproach.FROM + " : \"" +
                CombinedApproach.PROGRAM_PART + "\", " +
                CombinedApproach.PROGRAM_PART + " : \"" + PROGRAMPART + "\"";
        String descrTemplateSDGNode = 
                CombinedApproach.FROM + " : \"" +
                CombinedApproach.SDG_NODE + "\", "+
                CombinedApproach.SDG_NODE_ID + " : \"" + SDGNODEID  + "\", " +
                CombinedApproach.SDG_NODE + " : \""+ SDGNODE + "\"";
        if (selectionMethod.equals(AddSourceDialogController.PROGRAM_PART)) {
            String created = templateStr.replace(SEC_LEVEL, securityLevel);
            String desc = descrTemplateProgramPart.replace(PROGRAMPART, selection);
            return created.replace(DESCR, desc);
        } else if (selectionMethod.equals(AddSourceDialogController.CALLS_TO_METHOD)) {
            String created = templateStr.replace(SEC_LEVEL, securityLevel);
            String desc = descrTemplateMethod.replace(METHOD, selection);
            desc = desc.replace(PARAMPOS, String.valueOf(methodParam));
            return created.replace(DESCR, desc);
        } else if (selectionMethod.equals(AddSourceDialogController.SDG_NODE)) {
            String created = templateStr.replace(SEC_LEVEL, securityLevel);
            String desc = descrTemplateSDGNode.replace(SDGNODEID, String.valueOf(sdgNodeID));
            desc = desc.replace(SDGNODE, selection);
            return created.replace(DESCR, desc);
        }
        return "";
    }

    @Override
    public String toString() {
        return selectionMethod + ": " + selection;
    }   
    
}
