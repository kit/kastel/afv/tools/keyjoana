/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package specgui.joanahandler;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.ibm.wala.ipa.callgraph.pruned.ApplicationLoaderPolicy;
import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.util.CancelException;
import com.ibm.wala.util.NullProgressMonitor;
import com.ibm.wala.util.graph.GraphIntegrity;

import edu.kit.joana.api.IFCAnalysis;
import edu.kit.joana.api.sdg.SDGConfig;
import edu.kit.joana.api.sdg.SDGFormalParameter;
import edu.kit.joana.api.sdg.SDGMethod;
import edu.kit.joana.api.sdg.SDGProgram;
import edu.kit.joana.api.sdg.SDGProgramPart;
import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import edu.kit.joana.ifc.sdg.mhpoptimization.MHPType;
import edu.kit.joana.ifc.sdg.util.JavaMethodSignature;
import edu.kit.joana.util.Stubs;
import edu.kit.joana.wala.core.SDGBuilder;
import joanakey.SDGAnalysis;
import joanakey.StateSaver;
import joanakey.StateSaverCGConsumerBuilder;

/**
 *
 * @author holgerklein
 */
public class JoanaInstance {

    private String currentJarPath;

    //private String currentJavaFolderPath;
    private String currentMainMethod;
    private SDGProgram program;
    private IFCAnalysis ana;
    private SDG sdg;
    private String stateSaverSaveString;
    private Collection<SDGProgramPart> allProgramParts;
    private Collection<SDGMethod> allMethods;
    private SortedSet<String> allProgramPartsString = new TreeSet<>();
    private SortedSet<String> allMethodsString = new TreeSet<>();
    private SortedSet<String> allSDGNodes = new TreeSet<>();

    public JoanaInstance(String currentJarPath, String currentJavaFolderPath,
                         String currentMainMethod)
                                 throws ClassHierarchyException, IOException,
                                        GraphIntegrity.UnsoundGraphException,
                                        CancelException {
        this.currentJarPath = currentJarPath;
        //this.currentJavaFolderPath = currentJavaFolderPath;
        this.currentMainMethod = currentMainMethod;
        runJoanaCreateSDGAndIFCAnalyis();
    }

    public Collection<SDGProgramPart> getAllProgramParts() {
        return allProgramParts;
    }

    public Collection<SDGMethod> getAllMethods() {
        return allMethods;
    }

    private void runJoanaCreateSDGAndIFCAnalyis()
            throws ClassHierarchyException, IOException,
                   GraphIntegrity.UnsoundGraphException,
                   CancelException {
        JavaMethodSignature entryMethod =
                JavaMethodSignature.mainMethodOfClass(currentMainMethod);
       
        StateSaverCGConsumerBuilder stateSaverBuilder = new StateSaverCGConsumerBuilder();
        SDGConfig config =
                new SDGConfig(currentJarPath, entryMethod.toBCString(),
                              Stubs.JRE_15); //changed from JRE_14 to JRE_15
        config.setComputeInterferences(true);
        config.setMhpType(MHPType.PRECISE);
        config.setPointsToPrecision(SDGBuilder.PointsToPrecision.INSTANCE_BASED);
        config.setExceptionAnalysis(SDGBuilder.ExceptionAnalysis.INTERPROC);
        config.setFieldPropagation(SDGBuilder.FieldPropagation.OBJ_GRAPH_NO_MERGE_AT_ALL);

        // save intermediate results of SDG generation points to call graph
        config.setCGConsumer(stateSaverBuilder);
//        config.setCGConsumer(null);
        // Schneidet beim SDG application edges raus, so besser sichtbar mit dem graphviewer
        config.setPruningPolicy(ApplicationLoaderPolicy.INSTANCE);

        program = SDGProgram.createSDGProgram(config, System.out,
                                              new NullProgressMonitor());
        
        sdg = program.getSDG();
        
        //ana = new IFCAnalysis(program);
        ana = new SDGAnalysis(program);
        StateSaver saver = stateSaverBuilder.buildStateSaver(sdg, currentJarPath, entryMethod.toString());

//        System.out.println(saver.getCallGraph().toString());

        allProgramParts = program.getAllProgramParts();
        for (SDGProgramPart part : allProgramParts) {
            allProgramPartsString.add(part.toString());
        }
        allMethods = program.getAllMethods();
        for (SDGMethod method : allMethods) {
            for (SDGFormalParameter param : method.getParameters()) {
                allMethodsString.add(method.toString() + param.getName());
            }
        }
        for (SDGNode node : program.getSDG().vertexSet()) { //FIXME really all SDG nodes ? 
        	
        	if(!isGoodNode(node)) continue;
        	
            allSDGNodes.add(node.getId() + "::" +
                    program.getSDG().getEntry(node).getBytecodeMethod() 
                    + " -> " + node.getLabel() + " "+ node.getKind());
        }
        stateSaverSaveString = saver.getSaveString();
    }
    
    private boolean isGoodNode(SDGNode node){
    	
    	Set<SDGNode.Kind> goodNodes = new HashSet<>();
    	goodNodes.add(SDGNode.Kind.ENTRY);
    	goodNodes.add(SDGNode.Kind.EXIT);
    	goodNodes.add(SDGNode.Kind.FORMAL_IN);
    	goodNodes.add(SDGNode.Kind.FORMAL_OUT);
    	
    	return goodNodes.contains(node.getKind());
    }

    public Collection<String> getAllProgramPartsString() {
        return allProgramPartsString;
    }

    public Collection<String> getAllMethodsString() {
        return allMethodsString;
    }
    
    public Collection<String> getAllSDGNodes() {
        return allSDGNodes;
    }

    public SDGProgram getProgram() {
        return program;
    }
    
    public String getStateSaverSaveString() {
        return stateSaverSaveString;
    }

    public IFCAnalysis getAna() {
        return ana;
    }
    
    public SDG getSDG() {
        return sdg;
    }

}
