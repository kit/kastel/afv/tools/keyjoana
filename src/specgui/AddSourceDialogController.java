/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package specgui;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import joanakey.CombinedApproach;
import specgui.joanahandler.JoanaInstance;

/**
 * FXML Controller class
 *
 * @author holger
 */
public class AddSourceDialogController implements Initializable {

    @FXML
    private ComboBox<String> selectMethodCB;
    @FXML
    private ComboBox<String> selectionCB;

    @FXML
    public void onOk() {
        stage.close();
    }

    @FXML
    public void onCancel() {
        selectionCB.setValue(null);
        stage.close();
    }

    private Stage stage;
    private JoanaInstance joanaInstance;

    static final String PROGRAM_PART = "Program Part";
    static final String CALLS_TO_METHOD = "Calls to Method";
    static final String SDG_NODE = "SDG Node";

    private String[] addingMethods = {PROGRAM_PART, CALLS_TO_METHOD, SDG_NODE};

    public void setJoanaInstance(JoanaInstance joanaInstance) {
        this.joanaInstance = joanaInstance;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        selectMethodCB.getItems().clear();
        selectMethodCB.getItems().addAll(addingMethods);
        selectMethodCB.valueProperty().addListener((observable) -> {
            selectionCB.getItems().clear();
            String selected = selectMethodCB.getSelectionModel().getSelectedItem();
            if (selected != null && selected.equals(PROGRAM_PART)) {
                selectionCB.getItems().addAll(joanaInstance.getAllProgramPartsString());
            } else if (selected != null && selected.equals(CALLS_TO_METHOD)) {
                selectionCB.getItems().addAll(joanaInstance.getAllMethodsString());
            } else if (selected != null && selected.equals(SDG_NODE)) {
                selectionCB.getItems().addAll(joanaInstance.getAllSDGNodes());    
            }
        });
    }

    public Optional<SinkOrSource> showForSink() {
        return showForSinkOrSource(CombinedApproach.LOW_SECURITY);
    }

    public Optional<SinkOrSource> showForSrc() {
        return showForSinkOrSource(CombinedApproach.HIGH_SECURITY);
    }

    private Optional<SinkOrSource> showForSinkOrSource(String sinkOrSource) {
    	/* TODO BUG
    	 * If you click "add" to add a sink or source twice (without closing the first opened window),
    	 * an exception is thrown. This is due to the stage used being already displayed.
    	 * The bug can be retraced to here but probably has to be fixed else where.
    	 */
        stage.showAndWait();
        String selectMethodStr = selectMethodCB.getSelectionModel().getSelectedItem();
        String selectionStr = selectionCB.getSelectionModel().getSelectedItem();
        if (selectionStr != null && selectMethodStr.equals(PROGRAM_PART)) {
            return Optional.of(SinkOrSource.createProgramPart(selectionStr, sinkOrSource));
        } else if (selectionStr != null && selectMethodStr.equals(CALLS_TO_METHOD)) {
            return Optional.of(SinkOrSource.createMethod(selectionStr,
                                                         CombinedApproach.HIGH_SECURITY));
        } else if (selectionStr != null && selectMethodStr.equals(SDG_NODE)) {
            return Optional.of(SinkOrSource.createSDGNode(selectionStr,
                                                        CombinedApproach.HIGH_SECURITY));
        } else {
            return Optional.empty();
        }
    }
}