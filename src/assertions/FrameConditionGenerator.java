package assertions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGEdge;
import edu.kit.joana.ifc.sdg.graph.SDGEdge.Kind;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import joanakey.staticCG.JCallGraph;
import joanakey.staticCG.javamodel.StaticCGJavaMethod;

public class FrameConditionGenerator {
	
	private SDG sdg;
	private JCallGraph cg;
	private Map<String, Boolean> methodCallsNew;
	private ArrayList<SDGNode> methodList;
	private Map<String, Set<SDGNode>> methodsToRootParameters;
	private boolean keyConform;
	private Map<SDGNode, Boolean> nodeDependsOnSelf;
	private final String CODE = "Ljava/lang/Object.code";
	
	public FrameConditionGenerator(SDG sdg, JCallGraph cg, boolean keyConform) {
		this.sdg = sdg;
		this.cg = cg;
		this.keyConform = keyConform;
		this.nodeDependsOnSelf = new HashMap<SDGNode, Boolean>();
		initiateMethodCallMap();
	}
	
	
	public void collectRootParameterNodes() {
		methodsToRootParameters = new HashMap<String, Set<SDGNode>>();
		ArrayList<SDGNode> listOfEntryNodes = new ArrayList<SDGNode>();
		
		//collect all entry nodes
		for (SDGNode n : sdg.vertexSet()) {
			boolean alreadyFound = false;
			if (n.getKind() == SDGNode.Kind.ENTRY 
					&& !n.getLabel().startsWith("com.ibm.wala.")
					&& !n.getLabel().startsWith("java.lang.")) {
				for(SDGNode method : listOfEntryNodes) {
					if(method.getLabel().equals(n.getLabel())) {
						alreadyFound = true;
						break;
					}
				}
				if(!alreadyFound) {
					listOfEntryNodes.add(n);
				}
			}
		}
		
		this.methodList = listOfEntryNodes;
		
		//collect all root parameter nodes for every entry node
		for (SDGNode entry : listOfEntryNodes) {
			String entryName = entry.getLabel();
			Set<SDGNode> rootParameters = new HashSet<SDGNode>();
			for (SDGEdge e : sdg.edgesOf(entry)) {
				SDGNode y = e.getTarget();
				if (e.getKind().equals(Kind.PARAMETER_STRUCTURE)) {
					if(!(y.getBytecodeName().equals("<exit>") || 
							y.getBytecodeName().equals("<exception>"))) {
						rootParameters.add(y);
					}
				}
			}	
			//remove "this" parameter node from constructors if the output should be used in KeY
			if(entryName.contains("<init>") && keyConform) {
				for(SDGNode node : rootParameters) {
					if(node.getLabel().equals("this")) {
						rootParameters.remove(node);
						break;
					}
				}
			}
			methodsToRootParameters.put(entryName, rootParameters);
		}
	}
	
	public Map<SDGNode, String> generateClauses() {
		Map<SDGNode, String> result = new HashMap<SDGNode, String>();
		for(SDGNode method: methodList) {
			StringBuilder sb = new StringBuilder();
			
			//check if the method calls a constructor or allocates a new array
			boolean callsNew = true;
			checkForArrayDeclarations(method, new HashSet<String>());
			if(!(methodCallsNew.get(getSimpleMethodName(method.getLabel())) == null)) {
				callsNew = methodCallsNew.get(getSimpleMethodName(method.getLabel()));
			}
			
			Set<SDGNode> rootParameters = methodsToRootParameters.get(method.getLabel());
			Set<String> formalInAccesses = new HashSet<String>();
			Set<String> formalOutAccesses = new HashSet<String>();
			Set<String> clauses = new HashSet<String>();
						
			for(SDGNode node : rootParameters) {
				LinkedList<SDGNode> currentPath = new LinkedList<SDGNode>();
				Set<SDGEdge> completedEdges = new HashSet<SDGEdge>();
				SDGNode current = node;
				SDGEdge last = null;
				currentPath.add(node);
				//find all paths starting from this root node
				while(!currentPath.isEmpty()) {
					//node does not have outgoing parameter structure edges
					if(sdg.getOutgoingEdgesOfKind(current, Kind.PARAMETER_STRUCTURE).isEmpty()) {
						if(!current.getBytecodeName().equals(CODE) 
								&& !current.getLabel().equals("this") && !current.getLabel().contains(" $")) {
							if(current.getKind().equals(SDGNode.Kind.FORMAL_IN)) {
								formalInAccesses.add(pathToParameterName(currentPath, false));
							} else if(current.getKind().equals(SDGNode.Kind.FORMAL_OUT)) {
								formalOutAccesses.add(pathToParameterName(currentPath, false));
							}
						}
						if(current.getBytecodeName().equals(CODE)) {
							sdg.removeEdge(last);
						} else {
							completedEdges.add(last);
						}
						currentPath.removeLast();
						if(!currentPath.isEmpty()) {
							current = currentPath.getLast();	
						}
					} else {
						Boolean noEligibleSuccessor = true;
						for(SDGEdge edge : sdg.getOutgoingEdgesOfKind(current, Kind.PARAMETER_STRUCTURE)) {
							if(!completedEdges.contains(edge)) {
								noEligibleSuccessor = false;
								last = edge;
								current = edge.getTarget();
								//node is data dependent on itself, e.g., a recursive data structure
								if(nodeHasCyclicDependency(current)) {
									currentPath.add(current);
									boolean modifies = nodeHasFormalOutSuccessor(current);
									if(!current.getBytecodeName().equals(CODE)) {
										if(modifies) {
											formalOutAccesses.add(pathToParameterName(currentPath, true));
										} else {
											formalInAccesses.add(pathToParameterName(currentPath, true));
										}
									}
									completedEdges.add(last);
									currentPath.removeLast();
									if(!currentPath.isEmpty()) {
										current = currentPath.getLast();	
									}
									break;
								}
								//cycle detected
								if(currentPath.contains(edge.getTarget())) {
									if(!current.getBytecodeName().equals(CODE)) {
										if(current.getKind().equals(SDGNode.Kind.FORMAL_IN)) {
											formalInAccesses.add(pathToParameterName(currentPath, true));
										} else if(current.getKind().equals(SDGNode.Kind.FORMAL_OUT)) {
											formalOutAccesses.add(pathToParameterName(currentPath, true));
										}
									}
									completedEdges.add(last);
									currentPath.removeLast();
									current = currentPath.getLast();	
								} else {
									currentPath.add(current);
								}
								break;
							}
						}
						if(noEligibleSuccessor) {
							if(!current.getBytecodeName().equals(CODE) 
									&& !current.getLabel().equals("this") && !current.getLabel().contains(" $")
									&& current.getKind().equals(SDGNode.Kind.FORMAL_OUT)) {
								formalOutAccesses.add(pathToParameterName(currentPath, false));
							}
							completedEdges.add(last);
							currentPath.removeLast();
							if(!currentPath.isEmpty()) {
								current = currentPath.getLast();	
							}
						}
					}
				}
			}
			
			//convert formal-in accesses to accessible clauses
			for(String formalIn : formalInAccesses) {
				if(!formalOutAccesses.contains(formalIn)) {
					clauses.add("//@ accessible " + formalIn + ";");
				}
			}
			
			clauses.forEach((s) -> {
				sb.append(s + "\n");
			});
			clauses.clear();

			//convert formal-out accesses to assignable clauses
			for(String formalOut : formalOutAccesses) {
				clauses.add("//@ assignable " + formalOut + ";");
			}
			
			//add purity clauses if necessary
			if(clauses.isEmpty() && !callsNew) {
				sb.append("//@ assignable \\strictly_nothing; \n");
			} else if (clauses.isEmpty()) {
				sb.append("//@ assignable \\nothing; \n");
			} else {
				clauses.forEach((s) -> {
					sb.append(s + "\n");
				});
			}
			clauses.clear();
			result.put(method, sb.toString());
		}
		return result;
	}
	
	private boolean checkForArrayDeclarations(SDGNode method, Set<String> checkedMethods) {
		Set<SDGNode> allNodes = sdg.getNodesOfProcedure(method);
		//check if method allocates an array
		for(SDGNode node : allNodes) {
			if(node.getLabel().contains("new []")) {
				 methodCallsNew.put(getSimpleMethodName(method.getLabel()), true);
				 return true;
			}
		}
		//check if method calls a method that allocates an array
		for(SDGNode node : allNodes) {
			if(node.getKind().equals(SDGNode.Kind.CALL)) {
				List<SDGEdge> callEdges = sdg.getOutgoingEdgesOfKind(node, Kind.CALL);
				for(SDGEdge edge : callEdges) {
					if(!checkedMethods.contains(method.getLabel())) {
						HashSet<String> updatedSet = new HashSet<>();
						updatedSet.addAll(checkedMethods);
						updatedSet.add(method.getLabel());
						if(checkForArrayDeclarations(edge.getTarget(), updatedSet)) {
							methodCallsNew.put(getSimpleMethodName(method.getLabel()), true);
							return true;
						}
					}
				}
			}
		}
		return false;
	}


	private boolean nodeHasFormalOutSuccessor(SDGNode node) {
		if(node.getKind().equals(SDGNode.Kind.FORMAL_OUT)) return true;
		LinkedList<SDGNode> currentPath = new LinkedList<SDGNode>();
		Set<SDGEdge> completedEdges = new HashSet<SDGEdge>();
		SDGNode current = node;
		SDGEdge last = null;
		boolean noEdge = true;
		currentPath.add(node);
		
		while(!currentPath.isEmpty()) {
			noEdge = true;
			for(SDGEdge edge : sdg.getOutgoingEdgesOfKind(current, Kind.PARAMETER_STRUCTURE)) {
				if(!completedEdges.contains(edge)) {
					last = edge;
					if(!edge.getTarget().getBytecodeMethod().equals(CODE) 
							&& !currentPath.contains(edge.getTarget())){
							if(edge.getTarget().getKind().equals(SDGNode.Kind.FORMAL_OUT)) return true;
							current = edge.getTarget();
							currentPath.add(current);
							noEdge = false;
							break;
					} else {
						completedEdges.add(last);
					}
				}

			}
			if(noEdge) {
				//backtrack
				currentPath.removeLast();
				completedEdges.add(last);
				if(!currentPath.isEmpty()) {
					current = currentPath.getLast();
				}
			}
		}
		return false;
	}


	private void initiateMethodCallMap() {
		methodCallsNew = new HashMap<String, Boolean>();
		Set<StaticCGJavaMethod> allMethods = cg.getAlreadyFoundMethods();
		for(StaticCGJavaMethod method : allMethods) {
			boolean callsNew = false;
			//constructors always call new
			if(method.getId().equals("<init>")) {
				callsNew = true;
			}
			//check if a method calls new 
			for(StaticCGJavaMethod calledMethod : cg.getAllMethodsCalledByMethodRec(method)) {
				if(calledMethod.getId().equals("<init>")) {
					callsNew = true;
					break;
				}
			}
			methodCallsNew.put(method.getId(), callsNew);
		}
	}
	
	private String pathToParameterName(LinkedList<SDGNode> path, Boolean containsCycle) {
		String result = "";
		String[] split = {""};
		String firstLabel = path.getFirst().getLabel();
		
		if(firstLabel.equals("this") || firstLabel.contains(" $")) {
			//not static
			split = firstLabel.split("\\$");
			result = split[split.length - 1].strip();
		} else {
			//static
			split = path.getFirst().getBytecodeName().split("\\/");
			split = split[split.length  - 1].split("\\.");
			result = split[split.length - 2] + "." + firstLabel;
		}
		
		Boolean first = true;
		int arrayCount = 0;
		String[] arrayNames = new String[path.size()/2];
		
		for(SDGNode node : path) {
			if(!first) {
				split = node.getBytecodeName().split("\\.");
				String current = split[split.length - 1];
				if(path.getLast().equals(node)) {
					if(current.equals("<[]>")) {
						//last node is an array access
						result = result + "[*]";
					} else if(containsCycle) {
						result = result.substring(0, result.contains(".") ? result.lastIndexOf(".") : result.length());
						result = String.format("\\reachLocs(%s, %s)", current, result);
					} else {
						result = result + "." + current;
					}
				} else {
					if(current.equals("<[]>")) {
						//node is an array access
						arrayNames[arrayCount] = result;
						result = result + "[i_" + arrayCount + "]";
						arrayCount++;
					} else {
						result = result + "." + current;
					}
				}
			}
			first = false;
		}
		//one infinite union expression for each array access
		for(int i = arrayCount; i > 0; i--) {
			result = String.format("\\infinite_union(int i_%1$d; 0 <= i_%1$d && i_%1$d < %2$s.length; %3$s)",
					i - 1, arrayNames[i - 1], result);
		}
		
		return result;
	}
	
	private boolean nodeHasCyclicDependency(SDGNode node) {
		if(nodeDependsOnSelf.containsKey(node)) return nodeDependsOnSelf.get(node);
		for(SDGEdge edge : sdg.getOutgoingEdgesOfKind(node, Kind.DATA_HEAP)) {
			SDGNode current = edge.getTarget();
			if(current.kind.equals(SDGNode.Kind.ACTUAL_IN)) {
				for(SDGNode formalIn : sdg.getFormalIns(current)) {
					if(!nodeDependsOnSelf.containsKey(formalIn) && !formalIn.equals(node)) {
						nodeHasCyclicDependency(formalIn);
					} else if (!nodeDependsOnSelf.containsKey(formalIn)) {
						continue;
					}
					if(nodeDependsOnSelf.get(formalIn)) {
						nodeDependsOnSelf.put(node, true);
						return true;
					}
				};
			} else {
				boolean result = checkCompoundNodeForCycle(current);
				nodeDependsOnSelf.put(node, result);
				return result;
			}
		}
		nodeDependsOnSelf.put(node, false);
		return false;
	}
	
	private boolean checkCompoundNodeForCycle(SDGNode node) {
		LinkedList<SDGNode> currentPath = new LinkedList<SDGNode>();
		HashSet<SDGEdge> completedEdges = new HashSet<SDGEdge>();
		SDGNode current = node;
		SDGEdge last = null;
		currentPath.add(node);
		while(!currentPath.isEmpty()) {
			boolean noEdge = true;
			//search for new eligible edge
			for(SDGEdge edge : sdg.getOutgoingEdgesOfKind(current, Kind.DATA_DEP)) {
				if(!completedEdges.contains(edge)) {
					if(currentPath.contains(edge.getTarget())) return true;
					current = edge.getTarget();
					currentPath.add(current);
					last = edge;
					noEdge = false;
					break;
				}
			}
			//backtrack
			if(noEdge) {
				currentPath.removeLast();
				completedEdges.add(last);
				if(!currentPath.isEmpty()) current = currentPath.getLast();
			}
		}
		return false;
	}
	
	private String getSimpleMethodName(String longName) {
		String methodName = longName.split("\\(")[0];
		String[] split = methodName.split("\\.");
		methodName = split[split.length - 1];
		return methodName;
	}
	
}
