package assertions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.util.CancelException;
import com.ibm.wala.util.graph.GraphIntegrity;
import com.ibm.wala.util.graph.GraphIntegrity.UnsoundGraphException;
import edu.kit.joana.ifc.sdg.graph.SDGSerializer;
import joanakey.customlistener.PreProcessor;
import joanakey.errorhandling.ErrorLogger;
import joanakey.slicing.ProgramSimplifier;
import joanakey.slicing.SourceCodeSlicer;
import specgui.helper.JarCreator;
import specgui.joanahandler.JoanaInstance;

import java.io.IOException;
import java.util.Collection;

import com.ibm.wala.ipa.callgraph.pruned.ApplicationLoaderPolicy;
import com.ibm.wala.util.NullProgressMonitor;
import edu.kit.joana.api.IFCAnalysis;
import edu.kit.joana.api.sdg.SDGConfig;
import edu.kit.joana.api.sdg.SDGFormalParameter;
import edu.kit.joana.api.sdg.SDGMethod;
import edu.kit.joana.api.sdg.SDGProgram;
import edu.kit.joana.api.sdg.SDGProgramPart;
import edu.kit.joana.ifc.sdg.graph.SDG;
import edu.kit.joana.ifc.sdg.graph.SDGNode;
import edu.kit.joana.ifc.sdg.mhpoptimization.MHPType;
import edu.kit.joana.ifc.sdg.util.JavaMethodSignature;
import edu.kit.joana.util.Stubs;
import edu.kit.joana.wala.core.SDGBuilder;

public class SDGCreator {
	private String currentMainMethod;
	private String currentJarPath;
	private SDGProgram program;

	public SDGCreator(String mainMethod, String currentJarPath) {
		this.currentMainMethod = mainMethod;
		this.currentJarPath = currentJarPath;
	}
	
	/**
	 * create SDG program
	 * @param file
	 * @return true if sdg program generation was successful 
	 */
	public boolean createSDGProgram() {
		JavaMethodSignature entryMethod =
                JavaMethodSignature.mainMethodOfClass(currentMainMethod);
       
        SDGConfig config =
                new SDGConfig(currentJarPath, entryMethod.toBCString(),
                              Stubs.JRE_15); //changed from JRE_14 to JRE_15
        config.setComputeInterferences(true);
        config.setMhpType(MHPType.PRECISE);
        config.setPointsToPrecision(SDGBuilder.PointsToPrecision.OBJECT_SENSITIVE);
        config.setExceptionAnalysis(SDGBuilder.ExceptionAnalysis.IGNORE_ALL);
        config.setFieldPropagation(SDGBuilder.FieldPropagation.OBJ_GRAPH);

        // Schneidet beim SDG application edges raus, so besser sichtbar mit dem graphviewer
        config.setPruningPolicy(ApplicationLoaderPolicy.INSTANCE);
        SDGProgram program = null;
        boolean success = false;
        try {
			 program = SDGProgram.createSDGProgram(config, System.out,
			                                      new NullProgressMonitor());
			 success = true;
		} catch (ClassHierarchyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsoundGraphException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CancelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        this.program = program;
      
        if (success) {
        	return true;
        } else {
        	return false;
        }
	}

	public SDG getSDG() {
		return program.getSDG();
	}
	
	public Collection<SDGMethod> getAllSDGMethods() {
		return program.getAllMethods();
	}
}
