package assertions;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import edu.kit.joana.ifc.sdg.graph.SDGNode;
import joanakey.staticCG.JCallGraph;

public class start {
	
	public static void main(String[] args) {
		//class containing the main method
		String mainMethod = args[0];
		//path to the jar
		String currentJarPath = args[1];
		//path to the output file
		String outputPath = args[2];
		
		File f = new File(currentJarPath);
		if(!f.exists()) {
			System.out.println("Jar not found.");
			return;
		}
		SDGCreator creator = new SDGCreator(mainMethod, currentJarPath);
		creator.createSDGProgram();
		JCallGraph cg = new JCallGraph();
		
		try {
			cg.generateCG(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		FrameConditionGenerator generator = new FrameConditionGenerator(creator.getSDG(), cg, true);
		generator.collectRootParameterNodes();
		Map<SDGNode, String> methodsToClauses = generator.generateClauses();
		
		FileHandler fh = new FileHandler(outputPath, methodsToClauses);
		try {
			fh.generateOutput();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
