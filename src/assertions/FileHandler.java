package assertions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import edu.kit.joana.ifc.sdg.graph.SDGNode;

public class FileHandler {
	private String outputPath;
	private Map<SDGNode, String> methodsToClausesMap;
	
	public FileHandler(String path, Map<SDGNode, String> clausesToMethodMap) {
		this.outputPath = path;
		this.methodsToClausesMap = clausesToMethodMap;
	}
	
	public void generateOutput() throws IOException {
		StringBuilder result = new StringBuilder();
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputPath));
		methodsToClausesMap.forEach((node, clauses) ->{
			String methodName = node.getLabel();
			try {
				writer.write(clauses + methodName + "\n" + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		writer.close();
		System.out.println(result);
		
		
	}

}
