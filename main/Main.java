package main;

import conditions.Conditions;
import loop.Loops;
import multiplefields.MultipleFields;
import plusminus.PlusMinusFalsePos;
import running.Running;

public class Main 
{
	
	public static void main(String args[]){
		
		//new ClassA().falsePos(0);
		//runConditionsExample();
		//runLoopsExample();
		//runPlusMinusExample();
		//runMultipleFieldsExample();
		runRunningExample();
//sliced: 	}
	
	public static void runPlusMinusExample(){
		PlusMinusFalsePos example = new PlusMinusFalsePos();
		example.testMethod(1, 7);
	}
	
	public static void runMultipleFieldsExample(){
		MultipleFields mf = new MultipleFields();
		mf.testMethod();
	}
	
	public static void runLoopsExample(){
		Loops example = new Loops();
		example.testMethod(5, 7);
	}
	
	public static void runConditionsExample(){
		Conditions example = new Conditions();
		example.testMethod(1, 2);
	}
	
	public static void runRunningExample(){
		Running example = new Running();
		example.secure(1, 0);
//sliced: 	}

}
