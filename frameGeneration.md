# Frame Condition Generation
This project generates `assignable` and `accessible` clauses for methods of a given program from its SDG. An SDG of the program is generated using [JOANA](https://pp.ipd.kit.edu/projects/joana/). Clauses are generated for methods called either directly or indirectly by the main method.

## Getting Started
A jar file needs to be created from the java sources for which clauses should be generated. The frameGenerator.jar can be found in the folder [``runnableJars/FrameGeneration``](/runnableJars/FrameGeneration). Clauses for an example.jar with the main method in the Main.class can be generated and saved to the outputFolder as followed:
```
java -jar frameGenerator.jar Main example.jar outputFolder
```
The first argument for the frameGenerator.jar is the name of the class containing the main method. The second argument is the path to the jar file and the third argument is the path to the output folder.

## Examples

The evaluation.jar in the [``runnableJars/FrameGeneration``](/runnableJars/FrameGeneration) folder contains the examples used for the evalution of the program. The source code for theses examples can be found in the folder [``testdata/frameGenerationEvaluation``](/testdata/frameGenerationEvaluation). Clauses for all the examples in the evaluation.jar can be generated with the following command:
```
java -jar frameGenerator.jar eval evaluation.jar outputFolder
