package stateSaver;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.pruned.ApplicationLoaderPolicy;
import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.util.CancelException;
import com.ibm.wala.util.NullProgressMonitor;
import com.ibm.wala.util.graph.GraphIntegrity.UnsoundGraphException;

import edu.kit.joana.api.sdg.SDGBuildPreparation;
import edu.kit.joana.api.sdg.SDGConfig;
import edu.kit.joana.api.sdg.SDGProgram;
import edu.kit.joana.ifc.sdg.mhpoptimization.MHPType;
import edu.kit.joana.ifc.sdg.util.JavaMethodSignature;
import edu.kit.joana.util.Stubs;
import edu.kit.joana.wala.core.SDGBuilder;
import edu.kit.joana.wala.core.SDGBuilder.CGResult;
import edu.kit.joana.wala.core.SDGBuilder.SDGBuilderConfig;
/**
 * this testclass is just generated to determine if the cg generation is deterministic and generating two cg for the same program will result in an equal cg. 
 * @author joachim
 *
 */
public class CallGraphGenerationTests {
	
	static String pathToJar = "testdata/jUnitData/CallGraph/testdata/build/src.jar";
	static String currentMainMethod = "IFLoop";
	static CGResult result1;
	static CGResult result2;
	static CGResult result3;
	
	@Before
	public void setUp() throws ClassHierarchyException, IllegalArgumentException, IOException, UnsoundGraphException, CancelException {
		JavaMethodSignature entryMethod =
                JavaMethodSignature.mainMethodOfClass(currentMainMethod);
        //generate 3 call graphs and compare them
		result1 = generateCallGraphResult(pathToJar, entryMethod.toBCString());
    	System.out.println("Finished CallGraph\n");
		result2 = generateCallGraphResult(pathToJar, entryMethod.toBCString());
    	System.out.println("Finished CallGraph\n");
		result3 = generateCallGraphResult(pathToJar, entryMethod.toBCString());
    	System.out.println("Finished CallGraph\n");
	}
	
	public CGResult generateCallGraphResult(String pathToJar, String entryMethod) throws ClassHierarchyException, IOException, IllegalArgumentException, UnsoundGraphException, CancelException {System.out.println("Generate CallGraph");
    	SDGConfig config = new SDGConfig(pathToJar, entryMethod, Stubs.JRE_15); //changed from JRE_14 to JRE_15
        config.setComputeInterferences(true);
        config.setMhpType(MHPType.PRECISE);
        config.setPointsToPrecision(SDGBuilder.PointsToPrecision.INSTANCE_BASED);
        config.setExceptionAnalysis(SDGBuilder.ExceptionAnalysis.INTERPROC);
		config.setFieldPropagation(SDGBuilder.FieldPropagation.OBJ_GRAPH_NO_MERGE_AT_ALL);
		// Schneidet beim SDG application edges raus, so besser sichtbar mit dem
		// graphviewer
		config.setPruningPolicy(ApplicationLoaderPolicy.INSTANCE);

		SDGBuilderConfig conf = SDGBuildPreparation.prepareBuild(System.out,
				SDGProgram.makeBuildPreparationConfig(config), new NullProgressMonitor()).snd;
		conf.abortAfterCG = true;
//                #GraphWriter<CallGraph>;
		SDGBuilder builder = SDGBuilder.create(conf);
		CGResult result = builder.buildCallgraph(new NullProgressMonitor());
//		com.ibm.wala.ipa.callgraph.CallGraph callGraph = result.cg;
//		PointerAnalysis pointerAnalysis = result.pts;
    	return result;
	}
	
	@Test
	public void testCallgraphEqual() throws ClassHierarchyException, IOException, UnsoundGraphException, CancelException {
    	Iterator<CGNode> nodesIterator = result1.cg.iterator();
    	//compare if there are the same edges for each node pair (result1, 2 and 3)
    	while (nodesIterator.hasNext()) {
    		CGNode nodeSource = nodesIterator.next();
    		int id = nodeSource.getGraphNodeId();
    		Iterator<CGNode> nodesIteratorSecond = result1.cg.iterator();
    		while(nodesIteratorSecond.hasNext()) {
    			CGNode nodeSink = nodesIteratorSecond.next();
    			int id2 = nodeSink.getGraphNodeId();
    			CGNode node2Source = result2.cg.getNode(id);
    			CGNode node2Sink = result2.cg.getNode(id2);
    			CGNode node3Source = result3.cg.getNode(id);
    			CGNode node3Sink = result3.cg.getNode(id2);
    			boolean hasEdge = result1.cg.hasEdge(nodeSource, nodeSink);
    			boolean hasEdge2 = result2.cg.hasEdge(node2Source, node2Sink);
    			boolean hasEdge3 = result3.cg.hasEdge(node3Source, node3Sink);
    			if (hasEdge) {
    				assertTrue(hasEdge2);
    				assertTrue(hasEdge3);
    			} else {
    				assertFalse(hasEdge2);
    				assertFalse(hasEdge3);
    			}

    		}
    	}

    	Iterator<CGNode> nodesIterator2 = result2.cg.iterator();
    	//compare if there are the same edges for each node pair (2 and 3)
    	while (nodesIterator2.hasNext()) {
    		CGNode nodeSource = nodesIterator2.next();
    		int id = nodeSource.getGraphNodeId();
    		Iterator<CGNode> nodesIteratorSecond = result2.cg.iterator();
    		while(nodesIteratorSecond.hasNext()) {
    			CGNode nodeSink = nodesIteratorSecond.next();
    			int id2 = nodeSink.getGraphNodeId();
    			CGNode node3Source = result3.cg.getNode(id);
    			CGNode node3Sink = result3.cg.getNode(id2);
    			boolean hasEdge = result2.cg.hasEdge(nodeSource, nodeSink);
    			boolean hasEdge3 = result3.cg.hasEdge(node3Source, node3Sink);
    			if (hasEdge) {
    				assertTrue(hasEdge3);
    			} else {
    				assertFalse(hasEdge3);
    			}
    		}
//    		assertTrue(next.toString().equals(node2.toString()));
//    		assertTrue(next.toString().equals(node3.toString()));
    	}

	}
	
	@Test
	public void testPointsToEqual() {
		
	}
	
	@Test
	public void testCallGraphEqual2() {
//		//remove same edges
//		Iterator<CGNode> nodesIterator = result1.cg.iterator();
//    	while (nodesIterator.hasNext()) {
//    		CGNode sink = nodesIterator.next();
//    		int id = next.getGraphNodeId();
//    		CGNode next2 = result2.cg.getNode(id);
//    		CGNode next3 = result3.cg.getNode(id);
//    	}
//    	//result 2 and 3 shouldn have edges now !
//    	Iterator<CGNode> nodesIterator2 = result2.cg.iterator();
//    	while (nodesIterator2.hasNext()) {
//    		CGNode nodeSource = nodesIterator2.next();
//    		int id = nodeSource.getGraphNodeId();
//    		Iterator<CGNode> nodesIteratorSecond = result2.cg.iterator();
//    		while(nodesIteratorSecond.hasNext()) {
// 
	}
	
	@Test
	public void testCallGraphEqual3() {
		//compare each node with the same id 
		Iterator<CGNode> nodesIterator = result1.cg.iterator();
    	while (nodesIterator.hasNext()) {
    		CGNode next = nodesIterator.next();
    		int id = next.getGraphNodeId();
    		assertTrue(next.toString().equals(result3.cg.getNode(id).toString()));
    		assertTrue(next.toString().equals(result2.cg.getNode(id).toString()));
    	}
	}
}
