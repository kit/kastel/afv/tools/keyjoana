public class Main 
{
    public static void main ( String [ ] args ) 
    {
        int value = 5 ;
        callNoLeak ( value ) ;
    }
    public static int callNoLeak ( int h ) 
    {
        return noLeak ( h ) ;
    }
    public static int noLeak ( int h ) 
    {
        I i = new C1 ( ) ;
        i = new C2 ( ) ;
        return i . m ( h ) ;
    }
}
