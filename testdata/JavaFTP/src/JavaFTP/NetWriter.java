/*
 * Name:    NetWriter.java (extends NetIO (extends JFTPIO (extends JFTPSuper)))
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.1    12.03.2001 Tobias Kranz Creation
 *          13.03.2001 Tobias Kranz Changed from Thread to regular class
 *                                  (nearly total rewrite)
 * 0.0.1pA3 15.03.2001 Tobias Kranz splitted from JFTP; nothing big
 * 0.1.99   10.06.2001 Tobias Kranz added `getOutputStream()'
 */
 
package JavaFTP;

import java.net.Socket;
import java.io.OutputStream;

/**
 * Class NetWriter may be used to send any kind of data through an existing
 * Socket.
 *@since 0.0.1
 */
public class NetWriter
  extends NetIO
{
  private OutputStream outStream;
  private StdOut stdout = new StdOut();
  private StdErr stderr = new StdErr();

  /**
   * Sets the OutputStream.
   *@param socket Socket to write through.
   *@since 0.0.1
   */
  public NetWriter(Socket socket)
  {
    try
    {
      this.outStream = socket.getOutputStream();
    }
    catch(Exception e){}
  }

  /**
   * Returns the OutputStream of the used socket
   *@return  The OutputStream used for the current socket.
   *@since   0.1.99
   *@version 0
   */
  public OutputStream getOutputStream()
  { return( outStream ); }

  /*
   * Just send everything out to the server
   */
  /**
   * Writes data to the ftpserver
   *@param string The data to send
   *@since 0.0.1
   */
  public void write(String string)
  {
    int i;

    try
    {
      if (jftpSuper.getDebugLevel() >= 1)
        stdout.println("starting new write-loop");

      for (i=0; i < string.length(); i++)
      { 
        outStream.write(string.charAt(i));
        if (jftpSuper.getDebugLevel() >= 2)
          stdout.println("writing char: " + string.charAt(i));
      }
      if (string.charAt( string.length()-1 ) != '\n')
        outStream.write('\n'); // Finish the line :-)
      else
        if (jftpSuper.getDebugLevel() >= 2)
          stderr.println( "DON`T SEND NEWLINES!" );

      outStream.flush(); // make sure the chars are written

      if (jftpSuper.getDebugLevel() >= 1)
        stdout.println("ending write-loop");
    }
    catch(Exception e)
    {
      stderr.println("Exception in Outp(" + e.toString() + ")");
    }
  }
}
