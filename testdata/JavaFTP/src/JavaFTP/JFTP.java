/*
 * Name:    JFTP.java (extends JFTPSuper)
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name                     changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz             Creation
 * 0.0.2pA2 02.05.2001 Tobias Kranz             Added `nextParamIsNumeric()'
 *                                              & `nextParamExists()'
 * 0.0.2pA2 04.05.2001 Sebastian Schipper       Two bugs fixed
 */
package JavaFTP;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Main class.
 *@since 0.0.2pA1
 */
public class JFTP
  extends JFTPSuper
{
  private StdIn     stdin;
  private StdOut    stdout;
  private StdErr    stderr;

  private final char[] guiShort       = {'-','g'};
  private final char[] cliShort       = {'-','c'};
  private final char[] helpShort      = {'-','h'};
  private final char[] portShort      = {'-','P'};
  private final char[] passiveShort   = {'-','p'};
  private final char[] verboseShort   = {'-','v'};
  private final char[] anonymousShort = {'-','a'};
  private final char[] aPasswdShort   = {'-','A','p'};
  private final char[] guiLong        = {'-','-','g','u','i'};
  private final char[] cliLong        = {'-','-','c','l','i'};
  private final char[] helpLong       = {'-','-','h','e','l','p'};
  private final char[] portLong       = {'-','-','P','o','r','t'};
  private final char[] verboseLong    = {'-','-','v','e','r','b','o','s','e'};
  private final char[] passiveLong    = {'-','-','p','a','s','s','i','v','e'};
  private final char[] anonymousLong  = {'-','-','a','n','o','n','y','m','o',
                                         'u','s'};
  private final char[] aPasswdLong    = {'-','-','A','n','o','n','y','m','o',
                                         'u','s','P','a','s','s','w','d'};

  /**
   * Sets initial values.
   *@since 0.0.2pA1
   */
  public JFTP()
  {
    this.stdin     = new StdIn();
    this.stdout    = new StdOut();
    this.stderr    = new StdErr();
  }

  /**
   * Starts the prog.
   *@param CommandLineArguments.
   *@since 0.0.2pA1
   */
  public static void main(String[] args)
  {
    JFTP j = new JFTP();
    j.parseArgs(args); // scanning cmdLine

    if (jftpSuper.isCli())
      j.cli();
    else
      j.gui();
  }

  /**
   * Starts the CommandLineInterface (cli) version.
   *@since 0.0.2pA1
   */
  private void cli()
  {
    Parser clp = new CommandLineParser();

    if (jftpSuper.getDebugLevel() >= 1)
      stdout.println( "DebugLevel: " + jftpSuper.getDebugLevel() );
    stdout.println( jftpSuper.getJFTPInfo() );
    stdout.println( jftpSuper.getCopyright() );
    stdout.println( jftpSuper.getLicenseInfo() );

    if (jftpSuper.isAutoconnect())
    { clp.parse("open "+ jftpSuper.getServerIP()); }

    while (true)
    {
      stdout.print("jftp> ");

      try
      {
        clp.parse( stdin.readLine() );
      }
      catch (IOException e)
      { stderr.println("Couldn't read from StdIn!"); }
    }
  }

  /**
   * Starts the GUI version.
   *@since 0.0.2pA1
   */
  private void gui()
  {
    //  GUI gui1 = new GUI();
  }

  /**
   * Parses the commandLine arguments.
   *@param a String array to parse
   *@since 0.0.2pA1
   *@version 1
   */
  private void parseArgs(String[] a)
  {
    int i;

    try
    {
      for (i=0; i < a.length; i++)
      {
        if ( a[i].equals(String.valueOf(helpShort)) ||
             a[i].equals(String.valueOf(helpLong))  )
        { this.showHelp(); } // Help
        else if ( a[i].equals(String.valueOf(passiveShort)) ||
                  a[i].equals(String.valueOf(passiveLong))  )
        { jftpSuper.setPassive(); } // Passive mode
        else if ( a[i].equals(String.valueOf(anonymousShort)) ||
                  a[i].equals(String.valueOf(anonymousLong))  )
        { jftpSuper.setAnonymous(); } // Anonymous mode
        else if ( a[i].equals(String.valueOf(aPasswdShort)) ||
                  a[i].equals(String.valueOf(aPasswdLong))  )
        { // Anonymous Passwd
          if ( jftpSuper.isAnonymous() )
          {
            if (this.nextParamExists(a, i))
              jftpSuper.setAnonymousPasswdTo(a[++i]);
            else
              this.showHelp();
          }
          else
          {
            stderr.println("You have set the anonymous password but not anonymous login!?!?");
            stderr.println("Exiting with error (1).");
            System.exit(1);
          }

          if (jftpSuper.getDebugLevel() >= 2)
            stdout.println("Anonymous Password: " + jftpSuper.getAnonymousPasswd());
        }
        else if ( a[i].equals(String.valueOf(guiShort)) ||
                  a[i].equals(String.valueOf(guiLong))  )
        { jftpSuper.setGui(); } // GUI
        else if ( a[i].equals(String.valueOf(cliShort)) ||
                  a[i].equals(String.valueOf(cliLong))  )
        { jftpSuper.setCli(); } // CLI
        else if ( a[i].equals(String.valueOf(verboseShort)) ||
                  a[i].equals(String.valueOf(verboseLong))  )
        {                       // Verbosity
          if ( (i < a.length)                  &&
               (this.nextParamIsNumeric(a, i)) )
          {
            short tmpDebug = (short)Integer.parseInt(a[++i]);
            if ((tmpDebug <= 2) && (tmpDebug >= 0))
              jftpSuper.setDebugLevel(tmpDebug);
          }
          else
          {
            stderr.println("Missing or wrong parameter.");
            this.showHelp();
          }
        }                     // Port
        else if ( a[i].equals(String.valueOf(portShort)) ||
                  a[i].equals(String.valueOf(portLong))  )
        {
          if ( (i < a.length)                  &&
               (this.nextParamIsNumeric(a, i)) )
          {
            int tmpPort = Integer.parseInt(a[++i]);
            if ((tmpPort >= 1) && (tmpPort <= 65535)) // Ok !
            {
              jftpSuper.setServerPortTo( tmpPort );
              stdout.println("Don't use std port. Using "+jftpSuper.getServerPort()+" instead.");
            }
            else
            {
              stdout.println("!WARNING! Specified port not in proper range.");
              stdout.println("!WARNING! Falling back to default port (21).");
            }
          }
          else
          {
            stderr.println("Missing or wrong parameters");
            this.showHelp();
          }
        }
        else
        {
          if ( i == (a.length -1) ) // Last parameter
          {
            if (jftpSuper.setServerTo( a[i] ))
              jftpSuper.setAutoconnect();
          }
          else
          {
            stdout.println("Unknown Option: `" + a[i] + "'");
            this.showHelp();
          }
        }
      }

    }
    catch (Exception e)
    { stdout.print("Exception: "); e.printStackTrace(); }

  //if (jftpSuper.isPassive())  stdout.println("PASSIVE mode prefered.");
  if (jftpSuper.isAnonymous())stdout.println("We'll logon as \"anonymous\".");
  //if (jftpSuper.isCli())      stdout.println("starting CLI.");
  if (jftpSuper.isGui())      stdout.println("starting GUI.");
  }

  /**
   * Checks if the next parameter is a numeric value.
   *@param  a String array of parameters to check
   *@param  pos current position in array a
   *@return true if the next parameter exists and is numeric;otherwise false
   *@since 0.0.2pA2
   */
  private boolean nextParamIsNumeric(String[] a, int pos)
  {
    boolean rc = false;
    int tmp;

    try
    {
      tmp = Integer.parseInt(a[(++pos)]);
      rc  = true;
    }
    catch (IndexOutOfBoundsException iobe)
    {
      if (jftpSuper.getDebugLevel() >= 2)
        stderr.println("No more parameters.");
    }
    catch (NumberFormatException nfe)
    {
      if (jftpSuper.getDebugLevel() >= 2)
        stderr.println("No more parameters.");
    }

    return rc;
  }

  /**
   * Checks if the next parameter exists.
   *@param  a String array to check
   *@param  pos current position in array a
   *@return true if the next parameter exists; else false
   *@since  0.0.2pA2
   */
  private boolean nextParamExists(String[] a, int pos)
  {
    boolean rc = true;

    try
    { String tmp = a[++pos]; }
    catch (IndexOutOfBoundsException iobe)
    { rc = false; }

  // maybe "(pos < a.size) ? (return true) : (return false)

    return rc;
  }

  /**
   * Shows the help screen.
   *@since 0.0.1
   */
  private void showHelp()
  {
    /*
     * Verbosity level > 1 should only be used for development.
     * Stable versions should only contain an option for setting verbosity to
     * true or false.
     */
    stdout.println(jftpSuper.getJFTPInfo());
    stdout.println("usage: JFTP [-h[c|g]pa] [Server]");
    stdout.println("       JFTP [--help][--verbose X][--passive][--anonymous[--anonymousPasswd X]]");
    stdout.println("            [--port X][Server]\n");
    stdout.println("       -h,    --help               Shows this help");
    stdout.println("       -c,    --cli                Starts the CLI-version (Default).");
    stdout.println("       -g,    --gui                Starts the GUI-version.");
    stdout.println("       -p,    --passive            Force passive mode ftp.");
    stdout.println("                                   (Default is active mode ftp(NOT now))");
    stdout.println("       -a,    --anonymous          Causes jftp to bypass normal login procedure");
    stdout.println("                                   and use anonymous login instead.");
    stdout.println("       -Ap X, --AnonymousPasswd X  Causes jftp to use X as anonymous password.");
    stdout.println("       -v X,  --verbose X          Sets the vorbosity level where X = 0 - 2.");
    stdout.println("       -P X,  --Port X             Sets the port number to X.");
    System.exit(0); // <- !EXIT!
  }
}
