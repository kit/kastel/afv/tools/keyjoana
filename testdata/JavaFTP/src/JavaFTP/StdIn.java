/*
 * Name:    StdIn.java (extends StdIO (extends JFTPSuper))
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz Creation
 * 0.1.99t5 01.08.2001 Tobias Kranz added `readPassword()'
 */
package JavaFTP;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Represents the standard input channel (StdIn).
 *@since 0.0.2pA1
 */
public class StdIn
  extends StdIO
{
  private BufferedReader br;

  public StdIn()
  {
    this.br = new BufferedReader(
                new InputStreamReader( System.in ));
  }

  /**
   * Reads a whole line of data from the standard input channel.
   *@return The String beeing read.
   *@throws IOException if reading fails.
   *@since  0.0.2pA1
   */
  protected String readLine()
    throws IOException
  {
    return ( (String)br.readLine() );
  }

  /**
   * Reads a single char of data from the standard input channel.
   *@return The char beeing read.
   *@throws IOException if reading fails.
   *@since  0.0.2pA1
   */
  protected char readChar()
    throws IOException
  {
    return ( (char)br.read() );
  }

  /**
   * Reads an int from the standard input channel.
   *@return The int being read
   *@throws IOException if reading fails
   *@since  0.0.2pA1
   */
  protected int readInt()
    throws IOException
  {
    return ( Integer.valueOf(br.readLine()).intValue() );
  }

  /**
   * Reads an InetAddress from the standard input channel.
   *@return The InetAddress read
   *@throws IOException if reading fails
   *@throws UnknownHostException if host is unknown
   *@since  0.0.2pA1
   */
  protected InetAddress readInetAddress()
    throws IOException,
           UnknownHostException
  {
    return ( InetAddress.getByName(br.readLine()) );
  }

  /**
   * Reads a password and dissables the terminal-echo.
   *@return  The Password read from StdIn
   *@throws  IOException if reading fails
   *@since   0.1.99t5
   *@version 1
   */
  protected String readPasswd()
    throws IOException
  {
    String rc = "";
    StringBuffer sb = new StringBuffer();
	char c;

    /* Font-Colors:
     * 
     * Font-Colors are from 29 - 39.
     * Background-Colors beginning with 40.
     *
     * #  color
     * 29 Gray
     * 30 Black
     * 31 DarkRed
     * 32 DarkGreen
     * 33 Brown/DarkOrange
     * 34 DarkBlue
     * 35 Purple
     * 36 Marine
     * 37 Gray
     * 38 White
     * 39 Gray
     */
	 
    // Set Font-Color to Black
    sb.append('\u001b');
    sb.append('[');
    sb.append( 30); // Color
    sb.append('m');
    System.out.print( sb.toString() ); // Set it.

    // read
    rc = br.readLine();

    // (re)Set Font-Color to Gray
    sb = new StringBuffer(); // Reset it
    sb.append('\u001b');
    sb.append('[');
    sb.append( 39); // Color
    sb.append('m');
    System.out.print( sb.toString() ); // Set it.

    return ( rc );
  }
}
