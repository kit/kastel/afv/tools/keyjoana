/*
 * Name:    JFTPIO.java (extends JFTPSuper)
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz Creation
 */
package JavaFTP;

import JavaFTP.*;

/**
 * Non usable class.
 *@since 0.0.2pA1
 */
public class JFTPIO
  extends JFTPSuper
{
}
