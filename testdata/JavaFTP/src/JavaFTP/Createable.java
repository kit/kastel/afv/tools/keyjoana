/*
 * Name:    Createable.java  [INTERFACE] (extends JFTPSuperInterface)
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.1.99t4 19.07.2001 Tobias Kranz Creation
 */
package JavaFTP;

/**
 * Defines methods to create directories and files.
 *@since 0.1.99t4
 */
public interface Createable
  extends JFTPSuperInterface
{
  /**
   * Creates a Directory on the server with the given name
   *@return  True if successfully created; otherwise false.
   *@since   0.1.99t4
   *@version 1
   */
  public boolean createRemoteDir(String dirName);
  
  /**
   * Creates a Directory on the local fs with the given name
   *@return  True if successfully created; otherwise false.
   *@since   0.1.99t4
   *@version 1
   */
  public boolean createLocalDir(String dirName);
}
