/*
 * Name:    Parser.java (extends JFTPSuper)
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz Creation
 */
package JavaFTP;

/**
 * Abstract class. Defines methods to parse Strings.
 *@since 0.0.2pA1
 */
public abstract class Parser
  extends JFTPSuper
{
  public Parser()
  {
   boolean busy = false; /*
                          * There was an error msg at compile-time that 
                          * let me do such strange things. ;-)
                          */ 
  }

  abstract void parse(String s);
}
