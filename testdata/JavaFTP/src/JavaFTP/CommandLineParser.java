/*
 * Name:    CommandLineParser.java (extends Parser (extends JFTPSuper))
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz Creation.
 * 0.0.2pA2 03.05.2001 Martin Loh   GET implemented
 * 0.0.2pA4 15.05.2001 Tobias Kranz fixed `containsWildcards(String, int)' &&
 *                                  `parse(String)'
 * 0.0.2pA4 05.06.2001 Tobias Kranz added `license' command
 * 0.1.99   10.06.2001 Tobias Kranz added possibility to set Tx modes.
 * 0.1.99t2 17.07.2001 Tobias Kranz 2nd fix of `containsWildcards(String, int)'
 *                                  added 'createPattern(String)' &&
 *                                  `comparePatternWithList(String, String[])',
 *                                  implemented MGET and MPUT and the
 *                                  "local dir list" command-group.
 * 0.1.99t4 19.07.2001 Tobias Kranz added `void pager(String)' && fixed bug with
 *                                  MGET && added `String getKnownCommands()' &&
 *                                  implemented MKDIR && LMKDIR && PWD && LPWD
 * 0.1.99t5 23.07.2001 Tobias Kranz improved lcd
 */
package JavaFTP;

import java.util.StringTokenizer;
import java.util.NoSuchElementException;
import java.util.Vector;
//import java.util.regex; // Causes compiler errors. *why*

import java.io.IOException;

/**
 * Parses the CommandLine for known FTP-Commands.<BR>
 * This class is used by JFTP.<BR>
 *@since  0.0.2pA1
 */
public class CommandLineParser
  extends Parser
{
  private final char[] listLong1  = {'l'};
  private final char[] listLong2  = {'l','s',' ','-','l'};
  private final char[] listShort1 = {'l','s'};
  private final char[] listShort2 = {'d','i','r'};
  // the same for local
  private final char[] llistLong1  = {'l','l'};
  private final char[] llistLong2  = {'l','l','s',' ','-','l'};
  private final char[] llistShort1 = {'l','l','s'};
  private final char[] llistShort2 = {'l','d','i','r'};

  private FTPCmdServer fcs;
  private StdIn  stdin  = new StdIn();
  private StdOut stdout = new StdOut();
  private StdErr stderr = new StdErr();
  
  /**
   * Constructor
   *@since The beginning of Time.
   */
  public CommandLineParser()
  { this.fcs = new FTPCmdServer(); }

  /**
   * Parses the CommandLine.
   *@param   s String to parse.
   *@version 6
   *@since   0.0.2pA1
   */
  public void parse(String s)
  {
    int i;

    try
    { 

      /*
       * Have you ever wondered how the parsing works?
       * There are 3 main parts:
       * At the first point we are checking for commands that can be entered
       * in any situation. At the second point we are looking for commands
       * that only makes sense if we're connected. And at least there's a
       * section at the end where all command are handled which may entered
       * in an unconnected status.
       */

      s = s.trim();

      if (jftpSuper.getDebugLevel() >= 2)
        stderr.println("parsing \""+s+"\"");

      // OPENs a connection
      if (s.startsWith("open"))
      { this.logon( s ); }
      else if ( s.equals("info") )
      { jftpSuper.printLongJFTPInfo(); }
      else if ( s.equals("license") )
      { pager( jftpSuper.getLicense() ); }
      else if ( (s.equals("quit")) ||
                (s.equals("exit")) )
      { stdout.println( "obsolete. Use `bye' instead." ); }
      // LocalLiSt short
      else if ( s.equals(String.valueOf(llistShort1)) ||
                s.equals(String.valueOf(llistShort2)) )
      { stdout.print( fcs.getLocalShortDirList() ); }
      // LocalLiSt Long
      else if ( s.equals(String.valueOf(llistLong1)) ||
                s.equals(String.valueOf(llistLong2)) )
      { stdout.print( fcs.getLocalLongDirList() ); }
      // HELP
      else if ( s.equals("help") )
      {
        String tmp;
        tmp = getKnownCommands();

        if (jftpSuper.isConnected())
        {
          tmp += "Server Commands:\n";
          fcs.send( s ); // Sends `help' to the server
          tmp += fcs.readCtrl(); // Reads the servers commands.
        }
        pager( tmp );
      }
      // LMKDIR
      else if ( s.startsWith("lmkdir ") )
      {
        if ( fcs.createLocalDir( s.substring( 7 ) ) )
          stdout.println("Okay, directory \""+s.substring(7)+"\" was created.");
        else
          stdout.println("Unable to create a directory.");
      }
      // LRMDIR
      else if ( s.startsWith("lrmdir ") )
      {
        if ( fcs.removeLocalDir( s.substring( 7 ) ) )
          stdout.println("Okay, directory \""+s.substring(7)+"\" was removed.");
        else
          stdout.println("Unable to remove that directory.");
      }
      // LPWD
      else if ( s.equals("lpwd") )
      { stdout.println( fcs.printLocalWorkingDir() ); }
      // LCD
      else if (s.startsWith("lcd "))
      {
        if (! fcs.changeLocalWorkingDir( s.substring( 4 ) ))
          stdout.println("Can't change working dir to: " + s.substring( 4 ));
        else
          stdout.println("Okay, working dir changed.");
      }

      // The following if's are only interresting if we're connected
      else if (jftpSuper.isConnected()) 
      {
        // CLOSEs a connection
        if (s.equals("close"))
        { fcs.disconnect(); }
        // Change working Directory
        else if ( s.startsWith("cd") )
        { fcs.changeRemoteWorkingDir( s.substring(3).trim() ); }
        // LiSt short
        else if ( s.equals(String.valueOf(listShort1)) ||
                  s.equals(String.valueOf(listShort2)) )
        { stdout.print( fcs.getShortDirList() ); }
        // LiSt Long
        else if ( s.equals(String.valueOf(listLong1)) ||
                  s.equals(String.valueOf(listLong2)) )
        { stdout.print( fcs.getLongDirList() ); }
        // GET
        else if ( s.startsWith("get") )
        {
          if (containsWildcards(s, 4))
          { stdout.println( "Wildcards are NOT allowed. Use `mget' instead."); }
          else
          {
            if (! fcs.getFile(s.substring(4).trim(), s.substring(4).trim()) )
              stderr.println("Unable to get the file.");
          }
        }
        // MGET
        else if ( s.startsWith("mget") )
        {
          String[] tmp = comparePatternWithList(
                           createPattern( s.substring(4).trim() ),
                           fcs.getDirListArray()
                         );
          fcs.getFiles( tmp, tmp );
        }
        // PUT
        else if ( s.startsWith("put") )
        {
          if (containsWildcards(s, 4))
          { stdout.println( "Wildcards are NOT allowed. Use `mput' instead."); }
          else
          {
            if (! fcs.putFile(s.substring(4)) )
              stdout.println("Unable to put the file.");
          }
        }
        // MPUT
        else if ( s.startsWith("mput") )
        {
          String[] tmp = comparePatternWithList(
                           createPattern( s.substring(4).trim() ),
                           fcs.getLocalDirListArray()
                         );
          fcs.putFiles( tmp );
        }
        // setting the TX mode
        else if ( s.startsWith("type") ||
                  s.equals("bin")      ||
                  s.equals("asc")      )
        { fcs.setTxMode( s ); }
        // MKDIR
        else if ( s.startsWith("mkdir ") )
        { fcs.createRemoteDir( s.substring( 6 ) ); }
        // RMDIR
        else if ( s.startsWith("rmdir ") )
        { fcs.removeRemoteDir( s.substring( 6 ) ); }
        // PWD 
        /* Yes, this costs performance, but its easier to change the progx
         * behavior, for example when adapting another protocol.
         */
        else if ( s.equals("pwd") )
        { stdout.println( fcs.printRemoteWorkingDir() ); }
        else if ( s.equals("bye") )
        {
          fcs.disconnect();
          System.exit(0); // EXIT(0)
        }
        else 
        {
          if (s.length() > 0) // prevents errors if you just type '\n'
          {
        if (jftpSuper.getDebugLevel() >= 2)
              stdout.println( "sending: " + s );
            fcs.send( s );
            stdout.println( fcs.readCtrl() );
          }
        }
      }
      else // At this point we are not connected:
      {
        if ( s.equals("bye") )
        {
          System.exit(0); // EXIT(0)
        }
        else
        {
          if ( s.length() > 0 ) // prevents errors if you just type '\n'
          { stderr.println("Unable to execute " + s + " ."); }
        }
      }
    }
    catch (StringIndexOutOfBoundsException siobe)
    {
      if (jftpSuper.getDebugLevel() >= 2)
        stderr.println("Error in main-loop of Clp: " + siobe.toString());
    }
    catch (NullPointerException npe)
    {
      if (jftpSuper.getDebugLevel() >= 2)
       stdout.println("NullPointerException caugth (in clp)");
    }
    catch (NoSuchElementException nsee)
    {
      if (jftpSuper.getDebugLevel() >= 2)
       stdout.println("NoSuchElementException caugth (in clp)");
    }
  }

  /** 
   * Will be implemented when j2sdk1.4 is released!
   */
  /*
   * IS implemented! But in FTPCmdServer. 
   *\
  private void getFiles(String pattern)
  { }
   */
  
  /**
   * Manages the whole authentication/logon procedure
   *@param   s String containing all params entered
   *@since   0.0.2pA4
   *@version 2
   */
  private void logon(String s)
  {
    boolean serverIsValid = false;
    String  server;

    /*
     * s.substring(5) will `return false' if s.length < 5 !!!
     */
    if (s.length() > 5)
      server = s.substring(5).trim();
    else
      server = "";
 
    if (jftpSuper.getDebugLevel() >= 2)
      stderr.println("server=\""+server+"\"");

    if (jftpSuper.isConnected())
    { stdout.println("Unable to re-connect; You must disconnect first..."); }
    else
    {
      while (! serverIsValid)
      {
        if (server.length() > 4)//What's the min length of a valid inet Address?
        { serverIsValid = true; }
        else
        {
          stdout.print("(host): ");
          try 
          { server = stdin.readLine(); }
          catch (IOException ioe) {}
        }
      }

      if (jftpSuper.getDebugLevel() >= 2)
        stdout.println("contacting server " + server);

      if (jftpSuper.setServerTo( server )) // setting (new) Server (Server is _really_ there...:~) )
      {
        fcs.connect();           // CONNECT !
        if (jftpSuper.isConnected())     // connected ?
        {
          //if (jftpSuper.getDebugLevel() >= 2)
            stdout.println( fcs.readCtrl() );

          if (! fcs.authenticate())
          {
            /*
             * Should be handled by authenticate.
             */
            //stderr.println("Unable to authenticate.");
            parse("close"); // close connection from Server
          }
        }
      }
      else
      { stderr.println("Cannot connect to server."); }
    }
  }


  /**
   * Checks if the given String contains any wildcards ('*'/'?').
   *@param s String to check
   *@param start int position to start at.
   *@return true if the given String contains any wildcards; otherwise not
   *@since 0.0.2pA3
   */
  private boolean containsWildcards(String s, int start)
  {
    boolean rc = false;
    int i;

    // Check for a valid range
    if (start <= s.length())
    {
      for (i=start; i < s.length(); i++)
      {
        if ( ((char)s.charAt( i ) == '*') ||
             ((char)s.charAt( i ) == '?') ||
             ((char)s.charAt( i ) == '[') )
        {
          rc = true;
          break;
        }
      }
    }

    return( rc );
  }

  /**
   * Creates a regular-expression-pattern usable by java.lang.util.regex from
   * any posix-regex.
   * Example: 
   *     Posix-regex:              java.lang.util.regex: (see j2sdk1.4-doc)
   *     *[0-9]t.ab?               {graph}*[0-9]t.ab{graph}? ||
   *                               {graph}*{num}?t.ab{graph}?
   *@param   pattern The posix-regex to compute
   *@return  The matching java.lang.regex-regex
   *@since   0.1.99t2
   *@version 1
   */
  private String createPattern(String pattern)
  {
    // Initial things
    StringTokenizer st;
    String pat = pattern, // local-temporary pattern
           rc  = "";

    if (jftpSuper.getDebugLevel() >= 2)
      stderr.println("Pattern="+pattern);
    /* <Parsing for `*''s> */
    st = new StringTokenizer(pat, "*");
    
    if (st.hasMoreTokens())
      rc += st.nextToken();

    while (st.hasMoreTokens())
      rc += "{graph}*" + st.nextToken();

    // Check for start and end
    if (pat.charAt(0) == '*')
      rc = "{graph}*" + rc;

    if ((pattern.charAt(pattern.length()-1) == '*'))
      rc += "{graph}*";
    /* </Parsing for `*''s> */


    // Resetting
    st  = null;
    pat = rc;
    rc  = "";


    /* <Parsing for `?''s> */
    st = new StringTokenizer(pat, "?");
    
    if (st.hasMoreTokens())
      rc += st.nextToken();

    while (st.hasMoreTokens())
      rc += "{graph}?" + st.nextToken();

    // Check for start and end
    if (pat.charAt(0) == '?')
      rc = "{graph}?" + rc;

    if ((pattern.charAt(pattern.length()-1) == '?'))
      rc += "{graph}?";
    /* </Parsing for `?''s> */

/* Should be used before running another loop on the pattern.
    // Resetting
    st  = null;
    pat = rc;
    rc  = "";
*/
    return ( rc );
  }

  /**
   * Compares a java.lang.String array with a given java.util.regex 
   * compatible regular expression and returns a list of all matching entries.
   *@param   pattern The java.util.regex pattern to use.
   *@param   list A java.lang.String array which should be compared to the
   *         pattern
   *@return  A java.lang.String array which all entries from list that are
   *         matching the pattern.
   *@since   0.1.99t2
   *@version 1
   */
  private String[] comparePatternWithList(String pattern, String[] list)
  {
    int i;
    java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
    java.util.regex.Matcher m;
    Vector tmp = new Vector(1);

	for (i=0; i < list.length; i++)
	{
	  m = p.matcher( list[i] );
	  
	  if (m.matches())
        tmp.add( (String)list[i] );
	}

    // Vector tmp -> String rc[n]
    String[] rc = new String[tmp.size()];

    for (i=0; i < tmp.size(); i++)
      rc[i] = (String)tmp.elementAt( i );
	
	return ( rc );
  }

  /**
   * Show any txpe of text side by side. A pager.
   * This method looks for '\n''s (24 times), then shows a "Press return.."-msg
   * and waits for any return-terminated input and restarts that loop until EOF.
   *@param   text The text to display
   *@since   0.1.99.test4
   *@version 3
   */
  private void pager(String text)
  {
    int i,
        j=0;
    char c;

    text += "\n"; // Ugly way to make sure the prompt comes up in a new line.

    while (text.length() > j)
	{
      for (i=0; i < 23; i++)
      {
        do
        {
          c = text.charAt( j++ );
          stdout.print( c );
        } while (c != '\n');
      }

      stdout.print("\nPress [Return] to continue...");
      try
      { stdin.readLine(); }
      catch (IOException whoCares) { }
	}
  }

  /**
   * Returns a java.lang.String containing all commands known by `this'.
   *@return  A java.lang.String containing all commands.
   *@since   0.1.99test4
   *@version 1
   */
  private String getKnownCommands()
  {
    return
    (
"Commands recognized by JavaFTP: (* => unimplemented)\n" +
"\n" +
"Conntection-commands:\n" +
"\n" +
"open           Asks you the server's name or ip to connect to.\n" +
"open SERVER    Opens a connection to the server SERVER.\n" +
"close          Closes the active connection.\n" +
"\n" +
"List-commands:\n" +
"l\n" +
"ls -la         Shows a detailed list of the active directory " +
                "on the server.\n" +
"\n" +
"ls\n" +
"dir            Shows a short list of the active directory on the server.\n" +
"\n" +
"ll\n" +
"lls -la        Shows a detailed list of the active directory on " +
                "the localhost.\n" +
"\n" +
"lls\n" +
"ldir           Shows a short list of the active directory on " +
                "the localhost.\n" +
"\n" +
"Options and switches:\n" +
"type X         Sets the Tx-mode to X. (X=i(binary)||X=a(ascii))\n" +
"asc            Sets the Tx-mode to ascii.\n" +
"bin            Sets the Tx-mode to binary.\n" +
"\n" +
"Directory relative commands:\n" +
"cd PATH        Changes the active directory on the server to PATH.\n" +
"lcd PATH       Changes the active directory on the localhost to PATH.\n" +
"pwd            Shows the current working directory on the server.\n" +
"lpwd           Shows the current working directory on the localhost.\n" +
"mkdir DIR      Creates a directory named DIR on the server.\n" +
"rmdir DIR      Removes a directory named DIR on the server.\n" +
"lmkdir DIR     Creates a directory named DIR on the localhost.\n" +
"lrmdir DIR     Removes a directory named DIR on the localhost.\n" +
"\n" +
"Transfer-commands:\n" +
"get FILE       Gets a file named FILE\n" +
"put FILE       Puts a file named FILE\n" +
"mget PATTERN   Gets all files that matches the pattern PATTERN.\n" +
"mput PATTERN   Puts all files that matches the pattern PATTERN.\n" +
"\n" +
"Misc stuff:\n" +
"info           Shows a short info about JavaFTP.\n" +
"license        Shows the license under " +
                "wich you are allowed to use JavaFTP.\n" +
"help           Shows this helpscreen.\n" +
"bye            Quits the program and closses all open connections.\n" +
"\n"
    );
  }
}
