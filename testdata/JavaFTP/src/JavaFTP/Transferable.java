/*
 * Name:    Transferable.java  [INTERFACE] (extends JFTPSuperInterface)
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz Creation
 * 0.0.2pA2 03.05.2001 Tobias Kranz Changed `receive-Line/-Block' to `receive()'
 * 0.1.0    09.06.2001 Martin Loh   Changed the `getFile()' and `putFile()'
 *                                          method definitions so that they
 *                                          return an boolean and have Strings
 *                                          as args.
 * 0.1.99t2 17.07.2001 Tobias Kranz Added `getFiles(String[], String[])',
 *                                  corrected some comments.
 * 0.1.99t4 19.07.2001 Tobias Kranz cleaned up
 */
package JavaFTP;

/**
 * Defines methods to transfer "raw" data and files.
 *@since 0.0.2pA1
 */
public interface Transferable
  extends JFTPSuperInterface
{
  /**
   * Reads data from the ctrl-channel.
   *@return  The data read.
   *@since   0.0.2pA4
   *@version 0
   */
  public String readCtrl();
  
  /**
   * Receives a whole block of data.
   *@return  The block received
   *@since   0.0.2pA4
   *@version 0
   */
  public String readCtrlBlock();

  /**
   * Reads data until end of Stream
   *@return  The data read.
   *@since   0.0.2pA4
   *@version 0
   */
  public String readData();
  
  /**
   * Reads a whole block of data. 
   *@return  The block received
   *@since   0.0.2pA4
   *@version 0
   */
  public String readDataBlock();

  /**
   * Reads a single line of data.
   *@return  The data received.
   *@since   0.0.2pA4
   *@version 0
   */
  public String readDataLine(); 

  /**
   * Recieves a list of Files `serverlist` and saves it to 'localList'.
   *@param   serverList Filelist (java.lang.String array)
   *@param   localList  Filelist (java.lang.String array)
   *@return  true if successfully transfered; otherwise false.
   *@since   0.1.99t2
   *@version 1
   */
  public boolean getFiles(String[] serverList, String[] localList);

  /**
   * Recieves a File with name `Serverfile` and saves it to 'localfile'.
   *@param   ServerFile Filename
   *@param   LocalFile Filename
   *@return  true if successfully transfered; otherwise false.
   *@since   0.0.2pA1
   *@version 1
   */
  public boolean getFile(String ServerFile, String LocalFile);

  /**
   * Puts a File named `LocalFile`.
   *@param   LocalFile Filename
   *@return  true if the file was transfered successfully; false if not.
   *@since   0.0.2pA1
   *@version 0
   */
  public boolean putFile(String LocalFile);

  /**
   * Sends a string of data.
   *@param   s String to send.
   *@since   0.0.2pA1
   *@version 0
   */
  public void send(String s);
}
