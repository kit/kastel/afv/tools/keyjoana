/*
 * Name:    NetIO.java (extends JFTPIO)
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz Creation
 */
package JavaFTP;

/**
 * Class for network-io.
 *@since 0.0.2pA1
 */
public class NetIO
  extends JFTPIO
{
}
