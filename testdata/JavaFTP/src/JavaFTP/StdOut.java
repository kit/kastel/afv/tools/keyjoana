/*
 * Name:    StdOut.java (extends StdIO (extends JFTPSuper))
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz Creation
 * 0.0.2pA4 15.05.2001 Tobias Kranz fixed "double newline"-bug
 */
package JavaFTP;

/**
 * Represents the standard output channel (StdOut)
 *@since 0.0.2pA1
 */
public class StdOut
  extends StdIO
{
  /**
   * Prints an object to StdOut.
   *@param o Object to print
   *@since 0.0.2pA1
   */
  public void print(Object o)
  {
    if (jftpSuper.isCli())
    {
      System.out.print( o );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a double to StdOut.
   *@param d double to print
   *@since 0.0.2pA1
   */
  public void print(double d)
  {
    if (jftpSuper.isCli())
    {
      System.out.print( d );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a float to StdOut.
   *@param f float to print
   *@since 0.0.2pA1
   */
  public void print(float f)
  {
    if (jftpSuper.isCli())
    {
      System.out.print( f );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a long to StdOut.
   *@param l long to print
   *@since 0.0.2pA1
   */
  public void print(long l)
  {
    if (jftpSuper.isCli())
    {
      System.out.print( l );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints an int to StdOut.
   *@param i int to print
   *@since 0.0.2pA1
   */
  public void print(int i)
  {
    if (jftpSuper.isCli())
    {
      System.out.print( i );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a short to StdOut.
   *@param s short to print
   *@since 0.0.2pA1
   */
  public void print(short s)
  {
    if (jftpSuper.isCli())
    {
      System.out.print( s );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a byte to StdOut.
   *@param b byte to print
   *@since 0.0.2pA1
   */
  public void print(byte b)
  {
    if (jftpSuper.isCli())
    {
      System.out.print( b );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a char to StdOut.
   *@param c char to print
   *@since 0.0.2pA1
   */
  public void print(char c)
  {
    if (jftpSuper.isCli())
    {
      System.out.print( c );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a boolean to StdOut.
   *@param b boolean to print
   *@since 0.0.2pA1
   */
  public void print(boolean b)
  {
    if (jftpSuper.isCli())
    {
      System.out.print( b );
    }
    else
    {
      // Insert GUI-Code here
    }
  }



  /**
   * Prints an object and "\n\r" to StdOut.
   *@param   o Object to print
   *@since   0.0.2pA1
   *@version 0
   */
  public void println(Object o)
  {
    if (jftpSuper.isCli())
    {
      if ( (char)o.toString().charAt(o.toString().length()-1) == '\n' )
        System.out.print( o );
      else
        System.out.println( o );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a double and "\n\r" to StdOut.
   *@param d double to print
   *@since 0.0.2pA1
   */
  public void println(double d)
  {
    if (jftpSuper.isCli())
    {
      System.out.println( d );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a float and "\n\r" to StdOut.
   *@param f float to print
   *@since 0.0.2pA1
   */
  public void println(float f)
  {
    if (jftpSuper.isCli())
    {
      System.out.println( f );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a long and "\n\r" to StdOut.
   *@param l long to print
   *@since 0.0.2pA1
   */
  public void println(long l)
  {
    if (jftpSuper.isCli())
    {
      System.out.println( l );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints an int and "\n\r" to StdOut.
   *@param i int to print
   *@since 0.0.2pA1
   */
  public void println(int i)
  {
    if (jftpSuper.isCli())
    {
      System.out.println( i );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a short and "\n\r" to StdOut.
   *@param s short to print
   *@since 0.0.2pA1
   */
  public void println(short s)
  {
    if (jftpSuper.isCli())
    {
      System.out.println( s );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a byte and "\n\r" to StdOut.
   *@param b byte to print
   *@since 0.0.2pA1
   */
  public void println(byte b)
  {
    if (jftpSuper.isCli())
    {
      System.out.println( b );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a char and "\n\r" to StdOut.
   *@param c char to print
   *@since 0.0.2pA1
   */
  public void println(char c)
  {
    if (jftpSuper.isCli())
    {
      System.out.println( c );
    }
    else
    {
      // Insert GUI-Code here
    }
  }

  /**
   * Prints a boolean and "\n\r" to StdOut.
   *@param b boolean to print
   *@since 0.0.2pA1
   */
  public void println(boolean b)
  {
    if (jftpSuper.isCli())
    {
      System.out.println( b );
    }
    else
    {
      // Insert GUI-Code here
    }
  }
}
