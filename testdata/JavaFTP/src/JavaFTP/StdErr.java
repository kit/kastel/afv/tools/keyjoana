/*
 * Name:    StdErr.java (extends StdIO (extends JFTPSuper))
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz Creation
 */
package JavaFTP;

/**
 * Represents the standart error channel (StdErr).<BR>
 * The trailing '*' will be removed in stable versions.
 *@since 0.0.2pA1
 */
public class StdErr
  extends StdIO
{
  /**
   * Prints an object to StdErr.
   *@param o Object to print
   *@since 0.0.2pA1
   */
  public void print(Object o)
  { System.err.print("*" + o ); }

  /**
   * Prints a double to StdErr.
   *@param d double to print
   *@since 0.0.2pA1
   */
  public void print(double d)
  { System.err.print("*" + d ); }

  /**
   * Prints a float to StdErr.
   *@param f float to print
   *@since 0.0.2pA1
   */
  public void print(float f)
  { System.err.print("*" + f ); }

  /**
   * Prints a long to StdErr.
   *@param l long to print
   *@since 0.0.2pA1
   */
  public void print(long l)
  { System.err.print("*" + l ); }

  /**
   * Prints an int to StdErr.
   *@param i int to print
   *@since 0.0.2pA1
   */
  public void print(int i)
  { System.err.print("*" + i ); }

  /**
   * Prints a short to StdErr.
   *@param s short to print
   *@since 0.0.2pA1
   */
  public void print(short s)
  { System.err.print("*" + s ); }

  /**
   * Prints a char to StdErr.
   *@param c char to print
   *@since 0.0.2pA1
   */
  public void print(char c)
  { System.err.print("*" + c ); }

  /**
   * Prints a byte to StdErr.
   *@param b byte to print
   *@since 0.0.2pA1
   */
  public void print(byte b)
  { System.err.print("*" + b ); }

  /**
   * Prints a boolean to StdErr.
   *@param b boolean to print
   *@since 0.0.2pA1
   */
  public void print(boolean b)
  { System.err.print("*" + b ); }



  /**
   * Prints an object and "\n\r" to StdErr.
   *@param o Object to print
   *@since 0.0.2pA1
   */
  public void println(Object o)
  { System.err.println("*" + o ); }

  /**
   * Prints a double and "\n\r" to StdErr.
   *@param d double to print
   *@since 0.0.2pA1
   */
  public void println(double d)
  { System.err.println("*" + d ); }

  /**
   * Prints a float and "\n\r" to StdErr.
   *@param f float to print
   *@since 0.0.2pA1
   */
  public void println(float f)
  { System.err.println("*" + f ); }

  /**
   * Prints a long and "\n\r" to StdErr.
   *@param l long to print
   *@since 0.0.2pA1
   */
  public void println(long l)
  { System.err.println("*" + l ); }

  /**
   * Prints a int and "\n\r" to StdErr.
   *@param i int to print
   *@since 0.0.2pA1
   */
  public void println(int i)
  { System.err.println("*" + i ); }

  /**
   * Prints a short and "\n\r" to StdErr.
   *@param s short to print
   *@since 0.0.2pA1
   */
  public void println(short s)
  { System.err.println("*" + s ); }

  /**
   * Prints a char and "\n\r" to StdErr.
   *@param c char to print
   *@since 0.0.2pA1
   */
  public void println(char c)
  { System.err.println("*" + c ); }

  /**
   * Prints a byte and "\n\r" to StdErr.
   *@param b byte to print
   *@since 0.0.2pA1
   */
  public void println(byte b)
  { System.err.println("*" + b ); }

  /**
   * Prints a boolean and "\n\r" to StdErr.
   *@param b boolean to print
   *@since 0.0.2pA1
   */
  public void println(boolean b)
  { System.err.println("*" + b ); }
}
