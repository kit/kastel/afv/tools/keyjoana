/*
 * Name:    NetReader.java (extends NetIO (extends JFTPIO (extends JFTPSuper)))
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.1    12.03.2001 Tobias Kranz creation.
 *          13.03.2001 Tobias Kranz changed from Thread to regular class
 *                                  (nearly total rewrite).
 * 0.0.1pA3 15.03.2001 Tobias Kranz splitted from JFTP; nothing big.
 * 0.0.1pA6 03.04.2001 Tobias Kranz added comments.
 * 0.0.1pA7 04.04.2001 Tobias Kranz splitted `read()' to readLine() and
 *                                  readBlock().
 * 0.0.2pA2 03.05.2001 Tobias Kranz (re)added `read()' which implements now
 *                                  `readLine()' & `readBlock()'
 * 0.0.2pA4 15.05.2001 Tobias Kranz removed everything! created `readStream()'
 *                                  which must be wrapped by `receiveLine()' &&
 *                                  `receiveBlock()' in FTPCmdServer.java .
 *                                  See README.DEVELOPMENT for details.
 * 0.1.99   10.06.2001 Tobias Kranz added `getInputStream()'
 */

package JavaFTP;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

import java.io.IOException;

/**
 * Class NetReader may be used to read any kind of data through an existing
 * Socket.
 *@since 0.0.1
 */
public class NetReader
  extends NetIO
{
  private InputStream inStream;
  private StdOut stdout = new StdOut();
  private StdErr stderr = new StdErr();

  /**
   * Sets the InputStream.
   *@param socket Socket to listen on (This does NOT mean a listening socket!).
   *@since 0.0.1
   */
  public NetReader(Socket socket)
  {
    try
    {
      this.inStream = socket.getInputStream();
    }
    catch (Exception e)
    { stderr.println("Unable to get InputStream from Socket."); }
  }

  /* 
   * Get the stuff in.
   */
  /**
   * Autodetects the number of lines to read and give them back.
   *@return String read from server
   *@since 0.0.2pA2
   */
/*   
  public String read()
  {
    String rc = "",
           line;
    int i;

    InputStreamReader isr = new InputStreamReader(inStream);

    try
    {
      do                     // start reading the whole block (or just a line..)
      {
        line = "";

        do                   // beginn reading the line
        {
          i     = isr.read();/*? should we make `isr.ready()' call(s) ?
          line += (char)i;
        } while (i != 10 );  // until `(char)10' (means CR (0x0A)) is read

        rc += line;          // adding the read line to the return value

      } while ((char)line.charAt(3) != ' ');
    }
    catch (IOException ioe)
    { /* shall we do something here or should we throw this Exception ?? }

    return( rc.substring(0, (rc.length() - 1)) ); // trim last char ('\n')
  }
*/
  /**
   * Returns a InputStreamReader reading from the InputStream of the given
   * Socket.
   *@return InputStreamReader
   *@since  0.0.2pA4
   */
  public InputStreamReader getStream()
  { return( new InputStreamReader( inStream ) ); }

  /**
   * Returns the InputStream of the used socket
   *@return  An InputStream
   *@since   0.1.99
   *@version 0
   */
  public InputStream getInputStream()
  { return( inStream ); }

  /**
   * Reads a single line of data from the server .
   *@return Whatever the ftpserver sends us.
   *@since  0.0.1
   *@deprecated
   */
/*   
  public String readLine()
  { return( this.read() ); } // just to stay compatible
*/ 

  /**
   * Reads a whole block of data from the server.
   *@return Whatever the server sends.
   *@since  0.0.1
   *@deprecated
   */
/*  
  public String readBlock()
  {
    String rc = "";
    boolean done = false;
    int i; //char read from server

    try
    {
      InputStreamReader isr = new InputStreamReader(inStream);

      while (! done)
      {
        i = isr.read();

        if (i == -1) // -1 ~ EOF
        {
          if (jftpSuper.getDebugLevel() >= 2)
            stdout.println("finished reading block");
          done = true;
        }
        else
        {
          rc += (char)i;
          if (jftpSuper.getDebugLevel() >= 2)
            stdout.println("Reading " + rc);
        }
      }
    }
    catch (Exception e)
    { stderr.println("Exception while reading from server."+e.toString()); }

    return rc;
  }
*/ 
}
