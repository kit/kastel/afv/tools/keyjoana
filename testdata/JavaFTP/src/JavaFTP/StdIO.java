/*
 * Name:    StdIO.java (extends JFTPIO (extends JFTPSuper))
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz Creation
 */
package JavaFTP;

/**
 * Class for standard-io
 *@since 0.0.2pA1
 */
public class StdIO
  extends JFTPIO
{
}
