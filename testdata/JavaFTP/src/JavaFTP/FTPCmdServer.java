/*
 * Name:    FTPCmdServer.java (extends JFTPSuper)
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name               changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz       Creation
 * 0.0.2pA2 02.05.2001 Svenja Wittstadt   authenticate method implemented
 * 0.0.2pA3 06.05.2001 Tobias Kranz       added skeleton of `int getPort()'.
 * 0.0.2pA4 15.05.2001 Tobias Kranz       changed `String receiveLine()' &
 *                                        `String receiveBlock()'.
 * 0.0.2pA4 28.05.2001 Tobias Kranz       some small fixes...
 * 0.0.2pA4 06.06.2001 Tobias Kranz       REWRITE from scratch
 * 0.1.0    07.06.2001 Tobias Kranz &&
 *                     Sebastian Schipper Completed the rewrite.
 *                                        EVERYTHING�s FINE! ;-]
 * 0.1.0    09.06.2001 Martin Loh         Implemented the putFile Method and
 *                                        updated the getFile method
 * 0.1.99   10.06.2001 Tobias Kranz       Fixed bugs: cantListAfterPut, cantGet.
 *                                        add `setTxMode(String)' &
 *                                        `getFileSize(String)'
 * 0.1.99t1 11.06.2001 Tobias Kranz       added Progressbar in `putFile()' &&
 *                                        `getFile()'.
 * 0.1.99t1 12.06.2001 Tobias Kranz       added `getFiles(String regex)'
 *                                        'not sure if this is the right place..
 * 0.1.99t1 19.06.2001 Tobias Kranz       improved `getFileSize(String)' I
 * 0.1.99t2 17.07.2001 Tobias Kranz       improved `getFileSize(String)' II
 *                                        added `getDirListArray()' &&
 *                                        `getLocalDirListArray()' &&
 *                                        `getLocalLongDirList()' &&
 *                                        `getLocalShortDirList()'
 * 0.1.99t4 19.07.2001 Tobias Kranz       Made an improvement suggestion in 
 *                                        `getFile()' && started to implement
 *                                        the "Createable" && the "Removeable"
 *                                        interfaces.
 * 0.1.99t5 23.07.2001 Tobias Kranz       changed `getFile()' && `putFile()' to
 *                                        work in the local working dir
 * 0.1.99t5 01.08.2001 Tobias Kranz       changed `authenticate()'
 */
package JavaFTP;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.BindException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.util.StringTokenizer;

/**
 * This class contains the implementation of all supported FTP-Commands
 *@since 0.0.2pA1
 */
public final class FTPCmdServer
  extends JFTPSuper
    implements Connectable
              ,Authenticateable
              ,Listable
              ,Transferable
              ,Createable
              ,Removeable
              ,Changeable
{
  private Socket ctrlSock;
  private Socket dataSock;

  private NetReader ctrlReader;
  private NetWriter ctrlWriter;
  private NetReader dataReader;
  private NetWriter dataWriter;

  private InputStream  dataIs;
  private OutputStream dataOs;

  private InputStreamReader ctrlIsr;
  private InputStreamReader dataIsr;

  private StdIn  stdin  = new StdIn();
  private StdOut stdout = new StdOut();
  private StdErr stderr = new StdErr();

/* <START Implementing "Changeable"> */
  public boolean changeRemoteWorkingDir(String newDir)
  {
    boolean rc = false;
    String receive;

    send("cwd " + newDir);
    receive = readCtrl();
    stdout.println( receive );

    if (receive.equals("250"))
      rc = true;

    return ( rc );
  }

  public boolean changeLocalWorkingDir(String newDir)
  { return ( jftpSuper.setCurrentWorkingDir( newDir ) ); }

  /**
   * Sets the transfermode either to ascii or to binary
   *@param   mode The mode to use ("ascii" or "binary")
   *@return  true if mode is set, otherwise false
   *@since   0.1.99
   *@version 0
   */
  public boolean setTxMode(String mode)
  {
    boolean modeIsValid = false;

         if ( (mode.equals("ascii")) ||
              (mode.equals("asc"))   )
    {
      modeIsValid = true;
      send("type a");
    }
    else if ( (mode.equals("binary")) ||
              (mode.equals("bin"))    )
    {
      modeIsValid = true;
      send("type i");
    }
    else
    {
      stderr.println("Unable to recognize the desired tx mode.");
    }

    if (modeIsValid)
      stdout.println( readCtrl() );

    return ( modeIsValid );
  }

  public boolean setTxMode(char nm)
  {
    boolean rc = false;

         if ((nm == 'i') ||
             (nm == 'b') )
      rc = setTxMode("binary");
    else if (nm == 'a')
      rc = setTxMode("ascii");
    

    return ( rc );
  }
/* <END Implementing "Changeable"> */



/* <START Implementing "Removeable"> */

  /**
   * Removes a remote Directory called dirName
   *@param   dirName The name of the Directory to remove
   *@return  true if successfully removed; otherwise false
   *@since   0.1.99t4
   *@version 1
   */
  public boolean removeRemoteDir(String dirName)
  {
    boolean rc = false; // senseless; but the compiler wants it ;-)
    String receive;

    send( "rmd " + dirName.trim() );
    receive = readCtrl();
    stdout.println( receive );

    if (receive.startsWith("250"))
    { rc = true; }
    else if (receive.startsWith("550"))
    { rc = false; }

    return ( rc );
  }

  /**
   * Removes a local Directory called dirName
   *@param   dirName The name of the Directory to remove
   *@return  true if successfully removed; otherwise false
   *@since   0.1.99t4
   *@version 2
   */
  public boolean removeLocalDir(String dirName)
  {
    String cd = jftpSuper.getCurrentWorkingDir();
    File rmDir = new File( cd.substring(0, cd.length()-1) + dirName.trim() );

    return ( rmDir.delete() );
  }
  
/* <END Implementing "Removeable"> */



/* <START Implementing "Createable"> */

  /**
   * Creates a directory on the server named dirName
   *@param   dirName The name of the directory to create
   *@return  true if successfully created; otherwise false
   *@since   0.1.99t4
   *@version 1
   */
  public boolean createRemoteDir(String dirName)
  {
    boolean rc = false; // senseless; but the compiler wants it ;-)
    String receive;

    send( "mkd " + dirName.trim() );
    receive = readCtrl();
    stdout.println( receive );

    if (receive.startsWith("257"))
    { rc = true; }
    else if (receive.startsWith("521"))
    { rc = false; }

    return ( rc );
  }

  /**
   * Creates a directory on the localhost named dirName
   *@param   dirName The name of the directory to create
   *@return  true if successfully created; otherwise false
   *@since   0.1.99t4
   *@version 1
   */
  public boolean createLocalDir(String dirName)
  {
    File newDir = new File( jftpSuper.getCurrentWorkingDir() + 
                            File.separator +
                            dirName.trim() );

    return ( newDir.mkdir() );
  }
/* <END Implementing "Createable"> */



/* <START Implementing "Transferable"> */

  /**
   * Reads data from the FTPServer.
   *@return  String
   *@since   0.0.2pA4
   *@version 3
   */
  public String readCtrl()
  {
    String rc = "",
           line;
    int    i  = 0;

    try
    {
      do
      {
        line = "";

        do
        {
          i     = getCtrlStream().read();
          line += (char)i;
        } while (i != 10);

        rc += line;

      } while ((char)line.charAt(3) != ' ');
    }
    catch (Exception e)
    {
      stderr.println("Error while reading from server (in fcs): "+e.toString());
    }

    return ( rc );
  }

  /**
   * Reads a whole block of data from the ctrl-channel of the FTPServer.
   *@return  String
   *@since   0.0.2pA4
   *@version 2
   */
  public String readCtrlBlock()
  {
    String rc = "";
    int    i  = 0;    // char read from server.

    try
    {
      do
      {
        i = getCtrlStream().read();
        if (i == -1)
          break;
        rc += (char)i;
      } while (true);
    }
    catch (IOException e)
    { stderr.println("Error receiving Block."); }

    return ( rc );
  }

  /**
   * Reads data from the FTPServer.
   *@return  String
   *@since   0.0.2pA4
   *@version 1
   */
  public String readData()
  {
    String rc = "";
    int    i  = 0;    // char read from server.

    try
    {
      i = getDataStream().read();

      while (i != -1)
      {
        rc += (char)i;
        i = getDataStream().read();
//stderr.println("retrieving: " + (char) i);
      }
      closePassiveDataConnection();
    }
    catch (IOException e)
    { stderr.println("Error receiving Block."); }

    return ( rc );
  }


  /**
   * Reads a whole block of data from the data-channel of the FTPServer.
   *@return  String
   *@since   0.0.2pA4
   *@version 0
   */
  public String readDataBlock()
  {
    String rc = "";

    while ("this".equals("that"))
    {
      try
      {
        rc += getDataStream().read();
      }
      catch (Exception e)
      { stderr.println("Gotcha!"); }
    } // YES, this does NOT work.

    return( rc );
  }

  /**
   * Reads a single line of data from the data-channel of the FTPServer.
   *@return  String
   *@since   0.0.2pA4
   *@version 0
   */
  public String readDataLine()
  {
    String rc = "";

    while ("this".equals("that"))
    {
      try
      {
        rc += getDataStream().read();
      }
      catch (Exception e)
      { stderr.println("Gotcha!"); }
    } // YES, this does NOT work.

    return( rc );
  }

  /**
   * Sends a String to the FTPServer.
   *@param s String to send
   *@since 0.0.2pA1
   */
  public void send(String s)
  {
    ctrlWriter.write( s );
  }

  /**
   * Receives a list of files named `localList' and tx's them to the server.
   *@param   localList  a java.lang.String array where to save the files
   *@return  true if successfully transfered; else false
   *@since   0.1.99t2
   *@version 1
   */
  public boolean putFiles(String[] localList)
  {
    boolean rc = true;
    int i;

    for (i=0; i < localList.length; i++)
    {
      if (! putFile( localList[i] ))
        rc = false;
    }

    return ( rc );
  }
  
  /**
   * Receives a list of files named `serverList' and stores them in `localList'
   *@param   serverList a java.lang.String array of files to get
   *@param   localList  a java.lang.String array where to save the files
   *@return  true if successfully transfered; else false
   *@since   0.1.99t2
   *@version 1
   */
  public boolean getFiles(String[] serverList, String[] localList)
  {
    boolean rc = true;
    int i;

    if (localList.length == serverList.length)
      for (i=0; i < localList.length; i++)
      {
        if (! getFile( serverList[i], localList[i] ))
          rc = false;
      }
    else
      rc = false;

    return ( rc );
  }
  
  /**
   * Recieves a File named `ServerFile` and stores it at `LocalFile`
   *@param   serverFile The Filename at the Server's side
   *@param   localFile  The Filename where it is saved (local)
   *@return  True if the file is successfully transfered; else false
   *@since   0.0.2pA1
   *@version 2
   */
  public boolean getFile(String serverFile, String localFile)
  {
    // We must catch errors like `5xx Permission denied...'
    boolean rc = false;
    int    size  = -1,
              i  =  0;
    String s     = "";
    String serverResponse = "";
    FileOutputStream fos  = null;
    FileWriter flwWriter;

    size = getFileSize( serverFile );
    //stdout.println("Filesize is " + size);// Should only be used for debug(tk)

    if (openPassiveDataConnection())
    {
      if (size > 0)
      {
        /*
         * Here we may look first if the file already exists on the local fs,
         * compare the size and may only transfer the missing part(s).(tk)
         */
        send( "retr " + serverFile );
        //stdout.println( "retrieving file" );//debug

        // Checking return-value
        serverResponse = readCtrl();
        stdout.println( serverResponse );

        if (serverResponse.startsWith("550"))
        {
          stdout.println("Permission denied.");
        }
        else if (serverResponse.startsWith("150"))
        { // Server is ready to transfer.
          try
          {
            Progressbar pg = new Progressbar( size );
            DataInputStream dis = new DataInputStream( getDataInputStream() );
            i = 0;

            fos = new FileOutputStream(
                        jftpSuper.getCurrentWorkingDir() + localFile );

          /* This should be improved because single byte reading and writing is
           * _VERY_ slow.
           * Maybe, there is a kind of blockread and blockwrite somewhere or we
           * can connect directly the dis and fos...?
           */
          /*FML: Whats about using BufferedReader/Writer, too*/
            while (i < size)
            {
              fos.write( (byte)dis.read() );
	          i++;
              if ((i%1024) == 0) // 'hope this will save some CPU-cycles
                pg.set( i );
            }
            pg.set( i ); // set the Value again to get 100% even on small files.

            closePassiveDataConnection();
            fos.close();
            dis.close();
            rc = true;
          }
          catch (Exception e)
          { stderr.println( "Error while Saving File:" + e.toString() ); }
        }

        // Closing socket && receiving server reply
        try
        {
          stdout.println( readCtrl() );
          if (jftpSuper.isPassiveConnected())
            closePassiveDataConnection();
        }
        catch (Exception e)
        { stderr.println("Can't Read from ctrl-channel."); }
      }
    }

    return( rc );
  }

  /**
   * Puts a File `localFile` to the Server
   *@param   localFile The file to transmit
   *@return  true if the file was transfered successfully; otherwise false
   *@since   0.0.2pA1
   *@version 5
   */
  public boolean putFile(String localFile)
  {
    // We must catch errors like `5xx Permission denied...'
    DataOutputStream dos;
    FileInputStream fis;
    int size,
        i;

    if (! openPassiveDataConnection())
    {
      stdout.println( "Could not Open Passive Data connection" );
      return ( false );
    }
    send( "stor " + localFile );
    stdout.println( readCtrl() );

    try
    {
      dos = new DataOutputStream( getDataOutputStream() );
      BufferedOutputStream bos = new BufferedOutputStream(dos, 16384);
      i = 0;
      fis = new FileInputStream( jftpSuper.getCurrentWorkingDir() + localFile );
      BufferedInputStream bis = new BufferedInputStream(fis, 16384);
     /* This also should be improved because single byte writing is
      * _VERY_ slow.
      */
      size = fis.available(); 

      Progressbar pb = new Progressbar( size );

      while (i < size)
      {
    	bos.write((byte)bis.read());
        i++;
        if ((i%1024) == 0)
          pb.set( i );
      }
      pb.set( i );
      bos.flush();
      dos.flush();
      dos.close();
      closePassiveDataConnection(); 
      bis.close();
      
      // FML: What about reading "270 ..." ?
      stdout.println( readCtrl() );
    }
    catch (Exception e)
    {
      stdout.println("Exception occured while sending data to the server: " +
                     e.toString());
    }

    /*closePassiveDataConnection(); // FML: How often you want to do this?*/

    /*return ( false ); // FML: SURE???? */
    return ( true );
  }

 /* <START Implementing tools for "Transferable"> */

  /* May someone improve this ugly code ? PLEASE*/
  /**
   * Returns the size of a file named `file'. It first tries to use
   * the size command and falls back to a selfdetection mode by parsing
   * the output of a `list' command. If even this is not supported by
   * the server it fails.
   *@param   file The name of the file
   *@return  The size of the given file; or -1 if it can't detect it.
   *@since   0.1.99
   *@version 3
   */
  private int getFileSize(String file)
  {
    /* At first I'll try the size command, if it fails the method tries
     * to determinate(?) the filesize by parsing the output of `list'.
     */
    int rc = -1;
    String receive;

    send( "size " + file );
    receive = readCtrl();

    if (! receive.startsWith("550"))
    {
      try
      {
        if (receive.startsWith("213") &&
            receive.charAt(3) == ' '  )
          receive = receive.substring(3).trim();

        rc = Integer.parseInt(receive);
      }
      catch (Exception e)
      { stderr.println("Server responses an invalid filesize."); }
    }
    else // Now me must go the hard way. :-(
    {
      int lines,
          i;
      String list;

      list = getLongDirList();
 

      // Cutting the 1st and last line ("150 .." && "226 ..")
      list = list.substring( list.indexOf("\n")+1,               //after 1st NL
                         list.lastIndexOf("\n", list.length()-2)//before last NL
             );

      /* Now we should have cut the first and last line.
       * At next we have to to replace '\n''s by ' ', so that the tokenizer
       * works proper. ARRG!, Then we have to replace '\r''s with ' ''s, too.
       */
      list = list.replace('\n', ' ');
      list = list.replace('\r', ' '); // Yes..


      // Now we've got the String we want...going on.
      StringTokenizer st = new StringTokenizer( list, " " );
      String token;

      boolean firstRun = true;
      int size=0;
	  int j=1;

	  /* When tokenizing the first line, the needed fields are 8 & 4 and in
       * any further run they are 9 & 5.
       * Don't ask me why... but tell me if you know it! ;-)
       */
      for (i=0; st.hasMoreTokens(); i++)
      {
        token = st.nextToken();

        if (i == (9-j))
        { i=0;
          if (firstRun)
          {
            firstRun = false;
            j=0;
          }
          if (token.equals( file ))
          {
            // Since `(5-j)' is smaller than `(9-j)' we can assign `size' here.
            rc = size; 
            break;
          }
        }
        if (i == (5-j))
          size = Integer.parseInt( token );
      }
    }
    
    return ( rc );
  }

  /**
   * Returns the current InputStreamReader of the ctrl-channel.
   *@return  InputStreamReader from the current ctrl-channel
   *@since   0.0.2pA4
   *@version 0
   */
  private InputStreamReader getCtrlStream()
  {
    if (ctrlIsr == null)
    { this.ctrlIsr = ctrlReader.getStream(); }  

    return( ctrlIsr );
  }

  /**
   * Returns the current InputStreamReader of the data-channel.
   *@return  InputStreamReader from the current data-channel
   *@since   0.1.0
   *@version 0
   */
  private InputStreamReader getDataStream()
  {
    if (dataIsr == null)
    { this.dataIsr = dataReader.getStream(); }  

    return( dataIsr );
  }

  /**
   * Returns the current InputStream of the data-channel.
   *@return  InputStream from the current data-channel
   *@since   0.1.99
   *@version 0
   */
  private InputStream getDataInputStream()
  {
    if (dataIs == null)
    { this.dataIs = dataReader.getInputStream(); }

    return( dataIs );
  }

  /**
   * Returns the current OutputStream of the data-channel.
   *@return  OutputStream from the current data-channel
   *@since   0.1.99
   *@version 0
   */
  private OutputStream getDataOutputStream()
  {
    if (dataOs == null)
    { this.dataOs = dataWriter.getOutputStream(); }

    return( dataOs );
  }

  /**
   * Returns the port to use for PASV
   *@param s String to parse for the Port
   *@return the port to use; or -1 if it can't resolve the port.
   *@since 0.0.2pA1
   */
  private int getPort(String s)
  {
    /*
     * Now get the port to connect to.
     *
     * To do this we have to parse a string like this:
     * "227 Entering Passive Mode (10,1,1,1,4,13)"
     * where '10,1,1,1' is the IP-Ad. and '4,13' stands for the port.
     * ('4' are the first 8bits of an 16bit long number and '13' is the second.)
     * In this case the port would be 1037 (4*256+13).
     *
     * At first we are creating a substring containing "(10,1,1,1,4,13)" to
     * parse and get the 5th and 6th Token, convert them to int and calculate
     * the _REAL_ port to give it back.
     */
    int rc = -1,
        port1,
        port2,
        i;
    StringTokenizer st;

    try
    {
      s = s.substring(s.indexOf('('), s.indexOf(')'));

      st = new StringTokenizer(s, ",", false);

      if (st.countTokens() == 6)
      {
        for (i=0; i < 4; i++)
          st.nextToken();

        port1 = Integer.parseInt( st.nextToken() );
        port2 = Integer.parseInt( st.nextToken() );
       /*
        * Now "calculate" them this way:
        *
        * 01010011         .     10101100   = 0101001110101100
        *  ^               ^         ^          ^
        * 8bit             .        8bit    = 16bit
        *  ^               ^         ^          ^
        * 1st port  concatenation  2nd port = _REAL_ port
        */
        rc = (port1 * 256) + port2;
      }
    }
    catch (Exception e)
    { rc = -1; }

    return( rc );
  }

 /* <END Implementing tools for "Transferable"> */
/* <END Implementing "Transferable"> */

/* <START Implementing "Listable"> */

  /**
   * Return the current working directory on the server.
   *@return  A java.lang.String containing the working directory
   *@since   0.1.99t4
   *@version 1
   */
  public String printRemoteWorkingDir()
  {
    send( "pwd" );
    return ( readCtrl() );
  }

  /**
   * Return the current working directory on the localhost.
   *@return  A java.lang.String containing the working directory
   *@since   0.1.99t4
   *@version 1
   */
  public String printLocalWorkingDir()
  {
    return ( jftpSuper.getCurrentWorkingDir() );
  }

  /**
   * Gives a detailed directorylist of the active directory.
   *@return  Directorylist of the active directory.
   *@since   0.0.2pA1
   *@version 1
   */
  public String getLongDirList()
  {
    String rc = "";
  
    if (openPassiveDataConnection()) 
    {
    send("list");
    rc += readCtrl();
      if (! rc.startsWith("550"))
      {
        rc += readData();
        rc += readCtrl();
      }
    }
    else
    {
      rc = "Unable to establish a DataConnection.";
    }

    if (jftpSuper.isPassiveConnected())
      rc += "Unable to close the DataConnection.";

    return( rc );
  }

  /**
   * Gives a short directorylist of the active directory.
   *@return  Directorylist of the active directory.
   *@since   0.0.2pA1
   *@version 1
   */
  public String getShortDirList()
  {
    String rc = "";
    boolean con;

    if (! jftpSuper.isPassiveConnected())
      con = openPassiveDataConnection()  ;
    else
      con = true;

    if (con) 
    {
      send("nlst");
      rc += readCtrl();
      if (! rc.startsWith("550"))
      {
        rc += readData();
        rc += readCtrl();
      }
      else
      { closePassiveDataConnection(); }
    }
    else
    {
      rc = "Unable to establish a DataConnection.";
    }

    if (jftpSuper.isPassiveConnected())
      rc += "Unable to close the DataConnection.";

    return( rc );
  }

  /**
   * Returns a java.lang.String array containing the directorylist of the
   * active directory.
   *@return  A java.lang.String array.
   *@since   0.1.99t2
   *@version 1
   */
  public String[] getDirListArray()
  {
    int i;
    StringTokenizer st = new StringTokenizer(getShortDirList(), "\n\r");
    String[] rc = new String[st.countTokens()-2];
    String tmp = "";

    st.nextToken(); // pop "150 Openi..." to /dev/null

    for (i=0; i < rc.length; i++)
    {
      tmp = st.nextToken();
      rc[i] = tmp; 
    }

    st.nextToken(); // pop "226 Trans..." to /dev/null

    if (jftpSuper.getDebugLevel() >= 2)
      stderr.println("rc has # entries: " + rc.length);

    return ( rc );
  }

  /**
   * Returns a short directorylist of the current working-directory
   *@return  A java.lang.String
   *@since   0.1.99t2
   *@version 1
   */
  public String getLocalShortDirList()
  {
    int i;
    String rc = "";
    File fi = new File( jftpSuper.getCurrentWorkingDir() );
    String[] tmp = fi.list();

    for (i=0; i < tmp.length; i++)
    {
      rc += tmp[i] + "\n\r";
    }

    return ( rc );
  }

  /**
   * Returns a long directorylist of the current working-directory
   *@return  A java.lang.String
   *@since   0.1.99.t2
   *@version 1
   */
  public String getLocalLongDirList()
  {
    String rc = "";
    String[] tmp;
    File[] fl;
    int i,
        j;
    File fi = new File( jftpSuper.getCurrentWorkingDir() );

    fl = fi.listFiles();
    tmp = new String[fl.length];

    for (i=0; i < fl.length; i++)
    {
      // is Directory?
      if (fl[i].isDirectory()) tmp[i]  = "d";
      else                     tmp[i]  = "-";

      // is Readable ?  
      if (fl[i].canRead())     tmp[i] += "r";
      else                     tmp[i] += "-";

      // is Writeable ?  
      if (fl[i].canWrite())    tmp[i] += "w";
      else                     tmp[i] += "-";

      // Size
      //tmp[i] += " " + fl[i].length();

      tmp[i] += " " + fl[i].getName();
    }

    // String[] -> String
    for (i=0; i < tmp.length; i++)
    {
      rc += tmp[i] + "\n\r";
    }

    return ( rc );
  }
  
  /**
   * Returns a java.lang.String arry containing the directorylist of the local
   * working-directory.
   *@return  A java.lang.String array.
   *@since   0.1.99t2
   *@version 1
   */
  /*
   * I know that this is not a really good place... ;-( (tk)
   */
  public String[] getLocalDirListArray()
  {
    File fi = new File( jftpSuper.getCurrentWorkingDir() );

    return ( fi.list() );
  }

/* <END Implementing "Listable"> */


/* <START Implementing "Authenticateable"> */

  /**
   * Authenticates an user
   *@return  true if user is authenticated; otherwise false
   *@since   0.0.2pA1
   *@version 1
   */
  public boolean authenticate()
  {
    boolean rc     = true;
    String  passwd = "",
            strReceive;

    try
    {
      jftpSuper.unsetAuthenticated();
      stdout.print("Username: ");
      jftpSuper.setUserName( stdin.readLine() ); // shouldn't we check this?
     
      send("user " + jftpSuper.getUserName());
      strReceive = readCtrl();
      stdout.println( strReceive );
      if (strReceive.startsWith("331"))
      {
        if (jftpSuper.getUserName().equals("anonymous"))
        {
          passwd = jftpSuper.getAnonymousPasswd();
        }
        else
        {
          stdout.print("Password: ");
        }

        try
        { passwd = stdin.readPasswd(); }
        catch (IOException ioe)
        { stderr.println("Unable to read the password."); }

        send("pass " + passwd);

        if (jftpSuper.getDebugLevel() >= 2)
          stderr.println("User: " +jftpSuper.getUserName()+ " Pwd: " +passwd);

        strReceive = this.readCtrl();
        if (! (strReceive.startsWith("230")))
        {
          rc = false;
        }
        stdout.println( strReceive );
      }
      else
      {
        stderr.println("Login failed.");
        rc = false;
      }
    }
    catch (Exception e)
    { 
      rc = false;
      //stderr.println("Unable to authenticate.");//Only debug(tk)
    }

    return( rc );
  }

/* <END Implementing "Authenticateable"> */

/* <START Implementing "Connectable"> */

  /** 
   * Opens a connection
   *@return  true if connected; false if not
   *@since   0.1.0
   *@version 0
   */
  public boolean connect()
  { return ( openCtrlConnection() ); }

  /** 
   * Opens a ctrl connection
   *@return  true if connected; false if not
   *@since   0.1.0
   *@version 0
   */
  public boolean openCtrlConnection()
  {
    boolean rc = false;

    if ( (! jftpSuper.isConnected()) && 
         (bindCtrlSocket())          )  
    {
      if (jftpSuper.getDebugLevel() >= 2)
        stderr.println("Trying to get Streams from the ctrlSock.");

      this.ctrlReader = new NetReader( ctrlSock );
      this.ctrlWriter = new NetWriter( ctrlSock );

      jftpSuper.setConnected();
      rc = true;
    }
    else { stderr.println("Unable to connect."); }

    return ( rc );
  }

  /** 
   * Closes a connection
   *@return  true if disconnected; false if not
   *@since   0.1.0
   *@version 0
   */
  public boolean disconnect()
  { return ( closeCtrlConnection() ); }

  /** 
   * Closes a ctrl connection
   *@return  true if disconnected; false if not
   *@since   0.1.0
   *@version 0
   */
  public boolean closeCtrlConnection()
  {
    boolean rc = false;

    if (jftpSuper.isConnected())
    {
      send( "QuiT" );
      stdout.println( readCtrl() );

      try 
      {
        ctrlSock.close();
        ctrlSock   = null;
        ctrlIsr    = null;
        ctrlReader = null;
        ctrlWriter = null;

        rc = true;
      }
      catch (IOException ioe)
      { System.out.println("Exception while closing ControlSocket."); }

      jftpSuper.unsetConnected();
    }

    return ( rc );
  }

  /** 
   * Opens a passive connection for data.
   *@return  true if connected; false if not
   *@since   0.1.0
   *@version 0
   */
  public boolean openPassiveDataConnection()
  {
    int port;
    boolean rc = true;
    String receive;


    if ( (jftpSuper.isConnected())         &&
         (! jftpSuper.isPassiveConnected()) )
    {
      jftpSuper.unsetPassiveConnected();

      send("pasv");
      receive = readCtrl();
      port  = getPort( receive );

      if (port == -1)
      { rc = false; }
      else
      {
        if (bindDataSocket( port ))
        {
          this.dataReader = new NetReader( dataSock );
          this.dataWriter = new NetWriter( dataSock );

          jftpSuper.setPassiveConnected();
        }
        else // bindDataSocket(..) FAILED
        { rc = false; }
      }
    }

    return ( rc );
  }

  /** 
   * Closes a passive connection for data.
   *@return  true if disconnected; false if not
   *@since   0.1.0
   *@version 0
   */
  public boolean closePassiveDataConnection()
  {
    boolean rc = false;

    if (jftpSuper.isPassiveConnected())
    {
      try
      {
        dataSock.close();
        dataSock   = null;
        dataIs     = null;
        dataOs     = null;
        dataIsr    = null;
        dataReader = null;
        dataWriter = null;

        rc = true;
      }
      catch (IOException ioe)
      { System.out.println("E...while closeing dataSock."); }
    }

    jftpSuper.unsetPassiveConnected();

    return ( rc );
  }

 /* <START Implementing tools for "Connectable"> */
  /**
   * Binds a CtrlSocket to the Host/Port - pair set in JFTPSuper
   *@return  true if socket is bind; else false
   *@since   0.1.0
   *@version 0
   */
  private boolean bindCtrlSocket()
  {
    return ( bindSocket( 'c', jftpSuper.getServerIP(),
                              jftpSuper.getServerPort() )
           );
  }

  /**
   * Binds a DataSocket to the Host/Port - pair set in JFTPSuper
   *@return  true if socket is bind; else false.
   *@since   0.1.0
   *@version 0
   */
  private boolean bindDataSocket(int port)
  {
    return ( bindSocket( 'd', jftpSuper.getServerIP(), port ) );
  }

  /**
   * Binds a Socket to the given Host/Port - pair
   *@param   sock The type of socket (only 'c' or 'd' are valid).
   *@param   server The server to bind to.
   *@param   port The Port to bind to.
   *@return  true if socket is bind; else false
   *@since   0.1.0
   *@version 0
   */
  private boolean bindSocket(char sock, String server, int port)
  {
    boolean rc = false;

    if (jftpSuper.getDebugLevel() >= 2)
      stderr.println("Binding Socket to server "+server+":"+port );
                 /* NO!  ^  Not the beer */

    try
    {
           if (sock == 'd')
        dataSock = new Socket( server, port );
      else if (sock == 'c')
        ctrlSock = new Socket( server, port );
      else
        stderr.println("Ooops... You should __NEVER__ get here! hum?");

      rc = true;
    }
    catch (BindException be)
    { stderr.println( "Can't bind socket." ); }
    catch (MalformedURLException mue)
    { stderr.println( "Malformed URL." ); }
    catch (ConnectException ce)
    { stderr.println( "Connection refused by Server." ); }
    catch (NoRouteToHostException nrthe)
    { stderr.println( "No route to host." ); }
    catch (ProtocolException pe)
    { stderr.println( "A protocol error occured." ); }
    catch (UnknownHostException uhe)
    { stderr.println( "Unable to resolve IP-address of specified host." ); }
    catch (UnknownServiceException use)
    { stderr.println( "Used protocol/service is not known." ); }
    catch (IOException ioe)
    { stderr.println( "Unknown IO-Error." ); }

    if (jftpSuper.getDebugLevel() >= 2)
      stderr.println("Socket is bound (" + rc + ")");

    return( rc );
  }
 /* <END Implementing tools for "Connectable"> */
/* <END Implementing "Connectable"> */
}
