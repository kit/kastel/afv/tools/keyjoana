/*
 * Name:    Progressbar.java  (extens JFTPSuper.java)
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.1.99   11.06.2001 Tobias Kranz Creation
 * 0.1.99t5 23.07.2001 Tobias Kranz Added Comments
 */
package JavaFTP;

/** 
 * Shows a textual progressbar
 *@since 0.1.99
 *@version 1
 */
public class Progressbar
  extends JFTPSuper
{
  private long max;
  private long pos = 1;
  
  /**
   * Constructor. Here you must set the maximum value the progressbar may have.
   *@param   max The maximum value the progressbar may have.
   *@since   0.1.99
   *@version 1
   */
  public Progressbar(int max)
  {
    this.max = max;
	this.show();
  }

  /**
   * Sets the progressbar to the given value.
   *@param   pos The current position of the progressbar
   *@since   0.1.99
   *@version 1
   */
  protected void set(int pos)
  {
    if (pos <= max)
    {
	  this.pos = pos;
	}
	show();
  }

  /**
   * Shows the progressbar in it's current status
   *@since   0.1.99
   *@version 1
   */
  private void show()
  {
    int i,
	    stat=0;
	String graphStat="";
    double stat1 = ((double)100/max)*pos;
	
	String statstr = Double.toString(stat1);
    stat = Integer.parseInt( statstr.substring(0, statstr.indexOf('.')) );

	for (i=0; i < (int)20*(((float)stat)/100); i++)
	  graphStat += "="; // setting '=''s for reached %
	
	for (;i < 20; i++)
	  graphStat += " "; // filling the rest with ' ''s
	
	     if ((int)((float)stat)/100 >= 1)
      System.out.print("Progress: ["+graphStat+"]: "  +stat+"% complete ("+
	                   (int)pos+" bytes transfered)\n"); 
//	else if ((int)((float)stat)/10 >= 1)
//    System.out.print("Progress: ["+graphStat+"]:  " +stat+"% complete ("+
//	                   (int)pos/1024+" kb)\r"); 
	else
      System.out.print("Progress: ["+graphStat+"]:   "+stat+"% complete ("+
	                   (int)pos/1024+" kb)\r"); 
  }
}
