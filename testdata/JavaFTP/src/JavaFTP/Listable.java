/*
 * Name:    Listable.java  [INTERFACE] (extends JFTPSuperInterface)
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz Creation
 * 0.0.2pA4 06.05.2001 Tobias Kranz Rectifing comments
 * 0.1.99t2 17.07.2001 Tobias Kranz Added 4 methods.
 * 0.1.99t4 19.07.2001 Tobias Kranz Added 2 ~`printWorkingDir' methods.
 */
package JavaFTP;

/**
 * Defines methods to list directories.
 *@since 0.0.2pA1
 */
public interface Listable
  extends JFTPSuperInterface
{
  /**
   * Prints the current working directory on the server.
   *@return  A java.lang.String containing the directory
   *@since   0.1.99t4
   *@version 1
   */
  public String printRemoteWorkingDir();

  /**
   * Prints the current working directory on the local host.
   *@return  A java.lang.String containing the directory
   *@since   0.1.99t4
   *@version 1
   */
  public String printLocalWorkingDir();
  
  /**
   * Returns a detailed directory listing of the active directory.
   *@return Directorylist of the active directory
   *@since 0.0.2pA1
   */
  public String getLongDirList();

  /**
   * Returns a short directory listing of the active directory.
   *@return Directorylist of the active directory
   *@since 0.0.2pA1
   */
  public String getShortDirList();

  /**
   * Returns a short directory listing of the active directory in a 
   * jav.lang.String array.
   *@return  A java.lang.String array containing the Directorylist of the
   *         active directory
   *@since   0.1.99t2
   *@version 1
   */
  public String[] getDirListArray();


  /**
   * Returns a detailed directory listing of the active working-directory.
   *@return Directorylist of the active working-directory
   *@since 0.0.2pA1
   */
  public String getLocalLongDirList();

  /**
   * Returns a short directory listing of the active working-directory.
   *@return Directorylist of the active working-directory
   *@since 0.0.2pA1
   */
  public String getLocalShortDirList();

  /**
   * Returns a short directory listing of the active working-directory in a 
   * jav.lang.String array.
   *@return  A java.lang.String array containing the Directorylist of the
   *         active working-directory
   *@since   0.1.99t2
   *@version 1
   */
  public String[] getLocalDirListArray();
}
