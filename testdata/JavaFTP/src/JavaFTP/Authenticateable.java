/*
 * Name:    Authenticateable.java  [INTERFACE] (extends JFTPSuperInterface)
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 17.04.2001 Tobias Kranz Creation
 * 0.0.2pA4 06.06.2001 Tobias Kranz Changed name to __REAL__ english ;-)
 */
package JavaFTP;

/**
 * Defines methods to authenticate a user.
 *@since 0.0.2pA1
 */
public interface Authenticateable
  extends JFTPSuperInterface
{
  /**
   * Authenticates an user.
   *@return true if authentication succeds; otherwise false
   *@since 0.0.2pA1
   */
  public boolean authenticate();
}
