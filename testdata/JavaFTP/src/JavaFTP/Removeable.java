/*
 * Name:    Removeable.java  [INTERFACE] (extends JFTPSuperInterface)
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.1.99t4 19.07.2001 Tobias Kranz Creation
 */
package JavaFTP;

/**
 * Defines methods to remove directories and files.
 *@since 0.1.99t4
 */
public interface Removeable
  extends JFTPSuperInterface
{
  /**
   * Removes a Directory on the server with the given name
   *@return  True if successfully removed; otherwise false.
   *@since   0.1.99t4
   *@version 1
   */
  public boolean removeRemoteDir(String dirName);
  
  /**
   * Removes a Directory on the local fs with the given name
   *@return  True if successfully removed; otherwise false.
   *@since   0.1.99t4
   *@version 1
   */
  public boolean removeLocalDir(String dirName);
}
