/*
 * Name:    JFTPSuperInterface.java  [INTERFACE]
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 17.04.2001 Tobias Kranz Creation
 */
package JavaFTP;

import java.io.File;
/**
 * Nothing.
 *@since 0.0.2pA1
 */
public interface JFTPSuperInterface
{ }
