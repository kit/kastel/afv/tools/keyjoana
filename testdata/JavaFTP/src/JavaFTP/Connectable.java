/*
 * Name:    Connectable.java  [INTERFACE] (extends JFTPSuperInterface)
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz Creation
 */
package JavaFTP;

/**
 * Defines methods to connect to a server.
 *@since 0.0.2pA1
 */
public interface Connectable
  extends JFTPSuperInterface
{
  public boolean connect();

  public boolean disconnect();

  /**
   * Opens a ctrl connection.
   *@since 0.0.2pA4
   */
  public boolean openCtrlConnection(); 

  /**
   * Closes a ctrl connection.
   *@since 0.0.2pA4
   */
  public boolean closeCtrlConnection(); 

  /**
   * Opens a passive data connection.
   *@since 0.0.2pA4
   */
  public boolean openPassiveDataConnection();

  /**
   * Closes a passive data connection.
   *@since 0.0.2pA4
   */
  public boolean closePassiveDataConnection();
}
