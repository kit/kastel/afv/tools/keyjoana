/*
 * Name:    ServerResponseParser.java (extends Parser (extends JFTPSuper))
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.0.2pA1 10.04.2001 Tobias Kranz Creation
 */
package JavaFTP;

/**
 * Parses the FTPServers responses.
 *@since 0.0.2pA1
 */
public class ServerResponseParser
  extends Parser
{
  /**
   * Parses a given String for known FTPServer-replies.
   *@param  s String to parse
   *@since 0.0.2pA1
   */
  public void parse(String s)
  {
        /*
    if (s.equals(xxxxxx))
        {
        Insert known FTPServer replies here.
    }
        */
  }
}
