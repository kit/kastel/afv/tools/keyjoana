/*
 * Name:    Changeable.java  [INTERFACE] (extends JFTPSuperInterface)
 * Author:  JavaFTP-Group
 * License: GPL
 *
 * version  date       name         changes
 * 0.1.99t4 20.07.2001 Tobias Kranz Creation
 */
package JavaFTP;

/**
 * Defines methods to change things.
 *@since 0.1.99t4
 */
public interface Changeable
  extends JFTPSuperInterface
{
  /**
   * Changes the remote working dir.
   *@param   A java.lang.String containing the new path.
   *@return  true if succeded; else false.
   *@since   0.1.99t4
   *@version 1
   */
  public boolean changeRemoteWorkingDir(String newPath);
  
  /**
   * Changes the local working dir.
   *@param   A java.lang.String containing the new path.
   *@return  true if succeded; else false.
   *@since   0.1.99t4
   *@version 1
   */
  public boolean changeLocalWorkingDir(String newPath);

  /**
   * Sets the Tx mode either to "ascii" or to "binary"
   *@param   A java.lang.String containing the wished Tx-mode
   *         (Either `binary' or `ascii')
   *@return  true if succeded; otherwise false.
   *@since   0.1.99t4
   *@version 1
   */
  public boolean setTxMode(String nm);

  /**
   * Sets the Tx mode either to "ascii" or to "binary"
   *@param   A char containing the wished Tx-mode (Either 'i'/'b' for binary or
   *         'a' for ascii)
   *@return  true if succeded; otherwise false.
   *@since   0.1.99t4
   *@version 1
   */
  public boolean setTxMode(char nm);
}
