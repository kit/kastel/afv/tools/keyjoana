package plusminus;

public class PlusMinusFalsePos {

	public PlusMinusFalsePos()
	{

	}

	/*@ determines \result \by \low	    
	@*/
	public int testMethod(int high, int low) {		
		int i = 0;

		if(i == 5)
		{
			low = identity2(low, high);			
		}		
		else
		{
			if(i == 2)
			{
				if(i == 3)
				{
					low = identity2(low, high);
				}
				else
				{
					low = identity(low, high);
				}
			}
			else
			{
				low = identity2(low, high);
			}
		}
		return low;
	}

	public int identity(int l, int h) {
		l = l + h;
		l = l - h;
		return l;
	}

	public int identity2(int l, int h) {	
		return l;
	}
	/*@public normal_behaviour
	   ensures b;
	   assignable \strictly_nothing;
	@*/
	public void assume(boolean b){}
}


