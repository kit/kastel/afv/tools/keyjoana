public class BranchAnalysisTest 
{
   public static void main ( String args [ ] ) 
   {
      test1 ( false ) ;
   }
   static public int test1 ( boolean l ) 
   {
      int h1 = 0 ;
      int h2 = 1 ;
      if ( l ) 
      {
         h1 = 2 * h1 ;
      }
      else 
      {
         h2 = 3 * h2 ;
      }
      return h1 ;
   }
}
