import java . lang . ArithmeticException ;
public class DivisionByZero 
{
    public static int divide ( int l , int h ) 
    {
        int z = 0 ;
        try 
        {
            z = l / h ;
        }
        catch ( ArithmeticException e ) 
        {
            return h ;
        }
        return z ;
    }
    public static int callDivide ( int l , int h ) 
    {
        return divide ( l , h ) ;
    }
    public static void main ( String [ ] args ) 
    {
        callDivide ( randInt ( ) , randInt ( ) ) ;
    }
    static int randInt ( ) 
    {
        return 42 ;
    }
}
