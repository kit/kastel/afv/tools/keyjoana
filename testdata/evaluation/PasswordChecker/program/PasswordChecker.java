class PasswordChecker {

        static boolean isDigit(char c){
            return '0' <= c && c <= '9'; 
        }

        static boolean isLetter(char c){
           return 'a' <= c && c <= 'z';
        } 

        static boolean isCaps(char c){
           return 'A' <= c && c <= 'Z';
        }

	static int countAlphanumeric(char[] passwd) {
        int count = 0;
        for (int i = 0; i < passwd.length; i++){

            if(isDigit(passwd[i]) || isLetter(passwd[i]))
                count++;
        } 
        return count; 
	}
	
	static int countCaps(char[] passwd) {
        int count = 0;
        for (int i = 0; i < passwd.length; i++)
            if (isCaps(passwd[i]))
                count++;
        return count;
	}
        /*@determines \result \by \nothing;@*/
	static int passwordstrength(char[] passwd) {
		int strength = 0; 

		if (countAlphanumeric(passwd) < 3)
			strength = 1;		
		if (countCaps(passwd) < 3)
			strength = 2; 
		else
			strength = 3; 

		return strength;
	}

	public static void main (String args[]) {
        	char[] passwd = {'s', 'e', 'c', 'r', 'e', 't'};
		int strength  = passwordstrength(passwd);
		
	} 
}
