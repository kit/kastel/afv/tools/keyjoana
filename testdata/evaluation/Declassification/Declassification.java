public class Declassification{

/*@determines \result \by (h==0);@*/
public int secure(int h){

   if(h == 0){
      return 1;
   }else{
      return 2;
   }

}

/*@determines \result \by (h==0);@*/
public int insecure(int h){

   if(h == 1){
      return 1;
   }else{
      return 2;
   }  

}

}
