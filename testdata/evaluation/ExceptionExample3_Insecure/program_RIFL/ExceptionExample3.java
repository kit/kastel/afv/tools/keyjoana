
import java.lang.RuntimeException;
import java.lang.NullPointerException;
// JML* comment created by KeY RIFL Transformer.


public class ExceptionExample3  
  /*@ 
    @ determines \nothing \by n;
    @*/
 {

    public void m(int n) {
        if (n == 0)
         throw new RuntimeException();
         else
         throw new NullPointerException();
    }
}
