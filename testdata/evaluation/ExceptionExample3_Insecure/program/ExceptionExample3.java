import java.lang.RuntimeException;
import java.lang.NullPointerException;

public class ExceptionExample3 {
  /*@determines \exception \by \nothing \new_objects \exception;@*/
  public void m(int n) {
    if (n == 0)
      throw new RuntimeException();
    else
      throw new NullPointerException();
  }

}
