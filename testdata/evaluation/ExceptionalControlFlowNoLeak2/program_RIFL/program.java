public class program
// JML* comment created by KeY RIFL Transformer.
 {

    private static class T extends Exception {
    }  
  /*@ 
    @ determines \nothing \by h;
    @ determines \result \by \nothing;
    @*/


    /** Main test method parameter is the secret, return value is public */
    static boolean foo(boolean h) {
        boolean x =  false;
        try {
            if (h) {
                throw new T();}
             else {
                throw new T();}
        } catch (T t) {
            x = true;
        }
        return x;
    }

    public static void main(String[] args) {
        foo(randBool());
    }

    /** Helper method to obtain a random boolean */
    static boolean randBool() {
        return true;
    }
    /** Helper methot to obtain a random integer */
    static int randInt() {
        return 42;
    }
}
// JML* comment created by KeY RIFL Transformer.

