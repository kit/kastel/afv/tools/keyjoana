public class IFLoop {
    public int low;
    private int high;  
  /*@ 
    @ determines \nothing \by IFLoop.high;
    @ determines IFLoop.low \by \nothing;
    @*/



    public static void main(String[] args) {
        IFLoop ifl =  new IFLoop();
        ifl.insecure_ifl();
    }  
  /*@ 
    @ determines \nothing \by IFLoop.high;
    @ determines IFLoop.low \by \nothing;
    @*/



    public void insecure_ifl() {
        int x =  0;
        int y =  0;
        while (y < 10) {
            print(x);
            if (y == 5) {
                x = high;
            }
            x++;
            y++;
        }
    }  
  /*@ 
    @ determines \nothing \by IFLoop.high;
    @ determines IFLoop.low \by \nothing;
    @*/


    public void print(int x) {
            low = x;
    }
}
// JML* comment created by KeY RIFL Transformer.

