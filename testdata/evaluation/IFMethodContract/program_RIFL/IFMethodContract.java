public class IFMethodContract {
    public static int low;
    private static int high;  
  /*@ 
    @ determines \nothing \by IFMethodContract.high;
    @ determines IFMethodContract.low \by \nothing;
    @*/



    public static void main(String[] args) {
        IFMethodContract ifm =  new IFMethodContract();
        ifm.secure_if_high_n5_n1();
    }  
  /*@ 
    @ determines \nothing \by IFMethodContract.high;
    @ determines IFMethodContract.low \by \nothing;
    @*/



    void secure_if_high_n5_n1() {
        if (high > 0) {
            low = n5(high);
        } else {
            high = -high;
            low = n5(high + low);
        }
    }  
  /*@ 
    @ determines \nothing \by IFMethodContract.high;
    @ determines IFMethodContract.low \by \nothing;
    @*/



    int n5(int x) {
        high = 2 * x;
        return 15;
    }
}
// JML* comment created by KeY RIFL Transformer.

