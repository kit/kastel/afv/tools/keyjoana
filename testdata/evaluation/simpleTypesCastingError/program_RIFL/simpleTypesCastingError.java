class A {
}
// JML* comment created by KeY RIFL Transformer.

// JML* comment created by KeY RIFL Transformer.

class B extends A {
}
// JML* comment created by KeY RIFL Transformer.

class C extends A {
}
// JML* comment created by KeY RIFL Transformer.


public class simpleTypesCastingError {
        static boolean secret =  true;  
  /*@ 
    @ determines \nothing \by simpleTypesCastingError.secret;
    @*/


        public static void main(String[] args) {
                test();
    }  
  /*@ 
    @ determines \nothing \by simpleTypesCastingError.secret;
    @*/



        public static boolean test() {
                A obj;

                if (secret) {
                        obj = new B();
        } else {
                        obj = new C();
        }

                boolean reconstructed =  true;

                try {
                        A test =  ((B) obj);
        } catch (Exception e) {
                        reconstructed = false;
        } finally {
                        return reconstructed;
        }
    }
}
