
class A {}
class B extends A {}
class C extends A {}

public class simpleTypesCastingError {
	//boolean secret = true;
	
        /*@determines \result \by \nothing;
          @*/
        public boolean test(boolean secret){
                A obj;
		
		if(secret) {
			obj = new B();
		} else {
			obj = new C();
		}
		
		boolean reconstructed = true;
		
		try{
			A test = ((B) obj);
		} catch(Exception e){
			reconstructed = false;
		} 

                return reconstructed;
        }

     
}
