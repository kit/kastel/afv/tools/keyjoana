import java.lang.ArithmeticException;
public class DivisionByZero {
        /*@determines \result \by l;@*/
	public static int divide(int l, int h){
		int z = 0;
		try{
			z = l / h;
		}catch(ArithmeticException e){
			return h;
		}
		return z;
	}
	public static void main(String[] args) {
		divide(randInt(), randInt());
	}

    /** Helper method to obtain a random integer */
    static int randInt() {
        return 42;
    }
}
