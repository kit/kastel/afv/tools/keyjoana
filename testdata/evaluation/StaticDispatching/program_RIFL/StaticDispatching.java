public class StaticDispatching {

        int h, l;

        int lsink, hsink;  
  /*@ 
    @ determines \nothing \by StaticDispatching.h;
    @ determines StaticDispatching.lsink \by \nothing;
    @*/


        public void f()
        {
                if (l == 1)
                        set((long) h);
                else
                        set(h);
    }  
  /*@ 
    @ determines \nothing \by StaticDispatching.h;
    @ determines StaticDispatching.lsink \by \nothing;
    @*/


        public void set(long a) { lsink = (int) a;}  
  /*@ 
    @ determines \nothing \by StaticDispatching.h;
    @ determines StaticDispatching.lsink \by \nothing;
    @*/

        public void set(int a) { hsink = a;}  
  /*@ 
    @ determines \nothing \by StaticDispatching.h;
    @ determines StaticDispatching.lsink \by \nothing;
    @*/



        public static void main(String[] args) {
                StaticDispatching sd =  new StaticDispatching();
                sd.f();
    }
}
// JML* comment created by KeY RIFL Transformer.

