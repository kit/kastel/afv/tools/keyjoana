
import java.lang.IllegalArgumentException;
// JML* comment created by KeY RIFL Transformer.


public class FieldsOfParametersExample1  
  /*@ 
    @ determines \nothing \by loc;
    @*/
 {

    public void onLocationChanged(Location loc) {
                Location eiffel =  getPointOfInterest("Eiffel tower");
        System.out.println("latitude:" + eiffel.latitude + "longitude" + eiffel.longitude);
    }

        public Location getPointOfInterest(String name) {
                if (name.equals("Eiffel tower")) {
                        return new Location(48.858f, 2.294f);
        } else {
                         throw new IllegalArgumentException();
        }
    }
}
