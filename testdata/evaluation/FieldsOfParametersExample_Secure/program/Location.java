public class Location{
	
	public int longitude;
	public int latitude;

	public Location(int latitude, int longitude){ 
		this.longitude = longitude;
		this.latitude = latitude;
	}

}
