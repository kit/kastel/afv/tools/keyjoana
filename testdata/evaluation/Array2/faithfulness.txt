The RIFL specification realizes the security requirement, since it specifies the parameter value as high source and both the return value and the exception channel as low sinks.
