INSECURE
The secret user input is stored in the length of an int[] array. This is an illegal flow since the lengths of int[] arrays are considered to be observable by the attacker.
