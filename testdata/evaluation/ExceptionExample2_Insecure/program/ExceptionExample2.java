import java.lang.RuntimeException;

public class ExceptionExample2 {
  /*@determines \exception \by \nothing \new_objects \exception;@*/
  public void m(int n) {
    if (n == 0)
      throw new RuntimeException();

  }

}
