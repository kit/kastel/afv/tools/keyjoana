
class A {}
class B extends A {}
class C extends A {}

public class simpleTypes {
	boolean secret = false;
	
        /*@determines \result \by \nothing;@*/
        public boolean test(){
                A obj;
		
		if(secret) {
			obj = new B();
		} else {
			obj = new C();
		}
		
		boolean reconstructed = obj instanceof B;
                return reconstructed;
        }
}
