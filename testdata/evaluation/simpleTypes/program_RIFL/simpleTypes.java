class A {
}
// JML* comment created by KeY RIFL Transformer.

// JML* comment created by KeY RIFL Transformer.

class B extends A {
}
// JML* comment created by KeY RIFL Transformer.

class C extends A {
}
// JML* comment created by KeY RIFL Transformer.


public class simpleTypes {
        static boolean secret =  false;  
  /*@ 
    @ determines \nothing \by simpleTypes.secret;
    @*/


        public static void main(String[] args) {
                test();
    }  
  /*@ 
    @ determines \nothing \by simpleTypes.secret;
    @*/


        public static boolean test() {
                A obj;

                if (secret) {
                        obj = new B();
        } else {
                        obj = new C();
        }

                boolean reconstructed =  obj instanceof B;
                return reconstructed;
    }
}
