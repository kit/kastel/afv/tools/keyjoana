public class ArrayAccess 
{
    public static void main ( String [ ] args ) 
    {
        new ArrayAccess ( ) . testMethod ( 1 , 2 ) ;
    }
    public int testMethod ( int high , int low ) 
    {
        int l = arrayAccess ( low , high ) ;
        return l ;
    }
    private int arrayAccess ( int l , int h ) 
    {
        int [ ] array = new int [ 3 ] ;
        array [ 0 ] = l ;
        array [ 1 ] = h ;
        array [ 2 ] = array [ 1 ] ;
        return array [ 0 ] ;
    }
	/*@
	@ requires true;
	@ ensures b;
	@*/
	private void assume(boolean b) { 

	}
}
