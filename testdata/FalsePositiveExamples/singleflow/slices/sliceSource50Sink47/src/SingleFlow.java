public class SingleFlow 
{
    public static void main ( String [ ] args ) 
    {
        new SingleFlow ( ) . testMethod ( 1 , 2 ) ;
    }
    public int testMethod ( int high , int low ) 
    {
        int l = singleFlow ( high , low ) ;
        return l ;
    }
    public int singleFlow ( int high , int low ) 
    {
        low = plus ( low , high ) ;
        low = minus ( low , high ) ;
        return low ;
    }
    private int minus ( int low , int high ) 
    {
        low = low - high ;
        return low ;
    }
    private int plus ( int low , int high ) 
    {
        low = low + high ;
        return low ;
    }
	/*@
	@ requires true;
	@ ensures b;
	@*/
	private void assume(boolean b) { 

	}
}
