public class Branching 
{
    public static void main ( String [ ] args ) 
    {
        new Branching ( ) . testMethod ( 1 , 2 ) ;
    }
    public int testMethod ( int high , int low ) 
    {
        int l = branching ( high , low ) ;
        return l ;
    }
    public int branching ( int high , int low ) 
    {
        int i = plus ( low , high ) ;
        int j = minus ( low , high ) ;
        return i + j ;
    }
    private int plus ( int low , int high ) 
    {
        low = low + high ;
        return low ;
    }
    private int minus ( int low , int high ) 
    {
        low = low - high ;
        return low ;
    }
	/*@
	@ requires true;
	@ ensures b;
	@*/
	private void assume(boolean b) { 

	}
}
