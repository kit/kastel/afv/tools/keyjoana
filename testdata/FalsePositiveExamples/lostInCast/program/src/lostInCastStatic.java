public class lostInCastStatic
{
    public static int getRandomInt ( ) 
    {
        return 42 ;
    }
    public int doIt ( int h ) 
    {
        long x = h * 256 * 256 * 256 * 256 ;
        x += ( getRandomInt ( ) ) ;
        int l = ( int ) x ;
        return l ;
    }
    public int callDoIt ( int h ) 
    {
        return doIt ( h ) ;
    }
    public static void main ( String [ ] args ) 
    {
        int h = getRandomInt ( ) ;
        lostInCastStatic test = new lostInCastStatic ( ) ;
        int res = test . callDoIt ( h ) ;
    }
}
