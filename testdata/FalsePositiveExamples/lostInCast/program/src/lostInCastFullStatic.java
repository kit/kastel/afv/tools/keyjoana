public class lostInCastFullStatic 
{
    public static int getRandomInt ( ) 
    {
        return 42 ;
    }
    public static int doIt ( int h ) 
    {
        long x = h * 256 * 256 * 256 * 256 ;
        x += ( getRandomInt ( ) ) ;
        int l = ( int ) x ;
        return l ;
    }
    public static int callDoIt ( int h ) 
    {
        return doIt ( h ) ;
    }
    public static void main ( String [ ] args ) 
    {
        int h = getRandomInt ( ) ;
        int res = callDoIt ( h ) ;
    }
}
