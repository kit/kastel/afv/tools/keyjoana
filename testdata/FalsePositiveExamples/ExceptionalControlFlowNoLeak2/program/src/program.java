public class program 
{
    public static class T extends Exception 
    {
    }
    static boolean foo ( boolean h ) 
    {
        boolean x = false ;
        try 
        {
            if ( h ) 
            {
                throw new T ( ) ;
            }
            else 
            {
                throw new T ( ) ;
            }
        }
        catch ( T t ) 
        {
            x = true ;
        }
        return x ;
    }
    public static void main ( String [ ] args ) 
    {
        callFoo ( randBool ( ) ) ;
    }
    public static boolean callFoo ( boolean h ) 
    {
        return foo ( h ) ;
    }
    static boolean randBool ( ) 
    {
        return true ;
    }
    static int randInt ( ) 
    {
        return 42 ;
    }
}
