public class ExcludingStatements 
{
    public static void main ( String [ ] args ) 
    {
        new ExcludingStatements ( ) . testMethod ( 1 , 2 ) ;
    }
    public int testMethod ( int high , int low ) 
    {
        int l = excludingStatements ( 3 , low , high ) ;
        return l ;
    }
    public int excludingStatements ( int x , int l , int h ) 
    {
        int z = 0 ;
        if ( x == 3 ) 
        {
            z = h ;
        }
        if ( x != 3 ) 
        {
            l = z ;
        }
        z = 0 ;
        return l ;
    }
	/*@
	@ requires true;
	@ ensures b;
	@*/
	private void assume(boolean b) { 

	}
}
