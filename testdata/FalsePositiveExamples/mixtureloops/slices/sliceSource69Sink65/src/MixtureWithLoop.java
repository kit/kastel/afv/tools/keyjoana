public class MixtureWithLoop 
{
   public static void main ( String [ ] args ) 
   {
      new MixtureWithLoop ( ) . testMethod ( 1 , 2 ) ;
   }
   public int testMethod ( int high , int low ) 
   {
      int l = mixtureWithLoop ( low , high ) ;
      return l ;
   }
   private int mixtureWithLoop ( int l , int h ) 
   {
      int x = arrayInsecure ( l , h ) ;
      x = loopOverride ( l , x ) ;
      x += arrayAccess ( x , h ) ;
      x = justSet ( l , x ) ;
      int y = loopOverride ( l , h ) ;
      return x + y ;
   }
   public int loopOverride ( int l , int h ) 
   {
      int y = l ;
      for ( int i = 0 ;
      i < 5 ;
      i ++ ) 
      {
         if ( i < 4 ) 
         {
            l = l + h ;
         }
         else 
         {
            l = y ;
         }
      }
      return l ;
   }
   private int arrayInsecure ( int low , int high ) 
   {
      return 0 ;
   }
   private int arrayAccess ( int l , int h ) 
   {
      int [ ] array = new int [ 3 ] ;
      array [ 0 ] = l ;
      array [ 1 ] = h ;
      array [ 2 ] = array [ 1 ] ;
      return array [ 2 ] ;
   }
   private int justSet ( int l , int h ) 
   {
      int [ ] array = new int [ 5 ] ;
      array [ 0 ] = 5 ;
      array [ 1 ] = h ;
      l = array [ 0 ] ;
      return l ;
   }
	/*@
	@ requires true;
	@ ensures b;
	@*/
	private void assume(boolean b) { 

	}
}
