public class LoopOverride 
{
    public static void main ( String [ ] args ) 
    {
        new LoopOverride ( ) . testMethod ( 1 , 2 ) ;
    }
    public int testMethod ( int high , int low ) 
    {
        int l = loopOverride ( low , high ) ;
        return l ;
    }
    public int loopOverride ( int l , int h ) 
    {
        int y = l ;
        for ( int i = 0 ;
        i < 5 ;
        i ++ ) 
        {
            if ( i < 4 ) 
            {
                l = l + h ;
            }
            else 
            {
                l = y ;
            }
        }
        return l ;
    }
}
