public class Nested 
{
    public static void main ( String [ ] args ) 
    {
        new Nested ( ) . testMethod ( 1 , 2 ) ;
    }
    public int testMethod ( int high , int low ) 
    {
        int l = nested ( high , low ) ;
        return l ;
    }
    public int nested ( int high , int low ) 
    {
        low = plusMinus ( low , high ) ;
        return low ;
    }
    private int plusMinus ( int low , int high ) 
    {
        low = low + high ;
        low = minus ( low , high ) ;
        return low ;
    }
    private int minus ( int low , int high ) 
    {
        low = low - high ;
        return low ;
    }
	/*@
	@ requires true;
	@ ensures b;
	@*/
	private void assume(boolean b) { 

	}
}
