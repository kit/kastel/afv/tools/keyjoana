package webshopSimple;
public class Account implements Cart2AccountIF, AccountIF {

  private int creditCard;
  private int cvc;
  private char[] name;
  private char[] adress;

  
  /*@ public normal_behavior
    @ requires true;
    @ ensures true;
    @*/
  public void setName(char[] name) {
    this.name = name;
  }
  
  public void setAddress(char[] address) {
    this.adress = address;
  }
  
  public void setCreditCard(int ccnr) {
    this.creditCard = ccnr;
  }
  
  public void setCVC(int cvc) {
    this.cvc = cvc;
  }
  
  public char[] getName() {
    return this.name;
  }
  
  public char[] getAddress() {
    return this.adress;
  }
  
  public int getCreditCard() {
    return this.creditCard;
  }
  
  public int getCreditCardAnon() {
    return this.creditCard%10000;
  }
  
  public int getCVC() {
    return this.cvc;
  }
  
}
