package webshopSimple;
     
public interface Cart2AccountIF {
  
  //low billing: \call, \result
  //low delivery: \call, \result
  public char[] getName();
  
  //low billing: \call, \result
  //low delivery: \call, \result
  public char[] getAddress();
  
  //low billing: \call, \result
  //low delivery: \call, \result
  public int getCreditCard();
  
  //low billing: \call, \result
  //low delivery: \call, \result
  public int getCreditCardAnon();
  
  //low billing: \call, \result
  //low delivery: \call, \result
  public int getCVC();

} 
