package webshopSimple;

public interface DeliveryIF {
  
  //low billing: nothing
  //low delivery: \call, name, adress, product, amount
  public void deliverProduct(char[] name, char[] adress, int product, int amount);
  
}
