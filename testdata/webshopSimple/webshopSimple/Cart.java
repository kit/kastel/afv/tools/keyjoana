package webshopSimple;
 
 
 public class Cart implements CartIF {
 
   private Cart2AccountIF account;
   private Cart2SalesIF sales;
 
   private int product;
   private int amount;
   
   //@ model \seq billingData;
   //@ represents billingData = \seq_concat(\seq_singleton(this.product), \seq_singleton(this.amount));
 
   /*@ public normal_behavior
     @ requires true;
     @ ensures true;
     @
     @ cluster cart_addProduct_billing
     @ \lowIn this.addProduct.\call(productID, amount)
     @ \lowOut this.addProduct.\term(0)
     @ \lowState billingData
     @ \visible this.addProduct.\call(true), this.addProduct.\term(true)
     @ \new_objects \nothing;
     @*/
   public void addProduct(int productID, int amount) {
     this.product = productID;
     this.amount = amount;
   }
  
   /*@ public normal_behavior
     @ requires true;
     @ ensures true;
     @
     @ cluster cart_clearCart_billing
     @ \lowIn this.clearCart.\call(0)
     @ \lowOut this.clearCart.\term(0)
     @ \lowState billingData
     @ \visible this.clearCart.\call(true), this.clearCart.\term(true)
     @ \new_objects \nothing;
     @*/
   public void clearCart() {
     this.product = 0;
     this.amount = 0;
   }
  
   public void order() {
   
     int[] temp = new int[2];
     temp[0] = account.getCreditCard();
     temp[1] = account.getCreditCardAnon();
     
     sales.addToSales(this.product, this.amount, account.getName(), account.getAddress(), temp[0], temp[1], account.getCVC());
     sales.addToSales(this.product, this.amount, account.getName(), account.getAddress(), account.getCreditCard(), account.getCreditCardAnon(), account.getCVC());
   
   }
   
  
  }
