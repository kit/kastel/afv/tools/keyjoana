package webshopSimple;

public interface AccountIF {
  
  //low billing: \call, name 
  //low delivery: \call name
  public void setName(char[] name);
  
  //low billing: \call, address
  //low delivery: \call address
  public void setAddress(char[] address);
  
  //low billing: \call, ccnr%10000
  //low delivery: \call
  public void setCreditCard(int ccnr);
  
  //low billing: \nothing
  //low delivery: \nothing
  public void setCVC(int cvc);

}
