package webshopSimple;

public interface BillingIF {

  //low billing: \call, name, adress, product, amount, payment
  //low delivery: \nothing
  public void makeBill(char[] name, char[] adress, int product, int amount, int payment);
}
