package webshopSimple;
  
public interface CartIF {

  //low billing: \call, productID, amount
  //low delivery: \call, productID, amount
  public void addProduct(int productID, int amount);
  
  //low billing: \call
  //low delivery: \call
  public void clearCart();
  
  //low billing: \call
  //low delivery: \call
  public void order();

}
