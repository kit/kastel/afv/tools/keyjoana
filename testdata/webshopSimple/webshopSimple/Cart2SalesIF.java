package webshopSimple;
 
public interface Cart2SalesIF {

  //low billing: \call, prod, amount, name, adress, ccnrAnon
  //low delivery: \call, prod, amount, name, adress
  public void addToSales(int prod, int amount, char[] name, char[] adress, int ccnr, int ccnrAnon, int cvc);

}
