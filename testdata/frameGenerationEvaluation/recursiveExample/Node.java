package recursiveExample;

public class Node {
	
	Node next;
	int data;
	
	Node(int i) {
		this.data = i;
		this.next = null;
	}
	
	Node(int i, Node next) {
		this.data = i;
		this.next = next;
	}

}
