package recursiveExample;

public class RecExample {

	public static void main(String[] args) {
		Node n1 = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);
		n1.next = n2;
		n2.next = n3;
		RecExample.fieldAccess(n1);
		RecExample.recursiveAccess(n1,n2);
		RecExample.recursiveIndirectAccess(n1,n2);
		RecExample.recursiveModify(n1, n2);
		RecExample.recursiveIndirectModify(n1, n2);
	}
	
	public static int fieldAccess(Node n) {
		return n.data;
	}
	
	public static int recursiveAccess(Node n1, Node n2) {
		Node current = n1;
		while(current.next != null) {
			current = current.next;
		}
		int temp = n2.next.data;
		return current.data;
	}
	
	public static void recursiveIndirectAccess(Node n1, Node n2) {
		recursiveAccess(n1,n2);
	}
	
	public static int recursiveModify(Node n1, Node n2) {
		Node current = n1;
		while(current.next != null) {
			current = current.next;
			current.data = 1;
		}
		int temp = n2.next.data;
		return current.data;
	}
	
	public static void recursiveIndirectModify(Node n1, Node n2) {
		recursiveModify(n1,n2);
	}

}
