package arrayExample;

public class ArrayRecursiveExample {

	public static void f1(int i) {
		if(i > 0) {
			f2(i--);
		}
	}
	
	public static void f2(int i) {
		if(i > 0) {
			f1(i--);
		}
	}
	
	public static void main(String[] args) {
		f1(10);
	}

}
