package arrayExample;

public class ArrayExample {
	
	int[] array;
	
	public void noArray() {
		int i = 5;
	}
	
	public void newArray() {
		this.array = new int[10];
	}
	
	public void newArrayInd() {
		newArray();
	}
	
	public void newLocalArray() {
		int[] localArray = new int[10];
	}
	
	public void newLocalArrayInd() {
		newLocalArray();
	}


	public static void main(String[] args) {
		ArrayExample e = new ArrayExample();
		e.newArray();
		e.noArray();
		e.newArrayInd();
		e.newLocalArray();
		e.newLocalArrayInd();
	}

}
