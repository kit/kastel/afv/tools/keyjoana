package attributetest;

public class AttributeTest {
	
	public static A staticAttribute;
	
	public static void main(String[] args) { 
		AttributeTest at = new AttributeTest();
		staticAttribute = new A();
		A obj = new A();
		obj.createObjAttribute();
		int a = at.getAttribute(obj);
		int b = at.getAttributeOfAttribute(obj);
		at.setAttribute(obj, 10);
		at.setAttributeOfAttribute(obj, 10);
		at.setStaticAttribute(obj);
	}
	
	public void setStaticAttribute(A obj) {
		staticAttribute = obj;
	}
	public int getAttributeOfAttribute(A obj) {
		int result = obj.a2.a1;
		return result;
	}
	
	public int getAttribute(A obj) {
		int result = obj.a1;
		return result;
	}
	
	public void setAttribute(A obj, int i) {
		obj.a1 = i;
	}
	
	public void setAttributeOfAttribute(A obj, int i) {
		obj.a2.a1 = i;
	}
}