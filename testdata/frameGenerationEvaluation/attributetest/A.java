package attributetest;

public class A {
	
	public int a1;
	public A a2;
	
	public A() {
		this.a1 = 5;
		this.a2 = null;
	}
	
	public void createObjAttribute() {
		this.a2 = new A();
	}
}