package arraytest;

public class ArrayTest {
	
	private int[][][] intArray3;
	private int[][] intArray2;
	private int[] intArray;
	private A[] objectArray;
	
	public static void main(String[] args) { 
		ArrayTest at = new ArrayTest();
		A a = new A();
		at.addIntToArray(2, 1);
		at.addObjToArray(2, a);
		at.addIntToArray2(5);
		at.addIntToArray3(5);
		int i = at.getIntAtIndex(2);
		A obj = at.getObjAtIndex(2);
		i = at.getObjAttrAtIndex(2);
	}
	
	public ArrayTest() {
		this.intArray = new int[10];
		this.objectArray = new A[10];
		this.intArray2 = new int[10][10];
		this.intArray3 = new int[5][5][5];
	}
	
	public void addIntToArray(int index, int p) {
		this.intArray[index] = p; 
	}
	
	public void addObjToArray(int index, A p) {
		this.objectArray[index] = p;
	}
	
	public int getIntAtIndex(int index) {
		return this.intArray[index];
	}
	
	public A getObjAtIndex(int index) {
		return this.objectArray[index];
	}
	
	public int getObjAttrAtIndex(int index) {
		return this.objectArray[index].attribute;
	}
	
	public void addIntToArray2(int p) {
		this.intArray2[0][0] = p;
	}
	
	public void addIntToArray3(int p) {
		this.intArray3[0][0][0] = p;
	}
}