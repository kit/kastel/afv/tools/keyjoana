SETLOCAL ENABLEDELAYEDEXPANSION
SET "params="
FOR %%f in (%*) DO (
	javac -g %%f.java
	SET "params=!params! %%f.class"
)
jar -cf %1.jar %params%