package jzip;

public class MyFileOutputStream {
	private String location;
	private byte[] loc1;
	private byte[] loc2;
	private byte[][] loc3;
	private byte[][] loc4;
	private int index;

	public MyFileOutputStream(String location) {
		this.location = location;
		index = 0;
		loc1 = new byte[1024];
		loc2 = new byte[1024];
	}

	public byte[] write(byte[] input) {
		if (location.length() > 10) {
			loc1 = input;
			return loc1;
		} else {
			loc2 = input;
			return loc2;
		}
		
	}
	
	public byte[][] close(){
		if(location.length() > 10){
			return loc3;
		}
		else{
			return loc4;
		}
	}
}
