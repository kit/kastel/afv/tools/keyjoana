package jzip;

public class MyZipInputStream {

	private String zipFile;
	private MyZipEntry[] zes;
	private int index;

	public MyZipInputStream(String zipFile) {
		index = 0;
		this.zipFile = zipFile;
		if (zipFile.length() < 2) {
			zes = new MyZipEntry[1];
			zes[0] = new MyZipEntry();
		} else {
			zes = new MyZipEntry[2];
			zes[0] = new MyZipEntry();
			zes[1] = new MyZipEntry();
		}
	}

	public MyZipEntry getNextEntry() {
		return zes[index++];
	}

	public void closeEntry() {
		// not simulated

	}

	public void close() {
		// not simulated

	}

	public byte[] read() {
		return zipFile.getBytes();
	}
}
