package multiplefields;

public class MultipleFields {
	
	private int high1;
	
	private int high2;
	
	private int low1;
	
	private int low2;
	
	public MultipleFields(){
		high1 = 1;
		high2 = 2;
		low1= 3;
		low2 = 4;		
	}
	/*@ determines low1 \by low1, low2, high2;
	@*/
	public void testMethod(){
		low2 = low1 + 1;
		high2 = high2 + 1;
		low1 = high1 * 2;
	}
	
	

}
